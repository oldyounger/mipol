/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
    //config.extraPlugins = 'filebrowser';
    //config.filebrowserImageBrowseUrl = '/browser/browse.php';
    //config.filebrowserImageUploadUrl = '/uploader/upload.php';
    //config.filebrowserBrowseUrl = '/browser/browse.php';
    //config.filebrowserUploadUrl = '/uploader/upload.php';
    config.extraPlugins='imgbrowse';
    config.filebrowserImageBrowseUrl = '/themes/imgbrowser/imgbrowse.html';
    config.allowedContent = true;
    config.autoParagraph = false;

};




