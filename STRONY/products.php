<!DOCTYPE html>
<html lang="en">
<?php
@include ("head.php");
?>
<body>

<?php
@include ("mainMenu.php");
?>

<section class="mainPageSection" style="background: url('images/productsPage.jpg') no-repeat center center">
    <div class="container-fluid no-padding">
        <div class="mainTitleHolder pageSection">
            <div class="breakLine"></div>
            <div class="titleHolder">
                <h2>Lorem ipsum dolor sit amet.</h2>
            </div>
            <div class="breakLine"></div>
            <div class="subTitleHolder">
                <h3>Lorem ipsum dolor sit amet.</h3>
            </div>
        </div>
    </div>
    <div class="blackOverlay"></div>
    <div class="pattern"></div>
</section>

<section class="mainDescriptionSectionPage">
    <div class="container">
        <div class="col-md-12">
            <div class="mainTitleHolder">
                <div class="breakLine thinLine"></div>
                <div class="titleHolder">
                    <h3>Nasze produkty</h3>
                </div>
                <div class="breakLine thinLine"></div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="sectionPageDescription">
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aliquam aspernatur aut consequatur
                    corporis cum debitis deserunt dicta fuga, illo labore laboriosam laborum maxime modi molestiae
                    nemo nesciunt perferendis possimus provident quas quasi qui quis quod recusandae
                    rem tempore veritatis.
                </p>

                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aliquam aspernatur aut consequatur
                    corporis cum debitis deserunt dicta fuga, illo labore laboriosam laborum maxime modi molestiae
                    nemo nesciunt perferendis possimus provident quas quasi qui quis quod recusandae
                    rem tempore veritatis.
                </p>
            </div>
        </div>
    </div>
</section>

<section class="productSelection">
    <div class="container-fluid no-padding">
        <div class="col-md-6 no-padding productFullSize" style="background: url('images/product1.jpg') no-repeat center center;">
            <div class="mainTitleHolder abs">
                <div class="breakLine thinLine"></div>
                <div class="titleHolder">
                    <h4>Elementy wołowe</h4>
                </div>
                <div class="breakLine thinLine"></div>
                <a href="selectionOfParts.php" class="more">zobacz wiecej</a>
            </div>
            <div class="blackOverlay"></div>
            <div class="pattern"></div>
        </div>

        <div class="col-md-6 no-padding productFullSize">
            <div class="col-md-6 productHalfSize" style="background: url('images/product2.jpg') no-repeat center center;">
                <div class="mainTitleHolder abs">
                    <div class="breakLine thinLine"></div>
                    <div class="titleHolder">
                        <h4>wierci wołowe</h4>
                    </div>
                    <div class="breakLine thinLine"></div>
                    <a href="anotherProducts.php" class="more">zobacz wiecej</a>
                </div>
                <div class="blackOverlay"></div>
                <div class="pattern"></div>
            </div>

            <div class="col-md-6 productHalfSize" style="background: url('images/product3.jpg') no-repeat center center;">
                <div class="mainTitleHolder abs">
                    <div class="breakLine thinLine"></div>
                    <div class="titleHolder">
                        <h4>Mięsa drobne</h4>
                    </div>
                    <div class="breakLine thinLine"></div>
                    <a href="anotherProducts.php" class="more">zobacz wiecej</a>
                </div>
                <div class="blackOverlay"></div>
                <div class="pattern"></div>
            </div>

            <div class="col-md-6 productHalfSize" style="background: url('images/product4.jpg') no-repeat center center;">
                <div class="mainTitleHolder abs">
                    <div class="breakLine thinLine"></div>
                    <div class="titleHolder">
                        <h4>Podroby</h4>
                    </div>
                    <div class="breakLine thinLine"></div>
                    <a href="anotherProducts.php" class="more">zobacz wiecej</a>
                </div>
                <div class="blackOverlay"></div>
                <div class="pattern"></div>
            </div>

            <div class="col-md-6 productHalfSize" style="background: url('images/product5.jpg') no-repeat center center;">
                <div class="mainTitleHolder abs">
                    <div class="breakLine thinLine"></div>
                    <div class="titleHolder">
                        <h4>Inne</h4>
                    </div>
                    <div class="breakLine thinLine"></div>
                    <a href="anotherProducts.php" class="more">zobacz wiecej</a>
                </div>
                <div class="blackOverlay"></div>
                <div class="pattern"></div>
            </div>
        </div>
    </div>
</section>

<section class="certifications">
    <div class="container">
        <div class="col-md-12">
            <div class="mainTitleHolder">
                <div class="breakLine thinLine"></div>
                <div class="titleHolder">
                    <h3>Certyfikaty</h3>
                </div>
                <div class="breakLine thinLine"></div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="certificationsHolder">
                <ul>
                    <li>
                        <img class="img-responsive" src="images/certifications1.png" alt="certifications">
                    </li>
                    <li>
                        <img class="img-responsive" src="images/certifications2.png" alt="certifications">
                    </li>
                    <li>
                        <img class="img-responsive" src="images/certifications3.png" alt="certifications">
                    </li>
                    <li>
                        <img class="img-responsive" src="images/certifications4.png" alt="certifications">
                    </li>
                    <li>
                        <img class="img-responsive" src="images/certifications5.png" alt="certifications">
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>


<?php
@include ("downloadProducts.php");

@include ("map.php");

@include ("contactForm.php");

@include ("footer.php");
?>

</body>
</html>