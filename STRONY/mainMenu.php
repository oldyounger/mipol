<header id="mainNav" class="mainNav">
    <nav class="navbar navbar-default">
        <div class="container-fluid no-padding">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#mainMenu" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand logo" href="index.php"><img class="img-responsive" src="images/logo.png" alt="logo"></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse no-padding" id="mainMenu">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="news.php">Aktualności <span class="sr-only">Aktualna podstrona</span></a></li>
                    <li><a href="howWeOperate.php">Jak działamy</a></li>
                    <li><a href="aboutUs.php">O nas</a></li>
                    <li class="logoMenu">
                        <a href="index.php">
                            <img class="img-responsive normalLogo" src="images/logo.png" alt="">
                            <img class="img-responsive scrollLogo" src="images/logoScroll.png" alt="">
                        </a>
                        <div class="menuTriangle"></div>
                    </li>
                    <li><a href="products.php">Produkty</a></li>
                    <li><a href="purchaseOfCattle.php">Skup bydła</a></li>
                    <li><a href="contact.php">Kontakt</a></li>
                </ul>

            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
</header>