<section class="contact bgFirstShape">
    <div class="container">
        <div class="col-md-12">
            <div class="mainTitleHolder">
                <div class="breakLine thinLine"></div>
                <div class="titleHolder">
                    <h3>Napisz do nas</h3>
                </div>
                <div class="breakLine thinLine"></div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="contactForm clearfix">
                <form action="">
                    <div class="col-md-6">
                        <input type="text" name="name" placeholder="Wpisz imię i nazwisko" required>
                        <input type="email" name="email" placeholder="Wpisz adres e-mail" required>
                        <input type="tel" name="phone" placeholder="Wpisz numer telefonu">
                    </div>
                    <div class="col-md-6">
                        <input type="text" name="topic" placeholder="Wpisz temat" required>
                        <textarea name="description" id="" placeholder="Wpisz treść wiadomości" required></textarea>
                    </div>
                    <div class="col-md-12">
                        <input class="more mgTop_75" type="submit" name="submit" value="Wyślij">
                    </div>
                </form>
            </div>
        </div>
        <div class="col-md-12">
            <div class="contactInfo">
                <ul>
                    <li>
                        <span>
                            <img src="images/email.png" alt="">
                            <a href="mailto:test@test.pl">test@test.pl</a>
                        </span>
                    </li>
                    <li>
                        <span>
                            <img src="images/phone.png" alt="">
                            <a href="tel:+48777666555">+48 777 666 555</a>
                        </span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>