<!DOCTYPE html>
<html lang="en">
<?php
@include ("head.php");
?>
<body>

<?php
@include ("mainMenu.php");
?>

<section class="mainPageSection" style="background: url('images/aboutUsPage.jpg') no-repeat center center">
    <div class="container-fluid no-padding">
        <div class="mainTitleHolder pageSection">
            <div class="breakLine"></div>
            <div class="titleHolder">
                <h2>Lorem ipsum dolor sit amet.</h2>
            </div>
            <div class="breakLine"></div>
            <div class="subTitleHolder">
                <h3>Lorem ipsum dolor sit amet.</h3>
            </div>
        </div>
    </div>
    <div class="blackOverlay"></div>
    <div class="pattern"></div>
</section>

<section class="mainDescriptionSectionPage">
    <div class="container">
        <div class="col-md-12">
            <div class="mainTitleHolder">
                <div class="breakLine thinLine"></div>
                <div class="titleHolder">
                    <h3>Kilka słów o nas</h3>
                </div>
                <div class="breakLine thinLine"></div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="sectionPageDescription">
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aliquam aspernatur aut consequatur
                    corporis cum debitis deserunt dicta fuga, illo labore laboriosam laborum maxime modi molestiae
                    nemo nesciunt perferendis possimus provident quas quasi qui quis quod recusandae
                    rem tempore veritatis.
                </p>

                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aliquam aspernatur aut consequatur
                    corporis cum debitis deserunt dicta fuga, illo labore laboriosam laborum maxime modi molestiae
                    nemo nesciunt perferendis possimus provident quas quasi qui quis quod recusandae
                    rem tempore veritatis.
                </p>
            </div>
        </div>
    </div>
</section>

<section class="purchaseOfCattleDescriptionSection">
    <div class="container-fluid no-padding">
        <div class="sectionPageDescriptionFlexBox clearfix">
            <div class="col-md-6 no-padding imgAboutUs" style="background: url('images/aboutUs1.jpg') no-repeat center center">
                <div class="blackLighterOverlay"></div>
                <div class="pattern"></div>
            </div>
            <div class="col-md-6 no-padding">
                <div class="sectionPageDescriptionText">
                    <h4>Historia firmyy</h4>

                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam cupiditate dolores eveniet
                        impedit maiores numquam! A assumenda at eum explicabo iure pariatur ut? Ab aliquid asperiores,
                        consequuntur dolore illum nulla obcaecati omnis quam quidem ratione repellendus, tempore.
                        Autem, deserunt dolores.
                    </p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam cupiditate dolores eveniet
                        impedit maiores numquam! A assumenda at eum explicabo iure pariatur ut? Ab aliquid asperiores,
                        consequuntur dolore illum nulla obcaecati omnis quam quidem ratione repellendus, tempore.
                        Autem, deserunt dolores.
                    </p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam cupiditate dolores eveniet
                        impedit maiores numquam! A assumenda at eum explicabo iure pariatur ut? Ab aliquid asperiores,
                        consequuntur dolore illum nulla obcaecati omnis quam quidem ratione repellendus, tempore.
                        Autem, deserunt dolores.
                    </p>
                </div>
            </div>
        </div>

        <div class="sectionPageDescriptionFlexBox clearfix">
            <div class="col-md-6 col-md-push-6 no-padding imgAboutUs" style="background: url('images/aboutUs2.jpg') no-repeat center center">
                <div class="blackLighterOverlay"></div>
                <div class="pattern"></div>
            </div>
            <div class="col-md-6 col-md-pull-6 no-padding">
                <div class="sectionPageDescriptionText">
                    <h4>Rynki zbytu na przestrzeni lat</h4>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam cupiditate dolores eveniet
                        impedit maiores numquam! A assumenda at eum explicabo iure pariatur ut? Ab aliquid asperiores,
                        consequuntur dolore illum nulla obcaecati omnis quam quidem ratione repellendus, tempore.
                        Autem, deserunt dolores.
                    </p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam cupiditate dolores eveniet
                        impedit maiores numquam! A assumenda at eum explicabo iure pariatur ut? Ab aliquid asperiores,
                        consequuntur dolore illum nulla obcaecati omnis quam quidem ratione repellendus, tempore.
                        Autem, deserunt dolores.
                    </p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam cupiditate dolores eveniet
                        impedit maiores numquam! A assumenda at eum explicabo iure pariatur ut? Ab aliquid asperiores,
                        consequuntur dolore illum nulla obcaecati omnis quam quidem ratione repellendus, tempore.
                        Autem, deserunt dolores.
                    </p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam cupiditate dolores eveniet
                        impedit maiores numquam! A assumenda at eum explicabo iure pariatur ut? Ab aliquid asperiores,
                        consequuntur dolore illum nulla obcaecati omnis quam quidem ratione repellendus, tempore.
                        Autem, deserunt dolores.
                    </p>
                </div>
            </div>
        </div>

        <div class="sectionPageDescriptionFlexBox clearfix">
            <div class="col-md-6 no-padding imgAboutUs" style="background: url('images/aboutUs3.jpg') no-repeat center center">
                <div class="blackLighterOverlay"></div>
                <div class="pattern"></div>
            </div>
            <div class="col-md-6 no-padding">
                <div class="sectionPageDescriptionText">
                    <h4>Misja firmy</h4>

                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam cupiditate dolores eveniet
                        impedit maiores numquam! A assumenda at eum explicabo iure pariatur ut? Ab aliquid asperiores,
                        consequuntur dolore illum nulla obcaecati omnis quam quidem ratione repellendus, tempore.
                        Autem, deserunt dolores.
                    </p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam cupiditate dolores eveniet
                        impedit maiores numquam! A assumenda at eum explicabo iure pariatur ut? Ab aliquid asperiores,
                        consequuntur dolore illum nulla obcaecati omnis quam quidem ratione repellendus, tempore.
                        Autem, deserunt dolores.
                    </p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam cupiditate dolores eveniet
                        impedit maiores numquam! A assumenda at eum explicabo iure pariatur ut? Ab aliquid asperiores,
                        consequuntur dolore illum nulla obcaecati omnis quam quidem ratione repellendus, tempore.
                        Autem, deserunt dolores.
                    </p>
                </div>
            </div>
        </div>

    </div>
</section>

<section class="logoChange">
    <div class="container">
        <div class="col-md-12">
            <div class="logoChangeHolder">
                <ul>
                    <li>
                        <img src="images/mipolOldLogo.png" alt="old logo" class="img-responsive">
                    </li>
                    <li>
                        <img src="images/arrow.png" alt="arrow" class="img-responsive">
                    </li>
                    <li>
                        <img src="images/mipolNewLogo.png" alt="new logo" class="img-responsive">
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>


<?php
@include ("downloadProducts.php");

@include ("map.php");

@include ("contactForm.php");

@include ("footer.php");
?>

</body>
</html>