<section class="downloadProducts" style="background: url('images/downloadProducts.jpg') no-repeat center center">

    <a href="#" class="downloadProductsButton">
        <span class="downloadDescriptionHolder">
            <span class="textDownload">Pobierz katalog produktów</span>
            <img src="images/downloadCloud.png" alt="cloud" class="img-responsive">
        </span>
    </a>

    <div class="blackOverlay"></div>
    <div class="pattern"></div>
</section>