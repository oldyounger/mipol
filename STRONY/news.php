<!DOCTYPE html>
<html lang="en">
<?php
@include ("head.php");
?>
<body>

<?php
@include ("mainMenu.php");
?>

<section class="mainPageSection" style="background: url('images/newsPage.jpg') no-repeat center center">
    <div class="container-fluid no-padding">
        <div class="mainTitleHolder pageSection">
            <div class="breakLine"></div>
            <div class="titleHolder">
                <h2>Lorem ipsum dolor sit amet.</h2>
            </div>
            <div class="breakLine"></div>
            <div class="subTitleHolder">
                <h3>Lorem ipsum dolor sit amet.</h3>
            </div>
        </div>
    </div>
    <div class="blackOverlay"></div>
    <div class="pattern"></div>
</section>

<section class="news bgThirdShape">
    <div class="container">
        <div class="col-md-12">
            <div class="mainTitleHolder">
                <div class="breakLine thinLine"></div>
                <div class="titleHolder">
                    <h3>Aktualności</h3>
                </div>
                <div class="breakLine thinLine"></div>
            </div>
        </div>
        <div class="col-md-12 no-padding newsBoxHolder">

            <div class="col-md-4 newsElement">
                <div class="newsBox">
                    <div class="newsImg">
                        <img src="images/news1.jpg" alt="news1" class="img-responsive">
                    </div>
                    <div class="newsDescription">
                        <article>
                            <h4>Lorem ipsum dolor sit.</h4>

                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusamus amet commodi cum
                                deleniti dolore ea eaque eos
                            </p>
                        </article>
                    </div>
                    <div class="newsDateAndMore clearfix">
                        <div class="col-md-6 no-padding">
                            <p>20 Grudzień 2015</p>
                        </div>
                        <div class="col-md-6 no-padding">
                            <a href="newsDescription.php"> Czytaj więcej ></a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 newsElement">
                <div class="newsBox">
                    <div class="newsImg">
                        <img src="images/news1.jpg" alt="news1" class="img-responsive">
                    </div>
                    <div class="newsDescription">
                        <article>
                            <h4>Lorem ipsum dolor sit.</h4>

                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusamus amet commodi cum
                                deleniti dolore ea eaque eos
                            </p>
                        </article>
                    </div>
                    <div class="newsDateAndMore clearfix">
                        <div class="col-md-6 no-padding">
                            <p>20 Grudzień 2015</p>
                        </div>
                        <div class="col-md-6 no-padding">
                            <a href="newsDescription.php"> Czytaj więcej ></a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 newsElement">
                <div class="newsBox">
                    <div class="newsImg">
                        <img src="images/news1.jpg" alt="news1" class="img-responsive">
                    </div>
                    <div class="newsDescription">
                        <article>
                            <h4>Lorem ipsum dolor sit.</h4>

                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusamus amet commodi cum
                                deleniti dolore ea eaque eos
                            </p>
                        </article>
                    </div>
                    <div class="newsDateAndMore clearfix">
                        <div class="col-md-6 no-padding">
                            <p>20 Grudzień 2015</p>
                        </div>
                        <div class="col-md-6 no-padding">
                            <a href="newsDescription.php"> Czytaj więcej ></a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 newsElement">
                <div class="newsBox">
                    <div class="newsImg">
                        <img src="images/news1.jpg" alt="news1" class="img-responsive">
                    </div>
                    <div class="newsDescription">
                        <article>
                            <h4>Lorem ipsum dolor sit.</h4>

                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusamus amet commodi cum
                                deleniti dolore ea eaque eos
                            </p>
                        </article>
                    </div>
                    <div class="newsDateAndMore clearfix">
                        <div class="col-md-6 no-padding">
                            <p>20 Grudzień 2015</p>
                        </div>
                        <div class="col-md-6 no-padding">
                            <a href="newsDescription.php"> Czytaj więcej ></a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 newsElement">
                <div class="newsBox">
                    <div class="newsImg">
                        <img src="images/news1.jpg" alt="news1" class="img-responsive">
                    </div>
                    <div class="newsDescription">
                        <article>
                            <h4>Lorem ipsum dolor sit.</h4>

                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusamus amet commodi cum
                                deleniti dolore ea eaque eos
                            </p>
                        </article>
                    </div>
                    <div class="newsDateAndMore clearfix">
                        <div class="col-md-6 no-padding">
                            <p>20 Grudzień 2015</p>
                        </div>
                        <div class="col-md-6 no-padding">
                            <a href="newsDescription.php"> Czytaj więcej ></a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 newsElement">
                <div class="newsBox">
                    <div class="newsImg">
                        <img src="images/news1.jpg" alt="news1" class="img-responsive">
                    </div>
                    <div class="newsDescription">
                        <article>
                            <h4>Lorem ipsum dolor sit.</h4>

                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusamus amet commodi cum
                                deleniti dolore ea eaque eos
                            </p>
                        </article>
                    </div>
                    <div class="newsDateAndMore clearfix">
                        <div class="col-md-6 no-padding">
                            <p>20 Grudzień 2015</p>
                        </div>
                        <div class="col-md-6 no-padding">
                            <a href="newsDescription.php"> Czytaj więcej ></a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 newsElement">
                <div class="newsBox">
                    <div class="newsImg">
                        <img src="images/news1.jpg" alt="news1" class="img-responsive">
                    </div>
                    <div class="newsDescription">
                        <article>
                            <h4>Lorem ipsum dolor sit.</h4>

                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusamus amet commodi cum
                                deleniti dolore ea eaque eos
                            </p>
                        </article>
                    </div>
                    <div class="newsDateAndMore clearfix">
                        <div class="col-md-6 no-padding">
                            <p>20 Grudzień 2015</p>
                        </div>
                        <div class="col-md-6 no-padding">
                            <a href="newsDescription.php"> Czytaj więcej ></a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 newsElement">
                <div class="newsBox">
                    <div class="newsImg">
                        <img src="images/news1.jpg" alt="news1" class="img-responsive">
                    </div>
                    <div class="newsDescription">
                        <article>
                            <h4>Lorem ipsum dolor sit.</h4>

                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusamus amet commodi cum
                                deleniti dolore ea eaque eos
                            </p>
                        </article>
                    </div>
                    <div class="newsDateAndMore clearfix">
                        <div class="col-md-6 no-padding">
                            <p>20 Grudzień 2015</p>
                        </div>
                        <div class="col-md-6 no-padding">
                            <a href="newsDescription.php"> Czytaj więcej ></a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 newsElement">
                <div class="newsBox">
                    <div class="newsImg">
                        <img src="images/news1.jpg" alt="news1" class="img-responsive">
                    </div>
                    <div class="newsDescription">
                        <article>
                            <h4>Lorem ipsum dolor sit.</h4>

                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusamus amet commodi cum
                                deleniti dolore ea eaque eos
                            </p>
                        </article>
                    </div>
                    <div class="newsDateAndMore clearfix">
                        <div class="col-md-6 no-padding">
                            <p>20 Grudzień 2015</p>
                        </div>
                        <div class="col-md-6 no-padding">
                            <a href="newsDescription.php"> Czytaj więcej ></a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-md-12">
            <a href="#" class="more fullWidthButton" id="showMoreNewsElements">zobacz więcej</a>
        </div>
    </div>
</section>


<?php
@include ("downloadProducts.php");

@include ("map.php");

@include ("contactForm.php");

@include ("footer.php");
?>

</body>
</html>