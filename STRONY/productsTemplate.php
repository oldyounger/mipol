<div class="clearfix productBoxHolder">
    <div class="col-md-12">
        <h4 class="meatPartsSelection moreMargin">
            2. RIB
        </h4>
    </div>

    <div class="col-md-4 productElement">
        <div class="productBox">
            <div class="newsImg">
                <img src="images/stejk.jpg" alt="news1" class="img-responsive">
            </div>
            <div class="productDescription">
                <article>
                    <h4>Lorem ipsum dolor sit.</h4>

                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusamus amet commodi cum
                        deleniti dolore ea eaque eos
                    </p>
                </article>
            </div>
            <div class="productDateAndMore clearfix">
                <div class="col-md-6 no-padding">
                    <p>Nr indexu</p>
                </div>
                <div class="col-md-6 no-padding">
                    <a href="productsDescription.php"> Czytaj więcej ></a>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-4 productElement">
        <div class="productBox">
            <div class="productImg">
                <img src="images/stejk.jpg" alt="news1" class="img-responsive">
            </div>
            <div class="newsDescription">
                <article>
                    <h4>Lorem ipsum dolor sit.</h4>

                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusamus amet commodi cum
                        deleniti dolore ea eaque eos
                    </p>
                </article>
            </div>
            <div class="productDateAndMore clearfix">
                <div class="col-md-6 no-padding">
                    <p>Nr indexu</p>
                </div>
                <div class="col-md-6 no-padding">
                    <a href="productsDescription.php"> Czytaj więcej ></a>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-4 productElement">
        <div class="productBox">
            <div class="productImg">
                <img src="images/stejk.jpg" alt="news1" class="img-responsive">
            </div>
            <div class="productDescription">
                <article>
                    <h4>Lorem ipsum dolor sit.</h4>

                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusamus amet commodi cum
                        deleniti dolore ea eaque eos
                    </p>
                </article>
            </div>
            <div class="productDateAndMore clearfix">
                <div class="col-md-6 no-padding">
                    <p>Nr indexu</p>
                </div>
                <div class="col-md-6 no-padding">
                    <a href="productsDescription.php"> Czytaj więcej ></a>
                </div>
            </div>
        </div>
    </div>
</div>