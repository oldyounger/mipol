<!DOCTYPE html>
<html lang="en">
<?php
@include ("head.php");
?>
<body>

<?php
    @include ("mainMenu.php");
?>

<section class="slider">
    <div class="container-fluid no-padding">
        <div id="mainSlider" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#mainSlider" data-slide-to="0" class="active"></li>
                <li data-target="#mainSlider" data-slide-to="1"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div class="item active" style="background: url('images/slider.jpg') no-repeat center center;">
                    <div class="carousel-caption mainTitleHolder">
                        <div class="dateHolder">
                            <h5>2015</h5>
                        </div>
                        <div class="breakLine"></div>
                        <div class="titleHolder">
                            <h2>Lorem ipsum dolor sit amet.</h2>
                        </div>
                        <div class="breakLine"></div>
                        <div class="subTitleHolder">
                            <h3>Lorem ipsum dolor sit amet.</h3>
                        </div>
                        <a class="more" href="#">
                            czytaj więcej
                        </a>
                    </div>
                    <div class="blackOverlay"></div>
                    <div class="pattern"></div>
                </div>
                <div class="item" style="background: url('images/slider.jpg') no-repeat center center;">
                    <div class="carousel-caption mainTitleHolder">
                        <div class="dateHolder">
                            <h5>2015</h5>
                        </div>
                        <div class="breakLine"></div>
                        <div class="titleHolder">
                            <h2>Lorem ipsum dolor sit amet.</h2>
                        </div>
                        <div class="breakLine"></div>
                        <div class="subTitleHolder">
                            <h3>Lorem ipsum dolor sit amet.</h3>
                        </div>

                        <a class="more" href="#">
                            czytaj więcej
                        </a>
                    </div>
                    <div class="blackOverlay"></div>
                    <div class="pattern"></div>
                </div>
            </div>

            <!-- Controls -->
            <a class="left carousel-control" href="#mainSlider" role="button" data-slide="prev">
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#mainSlider" role="button" data-slide="next">
                <span class="sr-only">Next</span>
            </a>
            <a class="page-scroll" href="#news">
                <img src="images/arrowDown.png" alt="">
            </a>
        </div>
    </div>
</section>

<section id="news" class="news bgFirstShape">
    <div class="container">
        <div class="col-md-12">
            <div class="mainTitleHolder">
                <div class="breakLine thinLine"></div>
                <div class="titleHolder">
                    <h3>Aktualności</h3>
                </div>
                <div class="breakLine thinLine"></div>
            </div>
        </div>
        <div class="col-md-12 no-padding newsBoxHolder">
            <div class="col-md-4">
                <div class="newsBox">
                    <div class="newsImg">
                        <img src="images/news1.jpg" alt="news1" class="img-responsive">
                    </div>
                    <div class="newsDescription">
                        <article>
                            <h4>Lorem ipsum dolor sit.</h4>

                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusamus amet commodi cum
                                deleniti dolore ea eaque eos
                            </p>
                        </article>
                    </div>
                    <div class="newsDateAndMore clearfix">
                        <div class="col-md-6 no-padding">
                            <p>20 Grudzień 2015</p>
                        </div>
                        <div class="col-md-6 no-padding">
                            <a href="newsDescription.php"> Czytaj więcej ></a>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-md-4">
                <div class="newsBox">
                    <div class="newsImg">
                        <img src="images/news1.jpg" alt="news1" class="img-responsive">
                    </div>
                    <div class="newsDescription">
                        <article>
                            <h4>Lorem ipsum dolor sit.</h4>

                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusamus amet commodi cum
                                deleniti dolore ea eaque eos
                            </p>
                        </article>
                    </div>
                    <div class="newsDateAndMore clearfix">
                        <div class="col-md-6 no-padding">
                            <p>20 Grudzień 2015</p>
                        </div>
                        <div class="col-md-6 no-padding">
                            <a href="newsDescription.php"> Czytaj więcej ></a>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-md-4">
                <div class="newsBox">
                    <div class="newsImg">
                        <img src="images/news1.jpg" alt="news1" class="img-responsive">
                    </div>
                    <div class="newsDescription">
                        <article>
                            <h4>Lorem ipsum dolor sit.</h4>

                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusamus amet commodi cum
                                deleniti dolore ea eaque eos
                            </p>
                        </article>
                    </div>
                    <div class="newsDateAndMore clearfix">
                        <div class="col-md-6 no-padding">
                            <p>20 Grudzień 2015</p>
                        </div>
                        <div class="col-md-6 no-padding">
                            <a href="newsDescription.php"> Czytaj więcej ></a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-md-12">
            <a href="news.php" class="more">zobacz więcej</a>
        </div>
    </div>
</section>

<section class="offer" style="background: url('images/offer.jpg') no-repeat center center">
    <div class="container">
        <div class="col-md-12">
            <div class="mainTitleHolder abs">
                <div class="imgHolder">
                    <img src="images/meat.png" alt="meat">
                </div>
                <div class="breakLine thinLine"></div>
                <div class="titleHolder">
                    <h2>Lorem ipsum dolor sit amet.</h2>
                </div>
                <div class="breakLine thinLine"></div>
                <div class="subTitleHolder">
                    <h3>Lorem ipsum dolor sit amet.</h3>
                </div>
                <a class="more" href="#">
                    czytaj więcej
                </a>
            </div>
        </div>
    </div>
    <div class="blackOverlay"></div>
    <div class="pattern"></div>
</section>

<section class="purchaseOfCattle bgSecondShape">
    <div class="container">
        <div class="col-md-12">
            <div class="mainTitleHolder">
                <div class="breakLine thinLine"></div>
                <div class="titleHolder">
                    <h3>Skup bydła</h3>
                </div>
                <div class="breakLine thinLine"></div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="purchaseOfCattleDescription">
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab accusamus adipisci aliquam, animi
                    aut consequuntur corporis culpa cum debitis delectus dignissimos distinctio ea eos esse facilis
                    fugiat impedit in inventore itaque iure laborum neque nesciunt nulla odio odit officiis optio
                    perspiciatis quae quam quis rerum sed sequi sunt tempora ullam ut voluptatum. A aperiam aspernatur
                    culpa dignissimos dolorem eligendi error esse et eum itaque maiores, modi nulla optio placeat
                    provident, quibusdam quos saepe sapiente sint tempora veritatis vitae voluptate. Aliquid assumenda
                    blanditiis consequatur delectus deserunt eius eligendi eveniet expedita facilis hic illum iure
                    laboriosam, laudantium minus obcaecati officia quis sequi.
                </p>

                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab accusamus adipisci aliquam, animi
                    aut consequuntur corporis culpa cum debitis delectus dignissimos distinctio ea eos esse facilis
                    fugiat impedit in inventore itaque iure laborum neque nesciunt nulla odio odit officiis optio
                    perspiciatis quae quam quis rerum sed sequi sunt tempora ullam ut voluptatum. A aperiam aspernatur
                    culpa dignissimos dolorem eligendi error esse et eum itaque maiores, modi nulla optio placeat
                    provident, quibusdam quos saepe sapiente sint tempora veritatis vitae voluptate. Aliquid assumenda
                    blanditiis consequatur delectus deserunt eius eligendi eveniet expedita facilis hic illum iure
                    laboriosam, laudantium minus obcaecati officia quis sequi.
                </p>
            </div>
        </div>

        <div class="col-md-12">
            <a href="#" class="more">czytaj więcej</a>
        </div>
    </div>
</section>


<?php
@include ("downloadProducts.php");

@include ("map.php");

@include ("contactForm.php");

@include ("footer.php");
?>

</body>
</html>