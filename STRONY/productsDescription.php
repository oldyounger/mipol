<!DOCTYPE html>
<html lang="en">
<?php
@include ("head.php");
?>
<body>

<?php
@include ("mainMenu.php");
?>

<section class="mainPageSection" style="background: url('images/productsPage.jpg') no-repeat center center">
    <div class="container-fluid no-padding">
        <div class="mainTitleHolder pageSection">
            <div class="breakLine"></div>
            <div class="titleHolder">
                <h2>Lorem ipsum dolor sit amet.</h2>
            </div>
            <div class="breakLine"></div>
            <div class="subTitleHolder">
                <h3>Lorem ipsum dolor sit amet.</h3>
            </div>
        </div>
    </div>
    <div class="blackOverlay"></div>
    <div class="pattern"></div>
</section>

<section class="productsDescription">
    <div class="container">
        <div class="col-md-12">
            <div class="mainTitleHolder">
                <div class="breakLine thinLine"></div>
                <div class="titleHolder">
                    <h3>Baweta duża</h3>
                </div>
                <div class="breakLine thinLine"></div>
            </div>
        </div>

        <div class="col-md-12 productDescriptionFlexBox clearfix">
            <div class="col-md-6">
                <div id="productsSlider" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#productsSlider" data-slide-to="0" class="active"></li>
                        <li data-target="#productsSlider" data-slide-to="1"></li>
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <div class="item active">
                            <img class="img-responsive" src="images/productsDescription.jpg" alt="slider">
                        </div>
                        <div class="item">
                            <img class="img-responsive" src="images/productsDescription.jpg" alt="slider">
                        </div>
                    </div>
                    <!-- Controls -->
                    <a class="left carousel-control" href="#productsSlider" role="button" data-slide="prev">
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#productsSlider" role="button" data-slide="next">
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
            <div class="col-md-6">
                <div class="productDescriptionContent">
                    <h4>Opis części:</h4>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. A ab, accusantium assumenda
                        autem eaque earum eius iusto libero maiores minus molestiae obcaecati, odio officiis
                        provident quaerat sequi sit tempore vitae.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="howToPrepare">
    <div class="container">
        <div class="col-md-12">
            <div class="mainTitleHolder">
                <div class="breakLine thinLine whiteLine"></div>
                <div class="titleHolder">
                    <h4>Sposoby przygotowania</h4>
                </div>
                <div class="breakLine thinLine whiteLine"></div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="howToPrepareHolder">
                <ul>
                    <li>
                        <span class="cicrleHolder">
                            <img src="images/howToPrepare1.png" alt="" class="img-responsive">
                        </span>
                        <p>Lorem ipsum.</p>
                    </li>
                    <li>
                        <span class="cicrleHolder">
                           <img src="images/howToPrepare2.png" alt="" class="img-responsive">
                        </span>
                        <p>Lorem ipsum.</p>
                    </li>
                    <li>
                        <span class="cicrleHolder">
                            <img src="images/howToPrepare3.png" alt="" class="img-responsive">
                        </span>
                        <p>Lorem ipsum.</p>
                    </li>
                    <li>
                        <span class="cicrleHolder">
                            <img src="images/howToPrepare4.png" alt="" class="img-responsive">
                        </span>
                        <p>Lorem ipsum.</p>
                    </li>
                    <li>
                        <span class="cicrleHolder">
                            <img src="images/howToPrepare5.png" alt="" class="img-responsive">
                        </span>
                        <p>Lorem ipsum.</p>
                    </li>
                    <li>
                        <span class="cicrleHolder">
                            <img src="images/howToPrepare6.png" alt="" class="img-responsive">
                        </span>
                        <p>Lorem ipsum.</p>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="pattern"></div>
</section>

<section class="video">
    <div class="container">
        <div class="col-md-12">
            <div class="mainTitleHolder">
                <div class="breakLine thinLine"></div>
                <div class="titleHolder">
                    <h3>Zobacz wideo</h3>
                </div>
                <div class="breakLine thinLine"></div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="videoContent">
                <div class="videoWrapper">
                    <!-- Copy & Pasted from YouTube -->
                    <iframe width="560" height="349" src="http://www.youtube.com/embed/n_dZNLr2cME?rel=0&hd=1" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <a href="anotherProducts.php" class="more fullWidthButton">Powrót do produktów</a>
        </div>
    </div>
</section>


<?php
@include ("downloadProducts.php");

@include ("map.php");

@include ("contactForm.php");

@include ("footer.php");
?>

</body>
</html>