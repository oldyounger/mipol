<!DOCTYPE html>
<html lang="en">
<?php
@include ("head.php");
?>
<body>

<?php
@include ("mainMenu.php");
?>

<section class="mainPageSection" style="background: url('images/purchaseOfCattlePage.jpg') no-repeat center center">
    <div class="container-fluid no-padding">
        <div class="mainTitleHolder pageSection">
            <div class="breakLine"></div>
            <div class="titleHolder">
                <h2>Lorem ipsum dolor sit amet.</h2>
            </div>
            <div class="breakLine"></div>
            <div class="subTitleHolder">
                <h3>Lorem ipsum dolor sit amet.</h3>
            </div>
        </div>
    </div>
    <div class="blackOverlay"></div>
    <div class="pattern"></div>
</section>

<section class="mainDescriptionSectionPage">
    <div class="container">
        <div class="col-md-12">
            <div class="mainTitleHolder">
                <div class="breakLine thinLine"></div>
                <div class="titleHolder">
                    <h3>Kilka słów o nas</h3>
                </div>
                <div class="breakLine thinLine"></div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="sectionPageDescription">
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aliquam aspernatur aut consequatur
                    corporis cum debitis deserunt dicta fuga, illo labore laboriosam laborum maxime modi molestiae
                    nemo nesciunt perferendis possimus provident quas quasi qui quis quod recusandae
                    rem tempore veritatis.
                </p>

                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aliquam aspernatur aut consequatur
                    corporis cum debitis deserunt dicta fuga, illo labore laboriosam laborum maxime modi molestiae
                    nemo nesciunt perferendis possimus provident quas quasi qui quis quod recusandae
                    rem tempore veritatis.
                </p>
            </div>
        </div>
    </div>
</section>

<section class="purchaseOfCattleDescriptionSection">
    <div class="container-fluid no-padding">
        <div class="sectionPageDescriptionFlexBox clearfix">
            <div class="col-md-6 no-padding imgAboutUs" style="background: url('images/purchaseOfCattle1.jpg') no-repeat center center">
                <div class="blackLighterOverlay"></div>
                <div class="pattern"></div>
            </div>
            <div class="col-md-6 no-padding">
                <div class="sectionPageDescriptionText">
                    <h4>Skąd skupujemy</h4>

                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam cupiditate dolores eveniet
                        impedit maiores numquam! A assumenda at eum explicabo iure pariatur ut? Ab aliquid asperiores,
                        consequuntur dolore illum nulla obcaecati omnis quam quidem ratione repellendus, tempore.
                        Autem, deserunt dolores.
                    </p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam cupiditate dolores eveniet
                        impedit maiores numquam! A assumenda at eum explicabo iure pariatur ut? Ab aliquid asperiores,
                        consequuntur dolore illum nulla obcaecati omnis quam quidem ratione repellendus, tempore.
                        Autem, deserunt dolores.
                    </p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam cupiditate dolores eveniet
                        impedit maiores numquam! A assumenda at eum explicabo iure pariatur ut? Ab aliquid asperiores,
                        consequuntur dolore illum nulla obcaecati omnis quam quidem ratione repellendus, tempore.
                        Autem, deserunt dolores.
                    </p>
                </div>
            </div>
        </div>

        <div class="sectionPageDescriptionFlexBox clearfix">
            <div class="col-md-6 col-md-push-6 no-padding imgAboutUs" style="background: url('images/purchaseOfCattle2.jpg') no-repeat center center">
                <div class="blackLighterOverlay"></div>
                <div class="pattern"></div>
            </div>
            <div class="col-md-6 col-md-pull-6 no-padding">
                <div class="sectionPageDescriptionText">
                    <h4>System rozliczeń</h4>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam cupiditate dolores eveniet
                        impedit maiores numquam! A assumenda at eum explicabo iure pariatur ut? Ab aliquid asperiores,
                        consequuntur dolore illum nulla obcaecati omnis quam quidem ratione repellendus, tempore.
                        Autem, deserunt dolores.
                    </p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam cupiditate dolores eveniet
                        impedit maiores numquam! A assumenda at eum explicabo iure pariatur ut? Ab aliquid asperiores,
                        consequuntur dolore illum nulla obcaecati omnis quam quidem ratione repellendus, tempore.
                        Autem, deserunt dolores.
                    </p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam cupiditate dolores eveniet
                        impedit maiores numquam! A assumenda at eum explicabo iure pariatur ut? Ab aliquid asperiores,
                        consequuntur dolore illum nulla obcaecati omnis quam quidem ratione repellendus, tempore.
                        Autem, deserunt dolores.
                    </p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam cupiditate dolores eveniet
                        impedit maiores numquam! A assumenda at eum explicabo iure pariatur ut? Ab aliquid asperiores,
                        consequuntur dolore illum nulla obcaecati omnis quam quidem ratione repellendus, tempore.
                        Autem, deserunt dolores.
                    </p>
                </div>
            </div>
        </div>

        <div class="sectionPageDescriptionFlexBox clearfix">
            <div class="col-md-6 no-padding imgAboutUs" style="background: url('images/purchaseOfCattle3.jpg') no-repeat center center">
                <div class="blackLighterOverlay"></div>
                <div class="pattern"></div>
            </div>
            <div class="col-md-6 no-padding">
                <div class="sectionPageDescriptionText">
                    <h4>Z kim współpracujemy</h4>

                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam cupiditate dolores eveniet
                        impedit maiores numquam! A assumenda at eum explicabo iure pariatur ut? Ab aliquid asperiores,
                        consequuntur dolore illum nulla obcaecati omnis quam quidem ratione repellendus, tempore.
                        Autem, deserunt dolores.
                    </p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam cupiditate dolores eveniet
                        impedit maiores numquam! A assumenda at eum explicabo iure pariatur ut? Ab aliquid asperiores,
                        consequuntur dolore illum nulla obcaecati omnis quam quidem ratione repellendus, tempore.
                        Autem, deserunt dolores.
                    </p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam cupiditate dolores eveniet
                        impedit maiores numquam! A assumenda at eum explicabo iure pariatur ut? Ab aliquid asperiores,
                        consequuntur dolore illum nulla obcaecati omnis quam quidem ratione repellendus, tempore.
                        Autem, deserunt dolores.
                    </p>
                </div>
            </div>
        </div>

    </div>
</section>

<section class="priceList bgSecondShape">
    <div class="container">
        <div class="col-md-12">
            <div class="mainTitleHolder">
                <div class="breakLine thinLine"></div>
                <div class="titleHolder">
                    <h3>Cennik produktów</h3>
                </div>
                <div class="breakLine thinLine"></div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="priceListDescription">
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab accusamus adipisci aliquam, animi
                    aut consequuntur corporis culpa cum debitis delectus dignissimos distinctio ea eos esse facilis
                    fugiat impedit in inventore itaque iure laborum neque nesciunt nulla odio odit officiis optio
                    perspiciatis quae quam quis rerum sed sequi sunt tempora ullam ut voluptatum. A aperiam aspernatur
                    culpa dignissimos dolorem eligendi error esse et eum itaque maiores, modi nulla optio placeat
                    provident, quibusdam quos saepe sapiente sint tempora veritatis vitae voluptate. Aliquid assumenda
                    blanditiis consequatur delectus deserunt eius eligendi eveniet expedita facilis hic illum iure
                    laboriosam, laudantium minus obcaecati officia quis sequi.
                </p>

                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab accusamus adipisci aliquam, animi
                    aut consequuntur corporis culpa cum debitis delectus dignissimos distinctio ea eos esse facilis
                    fugiat impedit in inventore itaque iure laborum neque nesciunt nulla odio odit officiis optio
                    perspiciatis quae quam quis rerum sed sequi sunt tempora ullam ut voluptatum. A aperiam aspernatur
                    culpa dignissimos dolorem eligendi error esse et eum itaque maiores, modi nulla optio placeat
                    provident, quibusdam quos saepe sapiente sint tempora veritatis vitae voluptate. Aliquid assumenda
                    blanditiis consequatur delectus deserunt eius eligendi eveniet expedita facilis hic illum iure
                    laboriosam, laudantium minus obcaecati officia quis sequi.
                </p>
            </div>
        </div>

        <div class="col-md-12">
            <a href="#" class="more">pobierz cennik</a>
        </div>
    </div>
</section>

<section class="certifications">
    <div class="container">
        <div class="col-md-12">
            <div class="mainTitleHolder">
                <div class="breakLine thinLine"></div>
                <div class="titleHolder">
                    <h3>Certyfikaty</h3>
                </div>
                <div class="breakLine thinLine"></div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="certificationsHolder">
                <ul>
                    <li>
                        <img class="img-responsive" src="images/certifications1.png" alt="certifications">
                    </li>
                    <li>
                        <img class="img-responsive" src="images/certifications2.png" alt="certifications">
                    </li>
                    <li>
                        <img class="img-responsive" src="images/certifications3.png" alt="certifications">
                    </li>
                    <li>
                        <img class="img-responsive" src="images/certifications4.png" alt="certifications">
                    </li>
                    <li>
                        <img class="img-responsive" src="images/certifications5.png" alt="certifications">
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>


<?php
@include ("downloadProducts.php");

@include ("map.php");

@include ("contactForm.php");

@include ("footer.php");
?>

</body>
</html>