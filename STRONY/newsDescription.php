<!DOCTYPE html>
<html lang="en">
<?php
@include ("head.php");
?>
<body>

<?php
@include ("mainMenu.php");
?>

<section class="mainPageSection" style="background: url('images/newsPage.jpg') no-repeat center center">
    <div class="container-fluid no-padding">
        <div class="mainTitleHolder pageSection">
            <div class="breakLine"></div>
            <div class="titleHolder">
                <h2>Lorem ipsum dolor sit amet.</h2>
            </div>
            <div class="breakLine"></div>
            <div class="subTitleHolder">
                <h3>Lorem ipsum dolor sit amet.</h3>
            </div>
        </div>
    </div>
    <div class="blackOverlay"></div>
    <div class="pattern"></div>
</section>

<section class="newsDescription">
    <div class="container">
        <div class="col-md-12">
            <div id="descriptionSlider" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#descriptionSlider" data-slide-to="0" class="active"></li>
                    <li data-target="#descriptionSlider" data-slide-to="1"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="images/slider.jpg" alt="slider">
                    </div>
                    <div class="item">
                        <img src="images/slider.jpg" alt="slider">
                    </div>
                </div>
                <!-- Controls -->
                <a class="left carousel-control" href="#descriptionSlider" role="button" data-slide="prev">
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#descriptionSlider" role="button" data-slide="next">
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>

        <div class="col-md-12">
            <div class="titleNews">
                <div class="mainTitle">
                    <h4>Lorem ipsum dolor sit amet.</h4>
                </div>
                <div class="breakLine thinLineNews"></div>
                <div class="infoNewsHolder clearfix">
                    <ul>
                        <li>
                            <img src="images/calendar.png" alt="calendar">
                            <span>26 Listopad 2015</span>
                        </li>
                        <li>
                            <img src="images/profile.png" alt="profile">
                            <span>Jan Kowalski</span>
                        </li>

                    </ul>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="descriptionText">
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium consectetur deleniti
                    distinctio eos esse ex facilis, maiores nostrum porro quisquam, sequi sunt. Ab beatae dicta
                    ea itaque nesciunt quasi, sequi? Commodi, doloremque dolores dolorum eligendi ipsa minus
                    sequi. Ipsam, numquam?
                </p>

                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium consectetur deleniti
                    distinctio eos esse ex facilis, maiores nostrum porro quisquam, sequi sunt. Ab beatae dicta
                    ea itaque nesciunt quasi, sequi? Commodi, doloremque dolores dolorum eligendi ipsa minus
                    sequi. Ipsam, numquam?
                </p>

                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium consectetur deleniti
                    distinctio eos esse ex facilis, maiores nostrum porro quisquam, sequi sunt. Ab beatae dicta
                    ea itaque nesciunt quasi, sequi? Commodi, doloremque dolores dolorum eligendi ipsa minus
                    sequi. Ipsam, numquam?
                </p>

                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium consectetur deleniti
                    distinctio eos esse ex facilis, maiores nostrum porro quisquam, sequi sunt. Ab beatae dicta
                    ea itaque nesciunt quasi, sequi? Commodi, doloremque dolores dolorum eligendi ipsa minus
                    sequi. Ipsam, numquam?
                </p>

                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium consectetur deleniti
                    distinctio eos esse ex facilis, maiores nostrum porro quisquam, sequi sunt. Ab beatae dicta
                    ea itaque nesciunt quasi, sequi? Commodi, doloremque dolores dolorum eligendi ipsa minus
                    sequi. Ipsam, numquam?
                </p>

                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium consectetur deleniti
                    distinctio eos esse ex facilis, maiores nostrum porro quisquam, sequi sunt. Ab beatae dicta
                    ea itaque nesciunt quasi, sequi? Commodi, doloremque dolores dolorum eligendi ipsa minus
                    sequi. Ipsam, numquam?
                </p>

                <img class="img-responsive" src="images/slider.jpg" alt="newsPicture">

                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium consectetur deleniti
                    distinctio eos esse ex facilis, maiores nostrum porro quisquam, sequi sunt. Ab beatae dicta
                    ea itaque nesciunt quasi, sequi? Commodi, doloremque dolores dolorum eligendi ipsa minus
                    sequi. Ipsam, numquam?
                </p>

                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium consectetur deleniti
                    distinctio eos esse ex facilis, maiores nostrum porro quisquam, sequi sunt. Ab beatae dicta
                    ea itaque nesciunt quasi, sequi? Commodi, doloremque dolores dolorum eligendi ipsa minus
                    sequi. Ipsam, numquam?
                </p>

                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium consectetur deleniti
                    distinctio eos esse ex facilis, maiores nostrum porro quisquam, sequi sunt. Ab beatae dicta
                    ea itaque nesciunt quasi, sequi? Commodi, doloremque dolores dolorum eligendi ipsa minus
                    sequi. Ipsam, numquam?
                </p>

                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium consectetur deleniti
                    distinctio eos esse ex facilis, maiores nostrum porro quisquam, sequi sunt. Ab beatae dicta
                    ea itaque nesciunt quasi, sequi? Commodi, doloremque dolores dolorum eligendi ipsa minus
                    sequi. Ipsam, numquam?
                </p>
            </div>
        </div>

        <div class="col-md-12">
            <a href="news.php" class="more fullWidthButton"><< powrót do aktualności</a>
        </div>

    </div>
</section>


<?php

@include ("footer.php");
?>

</body>
</html>