<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Mipol</title>

    <!-- Bootstrap -->
    <link href="css/reset.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

    <link href='https://fonts.googleapis.com/css?family=Work+Sans:400,200,300,500,600,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js" type="text/javascript"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/jquery.easing.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/map.js"></script>
</head>
<body>

<?php
@include ("mainMenu.php");
?>

<section class="mainPageSection" style="background: url('images/productsPage.jpg') no-repeat center center">
    <div class="container-fluid no-padding">
        <div class="mainTitleHolder pageSection">
            <div class="breakLine"></div>
            <div class="titleHolder">
                <h2>Lorem ipsum dolor sit amet.</h2>
            </div>
            <div class="breakLine"></div>
            <div class="subTitleHolder">
                <h3>Lorem ipsum dolor sit amet.</h3>
            </div>
        </div>
    </div>
    <div class="blackOverlay"></div>
    <div class="pattern"></div>
</section>

<section class="products">
    <div class="container">
        <div class="col-md-12">
            <div class="mainTitleHolder">
                <div class="breakLine thinLine"></div>
                <div class="titleHolder">
                    <h3>Ćwierci wołowe</h3>
                </div>
                <div class="breakLine thinLine"></div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="sectionPageDescription">
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aliquam aspernatur aut consequatur
                    corporis cum debitis deserunt dicta fuga, illo labore laboriosam laborum maxime modi molestiae
                    nemo nesciunt perferendis possimus provident quas quasi qui quis quod recusandae
                    rem tempore veritatis.
                </p>

                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aliquam aspernatur aut consequatur
                    corporis cum debitis deserunt dicta fuga, illo labore laboriosam laborum maxime modi molestiae
                    nemo nesciunt perferendis possimus provident quas quasi qui quis quod recusandae
                    rem tempore veritatis.
                </p>
            </div>
        </div>

        <div class="col-md-12 no-padding productBoxHolder">

            <div class="col-md-4 productElement">
                <div class="productBox">
                    <div class="newsImg">
                        <img src="images/stejk.jpg" alt="news1" class="img-responsive">
                    </div>
                    <div class="productDescription">
                        <article>
                            <h4>Lorem ipsum dolor sit.</h4>

                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusamus amet commodi cum
                                deleniti dolore ea eaque eos
                            </p>
                        </article>
                    </div>
                    <div class="productDateAndMore clearfix">
                        <div class="col-md-6 no-padding">
                            <p>Nr indexu</p>
                        </div>
                        <div class="col-md-6 no-padding">
                            <a href="anotherProductsDescription.php"> Czytaj więcej ></a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 productElement">
                <div class="productBox">
                    <div class="productImg">
                        <img src="images/stejk.jpg" alt="news1" class="img-responsive">
                    </div>
                    <div class="newsDescription">
                        <article>
                            <h4>Lorem ipsum dolor sit.</h4>

                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusamus amet commodi cum
                                deleniti dolore ea eaque eos
                            </p>
                        </article>
                    </div>
                    <div class="productDateAndMore clearfix">
                        <div class="col-md-6 no-padding">
                            <p>Nr indexu</p>
                        </div>
                        <div class="col-md-6 no-padding">
                            <a href="anotherProductsDescription.php"> Czytaj więcej ></a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 productElement">
                <div class="productBox">
                    <div class="productImg">
                        <img src="images/stejk.jpg" alt="news1" class="img-responsive">
                    </div>
                    <div class="productDescription">
                        <article>
                            <h4>Lorem ipsum dolor sit.</h4>

                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusamus amet commodi cum
                                deleniti dolore ea eaque eos
                            </p>
                        </article>
                    </div>
                    <div class="productDateAndMore clearfix">
                        <div class="col-md-6 no-padding">
                            <p>Nr indexu</p>
                        </div>
                        <div class="col-md-6 no-padding">
                            <a href="anotherProductsDescription.php"> Czytaj więcej ></a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 productElement">
                <div class="productBox">
                    <div class="productImg">
                        <img src="images/stejk.jpg" alt="news1" class="img-responsive">
                    </div>
                    <div class="productDescription">
                        <article>
                            <h4>Lorem ipsum dolor sit.</h4>

                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusamus amet commodi cum
                                deleniti dolore ea eaque eos
                            </p>
                        </article>
                    </div>
                    <div class="productDateAndMore clearfix">
                        <div class="col-md-6 no-padding">
                            <p>Nr indexu</p>
                        </div>
                        <div class="col-md-6 no-padding">
                            <a href="anotherProductsDescription.php"> Czytaj więcej ></a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 productElement">
                <div class="productBox">
                    <div class="productImg">
                        <img src="images/stejk.jpg" alt="news1" class="img-responsive">
                    </div>
                    <div class="productDescription">
                        <article>
                            <h4>Lorem ipsum dolor sit.</h4>

                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusamus amet commodi cum
                                deleniti dolore ea eaque eos
                            </p>
                        </article>
                    </div>
                    <div class="productDateAndMore clearfix">
                        <div class="col-md-6 no-padding">
                            <p>Nr indexu</p>
                        </div>
                        <div class="col-md-6 no-padding">
                            <a href="anotherProductsDescription.php"> Czytaj więcej ></a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 productElement">
                <div class="productBox">
                    <div class="productImg">
                        <img src="images/stejk.jpg" alt="news1" class="img-responsive">
                    </div>
                    <div class="productDescription">
                        <article>
                            <h4>Lorem ipsum dolor sit.</h4>

                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusamus amet commodi cum
                                deleniti dolore ea eaque eos
                            </p>
                        </article>
                    </div>
                    <div class="productDateAndMore clearfix">
                        <div class="col-md-6 no-padding">
                            <p>Nr indexu</p>
                        </div>
                        <div class="col-md-6 no-padding">
                            <a href="anotherProductsDescription.php"> Czytaj więcej ></a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 productElement">
                <div class="productBox">
                    <div class="productImg">
                        <img src="images/stejk.jpg" alt="news1" class="img-responsive">
                    </div>
                    <div class="productDescription">
                        <article>
                            <h4>Lorem ipsum dolor sit.</h4>

                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusamus amet commodi cum
                                deleniti dolore ea eaque eos
                            </p>
                        </article>
                    </div>
                    <div class="productDateAndMore clearfix">
                        <div class="col-md-6 no-padding">
                            <p>Nr indexu</p>
                        </div>
                        <div class="col-md-6 no-padding">
                            <a href="anotherProductsDescription.php"> Czytaj więcej ></a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 productElement">
                <div class="productBox">
                    <div class="productImg">
                        <img src="images/stejk.jpg" alt="news1" class="img-responsive">
                    </div>
                    <div class="productDescription">
                        <article>
                            <h4>Lorem ipsum dolor sit.</h4>

                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusamus amet commodi cum
                                deleniti dolore ea eaque eos
                            </p>
                        </article>
                    </div>
                    <div class="productDateAndMore clearfix">
                        <div class="col-md-6 no-padding">
                            <p>Nr indexu</p>
                        </div>
                        <div class="col-md-6 no-padding">
                            <a href="anotherProductsDescription.php"> Czytaj więcej ></a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 productElement">
                <div class="productBox">
                    <div class="productImg">
                        <img src="images/stejk.jpg" alt="news1" class="img-responsive">
                    </div>
                    <div class="productDescription">
                        <article>
                            <h4>Lorem ipsum dolor sit.</h4>

                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusamus amet commodi cum
                                deleniti dolore ea eaque eos
                            </p>
                        </article>
                    </div>
                    <div class="productDateAndMore clearfix">
                        <div class="col-md-6 no-padding">
                            <p>Nr indexu</p>
                        </div>
                        <div class="col-md-6 no-padding">
                            <a href="anotherProductsDescription.php"> Czytaj więcej ></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<?php
@include ("downloadProducts.php");

@include ("map.php");

@include ("contactForm.php");

@include ("footer.php");
?>

</body>
</html>