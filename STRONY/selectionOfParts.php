<!DOCTYPE html>
<html lang="en">
<?php
@include ("head.php");
?>
<body>

<?php
@include ("mainMenu.php");
?>

<section class="mainPageSection" style="background: url('images/productsPage.jpg') no-repeat center center">
    <div class="container-fluid no-padding">
        <div class="mainTitleHolder pageSection">
            <div class="breakLine"></div>
            <div class="titleHolder">
                <h2>Lorem ipsum dolor sit amet.</h2>
            </div>
            <div class="breakLine"></div>
            <div class="subTitleHolder">
                <h3>Lorem ipsum dolor sit amet.</h3>
            </div>
        </div>
    </div>
    <div class="blackOverlay"></div>
    <div class="pattern"></div>
</section>

<section class="products">
    <div class="container">
        <div class="col-md-12">
            <div class="mainTitleHolder">
                <div class="breakLine thinLine"></div>
                <div class="titleHolder">
                    <h3>Elementy wołowe</h3>
                </div>
                <div class="breakLine thinLine"></div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="sectionPageDescription">
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aliquam aspernatur aut consequatur
                    corporis cum debitis deserunt dicta fuga, illo labore laboriosam laborum maxime modi molestiae
                    nemo nesciunt perferendis possimus provident quas quasi qui quis quod recusandae
                    rem tempore veritatis.
                </p>

                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aliquam aspernatur aut consequatur
                    corporis cum debitis deserunt dicta fuga, illo labore laboriosam laborum maxime modi molestiae
                    nemo nesciunt perferendis possimus provident quas quasi qui quis quod recusandae
                    rem tempore veritatis.
                </p>
            </div>
        </div>
        <div class="col-md-12 imgMapHolder">
            <h4 class="meatPartsSelection moreMargin">
                1. Zaznacz część
            </h4>
            <img class="img-responsive" src="images/partsSelection.jpg" alt="" usemap="#MapImg" />
            <map name="MapImg" id="MapImg">
                <area data-category = "shank" alt="" title="" href="#" shape="poly" coords="103,247,45,315,69,341,169,296" />
                <area data-category = "brisket" alt="" title="" href="#" shape="poly" coords="110,240,176,291,185,287,252,304,301,303,304,242,122,224" />
                <area data-category = "plate" alt="" title="" href="#" shape="poly" coords="320,244,317,302,384,299,477,297,478,236" />
                <area data-category = "flank" alt="" title="" href="#" shape="poly" coords="491,239,491,300,638,297,688,304" />
                <area data-category = "chuck" alt="" title="" href="#" shape="poly" coords="212,55,228,39,299,60,305,229,124,210,95,106,156,17" />
                <area data-category = "rib" alt="" title="" href="#" shape="poly" coords="311,61,318,230,478,222,473,90" />
                <area data-category = "loin" alt="" title="" href="#" shape="poly" coords="484,90,489,225,695,292,728,118,715,100,598,115" />
                <area data-category = "round" alt="" title="" href="#" shape="poly" coords="706,296,760,310,944,378,971,350,933,304,868,266,800,152,773,151,768,112,738,125" />
            </map>
        </div>
        <div class="col-md-12 no-padding clearfix" id="productElements"></div>

    </div>
</section>


<?php
@include ("downloadProducts.php");

@include ("map.php");

@include ("contactForm.php");

@include ("footer.php");
?>

</body>
</html>