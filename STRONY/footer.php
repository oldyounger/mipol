<section class="prefooter">
    <div class="container">
        <div class="col-md-12">
            <div class="prefooterDescription">
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis consectetur culpa distinctio
                    ea inventore libero molestiae mollitia nulla omnis temporibus.
                </p>
            </div>
        </div>
        <div class="col-md-12">
            <div class="socialHolder">
                <ul>
                    <li>
                        <a href="#">
                            <img src="images/facebook.png" alt="">
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img src="images/googleplus.png" alt="">
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="pattern"></div>
</section>

<footer>
    <div class="container">
        <div class="col-md-12">
            <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. A amet blanditiis consectetur deleniti
                iusto, molestiae mollitia officia placeat suscipit voluptates!
                <a href="#">lorem</a>
            </p>
        </div>
    </div>
    <div class="pattern"></div>
    <div class="blackOverlay"></div>
</footer>