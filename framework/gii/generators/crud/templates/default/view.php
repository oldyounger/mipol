<?php
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<?php echo "<?php\n"; ?>
/* @var $this <?php echo $this->getControllerClass(); ?> */
/* @var $model <?php echo $this->getModelClass(); ?> */

<?php
$nameColumn=$this->guessNameColumn($this->tableSchema->columns);
$label=$this->pluralize($this->class2name($this->modelClass));
echo "\$this->breadcrumbs = Array(
	'$label' => Array('index'),
	\$model->{$nameColumn},
);\n";
?>

$this->menu = Array(
	Array('label' => 'List <?php echo $this->modelClass; ?>', 'url' => Array('index')),
	Array('label' => 'Create <?php echo $this->modelClass; ?>', 'url' => Array('create')),
	Array('label' => 'Update <?php echo $this->modelClass; ?>', 'url' => Array('update', 'id' => $model-><?php echo $this->tableSchema->primaryKey; ?>)),
	Array('label' => 'Delete <?php echo $this->modelClass; ?>', 'url' => '#', 'linkOptions' => Array('submit'=> Array('delete', 'id' => $model-><?php echo $this->tableSchema->primaryKey; ?>), 'confirm'=>'Are you sure you want to delete this item?')),
	Array('label' => 'Manage <?php echo $this->modelClass; ?>', 'url' => Array('admin')),
);
?>

<h1>View <?php echo $this->modelClass." #<?php echo \$model->{$this->tableSchema->primaryKey}; ?>"; ?></h1>

<?php echo "<?php"; ?>
	$this->widget('zii.widgets.CDetailView', Array(
		'data' => $model,
		'attributes' => Array(
<?php
foreach($this->tableSchema->columns as $column)
	echo "\t\t\t'".$column->name."',\n";
?>
		),
	));
?>
