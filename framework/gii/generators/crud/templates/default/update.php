<?php
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<?php echo "<?php\n"; ?>
/* @var $this <?php echo $this->getControllerClass(); ?> */
/* @var $model <?php echo $this->getModelClass(); ?> */

<?php
$nameColumn=$this->guessNameColumn($this->tableSchema->columns);
$label=$this->pluralize($this->class2name($this->modelClass));
echo "\$this->breadcrumbs = Array(
	'$label' => Array('index'),
	\$model->{$nameColumn} => Array('view', 'id' => \$model->{$this->tableSchema->primaryKey}),
	'Update',
);\n";
?>

$this->menu = Array(
	Array('label' => 'List <?php echo $this->modelClass; ?>', 'url' => Array('index')),
	Array('label' => 'Create <?php echo $this->modelClass; ?>', 'url' => Array('create')),
	Array('label' => 'View <?php echo $this->modelClass; ?>', 'url' => Array('view', 'id' => $model-><?php echo $this->tableSchema->primaryKey; ?>)),
	Array('label' => 'Manage <?php echo $this->modelClass; ?>', 'url' => Array('admin')),
);
?>

<h1>Update <?php echo $this->modelClass." <?php echo \$model->{$this->tableSchema->primaryKey}; ?>"; ?></h1>

<?php echo "<?php \$this->renderPartial('_form', Array('model' => \$model)); ?>"; ?>