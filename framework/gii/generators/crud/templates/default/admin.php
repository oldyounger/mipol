<?php
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<?php echo "<?php\n"; ?>
/* @var $this <?php echo $this->getControllerClass(); ?> */
/* @var $model <?php echo $this->getModelClass(); ?> */

<?php
$label= $this->pluralize($this->class2name($this->modelClass));
echo "\$this->breadcrumbs = Array(
	'$label' => Array('index'),
	'Manage',
);\n";
?>

$this->menu = Array(
	Array('label' => 'List <?php echo $this->modelClass; ?>', 'url' => Array('index')),
	Array('label' => 'Create <?php echo $this->modelClass; ?>', 'url' => Array('create')),
);

Yii::app()->clientScript->registerScript('search', "
	$('.search-button').click(function(){
		$('.search-form').toggle();
		return false;
	});
	$('.search-form form').submit(function(){
		$('#<?php echo $this->class2id($this->modelClass); ?>-grid').yiiGridView('update', {
			data: $(this).serialize()
		});
		return false;
	});
");
?>

<h1>Manage <?php echo $this->pluralize($this->class2name($this->modelClass)); ?></h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo "<?php echo CHtml::link('Advanced Search', '#', Array('class' => 'search-button')); ?>"; ?>

<div class="search-form" style="display:none">
<?php echo "<?php
	\$this->renderPartial('_search', Array(
		'model' => \$model,
	));
?>\n"; ?>
</div><!-- search-form -->

<?php echo "<?php"; ?>
	$this->widget('zii.widgets.grid.CGridView', Array(
		'id' => '<?php echo $this->class2id($this->modelClass); ?>-grid',
		'dataProvider' => $model->search(),
		'filter' => $model,
		'columns' => Array(
<?php
$count=0;
foreach($this->tableSchema->columns as $column)
{
	if(++$count==7)
		echo "\t\t\t/*\n";
	echo "\t\t\t'".$column->name."',\n";
}
if($count>=7)
	echo "\t\t\t*/\n";
?>
			Array(
				'class' => 'CButtonColumn',
			),
		),
	));
?>
