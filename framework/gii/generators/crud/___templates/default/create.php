<?php
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<?php echo "<?php\n"; ?>
/* @var $this <?php echo $this->getControllerClass(); ?> */
/* @var $model <?php echo $this->getModelClass(); ?> */

<?php
$label = $this->pluralize($this->class2name($this->modelClass));
echo "\$this->breadcrumbs = Array(
	'$label' => Array('index'),
	'Create',
);\n";
?>

$this->menu = Array(
	Array('label' => 'List <?php echo $this->modelClass; ?>', 'url' => Array('index')),
	Array('label' => 'Manage <?php echo $this->modelClass; ?>', 'url' => Array('admin')),
);
?>

<h1>Create <?php echo $this->modelClass; ?></h1>

<?php echo "<?php \$this->renderPartial('_form', Array('model' => \$model)); ?>"; ?>
