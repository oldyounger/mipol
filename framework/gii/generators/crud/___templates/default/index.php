<?php
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<?php echo "<?php\n"; ?>
/* @var $this <?php echo $this->getControllerClass(); ?> */
/* @var $dataProvider CActiveDataProvider */

<?php
$label=$this->pluralize($this->class2name($this->modelClass));
echo "\$this->breadcrumbs = Array(
	'$label',
);\n";
?>

$this->menu = Array(
	Array('label' => 'Create <?php echo $this->modelClass; ?>', 'url' => Array('create')),
	Array('label' => 'Manage <?php echo $this->modelClass; ?>', 'url' => Array('admin')),
);
?>

<h1><?php echo $label; ?></h1>

<?php echo "<?php"; ?>
	$this->widget('zii.widgets.CListView', Array(
		'dataProvider' => $dataProvider,
		'itemView' => '_view',
	));
?>
