<?php
#error_reporting(E_ALL);
#ini_set('display_errors', 1);
if(isSet($_REQUEST['php-sessid'])){ //fix of known uploadify flash bugs with sessions!!
	$_COOKIE['PHPSESSID'] = $_REQUEST['php-sessid'];
}

/* Redirecting if we use www. (it breaks AJAX and everything..) */
if($_SERVER['REQUEST_METHOD'] !== 'POST') {
	if(isSet($_SERVER['HTTP_HOST']) && strpos($_SERVER['HTTP_HOST'], 'www.') === 0){
		$host = preg_replace('@^www\.@', '', $_SERVER['HTTP_HOST'], 1);
		$s = (isSet($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? 's' : NULL;
		header("Location: http$s://{$host}{$_SERVER['REQUEST_URI']}", true, 301);

		exit(0);
	}
}


// change the following paths if necessary
$yii=dirname(__FILE__).'/framework/yii.php';
$config=dirname(__FILE__).'/protected/config/mainConfig.php';

// remove the following lines when in production mode
defined('YII_DEBUG') or define('YII_DEBUG', true);
// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);

require_once($yii);

Yii::createWebApplication($config)->run();
