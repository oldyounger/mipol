<?php
return Array(
	'Forbidden You don\'t have permission to access this page' => 'Nie masz dostępu do tej strony',
	'The requested page does not exist.' => 'Żądana strona nie istnieje.',
	'Invalid request. Please do not repeat this request again.' => 'Nieprawidłowe żądanie. Nie powtarzaj tego zapytania ponownie.',
);