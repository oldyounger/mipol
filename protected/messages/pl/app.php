<?php
return Array(
	'Yes' => 'Tak',
	'No' => 'Nie',
	'Error' => 'Błąd',

	'Image Files' => 'Pliki obrazów',
	'Flash Files' => 'Pliki Flash',

	'years ago' => 'lat temu',
	'year ago' => 'rok temu',
	'month ago' => 'miesiąc temu',
	'months ago' => 'miesięcy temu',
	'day ago' => 'wczoraj',
	'days ago' => 'dni temu',
	'hours ago' => 'h temu',
	'minutes ago' => 'min temu',
	'just now' => 'przed chwilą',

	'in {count} years' => 'za {count} lat',
	'this year' => 'w tym roku',
	'this month' => 'w tym miesiącu',
	'this week' => 'w tym tygodniu',
	'today' => 'dzisiaj',
	'tomorrow' => 'jutro',
	'yesterday' => 'wczoraj',
	'in a moment' => 'za chwilę',
	'now' => 'teraz',

    'Date format is invalid.' => 'Format daty jest nieprawidłowy.',
    'Time format is invalid.' => 'Format czasu jest nieprawidłowy.',
);