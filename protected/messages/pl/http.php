<?php
return Array(
	"We're so sorry but the page you are looking for does not exists." => 'Bardzo nam przykro, ale strona, której szukasz nie istnieje.',
	"We're so sorry but you don't have access to this page." => 'Bardzo nam przykro, ale nie masz dostępu do tej strony.',
);