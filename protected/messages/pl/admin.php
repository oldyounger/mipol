<?php
return Array(
	'Successfully deleted this row.' => 'Pomyślnie usunięto ten wpis.',
	'Successfully updated this row.' => 'Pomyślnie zaktualizowano ten wpis.',
	'Successfully added this row.' => 'Pomyślnie dodano ten wpis.',
	'Encountered error while performing this action.' => 'Wystąpił błąd podczas wykonywania akcji.',

	'Price format is invalid.' => 'Format ceny jest nieprawidłowy.',
);