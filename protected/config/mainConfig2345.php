<?php
date_default_timezone_set('Europe/Warsaw');

function _joinpath($dir1, $dir2)
{
	return realpath($dir1 . DIRECTORY_SEPARATOR . $dir2);
}

$__homePath = dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..';
$__protectedPath = _joinpath($__homePath, 'protected');
$__runtimePath = _joinpath($__homePath, 'runtime');

define('ROOT_DIR', $__homePath);

//parts of config loaded from separate files
$routesArray = require('routesConfig.php');
$databaseArray = require('databaseConfig.php');

$debugArray = Array(
	Array(
		'class' => 'ext.yii-debug-toolbar.YiiDebugToolbarRoute',
		'ipFilters' => Array(
			'127.0.0.1',
			'*'
		),
	)
);

if ((!defined("YII_DEBUG") || !YII_DEBUG) || (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') || mb_strrpos(mb_strtolower($_SERVER['REQUEST_URI']), 'uploadify') !== false){
	$debugArray = Array();
}

//fix for uploading avatar by hash on debug mode
if (defined('YII_DEBUG') && YII_DEBUG && (mb_strrpos(mb_strtolower($_SERVER['REQUEST_URI']), 'avatar/temp') !== false || mb_strrpos(mb_strtolower($_SERVER['REQUEST_URI']), 'avatar/temp') !== false)){
	$debugArray = Array();
}
else if (defined('YII_DEBUG') && YII_DEBUG && (mb_strrpos(mb_strtolower($_SERVER['REQUEST_URI']), 'avatar/temp') !== false || mb_strrpos(mb_strtolower($_SERVER['REQUEST_URI']), 'galeria/zdjecie/dodaj') !== false)){
	$debugArray = Array();
}

$debugArray = Array();

return Array(
	'basePath' => $__protectedPath,
	'runtimePath' => $__runtimePath,
	'name' => 'Bunscha',
	'language' => 'pl',
	'theme' => 'white',
	'defaultController' => 'Site',
	'layout'=>'blank',
	'preload' => Array(
		'log',
		'bootstrap',
	),
	'import' => Array(
		'application.models.*',
		'application.components.*',
		'application.components.wideimage.WideImage',
		'application.extensions.*',
		'application.extensions.uploadify.components.*',
		'application.widgets.*',
	),
	'modules' => Array(
		'admin' => Array(
			//NOTE: use below only if admin panel uses bootstrap
			'preload' => Array(
				'bootstrap',
			),
			'components' => Array(
				'bootstrap' => Array(
					'class' => 'ext.bootstrap.components.Bootstrap',
					'responsiveCss' => false,
                    'coreCss' => false,
				),
			),
		),
		'panel' => Array(),
		'gii' => Array(
			'class' => 'system.gii.GiiModule',
			'password' => 'lol123',
			//'ipFilters' => array(''),
			'generatorPaths' => Array(
				'bootstrap.gii',
			),
		),
	),
	'components' => Array(
		'bootstrap' => Array(
			'class' => 'ext.bootstrap.components.Bootstrap',
			'responsiveCss' => false,
		),
		'localtime' => Array(
			'class' => 'LocalTime',
			'TimeZone' => 'Europe/Warsaw',
		),
		'user' => Array(
			'class' => 'admin.components.AuthWebUser',
			'loginUrl' => '/user/login',
			'logoutUrl' => '/user/logout',
			'registerUrl' => '/user/register',
			'allowAutoLogin' => true,
			'autoUpdateFlash' => false,
		),
		'authManager' => Array(
			'class' => 'CDbAuthManager',
			'connectionID' => 'db',
			'itemTable' => 'ofi_auth_item',
			'itemChildTable' => 'ofi_auth_item_child',
			'assignmentTable' => 'ofi_auth_assignment',
			'behaviors' => Array(
				'auth' => Array(
					'class' => 'admin.components.AuthBehavior',
				),
			),
			'defaultRoles' => Array('guest', 'user'),
		),
		'urlManager' => array(
			'urlFormat' => 'path',
			'showScriptName' => false,
			'caseSensitive' => false,
			'rules' => $routesArray,
		),
		'db' => $databaseArray,
		'errorHandler' => Array(
			'errorAction' => 'site/error',
		),
		'clientScript' => Array(
			'class' => 'ext.yii-less-extension.components.YiiLessCClientScript',
			'cache' => true, // Optional parameter to enable or disable LESS File Caching
		),
		'log' => Array(
			'class' => 'CLogRouter',
			'routes' => $debugArray,
		),
	),
	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params' => Array(
		'facebook' => Array(
			'appId' => '',
			'secret' => '',
		),
		'adminEmail' => 'noneluke@gmail.com',
		'smtp_mail_auth' => Array(
			/*'host' => '',
			'smtp_auth' => true,
			'user_name' => '',
			'password' => '',
			'send_from_email' => 'MAIL',
			'send_from_name' => 'HOST',*/
			//'smtp_secure' => '',
			'host' => 'smtp.gmail.com',
			'port' => '465',
			'smtp_auth' => true,
			'smtp_secure' => 'ssl',
			'user_name' => 'noneluke@gmail.com',
			'password' => 'jane9494',
			'send_from_email' => 'noneluke@gmail.com',
			'send_from_name' => 'Bunsha',
		)
	),
);