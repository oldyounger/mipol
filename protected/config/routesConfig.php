<?php
return Array(
	'<lang:(pl|en)>/login'																					=> 'user/login',
	'<lang:(pl|en)>/logout'																					=> 'user/logout',

    '<lang:(pl|en)>/'																        	    	    => 'site/index',
    '<lang:(pl|en)>/index'															    		    	    => 'site/index',
    '<lang:(pl|en)>/kontakt'															    		    	    => 'contact/index',
    '<lang:(pl|en)>/aktualnosci'															    		    => 'news/index',
    '<lang:(pl|en)>/aktualnosci/wpis'															    		 => 'news/show',
    '<lang:(pl|en)>/o-nas'															    		 => 'about/index',
    '<lang:(pl|en)>/skup-bydla'															    		 => 'about/cattle',







	//ogolnego przeznaczenia
	'admin/<controller:\w+>/<action:\w+>'																	=> 'admin/<controller>/<action>',
	'gii/<controller:\w+>/<action:\w+>'																		=> 'gii/<controller>/<action>',
	'<controller:\w+>/<action:\w+>'																			=> '<controller>/<action>',
);