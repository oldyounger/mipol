<?php
$databaseArray = require('databaseConfig.php');

return Array(
	'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
	'name' => 'NeraviaCMS - Console',
	'import' => array(
		'application.models.*',
		'application.components.*',
		'application.components.wideimage.WideImage',
		'application.extensions.*',
		'application.extensions.uploadify.components.*',
		'application.widgets.*',
	),
	'components' => Array(
		'localtime' => array(
			'class' => 'LocalTime',
			'TimeZone' => 'Europe/Warsaw',
		),
		'db' => $databaseArray,
	),
);