<div class="calendar-navigation clearfix">
	<h5 class="text-center">
		<button type="button" class="btn btn-mini btn-yellow pull-left" data-calendar-nav="prev">
			Wcześniejszy miesiąc
		</button>
		<button type="button" class="btn btn-mini btn-yellow pull-right" data-calendar-nav="next">
			Następny miesiąc
		</button>

		<span class="current-month" id="<?php echo $elId; ?>-currentDateShow"></span>
	</h5>
</div>
<div id="<?php echo $elId; ?>"></div>