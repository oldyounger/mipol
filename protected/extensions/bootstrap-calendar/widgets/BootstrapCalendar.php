<?php
class BootstrapCalendar extends CWidget
{
	public $dataSourceUrl;
	public $dateStart = false;
	public $defaultView = 'month';

	public static $moduleId = 0;
	protected $path;

	public function init()
	{
		parent::init();
	}

	public function run()
	{
		if($this->dateStart == false){
			$this->dateStart = date('Y-m-d', time(NULL));
		}

		$elId = 'bootstrapCalendar-'.++self::$moduleId;

		$this->registerClientScript();

		$templatePath = Yii::app()->baseUrl . $this->path . '/tmpls/';

		Yii::app()->clientScript->registerScript($elId, "
			var calInst_".self::$moduleId." = $('#" . $elId . "').calendar({
				events_source: '".$this->dataSourceUrl."',
				view: '".$this->defaultView."',
				tmpl_path: '".$templatePath."',
				tmpl_cache: false,
				language: 'pl-PL',
				day: '".$this->dateStart."',
				onAfterViewLoad: function(view) {
					$('#" . $elId . "-currentDateShow').text(this.getTitle());
				},
				views: {
					year: {
						slide_events: 1,
						enable: 1
					},
					month: {
						slide_events: 1,
						enable: 1
					},
					week: {
						enable: 0
					},
					day: {
						enable: 0
					}
				},
				classes: {
					months: {
						general: 'label'
					}
				}
			});

			$('button[data-calendar-nav]').click(function(){
				calInst_".self::$moduleId.".navigate($(this).data('calendar-nav'));
			});");

		$this->render('view', Array(
			'elId' => $elId,
		));
	}

	protected function registerClientScript()
	{
		$file = dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR;
		$this->path = Yii::app()->getAssetManager()->publish($file);

		$cs = Yii::app()->clientScript;

		$cs->registerLessFile($this->path . '/less/calendar.less');
		$cs->registerLessFile($this->path . '/less/events.less');
		$cs->registerLessFile($this->path . '/less/grid.less');
		$cs->registerLessFile($this->path . '/less/month.less');
		$cs->registerLessFile($this->path . '/less/theme.less');
		$cs->registerLessFile($this->path . '/less/variables.less');
		$cs->registerLessFile($this->path . '/less/week.less');
		$cs->registerScriptFile($this->path . '/js/language/pl-PL.js');
		$cs->registerScriptFile($this->path . '/js/calendar.js');
		$cs->registerScriptFile($this->path . '/js/underscore/underscore-min.js');
	}
}