<?php
class EGMapIconBoxMarkerWidget extends CWidget
{
	const LAT_CRACOW = 50.06465;
	const LON_CRACOW = 19.94498;
	const DEFAULT_ZOOM = 13;
	
	private $counter = 0;
	
	public $size = Array('width' => 300, 'height' => 300);
	public $zoom = self::DEFAULT_ZOOM;
	public $coordinates = Array('lat' => self::LAT_CRACOW, 'lon' => self::LON_CRACOW);
	//public $marker = Array('show' => false, 'lat' => self::LAT_CRACOW, 'lon' => self::LON_CRACOW);
	public $markers = Array(); //Array('title', 'lat', 'lon'),
	public $icons = Array('normal' => 'black', 'selected' => 'gold');
	
	public function init()
	{
		//
	}
	
	public function run()
	{
		//initialize default coordinates of Google Map. on edit, we will grab coords from DB :)
		Yii::import('ext.EGMap.*');
		
		$gMap = new EGMap();
		$gMap->zoom = $this->zoom;
		
		$gMap->setWidth($this->size['width']);
		$gMap->setHeight($this->size['height']);
		
		$mapTypeControlOptions = Array(
			'position' => EGMapControlPosition::LEFT_BOTTOM,
			'style' => EGMap::MAPTYPECONTROL_STYLE_DROPDOWN_MENU,
		);
		$gMap->mapTypeControlOptions = $mapTypeControlOptions;
		
		//fixes YiiBooster bug
		$gMap->setHtmlOptions(Array(
			'class' => 'google-maps'
		));
		
		if(isSet($this->coordinates['lat']) && isSet($this->coordinates['lon'])){
			$gMap->setCenter($this->coordinates['lat'], $this->coordinates['lon']);
		}

		if(count($this->markers) > 0){
			foreach($this->markers as $mrk){
				$markerTitle = (isSet($mrk['title']) ? $mrk['title'] : NULL);

				$state = 'normal';
				if(isSet($mrk['selected']) && $mrk['selected'] == true){
					$state = 'selected';
				}

				$icon = new EGMapMarkerImage(Yii::app()->baseUrl.'/themes/common/images/markers/'.$this->icons[$state].'.png');
				$icon->setSize(20, 32);
				$icon->setAnchor(10, 16);
				$icon->setOrigin(0, 0);

				$marker = new EGMapMarker(
					$mrk['lat'],
					$mrk['lon'],
					Array(
						'title' => $markerTitle,
						'icon' => $icon,
					),
					'marker_'.$this->counter++
				);

				$info_box = new EGMapInfoBox('<h5><a href="'.$mrk['box']['url'].'">'.$mrk['box']['name'].'</a></h5><small>'.$mrk['box']['description'].'</small>', 'info_box', Array('boxClass' => "'google-maps-infobox'"));
				$info_box->pixelOffset = new EGMapSize('-20', '19');
				$marker->addHtmlInfoBox($info_box);

				$gMap->addMarker($marker);
			}
		}

		/*
		if(isSet($this->marker['show']) && $this->marker['show'] == true){
			$markerTitle = (isSet($this->marker['title']) ? $this->marker['title'] : NULL);
			
			$marker = new EGMapMarker(
				$this->marker['lat'],
				$this->marker['lon'],
				Array(
					'title' => $markerTitle,
				),
				'marker_'.$this->counter++
			);
			
			$gMap->addMarker($marker);
		}*/
		
		$gMap->renderMap();
	}
}