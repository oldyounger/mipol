<?php
class EGMapDropMarkerWidget extends CWidget
{
	const LAT_CRACOW = 50.06465;
	const LON_CRACOW = 19.94498;
	const DEFAULT_ZOOM = 13;
	
	private static $counter = 0;
	private static $counterInput = 0;
	
	public $size = Array('width' => 300, 'height' => 300);
	public $zoom = self::DEFAULT_ZOOM;
	public $coordinates = Array('lat' => self::LAT_CRACOW, 'lon' => self::LON_CRACOW);
	public $marker = Array('lat' => self::LAT_CRACOW, 'lon' => self::LON_CRACOW);
	public $draggable = true;
	public $saveCoords = false;
	
	public function init()
	{
		$this->coordinates['lat'] === NULL
			? $this->coordinates['lat'] = self::LAT_CRACOW
			: NULL;
		
		$this->coordinates['lon'] === NULL
			? $this->coordinates['lon'] = self::LON_CRACOW
			: NULL;
		
		$this->marker['lat'] === NULL
			? $this->marker['lat'] = self::LAT_CRACOW
			: NULL;
		
		$this->marker['lon'] === NULL
			? $this->marker['lon'] = self::LON_CRACOW
			: NULL;
	}
	
	public function registerCss()
	{
		$path = Yii::getPathOfAlias('ext.EGMap.widgets.assets');
		$path2 = Yii::app()->getAssetManager()->publish($path);
		Yii::app()->clientScript->registerCssFile($path2.'/'.'egmap.css');
	}
	
	public function registerJs()
	{
		$path = Yii::getPathOfAlias('ext.EGMap.widgets.assets');
		$path2 = Yii::app()->getAssetManager()->publish($path);
		Yii::app()->clientScript->registerScriptFile($path2.'/'.'egmap.js');
	}
	
	public function run()
	{
		$this->registerCss();
		$this->registerJs();
		
		//initialize default coordinates of Google Map. on edit, we will grab coords from DB :)
		echo CHtml::openTag('div', Array(
			'class' => 'google-maps-wrapper',
		));
		
		Yii::import('ext.EGMap.*');
		
		$gMap = new EGMap();
		$gMap->zoom = $this->zoom;
		
		$gMap->setWidth($this->size['width']);
		$gMap->setHeight($this->size['height']);
		
		$mapTypeControlOptions = Array(
			'position' => EGMapControlPosition::LEFT_BOTTOM,
			'style' => EGMap::MAPTYPECONTROL_STYLE_DROPDOWN_MENU,
		);
		$gMap->mapTypeControlOptions = $mapTypeControlOptions;
		
		//fixes YiiBooster bug
		$gMap->setHtmlOptions(Array(
			'class' => 'google-maps'
		));
		
		if(isSet($this->coordinates['lat']) && isSet($this->coordinates['lon'])){
			$gMap->setCenter($this->coordinates['lat'], $this->coordinates['lon']);
		}
		
		$markerTitle = (isSet($this->marker['title']) ? $this->marker['title'] : NULL);
		$markerName = 'marker_draggable_'.self::$counter++;
		
		$dragEvent = NULL;
		if($this->saveCoords !== false){
			$dragEvent = new EGMapEvent('dragend',
				"function(event){
					var serializedCoords = event.latLng.lat() + ';' + event.latLng.lng();
					$('".$this->saveCoords."').val(serializedCoords);
					//alert(event.latLng.lat() + ' ' + event.latLng.lng());
				}",
				false,
				EGMapEvent::TYPE_EVENT_DEFAULT
			);
		}

		$marker = new EGMapMarker(
			$this->marker['lat'],
			$this->marker['lon'],
			Array(
				'title' => $markerTitle,
				'draggable' => $this->draggable,
			),
			$markerName,
			Array(
				'dragevent' => $dragEvent,
			)
		);

		$gMap->addMarker($marker);
		
		$gMap->renderMap();
		
		//we need also an input
		echo CHtml::textField('mapFinder_'.self::$counterInput++, '', Array(
			'placeholder' => 'Wpisz adres by wyszukać..',
			'class' => 'onChangeMapSearch',
			'data-maphandler' => $gMap->getJsName(),
			'data-markerhandler' => $markerName,
		));
		
		echo CHtml::closeTag('div');
	}
}