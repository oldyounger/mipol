$(document).ready(function(){
	var geocoder = new google.maps.Geocoder();
	
	$('.onChangeMapSearch').on('change', function(){
		var mapHandle = $(this).data('maphandler');
		var mapMarkerHandle = $(this).data('markerhandler');
		
		geocoder.geocode({
			'address': $(this).val(),
		}, function(results, status){
			if(status == google.maps.GeocoderStatus.OK){
				window[mapHandle].setCenter(results[0].geometry.location);
				
				window[mapMarkerHandle].setPosition(results[0].geometry.location);
				
				//little workaround for event object :)
				var obj = {
					latLng: new google.maps.LatLng(results[0].geometry.location.lat(), results[0].geometry.location.lng()),
				};
		
				google.maps.event.trigger(window[mapMarkerHandle], 'dragend', obj);
				//updatePosition
				
				//window[mapMarkerHandler].
				/*map.setCenter(results[0].geometry.location);
				var marker = new google.maps.Marker({
					map: map,
					position: results[0].geometry.location
				});*/
			}
			else{
				alert("Geocode was not successful for the following reason: " + status);
			}
		});
	});
});