<?php
interface IFileUpload
{
	/**
	 * @param string $name otyginally selected file
	 * @param string $type uploaded file MIME type
	 * @param string $tmpName server side temporary file name
	 * @param int $error File upload error code
	 * @param int $size	File size in bytes
	 * @param array $options An array with options passed from controller to a model
	 *
	 * @return mixed uploaded file unique identifier (for model it could be it's primary key value)
	 */
	public static function uploadFile($name, $type, $tmpName, $error, $size);
}
