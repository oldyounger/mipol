<?php
class CUploadifyAction extends CAction
{
    /**
     * Model Class name which implements IFileUpload interface
     * @var CModel
     */
    public $model;

    public $size = false;

    /**
     * @var array of thumbnail size (defaults to 200x200)
     */
    public $resize = Array('width' => 200, 'height' => 200);

    /**
     * Pushes received file to model
     */
    public function run()
    {
        try {
            if(isSet($_REQUEST['fileObjName'])){
                $fileKey = $_REQUEST['fileObjName'];
            }
            else{
                $fileKey = 'Filedata';
            }

            if(!isSet($_FILES[$fileKey])){
                throw new Exception("File array has no key '{$fileKey}'.");
            }

            $file = $_FILES[$fileKey];
            if($file['error'] !== UPLOAD_ERR_OK){
                throw new Exception('File upload error.');
            }

            if($this->size !== false && $file['size'] > $this->size){
                throw new Exception('File size too big.');
            }

            if(!isSet($this->model)){
                throw new Exception('No model class assigned.');
            }

            $modelClass = $this->model;

            $rc = new ReflectionClass($modelClass);
            if(!$rc->implementsInterface('IFileUpload')){
                throw new Exception("Model '{$modelClass}' must implements IFileUpload infertace");
            }

            $id = $modelClass::uploadFile($file['name'], $file['type'], $file['tmp_name'], $file['error'], $file['size'], true, $_REQUEST);
            if($rc->getName() == 'Image' || ($rc->getParentClass() && $rc->getParentClass()->getName() == 'Image')){ //create thumb if model is image (or child)
                $path = $modelClass::model()->findByPk($id)->getImageUrl(Array(
                    'resize' => Array(
                        'width' => $this->resize['width'],
                        'height' => $this->resize['height'],
                    ),
                ));
            }
            else{
                $path = $modelClass::model()->findByPk($id)->getFileUrl();
            }

            $this->finish(1, '', $id, $path);
        }
        catch(Exception $e){
            $this->finish(0, $e->getMessage());
        }
    }

    /**
     * Finishes script and returns JSON encoded information
     * @param int $status
     * @param string $message
     * @param int $extra
     */
    private function finish($status, $message = '', $extra = '', $path = '')
    {
        echo CJavaScript::jsonEncode(Array(
            'status' => $status,
            'message' => $message,
            'extra' => $extra,
            'path' => $path
        ));

        Yii::app()->end();
    }
}
