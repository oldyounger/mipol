<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class CUploadifyWidget extends CWidget
{
	private $_assetsUrl;
	private $_initialized = false;

	/**
	 * Uploadify options, look at @link http://www.uploadify.com/documentation/
	 */
	private $uploadifyOptions = array(
		'swf'					=> '',						// SWF file path retreived from assets on init()
		'uploader'				=> 'uploadify',				// uploader script url/path which default is controller action
		'auto'					=> true,					// should files be automatically uploaded after adding them to queue
		'buttonClass'			=> '',						// button class
		'buttonCursor'			=> 'hand',					// button cursor
		'buttonImage'			=> null,					// button image if not default
		'buttonText'			=> 'SELECT FILES',			// button text
		'checkExisting'			=> false,					// path to script which would check if uploaded file already exists and return 1 if file exists or 0 if it is not
		'debug'					=> false,					// turn debugging on/off
		'fileObjName'			=> 'Filedata',				// the key name in $_FILES array - $_FILES['Filedata']
		'fileSizeLimit'			=> 0,						// maximum uploaded file size, 0 means no limit
		'fileTypeDesc'			=> 'All Files',				// description of selectable files
		'fileTypeExts'			=> '*.*',					// `;` separated file extensions (i.e. '*.jpg; *.png; *.gif')
		'formData'				=> null,					// additional data to send with each uploaded file
		'height'				=> 30,						// widget's height
		'width'					=> 120,						// widget's width
		'itemTemplate'			=> false,					// template for each file in queue
		'method'				=> 'post',					// file upload method
		'multi'					=> true,					// uploading multiple files at once
		'overrideEvents'		=> array(),					// override events list
		'preventCaching'		=> true,					// if true an random string will be added to the SWF url so it does not cache
		'progressData'			=> 'percentage',			// percentage OR speed
		'queueID'				=> false,					// queue object's ID or false for auto generate
		'queueSizeLimit'		=> 999,						// maximum elements in queue
		'removeCompleted'		=> true,					// should completed uploads be removed from queue
		'removeTimeout'			=> 3,						// time after completed queue items will be removed
		'requeueErrors'			=> false,					// queue items that are generating errors will be enqueued again if true
		'successTimeout'		=> 30,						// time after which SWF will decide that upload has finished with success if there is no response from server
		'uploadLimit'			=> 999,						// maximum number of files you can upload
	);

	/**
	 * Uploadify callback functions are not included in above $uploadifyOptions
	 * array because they are included as CJavaScriptExpression before JSON encode.
	 * For more info look at @link http://www.uploadify.com/documentation/
	 */
	private $uploadifyCallbacks = array(
		'onCancel'				=> null,
		'onClearQueue'			=> null,
		'onDestroy'				=> null,
		'onDialogClose'			=> null,
		'onDialogOpen'			=> null,
		'onDisable'				=> null,
		'onEnable'				=> null,
		'onFallback'			=> null,
		'onInit'				=> null,
		'onQueueComplete'		=> null,
		'onSelect'				=> null,
		'onSelectError'			=> null,
		'onSWFReady'			=> null,
		'onUploadComplete'		=> null,
		'onUploadError'			=> null,
		'onUploadProgress'		=> null,
		'onUploadStart'			=> null,
		'onUploadSuccess'		=> null,
	);

	/**
	 * Input file field name
	 * @var type
	 */
	public $name;

	/**
	 * Input file field ID
	 * @var type
	 */
	public $id;


	/**
	 * A JS function name which will show our alerts
	 * @var type
	 */
	public $alertFunction = 'alert';


	public $onCancel = null;


	/**
	 *
	 */
	public function init()
	{
		if(!$this->_initialized)
		{
			Yii::app()->clientScript->registerCoreScript('jquery');
			Yii::app()->clientScript->registerCssFile($this->getAssetsUrl().'/css/uploadify.css');
			Yii::app()->clientScript->registerScriptFile($this->getAssetsUrl().'/js/'.( YII_DEBUG ? 'jquery.uploadify.js' : 'jquery.uploadify.min.js' ));

			$this->uploadifyOptions['swf']		= $this->getAssetsUrl().'/'.'flash'.'/'.'uploadify.swf';

            $urlToCreate = 'uploadify';
            if($this->uploadifyOptions['uploader']){
                $urlToCreate = $this->uploadifyOptions['uploader'];
            }
			
			$this->uploadifyOptions['uploader'] = Yii::app()->getController()->createUrl($urlToCreate);

			$this->_initialized = true;
		}
		
		parent::init();
	}

	/**
	 *
	 * @throws CException
	 */
	public function run()
	{
		if(!isset($this->name))
			throw new CException('UploadifyWidget: Widget\'s input `name` must be specified');

		if(!isset($this->id))
			$this->id = $this->name.'_uploadifyID';

		echo "<input type=\"file\" name=\"{$this->name}\" id=\"{$this->id}\" />";

		//$script = "jQuery('#{$this->id}').uploadify({$this->getUploadifyOptions()});";
		$script = "setTimeout(function(){ jQuery('#{$this->id}').uploadify({$this->getUploadifyOptions()}); }, 0);";

		Yii::app()->clientScript->registerScript($this->id, $script, CClientScript::POS_READY);

		parent::run();
	}

	/**
	 * This inserts JS function that will be called each time the file was
	 * successfully uploaded
	 */
	private function registerOnUploadSuccess()
	{
		if(!isset($this->uploadifyOptions['onUploadSuccess']))
		{
			$script = "function(file, data, response) {
				var dataObject = jQuery.parseJSON(data);
				if(dataObject.status === 0) { {$this->alertFunction}(dataObject.message); }
				else if(dataObject.status === 1) { jQuery('#{$this->id}').after( '<input type=\"hidden\" name=\"{$this->name}[]\" value=\"'+dataObject.extra+'\" />' ); }
			}";
			$this->uploadifyOptions['onUploadSuccess'] = new CJavaScriptExpression($script);
		}
	}

	/**
	 * Get JSON encoded Uploadify options
	 * @return string
	 */
	private function getUploadifyOptions()
	{
		if(!is_array($this->uploadifyOptions['formData'])){
			$this->uploadifyOptions['formData'] = Array( 'fileObjName' => $this->uploadifyOptions['fileObjName'] );
		}
		else if(!array_key_exists('fileObjName', $this->uploadifyOptions['formData'])){
			$this->uploadifyOptions['formData']['fileObjName'] = $this->uploadifyOptions['fileObjName'];
		}

		$this->uploadifyOptions['formData']['php-sessid'] = session_id();

		foreach($this->uploadifyCallbacks as $callbackName => $callback)
		{
			if($callback !== null)
				$this->uploadifyOptions[$callbackName] = $callback;
		}

		$this->registerOnUploadSuccess();

		return CJavaScript::encode($this->uploadifyOptions);
	}

	/**
	 * Setter
	 * @param string $name
	 * @param mixed $value
	 */
	public function __set($name, $value)
	{
		if(array_key_exists($name, $this->uploadifyOptions))
			$this->uploadifyOptions[$name] = $value;
		elseif(array_key_exists($name, $this->uploadifyCallbacks))
			$this->uploadifyOptions[$name] = new CJavaScriptExpression($value);
		else
			parent::__set($name, $value);

	}

	/**
	 * Getter
	 * @param string $name
	 * @return mixed
	 */
	public function __get($name)
	{
		if(array_key_exists($name, $this->uploadifyOptions))
			return $this->uploadifyOptions[$name];
		elseif(array_key_exists($name, $this->uploadifyCallbacks))
		{
			if($this->uploadifyOptions[$name] instanceof CJavaScriptExpression)
				return $this->uploadifyOptions[$name]->code;
			else
				return $this->uploadifyOptions[$name];
		}
		else
			return parent::__get($name);
	}

	/**
	* Returns the URL to the published assets folder.
	* @return string the URL
	*/
	protected function getAssetsUrl()
	{
		if (!isset($this->_assetsUrl))
			$this->_assetsUrl = Yii::app()->assetManager->publish(Yii::getPathOfAlias('ext.uploadify.assets'), false, -1, true);
		return $this->_assetsUrl;
	}
}
