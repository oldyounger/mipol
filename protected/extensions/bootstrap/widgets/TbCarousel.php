<?php
/**
 *## TbCarousel class file.
 *
 * @author Christoffer Niska <ChristofferNiska@gmail.com>
 * @copyright Copyright &copy; Christoffer Niska 2011-
 * @license [New BSD License](http://www.opensource.org/licenses/bsd-license.php) 
 * @since 0.9.10
 */

/**
 *## Bootstrap carousel widget.
 *
 * @see <http://twitter.github.com/bootstrap/javascript.html#carousel>
 *
 * @package booster.widgets.grouping
 */
class TbCarousel extends CWidget
{
	/**
	 * @var string the previous button label. Defaults to '&lsaquo;'.
	 */
	public $prevLabel = '&lsaquo;';

	/**
	 * @var string the next button label. Defaults to '&rsaquo;'.
	 */
	public $nextLabel = '&rsaquo;';

	/**
	 * @var boolean indicates whether the carousel should slide items.
	 */
	public $slide = true;

	/**
	 * @var boolean indicates whether to display the previous and next links.
	 */
	public $displayPrevAndNext = true;
	
	/**
	 * @var boolean should header and description in caption be separated by clearing div
	 */
	public $clearCaptionLines = false;

	/**
	 * @var array the carousel items configuration.
	 */
	public $items = array();

	/**
	 * @var array the options for the Bootstrap Javascript plugin.
	 */
	public $options = array();

	/**
	 * @var string[] the Javascript event handlers.
	 */
	public $events = array();

	/**
	 * @var array the HTML attributes for the widget container.
	 */
	public $htmlOptions = array();
	
	/** @var CClientScript $_cs */
	private $_cs = NULL;

	/**
	 *### .init()
	 *
	 * Initializes the widget.
	 */
	public function init()
	{
		if (!isset($this->htmlOptions['id'])) {
			$this->htmlOptions['id'] = $this->getId();
		}

		$classes = array('carousel');

		if ($this->slide === true) {
			$classes[] = 'slide';
		}

		if (!empty($classes)) {
			$classes = implode(' ', $classes);
			if (isset($this->htmlOptions['class'])) {
				$this->htmlOptions['class'] .= ' ' . $classes;
			} else {
				$this->htmlOptions['class'] = $classes;
			}
		}
	}

	/**
	 *### .run()
	 *
	 * Runs the widget.
	 */
	public function run()
	{
		$this->_cs = Yii::app()->getClientScript();
		
		$id = $this->htmlOptions['id'];

		echo CHtml::openTag('div', $this->htmlOptions);
		echo '<div class="carousel-inner">';
		$this->renderItems($this->items);
		echo '</div>';

		if ($this->displayPrevAndNext) {
			echo '<a class="carousel-control left" href="#' . $id . '" data-slide="prev">' . $this->prevLabel . '</a>';
			echo '<a class="carousel-control right" href="#' . $id . '" data-slide="next">' . $this->nextLabel . '</a>';
		}

		echo '</div>';
		
		$options = !empty($this->options) ? CJavaScript::encode($this->options) : '';
		$this->_cs->registerScript(__CLASS__ . '#' . $id, "jQuery('#{$id}').carousel({$options});");

		foreach ($this->events as $name => $handler) {
			$handler = CJavaScript::encode($handler);
			$this->_cs->registerScript(__CLASS__ . '#' . $id . '_' . $name, "jQuery('#{$id}').on('{$name}', {$handler});");
		}
	}

	/**
	 *### .renderItems()
	 *
	 * Renders the carousel items.
	 *
	 * @param array $items the item configuration.
	 */
	protected function renderItems($items)
	{
		foreach ($items as $i => $item) {
			if (!is_array($item)) {
				continue;
			}

			if (isset($item['visible']) && $item['visible'] === false) {
				continue;
			}

			if (!isset($item['itemOptions'])) {
				$item['itemOptions'] = array();
			}

			$classes = array('item');

			if ($i === 0) {
				$classes[] = 'active';
			}

			if (!empty($classes)) {
				$classes = implode(' ', $classes);
				if (isset($item['itemOptions']['class'])) {
					$item['itemOptions']['class'] .= ' ' . $classes;
				} else {
					$item['itemOptions']['class'] = $classes;
				}
			}

			echo CHtml::openTag('div', $item['itemOptions']);

			if (isset($item['image'])) {
				if (!isset($item['alt'])) {
					$item['alt'] = '';
				}

				if (!isset($item['imageOptions'])) {
					$item['imageOptions'] = array();
				}

				echo CHtml::image($item['image'], $item['alt'], $item['imageOptions']);
			}
			else if(isSet($item['content'])){
				echo CHTml::openTag('div', Array(
					'class' => 'slide-content-wrapper',
				));
				
				echo $item['content'];
				
				echo CHtml::closeTag('div');
			}

			if (isset($item['label']) || isset($item['caption'])) {
				if (!isset($item['captionOptions'])) {
					$item['captionOptions'] = array();
				}

				if (isset($item['captionOptions']['class'])) {
					$item['captionOptions']['class'] .= ' carousel-caption';
				} else {
					$item['captionOptions']['class'] = 'carousel-caption';
				}

				echo CHtml::openTag('div', $item['captionOptions']);

				if (isset($item['label']) && mb_strlen($item['label']) > 0) {
					echo '<h4>' . $item['label'] . '</h4>';
				}
				
				if($this->clearCaptionLines){
					echo '<div class="clear"></div>';
				}

				if (isset($item['caption']) && mb_strlen($item['caption']) > 0) {
					echo '<p>' . $item['caption'] . '</p>';
				}
				
				if (isset($item['button'])) {
					echo '<a href="'.$item['button']['link'].'" class="btn btn-yellow">' . $item['button']['caption'] . '</a>';
				}

				echo '</div>';
			}
			
			if(isSet($item['thumbs'])){
				$toShow = NULL;
				$extraArray = NULL;
				if(isSet($item['thumbs']['next'])){
					$count = max(0, (int)$item['thumbs']['next']);
					$toShow = array_slice($items, ($i + 1), $count, true);
//                    var_dump($item['thumbs']['allowFromStart']); die();
					if(count($toShow) < $count && isSet($item['thumbs']['allowFromStart']) && $item['thumbs']['allowFromStart'] == true){
						//echo 'x';die;
						//must check also index of current item (we can't include it and next items too..)
						$itemsToGet = min(max(0, $count - count($toShow)), $i+1);
						if($itemsToGet > 0){
							$extraArray = array_slice($items, 0, $itemsToGet);
						}


						/*if($itemsIndexStop > 0 && $itemsIndexStop < $i){
							$extraArray = array_slice($items, 0, $itemsIndexStop+1, true);
						}
						else if($itemsIndexStop > 0 && $itemsIndexStop > $i){
							$itemsIndexStop = $i - 1;
							$extraArray = array_slice($items, 0, $itemsIndexStop+1, true);
						}*/
					}
					//var_dump($count);
				}
				else if(isSet($item['thumbs']['prev'])){
					$count = max(0, (int)$item['thumbs']['prev']);
					$toShow = array_slice($items, ($i - 1), -$count, true);
					if(count($toShow) < $count && isSet($item['thumbs']['allowFromEnd']) && $item['thumbs']['allowFromEnd'] == true){
						//must check also index of current item (we can't include it and next items too..)
						$itemsIndexStop = count($items) - $count + count($toShow) - 1; // -1 as index starts from 0
						if($itemsIndexStop < $i){
							$extraArray = array_slice($items, -$itemsIndexStop+1, NULL, true);
						}
					}
				}
				
				if((!is_array($toShow) || count($toShow) == 0) && is_array($extraArray)){ //got extra but in normal we got nothing
					$toShow = $extraArray;
					$extraArray = NULL;
				}

				if(is_array($toShow) && count($toShow) > 0){
					echo CHtml::openTag('div', Array('class' => 'thumbContainer'));

					echo '<ul>';
					foreach($toShow as $w => $ts){
						if(isSet($ts['thumb'])){
							echo '<li>';

							echo CHtml::image($ts['thumb'], '', Array('data-number' => $w));

							/*if(isSet($ts['thumb']['events']) && is_array($ts['thumb']['events'])){
								foreach ($ts['thumb']['events'] as $name => $handler) {
									$handler = CJavaScript::encode($handler);
									$this->_cs->registerScript(__CLASS__ . '#' . $ts['thumb']['id'] . '_' . $name, "jQuery('#{$ts['thumb']['id']}').on('{$name}', {$handler});");
								}
							}*/

							echo '</li>';
						}
					}
					
					if(is_array($extraArray)){
						foreach($extraArray as $w => $ts){
							if(isSet($ts['thumb'])){
								echo '<li>';

								echo CHtml::image($ts['thumb'], '', Array('data-number' => $w));

								/*if(isSet($ts['thumb']['events']) && is_array($ts['thumb']['events'])){
									foreach ($ts['thumb']['events'] as $name => $handler) {
										$handler = CJavaScript::encode($handler);
										$this->_cs->registerScript(__CLASS__ . '#' . $ts['thumb']['id'] . '_' . $name, "jQuery('#{$ts['thumb']['id']}').on('{$name}', {$handler});");
									}
								}*/

								echo '</li>';
							}
						}
					}
					
					echo '</ul>';

					echo '</div>';
				}
			}
			
			echo '</div>';
		}
	}
}
