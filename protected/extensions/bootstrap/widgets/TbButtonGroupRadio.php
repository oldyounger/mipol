<?php
/**
 *##  TbButtonGroup class file.
 *
 * @author Christoffer Niska <ChristofferNiska@gmail.com>
 * @copyright Copyright &copy; Christoffer Niska 2011-
 * @license [New BSD License](http://www.opensource.org/licenses/bsd-license.php)
 * @since 0.9.10
 */

Yii::import('bootstrap.widgets.TbButton');

/**
 *## Bootstrap button group widget.
 *
 * @see <http://twitter.github.com/bootstrap/components.html#buttonGroups>
 *
 * @package booster.widgets.forms.buttons
 */
class TbButtonGroupRadio extends CWidget
{
	// Toggle options.
	const TOGGLE_CHECKBOX = 'checkbox';
	const TOGGLE_RADIO = 'radio';

	/**
	 * @var string the button callback type.
	 * @see BootButton::buttonType
	 */
	public $buttonType = TbButton::BUTTON_LINK;

	/**
	 * @var string the button type.
	 * @see BootButton::type
	 */
	public $type;

	/**
	 * @var string the button size.
	 * @see BootButton::size
	 */
	public $size;

	/**
	 * @var boolean indicates whether to encode the button labels.
	 */
	public $encodeLabel = true;

	/**
	 * @var array the HTML attributes for the widget container.
	 */
	public $htmlOptions = array();

	/**
	 * @var array the button configuration.
	 */
	public $buttons = array();

	/**
	 * @var boolean indicates whether to enable button toggling.
	 */
	public $toggle;


	/**
	 * @var string group input name
	 */
	public $name;

	/**
	 * @var mixed initial value of RADIO group
	 */
	public $value;

	/**
	 * @var mixed initial values of CHECKBOX group
	 */
	public $values = array();

	/**
	 * @var boolean indicates whether the button group appears vertically stacked. Defaults to 'false'.
	 */
	public $stacked = false;

	/**
	 * @var boolean indicates whether dropdowns should be dropups instead. Defaults to 'false'.
	 */
	public $dropup = false;
	/**
	 * @var boolean indicates whether button is disabled or not. Defaults to 'false'.
	 */
	public $disabled = false;


	private function registerScript() {
		Yii::app()->clientScript->registerScript(
			'button-toggle-group',
			"$(document).on('click', '.button-toggle-group', function(){
				$(this).children('input').prop('checked', !$(this).hasClass('active')).trigger('change');
			});",
			CClientScript::POS_READY);
	}


	/**
	 *### .init()
	 *
	 * Initializes the widget.
	 */
	public function init()
	{
		$classes = array('btn-group');

		if ($this->stacked === true) {
			$classes[] = 'btn-group-vertical';
		}

		if ($this->dropup === true) {
			$classes[] = 'dropup';
		}

		if (!empty($classes)) {
			$classes = implode(' ', $classes);
			if (isset($this->htmlOptions['class'])) {
				$this->htmlOptions['class'] .= ' ' . $classes;
			} else {
				$this->htmlOptions['class'] = $classes;
			}
		}

		$validToggles = array(self::TOGGLE_CHECKBOX, self::TOGGLE_RADIO);

		if (isset($this->toggle) && in_array($this->toggle, $validToggles)) {
			$this->htmlOptions['data-toggle'] = 'buttons-' . $this->toggle;
		}
	}

	/**
	 *### .run()
	 *
	 * Runs the widget.
	 */
	public function run()
	{
		echo CHtml::openTag('div', $this->htmlOptions);

		$hasToggle = false;
		$validToggles = array(self::TOGGLE_CHECKBOX, self::TOGGLE_RADIO);

		if(isset($this->toggle) && in_array($this->toggle, $validToggles)) {
			$this->registerScript();
			$hasToggle = true;
			$this->encodeLabel = false;
		}


		foreach ($this->buttons as $button) {
			if (isset($button['visible']) && $button['visible'] === false) {
				continue;
			}

			if ($hasToggle) {
				if (isset($button['htmlOptions'])) {
					$button['htmlOptions']['data-value'] = $button['value'];
				} else {
					$button['htmlOptions'] = array('data-value' => $button['value']);
				}

				if(isset($button['htmlOptions']['class'])) {
					$button['htmlOptions']['class'] .= ' button-toggle-group';
				} else {
					$button['htmlOptions']['class'] = ' button-toggle-group';
				}

				if($this->toggle == self::TOGGLE_RADIO) {
					$button['active'] = $this->value == $button['value'];
					$button['label'] = (isset($button['label']) ? $button['label'] : '').
					CHtml::radioButton($this->name, $button['active'], array('value' => $button['value'], 'style' => 'display:none;', 'id' => '__radio_button_'.$button['value'].'_'.rand(10000,99999)));
				} else {
					$button['active'] = in_array($button['value'], $this->values);
					$button['label'] = (isset($button['label']) ? $button['label'] : '').
					CHtml::checkBox($this->name, $button['active'], array('value' => $button['value'], 'style' => 'display:none;', 'id' => '__checkbox_button_'.$button['value'].'_'.rand(10000,99999)));
				}
			}

			$this->controller->widget(
				'bootstrap.widgets.TbButton',
				array(
					'buttonType' => isset($button['buttonType']) ? $button['buttonType'] : $this->buttonType,
					'type' => isset($button['type']) ? $button['type'] : $this->type,
					'size' => $this->size, // all buttons in a group cannot vary in size
					'icon' => isset($button['icon']) ? $button['icon'] : null,
					'label' => isset($button['label']) ? $button['label'] : null,
					'url' => isset($button['url']) ? $button['url'] : null,
					'active' => isset($button['active']) ? $button['active'] : false,
					'disabled' => isset($button['disabled']) ? $button['disabled'] : false,
					'items' => isset($button['items']) ? $button['items'] : array(),
					'ajaxOptions' => isset($button['ajaxOptions']) ? $button['ajaxOptions'] : array(),
					'htmlOptions' => isset($button['htmlOptions']) ? $button['htmlOptions'] : array(),
					'dropdownOptions' => isset($button['dropdownOptions']) ? $button['dropdownOptions'] : array(),
					'encodeLabel' => isset($button['encodeLabel']) ? $button['encodeLabel'] : $this->encodeLabel,
				)
			);
		}
		echo '</div>';
	}
}
