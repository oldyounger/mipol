<?php
class ReporterAction extends CAction
{
    public $model;
    public $bindGetParams = array();
    public $columns = array();

    public $data = NULL;
    public $urlParams = Array();
    public $fileName = NULL;

    public function run()
    {
        set_time_limit(60*60); //for big reports

        $reportDirectory = DataPaths::get('report');
        if($file = Yii::app()->request->getParam('file')){
            if(file_exists($reportDirectory . $file)){
                $ext = substr($file, strrpos($file, '.'));
                header('Content-Type: application/octet-stream');
                header("Content-Transfer-Encoding: Binary");
                header("Content-disposition: attachment; filename=\"report_" . ($this->fileName ? $this->fileName : $this->model) . $ext . "\"");
                readfile($reportDirectory . $file);
                Yii::app()->end();
            }
        }

        $modelClass = '';
        if(is_array($this->model)){
            $modelClass = $this->model['class'];
        }
        else{
            $modelClass = $this->model;
        }


        /* @var $mm CActiveRecord */
        $mm = new $modelClass('search');
        $mm->unsetAttributes();
        if(isset($_POST[$modelClass])){
            $mm->attributes = $_POST[$modelClass];
        }

        foreach($this->bindGetParams as $key => $param){
            $mm->$param = Yii::app()->request->getParam($key, '');
        }

        if(is_array($this->model)){
            if(isset($this->model['params'])){
                foreach($this->model['params'] as $param => $value){
                    $mm->$param = $value;
                }
            }
        }

        /* @var $dataProvider CActiveDataProvider */
        $dataProvider = NULL;
        if($this->data !== NULL){
            $dataProvider = new CArrayDataProvider($this->data);
        }
        else if(is_array($this->model) && isset($this->model['searchMethod'])){
            $shF = $this->model['searchMethod'];
            $dataProvider = $mm->$shF();
        }
        else{
            $dataProvider = $mm->search();
        }
        $iterator = new CDataProviderIterator($dataProvider);


// Turn off our amazing library autoload 
        $phpExcelPath = Yii::getPathOfAlias('application.vendors.PHPExcel.PHPExcel.Classes');
        spl_autoload_unregister(array('YiiBase', 'autoload'));

// procedure (PHPExcel_Autoloader::Register();)
        include(Yii::getPathOfAlias('application.vendors.PHPExcel') . DIRECTORY_SEPARATOR . 'PHPExcel.php');

        //Ustawienie cachowania
        ini_set("memory_limit", "256M");

        $cacheSettings = array();
        PHPExcel_Settings::setCacheStorageMethod(PHPExcel_CachedObjectStorageFactory::cache_in_memory_gzip, $cacheSettings);

// Create new PHPExcel object
        $objPHPExcel = new PHPExcel();

// Set properties
        $objPHPExcel->getProperties()->setCreator("NN")
            ->setLastModifiedBy("NN")
            ->setTitle("Report Document")
            ->setSubject("Report Document")
            ->setDescription("Report Document")
            ->setKeywords("Report Document")
            ->setCategory("Report Document");

        spl_autoload_register(array('YiiBase', 'autoload'));
        $row = 1;
        $c = 0;
        foreach($this->columns as $key => $value){
            if(is_int($key)){
                break;
            }
            $name = $mm->getAttributeLabel($key);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($c, 1, $name);
            $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($c)->setAutoSize(true);
            $c++;
            $row = 2;
        }

        if($row == 2){
            $style = $objPHPExcel->getActiveSheet()->getStyle('A1:' . $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($c - 1, 1)->getColumn() . '1');
            $style->applyFromArray(
                array('fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'eeeeee')
                ),
                    'font' => array(
                        'bold' => true
                    )
                ));
        }

        foreach($iterator as $data){
            $c = 0;
            foreach($this->columns as $one){
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($c, $row, @eval('return ' . $one . ';'));
                $c++;
            }
            $row++;
        }
        spl_autoload_unregister(array('YiiBase', 'autoload'));

// Rename sheet
        $objPHPExcel->getActiveSheet()->setTitle('Raport');
        $objPHPExcel->getActiveSheet()->calculateColumnWidths();
        $objPHPExcel->getActiveSheet()->getPageSetup()->setFitToWidth();
        $objPHPExcel->getActiveSheet()->freezePaneByColumnAndRow(0, 2);

// Set active sheet index to the first sheet, 
// so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);

        $status = 'OK';
        $message = '';
        $url = '';

        $additionalModule = ($this->controller->module ? $this->controller->module->id : NULL);

        $type = strtolower(Yii::app()->request->getParam('exportFile', 'excel'));
        if($type == 'pdf'){
            try{

                //$rendererName = PHPExcel_Settings::PDF_RENDERER_TCPDF;
                $rendererName = PHPExcel_Settings::PDF_RENDERER_MPDF;
                //$rendererLibrary = 'tcpdf';
                $rendererLibrary = 'MPDF56';
                $rendererLibraryPath = Yii::getPathOfAlias('application.vendors') . '/' . $rendererLibrary;

                if(!PHPExcel_Settings::setPdfRenderer(
                    $rendererName, $rendererLibraryPath
                )
                ){
                    die(
                        'Please set the $rendererName and $rendererLibraryPath values' .
                        PHP_EOL .
                        ' as appropriate for your directory structure'
                    );
                }

                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'PDF');

                $fileName = md5(time() . rand(1000, 99999)) . '.pdf';
                $path = $reportDirectory . $fileName;
//$objWriter->save('php://output');
                $objWriter->save($path);
            } catch(Exception $e){
                $message = $e->getMessage();
            }
            //$url = '/data/report/' . $fileName;
            $url = Yii::app()->createUrl($additionalModule . '/' . $this->controller->id . '/' . $this->controller->action->id, array_merge(Array('file' => $fileName), $this->urlParams));
            //$url = Yii::app()->createUrl('/' . Yii::app()->controller->id . '/' . Yii::app()->controller->action->id ,array('file' => $fileName));
        }
        else{
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

            $fileName = md5(time() . rand(1000, 99999)) . '.xls';
            $path = DataPaths::get('report') . $fileName;
            $objWriter->save($path);
            $url = Yii::app()->createUrl($additionalModule . '/' . $this->controller->id . '/' . $this->controller->action->id, array_merge(Array('file' => $fileName), $this->urlParams));
        }

        //$message = memory_get_peak_usage(true);

        echo '{
			"status":"' . $status . '",
			"url":"' . $url . '",
			"message":"' . $message . '"
			}';


        // Once we have finished using the library, give back the
        // power to Yii...
        spl_autoload_register(array('YiiBase', 'autoload'));
        Yii::app()->end();
    }
}