<?php

class ReportExporter extends CWidget {

    public $name = 'reportExporter';
    public $gridID = '';
    public $action = 'report';
    public $htmlOptions = array();
    public $params = array();

    public $id = '';

    public function init() {
        parent::init();
        $this->id = Chtml::getIdByName($this->name);
    }

    public function registerCss() {
        $path = Yii::getPathOfAlias('application.extensions.ReportExport.assets');
        $path2 = Yii::app()->getAssetManager()->publish($path);
        Yii::app()->clientScript->registerCssFile($path2.'/'.'report-exporter.css');
    }

    public function run() {
        $this->registerCss();
        $controller = $this->controller->id;
        $module = ($this->controller->module ? $this->controller->module->id : NULL);


        $elId = CHtml::getIdByName($this->name);
        echo Chtml::openTag('div',array('class' => 'report-exporter'));
        echo CHtml::tag('div', array('class' => 'txt'), Yii::t('main', 'Export data'));
        /*echo $this->widget('bootstrap.widgets.TbButton',array(
            'label' => Yii::t('main','Generate'),
            'loadingText'=>Yii::t('main','Generating').' <i class="myLoader"></i>',
            'id' => $elId . '_generate'
        ),true);*/

        echo $this->widget('bootstrap.widgets.TbButton',array(
            'label' => Yii::t('main','Generate'),
            'id' => $elId . '_generate'
        ),true);


        /*echo $this->widget('bootstrap.widgets.TGroupRadio', array(
            'toggle' => 'radio',
            'name' => 'exportFile',
            'id' => $this->id,
            'value' => 'excel',
            'elements' => array(
                array('label' => 'PDF', 'value' => 'pdf', 'icon' => 'pdf my-icon'),
                array('label' => 'XLS', 'value' => 'excel', 'icon' => 'excel my-icon')
            )), true
        );*/

        /*echo $this->widget('bootstrap.widgets.TbButtonGroupRadio', Array(
            'name' => 'exportFile',
            'id' => $this->id,
            'toggle' => 'true',
            'buttons' => Array(
                Array('label' => 'XLS', 'value' => 'excel', 'icon' => 'excel my-icon'),
                Array('label' => 'PDF', 'value' => 'pdf', 'icon' => 'pdf my-icon'),
            ),
        ), true);*/


        echo '<div class="clearfix"></div></div>';


        $additionalParams = Array('a' => '1'); //any param to make "?" in URL
        $additionalParams = array_merge($additionalParams, $this->params);

        $actions = $this->controller->actions();
        if (isset($actions[$this->action]) && isset($actions[$this->action]['bindGetParams'])) {
            foreach ($actions[$this->action]['bindGetParams'] as $key => $param)
                $additionalParams[$key] = Yii::app()->request->getParam($key, '');
        }

        $urlPath = $controller . '/' . $this->action;
        if(mb_strlen($module) > 0){
            $urlPath = $module . '/' . $urlPath;
        }

        Yii::app()->clientScript->registerScript($elId, "
			$('#" . $elId . "_generate').click(function(){
				var btn = $(this);
				//btn.button('loading');
				var sort = '';
				var s = " . ($this->gridID != NULL ? "$('#" . $this->gridID . " .keys').prop('title')" : "null") . ";
				if(s) {
					var m = s.match(/\?(.*)$/);
					if(m && m[1]) sort=m[1];
				}

				" . ($this->gridID != NULL ? "
				var dt = $('#" . $this->id . " input').serialize() + '&' + $('#" . $this->gridID . " .filters').find('input,select').serialize() + '&' + $('#" . $this->gridID . " .data input').serialize();
				" : "
				var dt = $('#" . $this->id . " input').serialize();
				" ) . "

				$.ajax({
					data: dt,
					type:'POST',
					'dataType':'json',
					'url':'" . Yii::app()->createUrl($urlPath, $additionalParams) . "'+'&'+sort,
					success: function(data){
						//btn.button('reset');
						if(data.status == 'OK') {
							window.location = data.url;
						}
						else {
							alert(data.message);
						}
					},
					error:function(){
						//btn.button('reset');
						alert('Error occured while generate report');
					}
				});
			});
			");
    }

}