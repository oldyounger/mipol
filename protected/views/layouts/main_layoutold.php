<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title><?php echo Yii::app()->name; ?><?php echo (mb_strlen($this->pageTitle) > 0 ? ' - '.CHtml::encode($this->pageTitle) : NULL); ?></title>

    <meta name="description" content="<?php echo CHtml::encode($this->pageDescription); ?>">
    <meta name="keywords" content="<?php echo CHtml::encode($this->pageKeywords); ?>">

    <meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=1" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic&subset=latin,latin-ext' rel='stylesheet' type='text/css'>




    <?php Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl.'/css/bootstrap.min.css'); ?>
    <?php Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl.'/css/jquery.fullPage.css'); ?>
    <?php Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl.'/css/font-awesome.min.css'); ?>
    <?php Yii::app()->clientScript->registerLessFile(Yii::app()->theme->baseUrl.'/css/style.css'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/jquery.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/bootstrap.min.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/jquery.easing.min.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl .'/js/jquery.slimscroll.min.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl .'/js/jquery.fullPage.min.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl .'/js/examples.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl .'/js/spin.js'); ?>






</head>

<body>


<!--<!--MENU-->
<header id="main">
    <div class="col-md-12">
        <div class="col-md-3">
            <div class="logo">
                <a href="/index.php"><img src="/themes/white/imgs/columbus.png" alt=""/></a>
            </div>
        </div>
        <div class="col-md-offset-8 col-md-1">
            <button class="show_menu">MENU<img src="/themes/white/imgs/hamburger.png" alt=""/></button>
        </div>
        <div class="menu">
            <div class="content_menu">
                <button class="exit"><img src="/themes/white/imgs/exit.png" alt=""/></button>
                <ul class="menu_items">
                    <li><a href="/">Strona Głowna</a></li>
                    <li><a href="<?php echo $this->createUrl('about/index'); ?>">O Nas</a></li>
                    <li><a href="<?php echo $this->createUrl('offer/index'); ?>">Oferta</a></li>
                    <li><a href="<?php echo $this->createUrl('news/index'); ?>">Aktualności</a></li>
                    <li><a href="<?php echo $this->createUrl('relationinvestor/index'); ?>">Relacje Inwestorskie</a></li>
                    <li><a href="<?php echo $this->createUrl('contact/index'); ?>">Kontakt</a></li>
                </ul>
                <ul class="social_items">
                    <li><a href="<?php echo Configuration::getValue('facebook_url'); ?>"><img src="/themes/white/imgs/facebook.png" alt=""/></a></li>
                    <li><a href="<?php echo Configuration::getValue('linkedin_url'); ?>"><img src="/themes/white/imgs/linkin.png" alt=""/></a></li>
                    <li><a href="<?php echo Configuration::getValue('twitter_url'); ?>"><img src="/themes/white/imgs/twitter.png" alt=""/></a></li>
                </ul>
            </div>
        </div>
        <div class="overlay-menu"></div>
    </div>
</header>
<!--END MENU-->
<div id="fullpage">


    <?php echo $content; ?>




</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>-->
<!-- Include all compiled plugins (below), or include individual files as needed -->


<script>
    //        MENU SCRIPT
    $(document).ready(function() {
        var FeelItem = 'NULL';
        $('button.show_menu').on('click', function () {
            if(FeelItem == 'NULL')
            {
                $('div.overlay-menu').addClass('show_overlay');
                $('.menu').addClass('show_block');
                FeelItem = '1';
            }
        });
        $('button.exit').on('click', function () {
            if(FeelItem == '1')
            {
                $('div.overlay-menu').removeClass('show_overlay');
                $('.menu').removeClass('show_block');
                FeelItem = 'NULL'
            }
        });
    });
    //        END MNU SCRIPT
</script>

<?php if($this->homePage): ?>
<script>
    $(document).ready(function() {
        var opts = {
            lines: 13 // The number of lines to draw
            , length: 10 // The length of each line
            , width: 5 // The line thickness
            , radius: 20 // The radius of the inner circle
            , scale: 1 // Scales overall size of the spinner
            , corners: 1 // Corner roundness (0..1)
            , color: '#fff' // #rgb or #rrggbb or array of colors
            , opacity: 0.25 // Opacity of the lines
            , rotate: 0 // The rotation offset
            , direction: 1 // 1: clockwise, -1: counterclockwise
            , speed: 1 // Rounds per second
            , trail: 60 // Afterglow percentage
            , fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
            , zIndex: 2e9 // The z-index (defaults to 2000000000)
            , className: 'spinner' // The CSS class to assign to the spinner
            , top: '50%' // Top position relative to parent
            , left: '50%' // Left position relative to parent
            , shadow: false // Whether to render a shadow
            , hwaccel: false // Whether to use hardware acceleration
            , position: 'absolute' // Element positioning
        };
        var target = document.getElementById('preloader');
        var spinner = new Spinner(opts).spin(target);
    });
</script>


<script type="text/javascript">
    //        FULLPAGE SCRIPT
    $(document).ready(function() {
        if($( window ).width()<=1025) {
            $('#fullpage').fullpage({
                sectionsColor: ['#ffffff', '#ccab81', '#ffffff', '#ccab81', '#ffffff', '#434343'],
                menu: '#menu',
                scrollBar:true,
                slidesNavigation: false,
                autoScrolling: false,
                fitToSection: false
            });
        }
        else{
            $('#fullpage').fullpage({
                sectionsColor: ['#ffffff', '#ccab81', '#ffffff', '#ccab81', '#ffffff', '#434343'],
                anchors: ['Columbus Capital', 'O nas', 'Relacje Inwestorskie', 'Aktualności', 'Mapa', 'Kontakt'],
                menu: '#menu',
                navigation: true,
                navigationPosition: 'left',
                navigationTooltips: ['Columbus Capital', 'O nas', 'Relacje Inwestorskie', 'Aktualności', 'Mapa', 'Kontakt'],
                scrollOverflow: true,
                scrollBar:true,
                slidesNavigation: true
            });
        }
//        END FULLPAGE SCRIPT
//            PRELOADER
       $(document).ready(function($) {
         // site preloader -- also uncomment the div in the header and the css style for #preloader
            $(window).load(function(){
                $('#preloader').fadeOut('slow',function(){$(this).remove();});
            });
        });
//            END PRELOADER
//            GOOGLE MAP SCRIPT

    });
    //        END GOOGLE MAP SCRIPT

</script>



<script>
    //        CHANGE LOGO AND MENU COLOR SCRIPT
    $(window).scroll(function() {
        if($( window ).width()>=1024) {
            var scroll = $(window).scrollTop();
            var about = $("section#section_about_us").position();
            var investors = $("section#investors_relation").position();
            var news = $("section#news").position();
            var map = $("section#google_map").position();
            var contact = $("section#contact").position();
            var logo_height = $(".logo img").height();
            if ( (scroll >= about.top-logo_height*4 && scroll < investors.top) || (scroll >= news.top-logo_height*4 && scroll < map.top) ) {
                $(".logo img").attr("src",'/themes/white/imgs/columbus_white.png');
                $(".show_menu img").attr("src",'themes/white/imgs/hamburger_white.png');
                $(".show_menu").addClass("white");
            }
            else {
                $(".logo img").attr("src",'themes/white/imgs/columbus.png');
                $(".show_menu img").attr("src",'themes/white/imgs/hamburger.png');
                $(".show_menu").removeClass("white");
            }
            if ( scroll >= map.top-logo_height*4 && scroll < contact.top ) {
                $(".logo img").attr("src",'themes/white/imgs/columbus.png');
                $(".show_menu img").attr("src",'themes/white/imgs/hamburger.png');
                $(".show_menu").removeClass("white");
            }
            if ( scroll >= map.top-logo_height*4 && scroll < contact.top ) {
                $("div.fp-tooltip.left").css("color","#ccab81");
                $("#fp-nav ul li a span").css("border","2px solid #ccab81");
                $(".fp-slidesNav ul li a span").css("border","2px solid #ccab81");
                $("#fp-nav ul li a.active span").css("background-color","#ccab81");
                $(".fp-slidesNav ul li a.active span").css("background-color","#ccab81");
            }
            else {
                $("div.fp-tooltip.left").css("color","#ffffff");
                $("#fp-nav ul li a span").css("border","2px solid #fff");
                $(".fp-slidesNav ul li a span").css("border","2px solid #fff");
                $("#fp-nav ul li a.active span").css("background-color","#fff");
                $(".fp-slidesNav ul li a.active span").css("background-color","#fff");
            }
        }
    });
    //      CHANGE LOGO AND MENU COLOR SCRIPT
</script>
<?php endif; ?>

<?php if(!$this->homePage):?>


    <script type="text/javascript">
        //        FULLPAGE SCRIPT
        $(document).ready(function() {
            $('#fullpage').fullpage({
                sectionsColor: ['#ffffff', '#ffffff','#434343'],
                menu: '#menu',
                scrollBar:true,
                slidesNavigation: false,
                autoScrolling: false,
                fitToSection: false
            });
        });
        //       END FULLPAGE SCRIPT

        //        TABS SCRIPT
        $('#myTabs a').click(function (e) {
            e.preventDefault()
            $(this).tab('show')
        })
        //        END TABS SCTIPT

        //        BACKGROUND LOGO AND MENU SCTIPT
        $(window).scroll(function() {
            if($( window ).width()<=1630 && $( window ).width()>991) {
                var scroll = jQuery(window).scrollTop();
                if (scroll >= 200) {
                    jQuery('header.page').addClass('page_active');
                }
                else {
                    jQuery('header.page').removeClass('page_active');
                }
            }
        });
        //        END BACKGROUND LOGO AND MENU SCTIPT
    </script>


    <script>

        $(document).ready(function(){

            $('div.fp-prev').click(function(e){
                e.preventDefault();
                $.fn.fullpage.moveSlideLeft();
            });

        });

    </script>

<?php endif;?>


<?php if($this->homePage): ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . 'http://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&language=pl'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl .'/js/map.js'); ?>
<?php endif; ?>
</body>
</html>