<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title><?php echo Yii::app()->name; ?><?php echo (mb_strlen($this->pageTitle) > 0 ? ' - '.CHtml::encode($this->pageTitle) : NULL); ?></title>
    <meta name="Resource-type" content="Document" />
    <meta name="description" content="<?php echo CHtml::encode($this->pageDescription); ?>">
    <meta name="keywords" content="<?php echo CHtml::encode($this->pageKeywords); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=1" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link href='https://fonts.googleapis.com/css?family=Work+Sans:400,200,300,500,600,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>

    <?php Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl.'/css/reset.css'); ?>
    <?php Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl.'/css/bootstrap.min.css'); ?>
    <?php Yii::app()->clientScript->registerLessFile(Yii::app()->theme->baseUrl.'/css/style.css'); ?>

    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/bootstrap.min.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/jquery.easing.min.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl .'/js/main.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl .'https://maps.googleapis.com/maps/api/js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl .'/js/map.js'); ?>




</head>
<body>
<header id="mainNav" class="mainNav">
    <nav class="navbar navbar-default">
        <div class="container-fluid nopadding">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#mainMenu" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand logo" href="#"><img class="img-responsive" src="/themes/white/images/logo.png" alt="logo"></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse nopadding" id="mainMenu">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="index.php">Home</a></li>
                    <li><a href="<?php echo $this->createUrl('news/index');?>">Aktualności <span class="sr-only">Aktualna podstrona</span></a></li>
                    <li><a href="<?php echo $this->createUrl('about/index');?>">Jak działamy</a></li>
                    <li class="logoMenu">
                        <a href="/">
                            <img class="img-responsive normalLogo" src="/themes/white/images/logo.png" alt="">
                            <img class="img-responsive scrollLogo" src="/themes/white/images/logoScroll.png" alt="">
                        </a>
                        <div class="menuTriangle"></div>
                    </li>
                    <li><a href="#">Produkty</a></li>
                    <li><a href="<?php echo $this->createUrl('about/cattle');?>">Skup bydła</a></li>
                    <li><a href="<?php echo $this->createUrl('contact/index');?>">Kontakt</a></li>
                </ul>

            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
</header>

<?php echo $content;?>

<section class="downloadProducts" style="background: url('/themes/white/images/downloadProducts.jpg') no-repeat center center">

    <a href="#" class="downloadProductsButton">
        <span class="downloadDescriptionHolder">
            <span class="textDownload">Pobierz katalog produktów</span>
            <img src="/themes/white/images/downloadCloud.png" alt="cloud" class="img-responsive">
        </span>
    </a>

    <div class="blackOverlay"></div>
    <div class="pattern"></div>
</section>

<section class="contactMap" style="position: relative; overflow: hidden; transform: translateZ(0px); background: url('') 50% 50% no-repeat;">
    <div class="container-fluid nopadding">
        <div id="map"></div>
    </div>
</section>


<section class="contact bgFirstShape">
    <div class="container">
        <div class="col-md-12">
            <div class="mainTitleHolder">
                <div class="breakLine thinLine"></div>
                <div class="titleHolder">
                    <h3>Napisz do nas</h3>
                </div>
                <div class="breakLine thinLine"></div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="contactForm clearfix">
                <form action="">
                    <div class="col-md-6">
                        <input type="text" name="name" placeholder="Wpisz imię i nazwisko" required>
                        <input type="email" name="email" placeholder="Wpisz adres e-mail" required>
                        <input type="tel" name="phone" placeholder="Wpisz numer telefonu">
                    </div>
                    <div class="col-md-6">
                        <input type="text" name="topic" placeholder="Wpisz temat" required>
                        <textarea name="description" id="" placeholder="Wpisz treść wiadomości" required></textarea>
                    </div>
                    <div class="col-md-12">
                        <input class="more mgTop_75" type="submit" name="submit" value="Wyślij">
                    </div>
                </form>
            </div>
        </div>
        <div class="col-md-12">
            <div class="contactInfo">
                <ul>
                    <li>
                        <span>
                            <img src="images/email.png" alt="">
                            <a href="mailto:test@test.pl">test@test.pl</a>
                        </span>
                    </li>
                    <li>
                        <span>
                            <img src="images/phone.png" alt="">
                            <a href="tel:+48777666555">+48 777 666 555</a>
                        </span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>

<section class="prefooter">
    <div class="container">
        <div class="col-md-12">
            <div class="prefooterDescription">
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis consectetur culpa distinctio
                    ea inventore libero molestiae mollitia nulla omnis temporibus.
                </p>
            </div>
        </div>
        <div class="col-md-12">
            <div class="socialHolder">
                <ul>
                    <li>
                        <a href="#">
                            <img src="/themes/white/images/facebook.png" alt="">
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img src="/themes/white/images/googleplus.png" alt="">
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="pattern"></div>
</section>

<footer>
    <div class="container">
        <div class="col-md-12">
            <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. A amet blanditiis consectetur deleniti
                iusto, molestiae mollitia officia placeat suscipit voluptates!
                <a href="#">lorem</a>
            </p>
        </div>
    </div>
    <div class="pattern"></div>
    <div class="blackOverlay"></div>
</footer>

</body>
</html>