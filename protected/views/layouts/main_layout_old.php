<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html ng-app="fiApp">
<head>
    <meta charset="utf-8" />
    <title><?php echo Yii::app()->name; ?><?php echo (mb_strlen($this->pageTitle) > 0 ? ' - '.CHtml::encode($this->pageTitle) : NULL); ?></title>

    <meta name="description" content="<?php echo CHtml::encode($this->pageDescription); ?>">
    <meta name="keywords" content="<?php echo CHtml::encode($this->pageKeywords); ?>">

    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!--[if lt IE 9]>
    <link href="<?php echo Yii::app()->baseUrl . '/lib/ie8.css'; ?>" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<?php echo Yii::app()->baseUrl . '/lib/ie8.js'; ?>"></script>
    <![endif]-->



    <?php $this->renderPartial('application.config.routesJS'); //js config ?>

    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/lib/jquery.cookie.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/translation.js'); ?>
    <?php Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl.'/css/bootstrap.min.css'); ?>
    <?php Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl.'/css/bootstrap-theme.css'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/lib/jquery-ui.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/lib/jquery.ui.touch-punch.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/lib/bootstrap.min.js'); ?>
    <?php Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl.'/css/font-awesome.min.css'); ?>
    <?php Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl.'/css/jquery-ui.min.css'); ?>

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,700italic,400,700,300&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Just+Me+Again+Down+Here&subset=latin,latin-ext' rel='stylesheet' type='text/css'>

    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>


    <?php Yii::app()->clientScript->registerLessFile(Yii::app()->theme->baseUrl.'/css/style.css'); ?>
    <?php Yii::app()->clientScript->registerLessFile(Yii::app()->theme->baseUrl.'/css/full-slider.css'); ?>

    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/lib/ui-bootstrap-tpls-0.11.0.min.js'); ?>

    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/cookies.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/main.js'); ?>

    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/directives/imagemapster.js'); ?>

    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/services/contact.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/services/storage.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/services/url.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/services/cookie.js'); ?>



    <?php if($this->homePage): ?>
        <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/script.js'); ?>
    <?php else: ?>
        <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/script-search.js'); ?>
        <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/main2.js'); ?>
    <?php endif; ?>



    <?php //Yii::app()->bootstrap->register(); ?>

</head>
<body class="<?php echo ($this->homePage ? 'home' : NULL); ?> <?php echo $this->bodyClass; ?>">


<!---->
<!--<!--header-->-->
<!--<header id="main">-->
<!--    <div class="container-fluid nopadding">-->
<!--        <div class="col-md-1 nopadding width_wrap right_float">-->
<!--            <a class="info_icon" href=""><img src="--><?php //Yii::app()->createUrl('site/index');?><!--/themes/white/css/images/mail.png" alt=""/></a>-->
<!--            <a class="info_icon" href=""><img src="--><?php //Yii::app()->createUrl('site/index');?><!--/themes/white/css/images/phone2.png" alt=""/></a>-->
<!--        </div>-->
<!--        <div class="col-md-1 nopadding width_wrap">-->
<!--            <a href="../" class="left_icons brown"><img src="--><?php //Yii::app()->createUrl('site/index');?><!--/themes/white/css/images/bunsha_icon.png" alt=""/></a>-->
<!--            <button class="left_icons social"><img src="--><?php //Yii::app()->createUrl('site/index');?><!--/themes/white/css/images/social_icon.png" alt=""/></button>-->
<!--        </div>-->
<!--        <div class="col-md-10 nopadding">-->
<!--            <ul class="menu">-->
<!--                <div class="container-fluid nopadding">-->
<!---->
<!---->
<!---->
<!--                    <li class="col-md-2 nopadding">-->
<!--                        <a class="--><?php //echo ($this->activeMenu === 'apartments' ? 'active' : NULL); ?><!--" href="--><?php //echo $this->createUrl('invest/index',Array('pageUrl' => 'asfasfa')); ?><!--">Inwestycja</a>-->
<!--                    </li>-->
<!---->
<!--                    <li class=" col-md-2 nopadding">-->
<!--                        <a class="--><?php //echo ($this->activeMenu === 'localisation' ? 'active' : NULL); ?><!--" href="--><?php //echo $this->createUrl('localisation/index'); ?><!--">Lokalizacja</a>-->
<!--                    </li>-->
<!--                    <li class="col-md-2 nopadding">-->
<!--                        <a class="--><?php //echo ($this->activeMenu === 'flat' ? 'active' : NULL); ?><!--" href="--><?php //echo $this->createUrl('flat/index'); ?><!--">Mieszkania</a>-->
<!--                    </li>-->
<!---->
<!--                    <li class="col-md-2 nopadding">-->
<!--                        <a class="--><?php //echo ($this->activeMenu === 'local' ? 'active' : NULL); ?><!--" href="--><?php //echo $this->createUrl('flat/locals'); ?><!--">Lokale</a>-->
<!--                    </li>-->
<!--                    <li class="col-md-2 nopadding">-->
<!--                        <a class="--><?php //echo ($this->activeMenu === 'estate' ? 'active' : NULL); ?><!--" href="--><?php //echo $this->createUrl('estate/galleryList'); ?><!--">Galeria</a>-->
<!--                    </li>-->
<!--                    <li class="col-md-2 nopadding">-->
<!--                        <a class="--><?php //echo ($this->activeMenu === 'contact' ? 'active' : NULL); ?><!--" href="--><?php //echo $this->createUrl('contact/index'); ?><!--">Kontakt</a>-->
<!--                    </li>-->
<!--                </div>-->
<!--            </ul>-->
<!--        </div>-->
<!---->
<!--    </div>-->
<!--</header>-->
<!--<!--/end-->-->




<?php echo $content; ?>



<footer id="main_footer">
    Copyrights by BUNSCHA PARK 2015
</footer>

<script>
    $('.carousel').carousel({
        interval: 5000 //changes the speed
    });

</script>


</body>
</html>