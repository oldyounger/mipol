<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">

	<title><?php echo Yii::app()->name; ?><?php echo (mb_strlen($this->pageTitle) > 0 ? ' - '.CHtml::encode($this->pageTitle) : NULL); ?></title>

	<meta name="description" content="<?php echo CHtml::encode($this->pageDescription); ?>">
	<meta name="keywords" content="<?php echo CHtml::encode($this->pageKeywords); ?>">

	<?php $this->renderPartial('application.config.routesJS'); //js config ?>

	<?php Yii::app()->getClientScript()->registerScriptFile(Yii::app()->baseUrl . '/js/application.js'); ?>
	<?php Yii::app()->getClientScript()->registerScriptFile(Yii::app()->baseUrl . '/js/ajax.js'); ?>
	<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/translation.js'); ?>

	<!--[if lt IE 9]>
	<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>js/lib/IE9.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>js/lib/html5shiv.js"></script>
	<![endif]-->

	<?php Yii::app()->bootstrap->register(); ?>

	<?php Yii::app()->clientScript->registerLessFile(Yii::app()->theme->baseUrl.'/css/style.less'); ?>
</head>

<body class="blank">
	<div class="content">
		<?php echo $content; ?>
	</div>
</body>
</html>
