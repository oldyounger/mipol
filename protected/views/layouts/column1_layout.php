<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main_layout'); ?>

<div id="content">
	<?php echo $content; ?>
</div>

<?php $this->endContent(); ?>