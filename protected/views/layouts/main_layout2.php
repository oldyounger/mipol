<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html ng-app="fiApp">
<head>
    <meta charset="utf-8" />
    <title><?php echo Yii::app()->name; ?><?php echo (mb_strlen($this->pageTitle) > 0 ? ' - '.CHtml::encode($this->pageTitle) : NULL); ?></title>

    <meta name="description" content="<?php echo CHtml::encode($this->pageDescription); ?>">
    <meta name="keywords" content="<?php echo CHtml::encode($this->pageKeywords); ?>">

    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!--[if lt IE 9]>
    <link href="<?php echo Yii::app()->baseUrl . '/lib/ie8.css'; ?>" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<?php echo Yii::app()->baseUrl . '/lib/ie8.js'; ?>"></script>
    <![endif]-->

    <?php $this->renderPartial('application.config.routesJS'); //js config ?>

    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/lib/jquery.cookie.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/application.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/ajax.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/translation.js'); ?>

    <?php Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl.'/css/bootstrap.min.css'); ?>
    <?php Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl.'/css/bootstrap-theme.css'); ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <?php //Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/lib/jquery-1.11.1.min.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/lib/jquery-ui.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/lib/jquery.ui.touch-punch.js'); ?>

    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/lib/bootstrap.min.js'); ?>

    <?php Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl.'/css/font-awesome.min.css'); ?>
    <?php Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl.'/css/jquery-ui.min.css'); ?>

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,700italic,400,700,300&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Just+Me+Again+Down+Here&subset=latin,latin-ext' rel='stylesheet' type='text/css'>

    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>

    <?php Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl.'/css/ng-table.css'); ?>
    <?php Yii::app()->clientScript->registerLessFile(Yii::app()->theme->baseUrl.'/css/style.css'); ?>
    <?php Yii::app()->clientScript->registerLessFile(Yii::app()->theme->baseUrl.'/css/style.less'); ?>

    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/lib/bootbox.min.js'); ?>

    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/lib/jquery.imagemapster.js'); ?>

    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/lib/angular.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/lib/angular-animate.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/lib/angular-cookies.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/lib/ng-table.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/lib/slider.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/lib/ui-bootstrap-tpls-0.11.0.min.js'); ?>

    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/cookies.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/main.js'); ?>

    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/directives/imagemapster.js'); ?>

    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/services/contact.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/services/storage.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/services/url.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/services/cookie.js'); ?>

    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/controllers/popup.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/controllers/home_slider.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/controllers/fast_contact.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/controllers/storage.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/controllers/locals.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/controllers/apartment.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/controllers/anniversary.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/controllers/search.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/controllers/landing_anniversary.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/controllers/landing_anniversary_contact.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/controllers/landing_apartments_contact.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/controllers/landing_flats_contact.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/controllers/landing_journey_contact.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/controllers/search_graphics.js'); ?>

    <?php if($this->homePage): ?>
        <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/script.js'); ?>
    <?php else: ?>
        <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/script-search.js'); ?>
    <?php endif; ?>

    <script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion_async.js" charset="utf-8"></script>

    <?php //Yii::app()->bootstrap->register(); ?>

    <script>(function() {
            var _fbq = window._fbq || (window._fbq = []);
            if (!_fbq.loaded) {
                var fbds = document.createElement('script');
                fbds.async = true;
                fbds.src = '//connect.facebook.net/en_US/fbds.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(fbds, s);
                _fbq.loaded = true;
            }
            _fbq.push(['addPixelId', '1503553406572742']);
        })();
        window._fbq = window._fbq || [];
        window._fbq.push(['track', 'PixelInitialized', {}]);
    </script>
    <noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?id=1503553406572742&amp;ev=PixelInitialized" /></noscript>
</head>
<body class="<?php echo ($this->homePage ? 'home' : NULL); ?> <?php echo $this->bodyClass; ?>">
<div class="navbar-main">
    <div class="navbar-top clearfix">
        <div class="icons-left pull-left">
            <a href="<?php echo $this->createUrl('site/index'); ?>" class="block z-block"></a>
            <i class="fi"></i>
        </div>
        <div class="icons-right pull-right">
            <div class="facebook">
                <a href="<?php echo Configuration::getValue('facebook_url'); ?>" target="_blank" class="block z-block"></a>
                <i class="fb"></i>
            </div>
            <div class="youtube">
                <a href="<?php echo Configuration::getValue('youtube_url'); ?>" target="_blank" class="block z-block"></a>
                <i class="yt"></i>
            </div>
            <div class="storage">
                <a href="<?php echo $this->createUrl('storage/index'); ?>" class="block z-block"></a>
                <i class="folder icon-hidden" id="storageCount" data-count="2"></i>
            </div>
        </div>
        <ul class="menu pull-right">
            <li class="<?php echo ($this->activeMenu === 'estate' ? 'active' : NULL); ?>">
                <a href="<?php echo $this->createUrl('estate/index', Array('pageUrl' => 'osiedle-zrownowazone')); ?>">Osiedle Fi</a>
            </li>
            <li class="<?php echo ($this->activeMenu === 'localisation' ? 'active' : NULL); ?>">
                <a href="<?php echo $this->createUrl('localisation/index', Array('pageUrl' => 'komunikacja')); ?>">Lokalizacja</a>
            </li>
            <li class="<?php echo ($this->activeMenu === 'flat' ? 'active' : NULL); ?>">
                <a href="<?php echo $this->createUrl('flat/searchGraphics'); ?>">Mieszkania</a>
            </li>
            <li class="<?php echo ($this->activeMenu === 'apartments' ? 'active' : NULL); ?>">
                <a href="<?php echo $this->createUrl('flat/apartments'); ?>">Apartamenty</a>
            </li>
            <li class="<?php echo ($this->activeMenu === 'local' ? 'active' : NULL); ?>">
                <a href="<?php echo $this->createUrl('flat/locals'); ?>">Lokale użytkowe</a>
            </li>
            <li class="<?php echo ($this->activeMenu === 'news' ? 'active' : NULL); ?>">
                <a href="<?php echo $this->createUrl('news/listAll'); ?>">Aktualności</a>
            </li>
            <li class="<?php echo ($this->activeMenu === 'contact' ? 'active' : NULL); ?>">
                <a href="<?php echo $this->createUrl('contact/index'); ?>">Kontakt</a>
            </li>
            <li>
                <i class="icon-phone"></i><?php echo Configuration::getValue('phone_number'); ?>
            </li>
        </ul>
    </div>
    <div class="header <?php echo ($this->homePage ? NULL : 'small'); ?> <?php echo $this->activeMenu; ?> clearfix">
        <?php if($this->homePage): ?>
            <?php /*<div class="logo"></div> */ ?>
            <div class="logo-right">
                <a href="<?php echo $this->createUrl('news/index', Array('newsUrl' => 'inwestycja-roku-2015')); ?>" class="block"></a>
            </div>

            <?php /*<form action="<?php echo $this->createUrl('flat/index'); ?>" method="get">
                <div class="search-home clearfix">
                    <div class="pull-right">
                        <button type="submit">
                            <i class="zoom"></i>Wyszukaj
                        </button>
                    </div>
                    <div class="main-container" ng-controller="HomeSliderController">
                        <?php $s = new Search(); ?>
                        <?php $smallestRoom = $s->getSmallestRoom(Flat::TYPE_HOUSE); ?>
                        <?php $biggestRoom = $s->getBiggestRoom(Flat::TYPE_HOUSE); ?>

                        <?php $smallestArea = $s->getSmallestArea(Flat::TYPE_HOUSE); ?>
                        <?php $biggestArea = $s->getBiggestArea(Flat::TYPE_HOUSE); ?>

                        Wybierz <strong>ilość pokoi</strong>
                        <div id="slider-rooms" ui-slider="{range: true, showHandleValue: true, callback: roomsSlideCallback}" min="<?php echo $smallestRoom; ?>" max="<?php echo $biggestRoom; ?>" step="1" valuemin="<?php echo $smallestRoom; ?>" valuemax="<?php echo $biggestRoom; ?>" ng-model="roomsValue"></div>

                        Wybierz <strong>metraż</strong>
                        <div id="slider-area" ui-slider="{range: true, showHandleValue: true, handleValueSuffix: ' m²', callback: areaSlideCallback}" min="<?php echo $smallestArea; ?>" max="<?php echo $biggestArea; ?>" step="1" valuemin="<?php echo $smallestArea; ?>" valuemax="<?php echo $biggestArea; ?>" ng-model="areaValue"></div>

                        <input type="hidden" ng-model="rooms" ng-init='rooms = <?php echo json_encode($s->getAreaStepsRoomGroupped(Flat::TYPE_HOUSE)); ?>' />

                        <input type="hidden" id="slider-rooms-input-min" name="r_min" value="{{ roomsValue[0] }}" />
                        <input type="hidden" id="slider-rooms-input-max" name="r_max" value="{{ roomsValue[1] }}" />

                        <input type="hidden" id="slider-area-input-min" name="a_min" value="{{ areaValue[0] }}" />
                        <input type="hidden" id="slider-area-input-max" name="a_max" value="{{ areaValue[1] }}" />
                    </div>

                    <div class="text-center">
                        <div class="search-button-home">
                            <i class="icon-search"></i>Wyszukiwarka graficzna

                            <a href="<?php echo $this->createUrl('flat/searchGraphics'); ?>" class="block"></a>
                        </div>
                    </div>
                </div>
            </form>

            <div class="text-center">
                <div class="what-is-fi">
                    Zobacz czym jest Fi? <i class="icon-chevron-down"></i>

                    <a href="#" class="block onClickScrollDown"></a>
                </div>
            </div>*/ ?>

            <div class="text-center">
                <div class="what-is-fi what-is-fi-full">
                    Zobacz <strong class="orange">więcej</strong>

                    <a href="<?php echo $this->createUrl('news/index', Array('newsUrl' => 'widocznie-najlepsze')); ?>" class="block"></a>
                </div>
            </div>

            <form action="<?php echo $this->createUrl('flat/index'); ?>" method="get">
                <div class="search-home clearfix">
                    <div class="pull-right">
                        <button type="submit">
                            <i class="zoom"></i>Wyszukaj
                        </button>
                    </div>
                    <div class="main-container" ng-controller="HomeSliderController">
                        <?php $s = new Search(); ?>
                        <?php $smallestRoom = $s->getSmallestRoom(Flat::TYPE_HOUSE); ?>
                        <?php $biggestRoom = $s->getBiggestRoom(Flat::TYPE_HOUSE); ?>

                        <?php $smallestArea = $s->getSmallestArea(Flat::TYPE_HOUSE); ?>
                        <?php $biggestArea = $s->getBiggestArea(Flat::TYPE_HOUSE); ?>

                        Wybierz <strong>ilość pokoi</strong>
                        <div id="slider-rooms" ui-slider="{range: true, showHandleValue: true, callback: roomsSlideCallback}" min="<?php echo $smallestRoom; ?>" max="<?php echo $biggestRoom; ?>" step="1" valuemin="<?php echo $smallestRoom; ?>" valuemax="<?php echo $biggestRoom; ?>" ng-model="roomsValue"></div>

                        Wybierz <strong>metraż</strong>
                        <div id="slider-area" ui-slider="{range: true, showHandleValue: true, handleValueSuffix: ' m²', callback: areaSlideCallback}" min="<?php echo $smallestArea; ?>" max="<?php echo $biggestArea; ?>" step="1" valuemin="<?php echo $smallestArea; ?>" valuemax="<?php echo $biggestArea; ?>" ng-model="areaValue"></div>

                        <input type="hidden" ng-model="rooms" ng-init='rooms = <?php echo json_encode($s->getAreaStepsRoomGroupped(Flat::TYPE_HOUSE)); ?>' />

                        <input type="hidden" id="slider-rooms-input-min" name="r_min" value="{{ roomsValue[0] }}" />
                        <input type="hidden" id="slider-rooms-input-max" name="r_max" value="{{ roomsValue[1] }}" />

                        <input type="hidden" id="slider-area-input-min" name="a_min" value="{{ areaValue[0] }}" />
                        <input type="hidden" id="slider-area-input-max" name="a_max" value="{{ areaValue[1] }}" />
                    </div>

                    <div class="text-center">
                        <div class="search-button-home">
                            <i class="icon-search"></i>Wyszukiwarka graficzna

                            <a href="<?php echo $this->createUrl('flat/searchGraphics'); ?>" class="block"></a>
                        </div>
                    </div>
                </div>
            </form>
        <?php else: ?>
            <div class="container">
                <ul class="breadcrumbs pull-right">
                    <?php echo CTools::createBreadcrumbs($this->breadcrumbs); ?>
                </ul>
            </div>
        <?php endif; ?>
    </div>
</div>
<div class="container content <?php echo $this->contentClass; ?> clearfix">
    <?php echo $content; ?>
</div>

<?php if($this->specialFooter): ?>
    <?php echo $this->specialFooter; ?>
<?php endif; ?>

<?php if(!$this->homePage && $this->showFooter): ?>
    <div class="fast-contact-push"></div>
    <div class="fast-contact" ng-controller="FastContactController">
        <div class="container">
            <span class="text-orange">Szybki kontakt:</span>
            <input type="text" name="FormContact[name]" ng-model="name" placeholder="Podaj imię" />
            <input type="text" name="FormContact[email]" ng-model="email" placeholder="Podaj e-mail" />
            <input type="text" name="FormContact[telephone]" ng-model="telephone" placeholder="Podaj nr telefonu" />

            <div class="textarea-btn-container" ng-class="{'active': textareaActive}">
                <button class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown" ng-click="textareaActive = !textareaActive">
                    Treść wiadomości
                </button>
                <div class="textarea-container">
                    <textarea placeholder="Treść wiadomości" ng-model="content"></textarea>
                </div>
            </div>
            <input type="hidden" name="FormContact[hour]" ng-value="selectedHour" />

            <button type="button" class="button-send" ng-click="sendRequest()">Wyślij</button>
        </div>
    </div>
<?php endif; ?>

<script type="text/ng-template" id="modalTemplate.html">
    <div class="modal-body modal-flat-contact clearfix" ng-if="step == 1">
        <div class="close-btn" ng-click="cancel()"></div>

        <strong>Jestem zainteresowany/zainteresowana mieszkaniem</strong>

        <table class="table table-fixed search-table">
            <thead>
                <tr>
                    <th>Stan</th>
                    <th>Budynek</th>
                    <th>Pokoje</th>
                    <th>Metraż</th>
                    <th>Nr</th>
                    <th>Balkon/Ogród</th>
                    <th>Piętro</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td data-title-text="Stan">
                        {{ flat.state }}
                    </td>
                    <td data-title-text="Budynek">
                        {{ flat.building }}
                    </td>
                    <td data-title-text="Pokoje">
                        {{ flat.rooms }}
                    </td>
                    <td data-title-text="Metraż">
                        {{ flat.area }}
                    </td>
                    <td data-title-text="Nr">
                        {{ flat.name }}
                    </td>
                    <td data-title-text="Balkon/Ogród">
                        {{ flat.balcony }}
                    </td>
                    <td data-title-text="Piętro">
                        {{ flat.level }}
                    </td>
                    <td data-title-text="Status">
                        {{ flat.status }}
                    </td>
                </tr>
            </tbody>
        </table>

        <strong>Proszę o kontakt:</strong>
        <input type="text" name="FormModal[name]" ng-model="name" placeholder="Podaj imię" />
        <span class="required">*</span>

        <input type="text" name="FormModal[email]" ng-model="email" placeholder="Podaj e-mail" />
        <span class="required">*</span>

        <input type="text" name="FormModal[telephone]" ng-model="telephone" placeholder="Podaj nr telefonu" />
        <span class="required">*</span>

        <div class="pull-right clearfix">
            <span class="lbl">Wyślij kopię na mój e-mail</span>
            <div class="as-checkbox" ng-class="{'checked': sendCopy}" ng-click="changeCheckbox()"></div>
        </div>

        <input type="hidden" name="FormModal[send_copy]" ng-model="sendCopy" />

        <div class="clearfix"></div>
        <div class="clearfix">
            <div class="checkbox">
                <input type="checkbox" id="accept" name="FormModal[accept]" ng-model="accept" />
                <label for="accept">
                    Oświadczam, że zapoznałem/am się z <a href="<?php echo $this->createUrl('html/page', Array('pageUrl' => 'klauzula-o-przetwarzaniu-danych-osobowych')); ?>">Informacją</a> dotyczącą przetwarzania danych osobowych i wyrażam zgodę na przetwarzanie moich danych osobowych przez Megapolis Sp. z o. o. w celach przekazywania informacji o produktach oferowanych przez spółkę, w tym na przesyłanie informacji handlowych na w/w adres poczty elektronicznej i na w/w numer telefonu oraz na przetwarzanie moich danych osobowych w celu badania jakości obsługi.
                </label>
            </div>
            <button type="button" class="button-send pull-right" ng-click="sendRequest()" ng-class="{'disabled': buttonDisabled}">Wyślij</button>
        </div>
    </div>
    <div class="modal-body modal-flat-contact modal-flat-contact-step-second clearfix" ng-if="step == 2">
        <div class="close-btn" ng-click="cancel()"></div>

        <h2>Dziękujemy za wypełnienie formularza.</h2>
        <h2>Doradca Handlowy skontaktuje się z państwem w ciągu 24 godzin.</h2>

        <h2 class="margined">Godziny otwarcia biura:</h2>
        <h2 class="text-orange">Poniedziałek - Piątek 9:00 - 18:00</h2>
    </div>
</script>

<?php if($this->homePage): ?>
    <div class="top-button"></div>
<?php endif; ?>

<?php if($this->showFooter): ?>
    <footer>
        Copyright &copy; 2014 Osiedle Fi. All Rights Reserved. Wykonanie beloud.com.pl. <a href="<?php echo $this->createUrl('html/page', Array('pageUrl' => 'polityka-prywatnosci')); ?>">Polityka prywatności</a>
    </footer>
<?php endif; ?>
    <div id="cookieBar">
        <div class="close-btn">Zamknij</div>

        <a href="<?php echo $this->createUrl('html/page', Array('pageUrl' => 'polityka-prywatnosci')); ?>">
            Strona korzysta z plików cookies w celu realizacji usług i zgodnie z Polityką Plików Cookies.
            Możesz określić warunki przechowywania lub dostępu do plików cookies w Twojej przeglądarce.
        </a>
    </div>


<script type="text/javascript">
    var _riseA = _riseA || [];
    _riseA.push([ 'setTracker', 'AAB4F6A2-6DEF-F12E-D362-C233142ED657' ]);

    (function() {
        var snrs = document.createElement('script');
        snrs.type = 'text/javascript';
        snrs.async = true;
        snrs.src = ('https:' == document.location.protocol ? 'https://'
            : 'http://')
        + 'tc.synerise.com/snrs-2.0.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(snrs, s);
    })();
</script>

<?php if(!defined('YII_DEBUG') || !YII_DEBUG): ?>
    <script type="text/javascript">
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-34687452-1']);
        _gaq.push(['_trackPageview']);
        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>

    <script type="text/javascript">
        /* <![CDATA[ */
        var google_conversion_id = 966244883;
        var google_custom_params = window.google_tag_params;
        var google_remarketing_only = true;
        /* ]]> */
    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
    </script>
    <noscript>
        <div style="display:inline;">
            <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/966244883/?value=0&amp;guid=ON&amp;script=0"/>
        </div>
    </noscript>
    <div id="hiddenScript" style="display: none;"></div>




    <script>
        (function(w,d) {
            w.hj = w.hj || function() { (w.hj.q=w.hj.q||[]).push(arguments); };
            w._hjSettings = { hjid: 2361 };
            var s = d.createElement('script');
            s.src = '//insights.hotjar.com/static/client/insights.js';
            d.getElementsByTagName('head')[0].appendChild(s);
        }(window,document));
    </script>

    <script>
        (function(){
            var _fbq = window._fbq || (window._fbq = []);
            if (!_fbq.loaded) {
                var fbds = document.createElement('script');
                fbds.async = true;
                fbds.src = '//connect.facebook.net/en_US/fbds.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(fbds, s);
                _fbq.loaded = true;
            }
        })();
        window._fbq = window._fbq || [];
    </script>
<?php endif; ?>
</body>
</html>