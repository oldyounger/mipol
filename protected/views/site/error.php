<?php
/* @var $this SiteController */
/* @var $error array */

$this->pageTitle = Yii::app()->name . ' - '.Yii::t('app', 'Error');
$this->breadcrumbs = Array(
	Array(Yii::t('app', 'Error'), '#'),
);
?>

<div class="center">
</div>
<div class="whole_bg">
	<div class="siteTitle">
		<div class="icon bow left orange"></div>
		<h1><?php echo Yii::t('app', 'Error').' '.$code; ?></h1>
		<div class="icon bow right orange"></div>
	</div>

	<div class="error">
		<?php echo CHtml::encode($message); ?>
	</div>
</div>