<?php
/* @var $this SiteController */
/* @var $model FormLogin */
/* @var $form TbActiveForm  */
$this->pageTitle = 'Logowanie';
$this->breadcrumbs = Array(
	Array('Logowanie', '#'),
);
?>




<h3>Logowanie</h3>
<div class="login-page" style="display: table;color:#000 !important;margin-top: 200px;">
    <?php
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', Array(
        'id' => 'lf',
        'enableClientValidation' => false,
        'clientOptions' => Array(
            'validateOnSubmit' => false,
        ),
    ));
    ?>

    <div class="errors">
        <?php echo $form->errorSummary($model); ?>
    </div>

    <div class="col-xs-12">
        <?php echo $form->textFieldRow($model, 'email', Array('placeholder' => 'E-mail')); ?>
    </div>
    <div class="col-xs-12">
        <?php echo $form->passwordFieldRow($model, 'password', Array('placeholder' => 'Hasło')); ?>
    </div>

    <div class="col-xs-12">
        <button class="btn btn-primary" type="submit">Zaloguj się</button>
    </div>

    <?php $this->endWidget(); ?>
</div>
