<?php
/** @var $this ActivityController */
/** @var $model Page */


$this->pageTitle = 'O nas';
?>

<section class="mainPageSection" style="background: url('/themes/white/images/howWeOperatePage.jpg') no-repeat center center">
    <div class="container-fluid nopadding">
        <div class="mainTitleHolder pageSection">
            <div class="breakLine"></div>
            <div class="titleHolder">
                <h2>Lorem ipsum dolor sit amet.</h2>
            </div>
            <div class="breakLine"></div>
            <div class="subTitleHolder">
                <h3>Lorem ipsum dolor sit amet.</h3>
            </div>
        </div>
    </div>
    <div class="blackOverlay"></div>
    <div class="pattern"></div>
</section>

<section class="aboutUs">
    <div class="container">
        <div class="col-md-12">
            <div class="mainTitleHolder">
                <div class="breakLine thinLine"></div>
                <div class="titleHolder">
                    <h3>Kilka słów o nas</h3>
                </div>
                <div class="breakLine thinLine"></div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="aboutUsDescription">
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aliquam aspernatur aut consequatur
                    corporis cum debitis deserunt dicta fuga, illo labore laboriosam laborum maxime modi molestiae
                    nemo nesciunt perferendis possimus provident quas quasi qui quis quod recusandae
                    rem tempore veritatis.
                </p>

                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aliquam aspernatur aut consequatur
                    corporis cum debitis deserunt dicta fuga, illo labore laboriosam laborum maxime modi molestiae
                    nemo nesciunt perferendis possimus provident quas quasi qui quis quod recusandae
                    rem tempore veritatis.
                </p>
            </div>
        </div>
    </div>
</section>

<section class="aboutUsDescriptionSection">
    <div class="container-fluid nopadding">
        <div class="aboutUsFlexBox clearfix">
            <div class="col-md-6 nopadding imgAboutUs" style="background: url('/themes/white/images/aboutUs1.jpg') no-repeat center center">
                <div class="blackLighterOverlay"></div>
                <div class="pattern"></div>
            </div>
            <div class="col-md-6 nopadding">
                <div class="aboutUsText">
                    <h4>Jakość surowca</h4>

                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam cupiditate dolores eveniet
                        impedit maiores numquam! A assumenda at eum explicabo iure pariatur ut? Ab aliquid asperiores,
                        consequuntur dolore illum nulla obcaecati omnis quam quidem ratione repellendus, tempore.
                        Autem, deserunt dolores.
                    </p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam cupiditate dolores eveniet
                        impedit maiores numquam! A assumenda at eum explicabo iure pariatur ut? Ab aliquid asperiores,
                        consequuntur dolore illum nulla obcaecati omnis quam quidem ratione repellendus, tempore.
                        Autem, deserunt dolores.
                    </p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam cupiditate dolores eveniet
                        impedit maiores numquam! A assumenda at eum explicabo iure pariatur ut? Ab aliquid asperiores,
                        consequuntur dolore illum nulla obcaecati omnis quam quidem ratione repellendus, tempore.
                        Autem, deserunt dolores.
                    </p>
                </div>
            </div>
        </div>

        <div class="aboutUsFlexBox clearfix">
            <div class="col-md-6 col-md-push-6 nopadding imgAboutUs" style="background: url('/themes/white/images/aboutUs2.jpg') no-repeat center center">
                <div class="blackLighterOverlay"></div>
                <div class="pattern"></div>
            </div>
            <div class="col-md-6 col-md-pull-6 nopadding">
                <div class="aboutUsText">
                    <h4>Ubój / rozbiór</h4>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam cupiditate dolores eveniet
                        impedit maiores numquam! A assumenda at eum explicabo iure pariatur ut? Ab aliquid asperiores,
                        consequuntur dolore illum nulla obcaecati omnis quam quidem ratione repellendus, tempore.
                        Autem, deserunt dolores.
                    </p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam cupiditate dolores eveniet
                        impedit maiores numquam! A assumenda at eum explicabo iure pariatur ut? Ab aliquid asperiores,
                        consequuntur dolore illum nulla obcaecati omnis quam quidem ratione repellendus, tempore.
                        Autem, deserunt dolores.
                    </p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam cupiditate dolores eveniet
                        impedit maiores numquam! A assumenda at eum explicabo iure pariatur ut? Ab aliquid asperiores,
                        consequuntur dolore illum nulla obcaecati omnis quam quidem ratione repellendus, tempore.
                        Autem, deserunt dolores.
                    </p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam cupiditate dolores eveniet
                        impedit maiores numquam! A assumenda at eum explicabo iure pariatur ut? Ab aliquid asperiores,
                        consequuntur dolore illum nulla obcaecati omnis quam quidem ratione repellendus, tempore.
                        Autem, deserunt dolores.
                    </p>
                </div>
            </div>
        </div>

        <div class="aboutUsFlexBox clearfix">
            <div class="col-md-6 nopadding imgAboutUs" style="background: url('/themes/white/images/aboutUs3.jpg') no-repeat center center">
                <div class="blackLighterOverlay"></div>
                <div class="pattern"></div>
            </div>
            <div class="col-md-6 nopadding">
                <div class="aboutUsText">
                    <h4>Technologia zakładu</h4>

                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam cupiditate dolores eveniet
                        impedit maiores numquam! A assumenda at eum explicabo iure pariatur ut? Ab aliquid asperiores,
                        consequuntur dolore illum nulla obcaecati omnis quam quidem ratione repellendus, tempore.
                        Autem, deserunt dolores.
                    </p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam cupiditate dolores eveniet
                        impedit maiores numquam! A assumenda at eum explicabo iure pariatur ut? Ab aliquid asperiores,
                        consequuntur dolore illum nulla obcaecati omnis quam quidem ratione repellendus, tempore.
                        Autem, deserunt dolores.
                    </p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam cupiditate dolores eveniet
                        impedit maiores numquam! A assumenda at eum explicabo iure pariatur ut? Ab aliquid asperiores,
                        consequuntur dolore illum nulla obcaecati omnis quam quidem ratione repellendus, tempore.
                        Autem, deserunt dolores.
                    </p>
                </div>
            </div>
        </div>

        <div class="aboutUsFlexBox clearfix">
            <div class="col-md-6 col-md-push-6 nopadding imgAboutUs" style="background: url('/themes/white/images/aboutUs4.jpg') no-repeat center center">
                <div class="blackLighterOverlay"></div>
                <div class="pattern"></div>
            </div>
            <div class="col-md-6 col-md-pull-6 nopadding">
                <div class="aboutUsText">
                    <h4>Własny transport</h4>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam cupiditate dolores eveniet
                        impedit maiores numquam! A assumenda at eum explicabo iure pariatur ut? Ab aliquid asperiores,
                        consequuntur dolore illum nulla obcaecati omnis quam quidem ratione repellendus, tempore.
                        Autem, deserunt dolores.
                    </p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam cupiditate dolores eveniet
                        impedit maiores numquam! A assumenda at eum explicabo iure pariatur ut? Ab aliquid asperiores,
                        consequuntur dolore illum nulla obcaecati omnis quam quidem ratione repellendus, tempore.
                        Autem, deserunt dolores.
                    </p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam cupiditate dolores eveniet
                        impedit maiores numquam! A assumenda at eum explicabo iure pariatur ut? Ab aliquid asperiores,
                        consequuntur dolore illum nulla obcaecati omnis quam quidem ratione repellendus, tempore.
                        Autem, deserunt dolores.
                    </p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam cupiditate dolores eveniet
                        impedit maiores numquam! A assumenda at eum explicabo iure pariatur ut? Ab aliquid asperiores,
                        consequuntur dolore illum nulla obcaecati omnis quam quidem ratione repellendus, tempore.
                        Autem, deserunt dolores.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="certifications">
    <div class="container">
        <div class="col-md-12">
            <div class="mainTitleHolder">
                <div class="breakLine thinLine"></div>
                <div class="titleHolder">
                    <h3>Certyfikaty</h3>
                </div>
                <div class="breakLine thinLine"></div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="certificationsHolder">
                <ul>
                    <li>
                        <img class="img-responsive" src="/themes/white/images/certifications1.png" alt="certifications">
                    </li>
                    <li>
                        <img class="img-responsive" src="/themes/white/images/certifications2.png" alt="certifications">
                    </li>
                    <li>
                        <img class="img-responsive" src="/themes/white/images/certifications3.png" alt="certifications">
                    </li>
                    <li>
                        <img class="img-responsive" src="/themes/white/images/certifications4.png" alt="certifications">
                    </li>
                    <li>
                        <img class="img-responsive" src="/themes/white/images/certifications5.png" alt="certifications">
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>