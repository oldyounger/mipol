<?php
/** @var $this ActivityController */
/** @var $model Page */


$this->pageTitle = 'Galeria';
?>


<?php if($galleryImages && count($galleryImages)>0):?>
    <section id="officeSpaceGallery" class="siteGallery">
        <div class="container">
            <div class="col-md-12 nopadding">
                <h3>Galeria</h3>
            </div>


            <div class="col-md-12 nopadding" id="slider">
                <div class="col-md-12 nopadding" id="carousel-bounding-box">
                    <div id="gallery-1" class="carousel slide" data-interval="false">
                        <!-- main slider carousel items -->
                        <div class="carousel-inner">

                            <?php
                            $index = 0;
                            foreach($galleryImages as $galleryImage): ?>
                                <div class="<?php echo $index==0 ? 'active' : ''?> item" data-slide-number="<?php echo $index++?>">
                                    <img src="<?php echo $galleryImage->getImageUrl(); ?>" class="img-responsive">
                                </div>
                            <?php endforeach; ?>
                        </div>
                        <!-- main slider carousel nav controls -->
                        <a class="carousel-control left" href="#gallery-1" data-slide="prev"><img class="img-responsive" src="/themes/white/images/arrowLeft.png" alt=""></a>

                        <a class="carousel-control right" href="#gallery-1" data-slide="next"><img class="img-responsive" src="/themes/white/images/arrowRight.png" alt=""></a>
                    </div>
                </div>

            </div>



            <!-- thumb navigation carousel -->
            <div class="col-md-12 nopadding hidden-sm hidden-xs" id="slider-thumbs">
                <button id="thumbanailsShow-1" class="thumbnailButton">Pokaż miniatury</button>
            </div>
            <div class="col-md-12 nopadding">
                <!-- thumb navigation carousel items -->
                <div id="thumbanailsShow-1-list" class="thumbnailList">
                    <ul class="thumbnailListHolder">
                        <?php
                        $index = 0;
                        foreach($galleryImages as $galleryImage): ?>
                            <li>
                                <a id="carousel-selector-<?php echo $index;?>" class="<?php echo $index++==0 ? 'selected' : ''?>">
                                    <img src="<?php echo $galleryImage->getImageUrl(
                                        Array(  'resize' => Array(
                                            'width' => 458,
                                            'height' => 416,
                                            'fit' => 'outside'
                                        ),
                                            'crop' => Array(
                                                'width' => 293,
                                                'height' => 208
                                            )
                                        )


                                    ); ?>" class="img-responsive">
                                    <span class="hoverState"></span>
                                </a>
                            </li>

                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>





            <div class="col-md-12 nopadding">
                <div class="previewNextGallery">


                    <a href="<?php echo $prev ? $this->createUrl('gallery/index',Array('galleryId'=>$prev->id)) : '#'?>"><img class="img-responsive" src="/themes/white/images/arrowLeft.png" alt="">Poprzednia galeria</a>
                    <a href="<?php echo $next ? $this->createUrl('gallery/index',Array('galleryId'=>$next->id)) : '#'?>">Następna galeria<img class="img-responsive" src="/themes/white/images/arrowright.png" alt=""></a>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>