<?php
class AdminModule extends CWebModule
{
	public $strictMode = true;
	public $userClass = 'User';
	public $userIdColumn = 'id';
	public $userNameColumn = 'email';
	public $flashKeys = array();
	public $forceCopyAssets = true;
	private $_assetsUrl;

	/**
	 * Initializes the module.
	 */
	public function init()
	{
		$this->setImport(array(
			'admin.components.*',
			'admin.controllers.*',
			'admin.models.*',
			'admin.widgets.*',
		));

		$this->registerCss();
		$this->registerJs();

		$this->flashKeys = array_merge($this->flashKeys, array(
			'error' => 'error',
			'info' => 'info',
			'success' => 'success',
			'warning' => 'warning',
		));
	}

	/**
	 * Registers the module CSS.
	 */
	public function registerCss()
	{
		Yii::app()->clientScript->registerCssFile($this->getAssetsUrl().'/css/auth.css');
		Yii::app()->clientScript->registerCssFile($this->getAssetsUrl().'/css/admin.css');
	}

	/**
	 * Registers the module JS.
	 */
	public function registerJs()
	{
        Yii::app()->clientScript->registerScriptFile($this->getAssetsUrl().'/js/bootbox.min.js');
		Yii::app()->clientScript->registerScriptFile($this->getAssetsUrl().'/js/admin.js');
        Yii::app()->clientScript->registerScriptFile($this->getAssetsUrl().'/js/flat_canvas.js');
	}


	/**
	 * The pre-filter for controller actions.
	 * @param CController $controller the controller.
	 * @param CAction $action the action.
	 * @return boolean whether the action should be executed.
	 * @throws CHttpException if user is denied access.
	 */
	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			$user = Yii::app()->getUser();
			if($user->isGuest)
			{
				$user->loginRequired();
			}
			elseif($user instanceof AuthWebUser)
			{
				if($user->checkAccess('admin.admin_main'))
					return true;
			}
			else
				throw new CException('Komponent WebUser nie jest instancją AuthWebUser.');
		}
		throw new CHttpException(401, Yii::t('AdminModule.main', 'Moduł dostępny tylko dla zalogowanych administratorów.'));
	}

	/**
	 * Returns the URL to the published assets folder.
	 * @return string the URL.
	 */
	protected function getAssetsUrl()
	{
		if (isset($this->_assetsUrl))
			return $this->_assetsUrl;
		else
		{
			$assetsPath = Yii::getPathOfAlias('admin.assets');
			$assetsUrl = Yii::app()->assetManager->publish($assetsPath, false, -1, $this->forceCopyAssets);

			return $this->_assetsUrl = $assetsUrl;
		}
	}

	/**
	 * Returns the module version number.
	 * @return string the version.
	 */
	public function getVersion()
	{
		return '1.0.0';
	}
}
