var bindEvent = function(scope, fn){
	return function(){
		return fn.apply(scope, arguments);
	}
};

function FlatCanvas()
{
	this.canvas = null;
	this.canvasContext = null;

	this.image = null;
	this.imagePosition = [0, 0];

	this.stateObject = null;
	this.buttonCancelObject = null;
    this.imagePreviewObject = null;

	this.currentState = null;
	this.currentStateData = null;

	this.zoom = 1;
	this.alignLines = false;

	this.addedFlats = []; //{flatId: 1, coordinates: [[0, 0], [1, 1]]}

	this.balloon = null;

	this.states = {
		'NO_ACTION': 'Wybierz akcję',
		'PANNING': 'Przesuwanie',
		'CHOOSE_AREA': 'Dodawanie, zaznacz mieszkanie na mapie',
		'CHOOSE_FLAT': 'Usuwanie, wybierz mieszkanie na mapie'
	};
}

/**
 * Init canvas
 *
 * @param img Image object
 * @param stateObject object which will contain current state of canvas
 * @param buttonCancelObject button cancel object
 * @param imagePreviewObject preview object (will be cleaned on some states)
 */
FlatCanvas.prototype.init = function(img, stateObject, buttonCancelObject, imagePreviewObject){
	//Canvas element
	this.canvas = document.getElementById('levelCanvas');

	this.image = img;
	this.stateObject = stateObject;
	this.buttonCancelObject = buttonCancelObject;
    this.imagePreviewObject = imagePreviewObject;

	//Change state
	this.changeState('NO_ACTION');

	//Resize canvas to image width & height
	var parentWidth = parseInt(window.getComputedStyle(this.canvas.parentNode).width);
	this.canvas.width = (!isNaN(parentWidth) ? parentWidth : this.image.width);
	this.canvas.height = this.image.height;

	//Create context
	this.canvasContext = this.canvas.getContext('2d');

	this.canvasContext.imageSmoothingEnabled = false;

    var self = this;

	//Bind events
	this.canvas.addEventListener('click', bindEvent(this, this.clickHandler), false);
	this.canvas.addEventListener('mousemove', bindEvent(this, this.mouseMoveHandler), false);
	this.canvas.addEventListener('mousedown', bindEvent(this, this.mouseDownHandler), false);
	this.canvas.addEventListener('mouseup', bindEvent(this, this.mouseUpHandler), false);
    this.canvas.addEventListener('mousewheel', function(event){
        self.mouseWheelHandler(event);
        event.preventDefault();
        return false;
    }, false);

	this.buttonCancelObject.addEventListener('click', bindEvent(this, this.cancelClickHandler), false);

	//Draw
	this.redraw();
};

/**
 * Redraw canvas
 */
FlatCanvas.prototype.redraw = function(){
	//Clear canvas
	this.canvasContext.save();
	this.canvasContext.setTransform(1, 0, 0, 1, 0, 0);
	this.canvasContext.clearRect(0, 0, this.canvas.width, this.canvas.height);
	this.canvasContext.restore();

	//Draw level image
	this.canvasContext.drawImage(this.image, this.imagePosition[0], this.imagePosition[1]);

	//Draw added flats
	var i, k, coordinatesLength, startCoordinates, end, endCoordinates;
	for(i = 0; i < this.addedFlats.length; ++i){
		coordinatesLength = this.addedFlats[i].coordinates.length;
		for(k = 0; k < coordinatesLength; ++k){
			this.canvasContext.beginPath();
			this.canvasContext.strokeStyle = '#00ff00';

			startCoordinates = this.sumCoordinates(this.imagePosition, this.addedFlats[i].coordinates[k]);
			this.canvasContext.moveTo(startCoordinates[0], startCoordinates[1]);

			end = (k + 1) % coordinatesLength;
			endCoordinates = this.sumCoordinates(this.imagePosition, this.addedFlats[i].coordinates[end]);
			this.canvasContext.lineTo(endCoordinates[0], endCoordinates[1]);

			this.canvasContext.stroke();
		}
	}

	//Draw temp polygons
	if(this.currentState === 'CHOOSE_AREA' && this.currentStateData.clickedCoordinates.length){
		//Clone array
		var combinedArray = this.currentStateData.clickedCoordinates.slice(0);

		//Add temp coordinate if any
		if(this.currentStateData.tempCoordinate !== null){
			combinedArray.push(this.currentStateData.tempCoordinate);
		}

		for(i = 0; i < combinedArray.length; ++i){
			if((i + 1) >= combinedArray.length && combinedArray.length <= 2){
				//If adding first line then do not show red one (it will overlay first line)
				continue;
			}

			this.canvasContext.beginPath();
			this.canvasContext.strokeStyle = '#ff00ff';

			startCoordinates = this.sumCoordinates(this.imagePosition, combinedArray[i]);
			this.canvasContext.moveTo(startCoordinates[0], startCoordinates[1]);

			end = (i + 1) % combinedArray.length;
			if(end < i){ //Last line, use different color
				this.canvasContext.strokeStyle = '#cc0000';
			}

			endCoordinates = this.sumCoordinates(this.imagePosition, combinedArray[end]);
			this.canvasContext.lineTo(endCoordinates[0], endCoordinates[1]);

			this.canvasContext.stroke();
		}
	}

	//Draw balloon
	if(this.balloon !== null){
		//Little hacky because we are basing on zoom to prevent zooming balloon
		
		this.canvasContext.font = (16 / this.zoom) + 'px Arial';

		//Calculate width of balloon
		var textDimensions = this.canvasContext.measureText(this.balloon.content);

		//Background
		this.canvasContext.rect(this.balloon.position[0] + (10 / this.zoom), this.balloon.position[1] + (10 / this.zoom), textDimensions.width + (20 / this.zoom), (26 / this.zoom));
		this.canvasContext.fillStyle = '#cccccc';
		this.canvasContext.fill();

		//Text
		this.canvasContext.fillStyle = 'black';
		this.canvasContext.fillText(this.balloon.content, this.balloon.position[0] + (20 / this.zoom), this.balloon.position[1] + (29 / this.zoom));
	}
};

/**
 * Mouse click handler
 * Used in adding line while selecting area
 *
 * @param evt
 */
FlatCanvas.prototype.clickHandler = function(evt){
	var coords;
	if(this.currentState === 'CHOOSE_AREA'){
		coords = this.diffCoordinates([~~(evt.offsetX / this.zoom), ~~(evt.offsetY / this.zoom)], this.imagePosition);
		if(this.alignLines && this.currentStateData.clickedCoordinates.length){ //Align to closest 45 degree
			coords = this.alignToClosestHalfFullDegree(this.currentStateData.clickedCoordinates.slice(-1)[0], coords);
		}

		if(this.areCoordinatesEqual(this.currentStateData.clickedCoordinates[0], coords)){
			//Coordinates are equal (or almost), finish drawing
			this.currentStateData.clickedCoordinates.push(coords);

			this.addFlat(this.currentStateData.flatId, this.currentStateData.flatName, this.currentStateData.clickedCoordinates);
			this.clearState();
		}
		else{ //Not equal, so just add new coord
			this.currentStateData.clickedCoordinates.push(coords);
		}

		this.redraw();
	}
	else if(this.currentState === 'CHOOSE_FLAT'){
		coords = this.diffCoordinates([~~(evt.offsetX / this.zoom), ~~(evt.offsetY / this.zoom)], this.imagePosition);

		var flat = this.findFlatByCoordinates(coords);
		if(flat !== null){
			this.removeFlat(flat.flatId);
			this.clearState();
		}
	}
};

/**
 * Mouse down handler
 * Used in panning
 */
FlatCanvas.prototype.mouseDownHandler = function(){
	if(this.currentState === 'NO_ACTION' && this.zoom > 1){ //Panning start
		this.changeState('PANNING');
	}
};

/**
 * Mouse up handler
 * Used in panning
 */
FlatCanvas.prototype.mouseUpHandler = function(){
	if(this.currentState === 'PANNING'){
		this.changeState('NO_ACTION');
	}
};

/**
 * Mouse move handler
 * Used for drawing temp line while selecting area
 * Used also on panning
 *
 * @param evt
 */
FlatCanvas.prototype.mouseMoveHandler = function(evt){
	var coords;

	//Preview of line
	if(this.currentState === 'CHOOSE_AREA' && this.currentStateData.clickedCoordinates.length){
		coords = this.diffCoordinates([~~(evt.offsetX / this.zoom), ~~(evt.offsetY / this.zoom)], this.imagePosition);
		if(this.alignLines){ //Align to closest 45 degree
			coords = this.alignToClosestHalfFullDegree(this.currentStateData.clickedCoordinates.slice(-1)[0], coords);
		}

		this.currentStateData.tempCoordinate = coords;
		this.redraw();
	}
	else if(this.currentState === 'PANNING'){
		coords = [~~(evt.offsetX / this.zoom), ~~(evt.offsetY / this.zoom)];

		var tempCoordinate = this.currentStateData.tempCoordinate;
		if(tempCoordinate === null){ //First move
			this.currentStateData.tempCoordinate = coords;
			return;
		}

		var diff = this.diffCoordinates(coords, tempCoordinate);
		var tempImagePosition = this.sumCoordinates(this.imagePosition, diff);

		//Image bounds
		if(Math.abs(tempImagePosition[0]) > this.image.width || Math.abs(tempImagePosition[1]) > this.image.height){
			return;
		}

		this.imagePosition = tempImagePosition;

		this.currentStateData.tempCoordinate = coords;
		this.redraw();
	}
	else{ //No state, show balloon of hovered flat (if any)
		coords = this.diffCoordinates([~~(evt.offsetX / this.zoom), ~~(evt.offsetY / this.zoom)], this.imagePosition);

		var flat = this.findFlatByCoordinates(coords);
		if(flat !== null){
			this.balloon = {
				position: [~~(evt.offsetX / this.zoom), ~~(evt.offsetY / this.zoom)],
				content: 'Mieszkanie ID: ' + flat.flatId + ', Nazwa: ' + flat.flatName
			};

			this.redraw();
		}
		else if(this.balloon !== null){
			this.balloon = null;

			this.redraw();
		}
	}
};

/**
 * Mouse wheel handler, used for scrolling
 * @param evt
 */
FlatCanvas.prototype.mouseWheelHandler = function(evt){
    var diff = [evt.wheelDeltaX / 3, evt.wheelDeltaY / 3];
    var tempImagePosition = this.sumCoordinates(this.imagePosition, diff);

    //Image bounds
    if(Math.abs(tempImagePosition[0]) > this.image.width || Math.abs(tempImagePosition[1]) > this.image.height){
        return;
    }

    this.imagePosition = tempImagePosition;

    this.redraw();
};

/**
 * Change canvas state
 *
 * @param stateId
 * @param additionalData
 */
FlatCanvas.prototype.changeState = function(stateId, additionalData){
	this.currentState = stateId;
	this.stateObject.innerHTML = this.states[stateId];

	this.canvas.className = stateId.toLowerCase();

	if(stateId === 'CHOOSE_AREA'){ //Add flat to map, additional data must be flat id
		this.buttonCancelObject.disabled = false;

		this.currentStateData = {
			flatId: additionalData[0],
			flatName: additionalData[1],
			clickedCoordinates: [],
			tempCoordinate: null
		};
	}
	else if(stateId === 'PANNING'){
		this.currentStateData = {
			tempCoordinate: null
		};
	}
	else if(stateId === 'CHOOSE_FLAT'){
		this.buttonCancelObject.disabled = false;
	}
};

/**
 * Clear canvas state
 */
FlatCanvas.prototype.clearState = function(){
	this.changeState('NO_ACTION');
	this.currentStateData = null;

    if(this.imagePreviewObject !== null){
        this.imagePreviewObject.src = '';
    }

	this.buttonCancelObject.disabled = true;
};

/**
 * Change zoom of canvas
 *
 * @param newZoom
 */
FlatCanvas.prototype.changeZoom = function(newZoom){
	if(newZoom <= 1){ //Reset image position
		this.imagePosition = [0, 0];
	}
	else{ //Center
		var zoom = newZoom / this.zoom;
		this.imagePosition = [this.imagePosition[0] * zoom, this.imagePosition[1] * zoom];
	}

	var backZoom = 1 / this.zoom;
	this.zoom = newZoom;

	this.redraw();

	//Go back to first zoom
	this.canvasContext.scale(backZoom, backZoom);

	//Then scale up
	this.canvasContext.scale(newZoom, newZoom);
};

/**
 * Align two coordinates to half-full degree (0, 45, 90, 135..)
 *
 * @param oldCoords
 * @param newCoords
 * @returns {Array} new coordinates
 */
FlatCanvas.prototype.alignToClosestHalfFullDegree = function(oldCoords, newCoords){
	if(newCoords[0] === oldCoords[0] || newCoords[1] === oldCoords[1]){ //Full degree, return
		return newCoords;
	}

	var diff = [newCoords[0] - oldCoords[0], newCoords[1] - oldCoords[1]];

	//22.5, 67.5, 112.5, 157.5, 202.5, 247.5, 292.5, 337.5
	var tanBreakpoints = {
		//0: [0, 22.5], //337.5 360
		45: [22.5, 67.5],
		90: [67.5, 112.5],
		135: [112.5, 157.5],
		180: [157.5, 202.5],
		225: [202.5, 247.5],
		270: [247.5, 292.5],
		315: [292.5, 337.5]
	};

	//Convert tan to degree
	var deg = Math.atan2(diff[1], diff[0]) * 180 / Math.PI;
	if(deg < 0){
		deg = 360 + deg;
	}

	var alignTo = 'x';
	var lineDegree = 0;
	for(var i in tanBreakpoints){
		if(tanBreakpoints.hasOwnProperty(i)){
			if(deg >= tanBreakpoints[i][0] && deg <= tanBreakpoints[i][1]){ //If fits between breakpoints
				if(deg >= i){ //Align to y instead of x
					alignTo = 'y';
				}

				lineDegree = ~~i;
				break;
			}
		}
	}

	var newX, newY;
	if(lineDegree === 0 || lineDegree === 180){ //y = 1
		return [newCoords[0], oldCoords[1]];
	}
	else if(lineDegree === 90 || lineDegree === 270){ //x = 1
		return [oldCoords[0], newCoords[1]];
	}
	else if(lineDegree === 45 || lineDegree === 225){ //y = -x
		if(alignTo === 'x'){ //y is const
			newX = oldCoords[0] + newCoords[1] - oldCoords[1];
			return [newX, newCoords[1]];
		}
		else{ //x is const
			newY = oldCoords[1] + newCoords[0] - oldCoords[0];
			return [newCoords[0], newY];
		}
	}

	//y = x
	if(alignTo === 'x'){ //x is const
		newY = oldCoords[1] + oldCoords[0] - newCoords[0];
		return [newCoords[0], newY];
	}
	else{ //y is const
		newX = oldCoords[0] - newCoords[1] + oldCoords[1];
		return [newX, newCoords[1]];
	}
};

/**
 * Checks if coordinates are equal with toleration (if any)
 *
 * @param a
 * @param b
 * @param toleration if undefined then 5
 * @returns {boolean}
 */
FlatCanvas.prototype.areCoordinatesEqual = function(a, b, toleration){
	//In px
	toleration = toleration || 5;

	if(a === undefined || b === undefined){
		return false;
	}

	//Check if b is in circle with radius 3 and center point a
	return ((b[0] - a[0]) * (b[0] - a[0]) + (b[1] - a[1]) * (b[1] - a[1]) <= toleration * toleration);
};

/**
 * Add flat to registry
 *
 * @param flatId
 * @param flatName
 * @param coordinates
 */
FlatCanvas.prototype.addFlat = function(flatId, flatName, coordinates){
	var obj = {
		flatId: flatId,
		flatName: flatName,
		coordinates: coordinates
	};

	this.addedFlats.push(obj);
};

/**
 * Remove flat by id
 *
 * @param flatId
 */
FlatCanvas.prototype.removeFlat = function(flatId){
	for(var i = 0; i < this.addedFlats.length; ++i){
		if(this.addedFlats[i].flatId === flatId){
			this.addedFlats.splice(i, 1);
			break;
		}
	}
};

/**
 * Get added flats
 *
 * @returns {Array}
 */
FlatCanvas.prototype.getFlats = function(){
	return this.addedFlats;
};

/**
 * Finds flat by coordinates using ray-casting alghoritm
 * @see http://www.ecse.rpi.edu/Homepages/wrf/Research/Short_Notes/pnpoly.html
 *
 * @param coordinates
 * @returns {*}
 */
FlatCanvas.prototype.findFlatByCoordinates = function(coordinates){
	var x = coordinates[0], y = coordinates[1];

	var q, i, j, inside;
	for(q = 0; q < this.addedFlats.length; ++q){
		var vs = this.addedFlats[q].coordinates;
		inside = false;
		for(i = 0, j = vs.length - 1; i < vs.length; j = i++){
			var xi = vs[i][0], yi = vs[i][1];
			var xj = vs[j][0], yj = vs[j][1];

			var intersect = ((yi > y) != (yj > y))
				&& (x < (xj - xi) * (y - yi) / (yj - yi) + xi);
			if(intersect) inside = !inside;
		}

		if(inside){
			return this.addedFlats[q];
		}
	}

	return null;
};

/**
 * Calculate sum of coordinates
 *
 * @param a
 * @param b
 * @returns {Array}
 */
FlatCanvas.prototype.sumCoordinates = function(a, b){
	return [a[0] + b[0], a[1] + b[1]];
};

/**
 * Calculate difference between coordinates
 *
 * @param a
 * @param b
 * @returns {Array}
 */
FlatCanvas.prototype.diffCoordinates = function(a, b){
	return [a[0] - b[0], a[1] - b[1]];
};

/**
 * Cancel button click handler
 */
FlatCanvas.prototype.cancelClickHandler = function(){
	this.clearState();
	this.redraw();

	this.buttonCancelObject.disabled = true;
};

//Inject into global namespace
window.FlatCanvas = FlatCanvas;