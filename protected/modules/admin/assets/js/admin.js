function dialogBox(title, message)
{
	if($('#jqueryUiDialogBox').size() === 0){
		$('body').append('<div title="" id="jqueryUiDialogBox"></div>');
	}

	$('#jqueryUiDialogBox').attr('title', title).html(message).dialog({
		modal: true,
		buttons: {
			Ok: function()
			{
				$(this).dialog("close");
			}
		}
	});
}

$(document).ready(function(){
	$('.onChangeCorrectPrice').change(function(){
		var fixedVal = $(this).val().replace(',', '.');
		//now run a regexp :)
		var regexpTest = /([0-9]+)(\.([0-9]+))?/i;
		if(regexpTest.test(fixedVal)){
			var matches = regexpTest.exec(fixedVal);
			//matches[1] - price first
			//matches[3] - price second

			var finalPrice = parseFloat(parseInt(matches[1]) + '.' + matches[3]).toFixed(2);
			$(this).val(finalPrice);
		}
		else{
			$(this).val('');
		}
	});
});