<?php
$this->breadcrumbs = Array(
    'Elementy' => Array('index'),
    $model->title,
);
?>

    <h1>Edycja elementu <?php echo $model->title; ?></h1>

<?php echo $this->renderPartial('_form', Array('model' => $model)); ?>