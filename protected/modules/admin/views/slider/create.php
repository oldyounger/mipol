<?php
$this->breadcrumbs = Array(
    'Elementy' => Array('index'),
    'Nowy',
);
?>

    <h1>Nowy element</h1>

<?php echo $this->renderPartial('_form', Array('model' => $model)); ?>