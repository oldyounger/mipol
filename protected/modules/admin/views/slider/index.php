<?php
$this->breadcrumbs = Array(
    'Elementy' => Array('index'),
    'Lista',
);
?>

<h1>Elementy slidera</h1>

<p>
    Możesz użyć następujących warunków (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
    lub <b>=</b>) na początku każdej reguły wyszukiwania aby określić jak powinny zostać porównane wartości z bazy.
</p>

<?php
$this->widget('bootstrap.widgets.TbGridView', Array(
    'id' => 'event-grid',
    'ajaxUpdate' => false,
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => Array(
        Array(
            'name' => 'id',
            'sortable' => true,
            'htmlOptions' => Array('width' => '50px'),
        ),
        Array(
            'name' => 'title',
            'sortable' => true,
            'htmlOptions' => Array('width' => '575px'),
        ),
        Array(
            'name' => 'hidden',
            'sortable' => true,
            'value' => '$data->hidden ? Yii::t("app", "Yes") : Yii::t("app", "No")',
            'filter' => Array('0' => Yii::t('app', 'No'), '1' => Yii::t('app', 'Yes')),
            'htmlOptions' => Array('width' => '65px'),
        ),
        Array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{update} {delete}', // '{view} {update} {delete}'
        ),
    ),
));
?>
