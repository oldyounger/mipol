<?php
$this->breadcrumbs = Array(
    'Elementy' => Array('index'),
    'Kolejność',
);

/* @var $form TbActiveForm */
/* @var $model SliderMain */
/* @var $this SliderController */
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', Array(
	'id' => 'slider-form',
	'enableAjaxValidation' => false,
	'htmlOptions' => Array('enctype' => 'multipart/form-data'),
));
?>

<h1>Kolejność elementów slidera</h1>

<?php
$items = Array();
foreach($carouselData as $slider):
	$text = '
		<div class="thumbnail" style="text-align: left;">
			<input type="hidden" name="SliderMain[image_sort][]" value="'.$slider['id'].'" />
			<img src="'.$slider['thumb'].'" alt="" />
			<h5>ID '.$slider['id'].'</h5>
			<h5>'.($slider['label'] ? $slider['label'] : '&nbsp;').'</h5>
	';

	$text .= '
		</div>
	';

	$items['slider-'.$slider['id']] = $text;
endforeach;

$this->widget('zii.widgets.jui.CJuiSortable', Array(
	'itemTemplate' => '<li class="span2" id="{id}">{content}</li>',
	'items' => $items,
	'htmlOptions' => Array('class' => 'thumbnails'),
	'options' => Array(
		'cursor' => 'move',
	),
));
?>

<div class="row">
	<div class="span12">
		<div class="form-actions">
			<?php
			$this->widget('bootstrap.widgets.TbButton', Array(
				'buttonType' => 'submit',
				'type' => 'primary',
				'label' => 'Zapisz',
			));
			?>
		</div>
	</div>
</div>

<?php $this->endWidget(); ?>