<?php
/* @var $this SliderController */
/* @var $image Image */
/* @var $file File */
?>

<?php if(isSet($image) && $image !== NULL): ?>
	<li class="span2">
		<div class="thumbnail">
			<a target="_blank" href="<?php echo $image->getImageUrl(); ?>" class="thumbnail fancybox" rel="tooltip" data-title="" data-original-title="">
				<img src="<?php echo $image->getImageUrl(Array(
					'resize' => Array(
						'width' => 160,
						'height' => 160,
						'fit' => 'outside'
					),
					'crop' => Array(
						'width' => 160,
						'height' => 160
					)
				)); ?>" alt="" />
			</a>

			<div class="caption">
				<a class="btn btn-danger" href="<?php echo $this->createUrl('imagedelete', Array('id' => $image->getPrimaryKey(), 'parent_id' => $model->getPrimaryKey())); ?>" onclick="return confirm('Czy na pewno chcesz usunąć?');">Usuń</a>
			</div>
		</div>
	</li>
<?php elseif(isSet($file) && $file !== NULL): ?>
	<li class="span2">
		<div class="thumbnail">
			<div class="text-center">
				<a target="_blank" href="<?php echo $file->getFileUrl(); ?>" class="fancybox" rel="tooltip" data-title="" data-original-title="">
					<?php echo $file->name; ?>
				</a>
			</div>
			<div class="caption">
				<a class="btn btn-danger" href="<?php echo $this->createUrl('filedelete', Array('id' => $file->getPrimaryKey(), 'parent_id' => $model->getPrimaryKey())); ?>" onclick="return confirm('Czy na pewno chcesz usunąć?');">Usuń</a>
			</div>
		</div>
	</li>
<?php endif; ?>