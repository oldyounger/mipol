<?php
/* @var $form TbActiveForm */
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', Array(
    'id' => 'slider-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => Array('enctype' => 'multipart/form-data'),
));
?>
    <p class="help-block">Pola oznaczone <span class="required">*</span> są wymagane.</p>

    <div class="row">
        <div class="span9">
            <?php echo $form->errorSummary($model); ?>
        </div>
    </div>

    <div class="row">
        <div class="span7">
            <div class="row-fluid">
                <?php
                echo $form->textFieldRow($model, 'title', Array('class' => 'span10', 'maxlength' => 128));
                echo $form->textFieldRow($model, 'order', Array('class' => 'span10', 'maxlength' => 128));
                echo $form->textFieldRow($model, 'link', Array('class' => 'span10', 'maxlength' => 512));
                echo $form->ckEditorRow($model, 'content');
                echo $form->dropDownListRow($model, 'hidden',
                    Array(0 => Yii::t('app', 'No'), 1 => Yii::t('app', 'Yes'))
                );
                ?>
            </div>
        </div>
        <?php
        ?>

        <div class="span5">
            <?php $uploadifyFieldInput = 'image_id'; ?>
            <?php echo CHtml::label(Slidermain::model()->getAttributeLabel($uploadifyFieldInput), $uploadifyFieldInput.'_uploadifyID-button'); ?>

            <?php
            $this->widget('ext.uploadify.widgets.CUploadifyWidget', Array(
                'name' => $uploadifyFieldInput,
                'uploadLimit' => 1,
                'fileTypeDesc' => Yii::t('app', 'Image Files'),
				'fileTypeExts' => '*.jpg;*.gif;*.png;*.jpeg;*.jpe;*.jfif;*.jif',
                'buttonText' => 'Dodaj plik..',
                'buttonClass' => 'btn btn-success',
                'onUploadSuccess' => "function(object, file, data, response){
					var json = $.parseJSON(file);
					$('.thumbnailInput_".$uploadifyFieldInput."').val(json.extra);
				}",
            ));
            ?>

            <div class="hiddenContainer">
                <input type="hidden" class="thumbnailInput_<?php echo $uploadifyFieldInput; ?>" name="Slidermain[<?php echo $uploadifyFieldInput; ?>]" value="<?php echo $model->$uploadifyFieldInput; ?>" />
            </div>

            <ul class="thumbnails">
                <?php
                if($model->$uploadifyFieldInput){
                    $this->renderPartial('_thumb', Array('image' => Image::model()->findByPk($model->$uploadifyFieldInput), 'model' => $model));
                }
                ?>
            </ul>
        </div>

    </div>

    <div class="row">
        <div class="span12">
            <div class="form-actions">
                <?php
                $this->widget('bootstrap.widgets.TbButton', Array(
                    'buttonType' => 'submit',
                    'type' => 'primary',
                    'label' => ($model->isNewRecord ? 'Utwórz' : 'Zapisz'),
                ));
                ?>
            </div>
        </div>
    </div>

<?php $this->endWidget(); ?>