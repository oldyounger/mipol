<?php
$this->breadcrumbs = Array(
	'Użytkownicy' => Array('index'),
	'Nowy',
);
?>

<h1>Nowy użytkownik</h1>

<?php echo $this->renderPartial('_form', Array('model' => $model)); ?>