<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', Array(
	'id' => 'user-form',
	'enableAjaxValidation' => false,
)); ?>
	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model, 'email', Array('class' => 'span5', 'maxlength' => 128)); ?>

	<?php echo $form->passwordFieldRow($model, 'password', Array('class' => 'span5', 'maxlength' => 64, 'value' => '')); ?>

	<?php echo $form->passwordFieldRow($model, 'repassword', Array('value' => '', 'class' => 'span5', 'maxlength' => 64)); ?>

	<?php echo $form->textFieldRow($model, 'firstname', Array('class' => 'span5', 'maxlength' => 32)); ?>

	<?php echo $form->textFieldRow($model, 'lastname', Array('class' => 'span5', 'maxlength' => 32)); ?>

	<?php echo $form->checkboxRow($model, 'active', Array()); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', Array(
			'buttonType' => 'submit',
			'type' => 'primary',
			'label' => $model->isNewRecord ? 'Utwórz' : 'Zapisz',
		)); ?>
	</div>
<?php $this->endWidget(); ?>
