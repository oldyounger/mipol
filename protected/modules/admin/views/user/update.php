<?php
$this->breadcrumbs = Array(
	'Użytkownicy' => Array('index'),
	$model->getName(),
);
?>

<h1>Edycja użytkownika ID <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', Array('model' => $model)); ?>