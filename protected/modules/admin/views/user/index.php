<?php
$this->breadcrumbs = Array(
	'Użytkownicy' => Array('index'),
	'Lista',
);
?>

<h1>Lista użytkowników</h1>

<p>
Możesz użyć następujących warunków (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
lub <b>=</b>) na początku każdej reguły wyszukiwania aby określić jak powinny zostać porównane wartości z bazy.
</p>

<?php $this->widget('bootstrap.widgets.TbGridView', Array(
	'id' => 'user-grid',
	'ajaxUpdate' => false,
	'dataProvider' => $model->search(),
	'filter' => $model,
	'columns' => Array(
		Array(
			'name' => 'id',
			'sortable' => true,
			'htmlOptions' => Array('width' => '50px'),
		),
		Array(
			'name' => 'email',
			'sortable' => true,
		),
		Array(
			'name' => 'firstname',
			'sortable' => true,
			'htmlOptions' => Array('width' => '175px'),
		),
		Array(
			'name' => 'lastname',
			'sortable' => true,
			'htmlOptions' => Array('width' => '175px'),
		),
		Array(
			'class' => 'bootstrap.widgets.TbButtonColumn',
			'template' => '{update} {delete}' // '{view} {update} {delete}'
		),
	),
)); ?>
