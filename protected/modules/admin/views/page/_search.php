<?php
	$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', Array(
		'action' => Yii::app()->createUrl($this->route),
		'method' => 'get',
	));
?>

		<?php echo $form->textFieldRow($model, 'id', Array('class' => 'span5', 'maxlength' => 10)); ?>

		<?php echo $form->textFieldRow($model, 'name', Array('class' => 'span5', 'maxlength' => 128)); ?>

		<?php echo $form->textFieldRow($model, 'url', Array('class' => 'span5', 'maxlength' => 128)); ?>

		<?php echo $form->textFieldRow($model, 'description', Array('class' => 'span5', 'maxlength' => 128)); ?>

		<?php echo $form->textFieldRow($model, 'keywords', Array('class' => 'span5', 'maxlength' => 128)); ?>

		<?php echo $form->textFieldRow($model, 'content', Array('class' => 'span5', 'maxlength' => 128)); ?>

	<div class="form-actions">
		<?php
			$this->widget('bootstrap.widgets.TbButton', Array(
				'buttonType' => 'submit',
				'type' => 'primary',
				'label' => 'Szukaj',
			));
		?>
	</div>

<?php $this->endWidget(); ?>
