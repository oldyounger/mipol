<?php
	/* @var $form TbActiveForm */
	/* @var $model Page */
	/* @var $this PageController */
	$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', Array(
		'id' => 'product-form',
		'enableAjaxValidation' => false,
		'htmlOptions' => Array('enctype' => 'multipart/form-data')
	));
?>

	<p class="help-block">Pola oznaczone <span class="required">*</span> są wymagane.</p>

	<div class="row">
		<div class="span10">
			<?php echo $form->errorSummary($model); ?>
		</div>
	</div>

	<div class="row">
		<div class="span10">

            <?php echo $form->textFieldRow($model, 'name', array('class' => 'span5', 'maxlength' => 128)); ?>

            <?php echo $form->textFieldRow($model, 'url', array('class' => 'span5', 'maxlength' => 128)); ?>

            <?php echo $form->dropDownListRow($model, 'internal',
                Array(0 => Yii::t('app', 'No'), 1 => Yii::t('app', 'Yes'))
            ); ?>

            <?php /*echo $form->dropDownListRow($model, 'show_slider',
			Array(0 => Yii::t('app', 'No'), 1 => Yii::t('app', 'Yes'))
		);*/
            ?>

            <?php echo $form->textAreaRow($model, 'description', array('rows' => 6, 'cols' => 50, 'class' => 'span8')); ?>

            <?php echo $form->textAreaRow($model, 'keywords', array('rows' => 6, 'cols' => 50, 'class' => 'span8')); ?>

            <?php echo $form->labelEx($model, 'content'); ?>
            <?php $this->widget('ext.editMe.widgets.ExtEditMe', array('model' => $model, 'attribute' => 'content')); ?>

            <?php /*echo $form->dropDownListRow($model, 'gallery_id',
                CHtml::listData(Gallery::model()->findAll(), 'id', 'title'),
                Array('empty' => '- wybierz -')
            );*/ ?>
<!--			--><?php //echo $form->textAreaRow($model, 'test' /*array('rows' => 6, 'cols' => 50, 'class' => 'span8')*/); ?>

        </div>
	</div>

	<div class="row">
		<div class="span10">
			<div class="form-actions">
				<?php $this->widget('bootstrap.widgets.TbButton', array(
						'buttonType'=>'submit',
						'type'=>'primary',
						'label'=>$model->isNewRecord ? 'Utwórz' : 'Zapisz',
					)); ?>
			</div>
		</div>
	</div>


<?php $this->endWidget(); ?>
