<?php
$this->breadcrumbs = Array(
	'Strony' => Array('index'),
	$model->name => Array('view', 'id' => $model->id),
	'Aktualizuj',
);
?>

<h1><?php echo $model->name; ?></h1>

<?php echo $this->renderPartial('_form', Array('model' => $model)); ?>