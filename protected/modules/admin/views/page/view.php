<?php
$this->breadcrumbs = Array(
	'Strony' => Array('index'),
	$model->name => Array('update','id' => $model->id),
	'Podgląd',
);

$this->menu[] = Array(
	'label' => 'Usuń stronę',
	'icon' => 'trash',
	'url' => '#',
	'linkOptions' => Array(
		'submit' => Array(
			'delete',
			'id' => $model->id
		),
		'confirm' => 'Na pewno chcesz usunąć stronę?'
	)
);

$this->menu[] = Array(
	'label' =>'Edytuj stronę',
	'icon' => 'pencil',
	'url' => Array('page/update', 'id' => $model->id),
);

?>

<h1>Strona &bdquo;<?php echo $model->name; ?>&rdquo;</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data' => $model,
	'attributes' => Array(
		'id',
		'name',
		Array(
            'label' => 'Adres',
            'type' => 'raw',
            'value' => $model->getLink(false, 'self::url', '_blank'),
		),
		'description',
		'keywords',
		'content:html',
        Array(
            'label' => 'Galeria',
            'value' => ($model->gallery ? $model->gallery->title : 'brak'),
        ),
	),
)); ?>
