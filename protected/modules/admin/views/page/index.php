<?php
$this->breadcrumbs = Array(
	'Strony' => Array('index'),
	'Lista',
);

Yii::app()->clientScript->registerScript('search', "
	$('.search-button').click(function(){
		$('.search-form').toggle();
		return false;
	});
	$('.search-form form').submit(function(){
		$.fn.yiiGridView.update('page-grid', {
			data: $(this).serialize()
		});
		return false;
	});
");
?>

<h1>Lista Stron</h1>

<p>Możesz użyć następujących warunków (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
lub <b>=</b>) na początku każdej reguły wyszukiwania aby określić jak powinny zostać porównane wartości z bazy.
</p>

<?php echo CHtml::link('Wyszukiwanie zaawansowane','#', Array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
	
<?php
	$this->renderPartial('_search', Array(
		'model'=>$model,
	));
?>
</div><!-- search-form -->

<?php
	$this->widget('bootstrap.widgets.TbGridView', Array(
		'id' => 'page-grid',
		'dataProvider' => $model->search(),
		'filter' => $model,
		'columns' => Array(
			Array(
				'name' => 'id',
				'sortable' => true,
				'htmlOptions' => Array( 'width' => '40px' ),
			),
			Array(
				'name' => 'name',
				'sortable' => true,
			),
			Array(
				'name' => 'url',
				'type' => 'raw',
				'value' => '$data->getLink(false, "self::url", "_blank")',
				'sortable' => true,
			),
			Array(
				'class' => 'bootstrap.widgets.TbButtonColumn',
			),
		),
	));
?>
