<?php
/** @var Gallery $model */
$this->breadcrumbs = Array(
	'Pliki' => Array('index'),
	'Lista',
);
?>

<h1>Lista plików</h1>

<p>
	Możesz użyć następujących warunków (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
	lub <b>=</b>) na początku każdej reguły wyszukiwania aby określić jak powinny zostać porównane wartości z bazy.
</p>


<?php
	$this->widget('bootstrap.widgets.TbGridView', Array(
		'id' => 'gallery-grid',
		'ajaxUpdate' => false,
		'dataProvider' => $model->search(),
		'filter' => $model,
		'columns' => Array(

			Array(
				'name' => 'name',
				'sortable' => true,

			),





			Array(
				'name' => 'uniqueDir',
				'filter' => false,
				'value'=>function($data) {

					$extension=explode('.',$data->uniqueDir);
					$extension=$extension[1];
					if($extension !== 'zip' && $extension !== 'pdf') {

					 echo '<img src="'.$data->uniqueDir.'" style="width:auto;max-height:100px;display:block;"/>';


					}





				  }



),

			Array(

				'value'=>function($data) {

					$extension=explode('.',$data->uniqueDir);
					$extension=$extension[1];
					if($extension !== 'zip' && $extension !== 'pdf') {

						echo '<i>Ścieżka dostępu: </i> '.$data->uniqueDir;


					}

					else if($extension == 'pdf'){
						echo  '<i>Ścieżka dostępu:</i> '.$data->uniqueDir;
					}
					else if($extension == 'zip'){
						echo  '<i>Ścieżka dostępu:</i> '.$data->uniqueDir;
					}



				}



			),

			Array(

				'name'=>'mime',
				'sortable' => false,
				'filter' => false,
				'value'=> function($data){

					$extension=explode('.',$data->uniqueDir);
					$extension=$extension[1];
					if($extension !== 'zip' && $extension !== 'pdf') {



						echo '<i class="fa fa-file-image-o"></i>';

					}

					else if($extension == 'pdf'){
						echo  '<i class="fa fa-file-pdf-o"></i>';
					}

					else if($extension == 'zip'){
						echo  '<i class="fa fa-file-archive-o"></i> ';
					}

				},



			),
			/*Array(
				'name' => 'type',
				'sortable' => true,
				'value' => function($data){
					return Gallery::$VALID_TYPES_NAME[$data->type];
				},
				'filter' => false,
				//'value' => '$data->hidden ? Yii::t("app", "Yes") : Yii::t("app", "No")',
				//'filter' => Array('0' => Yii::t('app', 'No'), '1' => Yii::t('app', 'Yes')),
			),*/
//			Array(
//				'name' => 'date',
//				'sortable' => true,
//			),
//			Array(
//				'name' => 'hidden',
//				'sortable' => true,
//				'value' => '$data->hidden ? Yii::t("app", "Yes") : Yii::t("app", "No")',
//				'filter' => Array('0' => Yii::t('app', 'No'), '1' => Yii::t('app', 'Yes')),
//				'htmlOptions' => Array('width' => '65px'),
//			),
			Array(
				'class' => 'bootstrap.widgets.TbButtonColumn',
				'template' => '{delete}', // '{view} {update} {delete}'
			),
		),
	));
?>
