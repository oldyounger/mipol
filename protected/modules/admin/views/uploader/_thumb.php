<?php
	/* @var $this GalleryController */
	/* @var $image Image */
	/* @var $file File */
?>

<?php if(isSet($image) && $image !== NULL): ?>
	<li class="span2">
		<div class="thumbnail">
			<a target="_blank" href="<?php echo $image->getImageUrl(); ?>" class="thumbnail fancybox" rel="tooltip" data-title="" data-original-title="">
				<img src="<?php echo $image->getImageUrl(Array(
					'resize' => Array(
						'width' => 160,
						'height' => 160,
						'fit' => 'outside'
					),
					'crop' => Array(
						'width' => 160,
						'height' => 160
					)
				)); ?>" alt="" />
			</a>
			<?php if(!isSet($viewOnly) || $viewOnly == false): ?>
				<div class="caption">
					<a class="btn btn-danger" href="<?php echo $this->createUrl('imagedelete', Array('id' => $image->getPrimaryKey(), 'parent_id' => $model->getPrimaryKey(), 'model' => get_class($image))); ?>" onclick="return confirm('Czy na pewno chcesz usunąć?');">Usuń</a>

					<?php if(isSet($canSelectThumb) && $canSelectThumb == true): ?>
						<a class="btn <?php echo ($model->thumb_id == $image->id ? 'btn-primary' : 'btn-info'); ?>" href="<?php echo $this->createUrl('setthumb', Array('id' => $image->getPrimaryKey(), 'parent_id' => $model->getPrimaryKey(), 'model' => get_class($image))); ?>" title="Ustaw jako miniaturka galerii">Gł.</a>
					<?php endif; ?>
				</div>
			<?php endif; ?>
		</div>
	</li>
<?php elseif(isSet($file) && $file !== NULL): ?>
	<li class="span2">
		<div class="thumbnail">
			<div class="text-center">
				<a target="_blank" href="<?php echo $file->getFileUrl(); ?>" class="fancybox" rel="tooltip" data-title="" data-original-title="">
					<?php echo $file->name; ?>
				</a>
			</div>
			<?php if(!isSet($viewOnly) || $viewOnly == false): ?>
				<div class="caption">
					<a class="btn btn-danger" href="<?php echo $this->createUrl('filedelete', Array('id' => $file->getPrimaryKey(), 'parent_id' => $model->getPrimaryKey(), 'model' => get_class($file))); ?>" onclick="return confirm('Czy na pewno chcesz usunąć?');">Usuń</a>
				</div>
			<?php endif; ?>
		</div>
	</li>
<?php endif; ?>