<?php
/* @var $form TbActiveForm */
/* @var $model Uploader */
/* @var $this UploaderController */
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', Array(
	'id' => 'uploader-form',
	'enableAjaxValidation' => false,
	'htmlOptions' => Array('enctype' => 'multipart/form-data'),
));



?>


	<div class="row">
		<div class="span9">
			<?php echo $form->errorSummary($model); ?>
		</div>
	</div>



	<div class="span5">


		<?php
		$this->widget('ext.uploadify.widgets.CUploadifyWidget', array(
			'name'=>'name',
			'uploadLimit' => 5,
			'fileTypeDesc' => '*',
			'fileTypeExts' => '*.pdf;*.jpg;*.gif;*.png;*.jpeg;*.jpe;*.jfif;*.jif;*.zip',
			'buttonText' => 'Dodaj plik..',
			'buttonClass' => 'btn btn-success',
			'onUploadSuccess' => "function(object, file, data, response){
					var json = $.parseJSON(file);
//
				}",
		));
		?>











	</div>

	<div class="row">
		<div class="span12">
			<div class="form-actions">
				<?php
				$this->widget('bootstrap.widgets.TbButton', Array(
					'buttonType' => 'submit',
					'type' => 'primary',
					'label' => 'Zapisz',
				));
				?>
			</div>
		</div>
	</div>



<?php $this->endWidget(); ?>