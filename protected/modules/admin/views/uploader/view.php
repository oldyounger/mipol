<?php
/* @var $this GalleryController */
/* @var $model Gallery */

$this->breadcrumbs = Array(
	'Galerie' => Array('index'),
	'Pokaż',
);
?>

<h1>Pokaż galerię ID <?php echo $model->id; ?></h1>

<?php
	$this->widget('bootstrap.widgets.TbDetailView', Array(
		'data' => $model,
		'attributes' => Array(
			'title',
			'url',
			'date_formatted',
			'hidden' => Array(
				'name' => 'hidden',
                'value' => ($model->hidden ? Yii::t('app', 'Yes') : Yii::t('app', 'No')),
				/*'value' => function($data){
					return $data->hidden ? Yii::t('app', 'Yes') : Yii::t('app', 'No');
				},*/
			),
			'view_count',

		),
	));
?>

<?php /*if($model->headerImage): ?>
	<h1>Zdjęcie tytułowe</h1>
	<ul class="thumbnails">
		<?php $this->renderPartial('_thumb', Array('image' => $model->headerImage, 'model' => $model, 'viewOnly' => true)); ?>
	</ul>
<?php endif;*/ ?>

<?php if($model->thumb): ?>
	<h1>Miniaturka</h1>
	<ul class="thumbnails">
		<?php $this->renderPartial('_thumb', Array('image' => $model->thumb, 'model' => $model, 'viewOnly' => true)); ?>
	</ul>
<?php endif; ?>

<?php if($model->galleryImages): ?>
	<h1>Zdjęcia</h1>
	<ul class="thumbnails">
		<?php
		foreach($model->galleryImages as $md):
			$this->renderPartial('_thumb', Array('image' => $md, 'model' => $model, 'viewOnly' => true));
		endforeach;
		?>
	</ul>
<?php endif; ?>
