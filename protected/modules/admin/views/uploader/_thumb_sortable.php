<?php
/** @var $this GalleryController */
/** @var $image GalleryImage */
/** @var $file File */
/** @var $model Gallery */
?>

<?php
$items = Array();

foreach($model->$field as $image):
	$text = '
		<div class="thumbnail">
			<input type="hidden" name="Gallery[image_sort][]" value="'.$image->id.'" />
			<a target="_blank" href="'.$image->getImageUrl().'" class="thumbnail fancybox" rel="tooltip" data-title="" data-original-title="">
				<img src="'.$image->getImageUrl(Array(
					'resize' => Array(
						'width' => 160,
						'height' => 160,
						'fit' => 'outside'
					),
					'crop' => Array(
						'width' => 160,
						'height' => 160
					)
				)).'" alt="" />
			</a>
	';

	if(!isSet($viewOnly) || $viewOnly == false):
		$text .= '
				<div class="caption">
					<a class="btn btn-danger" href="'.$this->createUrl('imagedelete', Array('id' => $image->getPrimaryKey(), 'parent_id' => $model->getPrimaryKey(), 'model' => get_class($image))).'" onclick="return confirm(\'Czy na pewno chcesz usunąć?\');">Usuń</a>
		';

		if(isSet($canSelectThumb) && $canSelectThumb == true):
			$text .= '<a class="btn '.($model->thumb_id == $image->id ? 'btn-primary' : 'btn-info').'" href="'.$this->createUrl('setthumb', Array('id' => $image->getPrimaryKey(), 'parent_id' => $model->getPrimaryKey(), 'model' => get_class($image))).'" title="Ustaw jako miniaturka galerii">Gł.</a>';
			/*$text .= '<div style="height: 5px;"></div>';
			$text .= '<a data-id="'.$image->id.'" data-movie-type="' . $image->movie_type .'" data-movie-url="'.($image->movie_id != NULL ? CMovie::getURL($image->movie_type, $image->movie_id) : NULL).'" class="onClickSetMovie btn '.($image->movie_id != NULL ? 'btn-primary' : 'btn-info').'" href="#" title="Wstaw film">Film</a>';
			$text .= '<div style="height: 5px;"></div>';
			$text .= '<a data-id="'.$image->id.'" data-description="' . $image->description . '" class="onClickSetDescription btn '.(mb_strlen($image->description) > 0 ? 'btn-primary' : 'btn-info').'" href="#" title="Zmień opis">Opis</a>';*/
		endif;

		$text .= '
				</div>
		';
	endif;

	$text .= '
		</div>
	';

	$items['image-'.$image->id] = $text;
endforeach;

$this->widget('zii.widgets.jui.CJuiSortable', Array(
	'itemTemplate' => '<li class="span2" id="{id}">{content}</li>',
	'items' => $items,
	'htmlOptions' => Array('class' => 'thumbnails'),
	'options' => Array(
		'cursor' => 'move',
	),
));

$movieTypeSelect = CTools::fixSingleQuote(CTools::removeNewLine(CHtml::dropDownList('movieType', NULL, CMovie::$TYPES, Array(
	'id' => 'inputMovieType',
))));

$youtubeUrl = $this->createUrl('//admin/gallery/imageMovie');
Yii::app()->clientScript->registerScript('MovieScript', <<<EOT
	var currentItem = null;
	$('.onClickSetMovie').click(function(){
		currentItem = $(this);
		var type = $(this).attr('data-movie-type');
		var value = $(this).attr('data-movie-url');

		var selectParent = $('<div></div>');
		selectParent.append('{$movieTypeSelect}');
		$('#inputMovieType option[value="'+type+'"]', selectParent).attr('selected', 'selected');
		bootbox.dialog('Podaj link do filmu<br />'+selectParent.html()+'<br /><input type="text" style="display: block; width: 300px;" id="inputMovieLink" value="'+value+'">', [
		{
			"label" : "Zapisz",
			"class" : "btn-primary",
			"callback": function(){
				//var movieId = '';
				var movieUrl = $('#inputMovieLink').val();
				var movieType = $('#inputMovieType').val();
				/*if(result.length > 0){
					res = /(?:https?:\/\/)?(?:www\.)?youtu(?:\.be|be\.com)\/(?:watch\?v=)?([a-zA-Z0-9\-_]+)/.exec(result);
					movieId = res[1];
				}*/

				$.ajax({
					url: '{$youtubeUrl}',
					type: 'POST',
					dataType: 'json',
					data: {
						'imageId': currentItem.attr('data-id'),
						'movieUrl': movieUrl,
						'movieType': movieType,
					},
				}).done(function(response){
					try {
						if(response.success == true){
							bootbox.alert('Pomyślnie zmieniono.');
							currentItem.attr('data-movie-url', response.url);
							currentItem.attr('data-movie-type', response.type);
						}
						else{
							bootbox.alert('Nie udało się zmienić filmu. Prawdopodobnie adres jest nieprawidłowy.');
						}
					}
					catch(e){
						bootbox.alert('Wystąpił błąd.');
					}
				}).fail(function(){
					bootbox.alert('Wystąpił błąd.');
				});
			}
		},
		{
			"label" : "Anuluj",
			"class" : "btn-gray",
			"callback": function(){
				//
			}
		}]);

		return false;
	});
EOT
, CClientScript::POS_READY
);

$descriptionUrl = $this->createUrl('//admin/gallery/imageDescription');
Yii::app()->clientScript->registerScript('DescriptionScript', <<<EOT
	var currentItem = null;
	$('.onClickSetDescription').click(function(){
		currentItem = $(this);
		var value = $(this).attr('data-description');
		bootbox.dialog('Opis <textarea style="display: block; width: 500px; height: 300px;" id="inputDescription">'+value+'</textarea>', [
		{
			"label" : "Zapisz",
			"class" : "btn-primary",
			"callback": function(){
				var youtubeId = '';
				var result = $('#inputDescription').val();

				$.ajax({
					url: '{$descriptionUrl}',
					type: 'POST',
					dataType: 'json',
					data: {
						'imageId': currentItem.attr('data-id'),
						'description': result,
					},
				}).done(function(response){
					try {
						if(response.success == true){
							bootbox.alert('Pomyślnie zmieniono.');
							currentItem.attr('data-description', result);
						}
					}
					catch(e){
						bootbox.alert('Wystąpił błąd.');
					}
				}).fail(function(){
					bootbox.alert('Wystąpił błąd.');
				});
			}
		},
		{
			"label" : "Anuluj",
			"class" : "btn-gray",
			"callback": function(){
				//
			}
		}]);

		return false;
	});
EOT
	, CClientScript::POS_READY
);
?>