<?php
/* @var $form TbActiveForm */
/* @var $model Gallery */
/* @var $this GalleryController */
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', Array(
	'id' => 'gallery-form',
	'enableAjaxValidation' => false,
	'htmlOptions' => Array('enctype' => 'multipart/form-data'),
));



?>
	<p class="help-block">Pola oznaczone <span class="required">*</span> są wymagane.</p>

	<div class="row">
		<div class="span9">
			<?php echo $form->errorSummary($model); ?>
		</div>
	</div>

	<div class="row">
		<div class="span7">
			<div class="row-fluid">
				<?php
				echo $form->textFieldRow($model, 'title', Array('class' => 'span10', 'maxlength' => 128));

				echo $form->textFieldRow($model, 'url', Array('class' => 'span10', 'maxlength' => 134));






				/*echo $form->dropDownListRow($model, 'type',
					Gallery::$VALID_TYPES_NAME
				);*/
                echo $form->hiddenField($model, 'type', Array('value' => Gallery::TYPE_PHOTO));
				echo $form->dropDownListRow($model, 'home',
					Array(0 => Yii::t('app', 'Nie'), 1 => Yii::t('app', 'Tak'))
				);

				echo $form->datepickerRow($model, 'date_formatted', Array('options' => Array('dateFormat' => 'dd.mm.yy')));

				echo $form->dropDownListRow($model, 'hidden',
					Array(0 => Yii::t('app', 'No'), 1 => Yii::t('app', 'Yes'))
				);
				?>
			</div>
		</div>

		<?php /*<div class="span5">
			<?php echo CHtml::label('Zdjęcie tytułowe', 'header_image_uploadifyID-button'); ?>

			<?php
			$this->widget('ext.uploadify.widgets.CUploadifyWidget', array(
				'name' => 'header_image',
				'uploadLimit' => 1,
				'fileTypeDesc' => Yii::t('app', 'Image Files'),
				'fileTypeExts' => '*.jpg;*.gif;*.png;*.jpeg;*.jpe;*.jfif;*.jif',
				'buttonText' => 'Dodaj plik..',
				'buttonClass' => 'btn btn-success',
				'onUploadSuccess' => "function(object, file, data, response){
					var json = $.parseJSON(file);
					$('.headerImageInputThumbnail').val(json.extra);
				}",
			));
			?>

			<div class="hiddenContainer">
				<input type="hidden" class="headerImageInputThumbnail" name="Gallery[header_image]"
					   value="<?php echo $model->header_image; ?>"/>
			</div>

			<ul class="thumbnails">
				<?php
				if($model->header_image):
					$this->renderPartial('_thumb', Array('image' => Image::model()->findByPk($model->header_image), 'model' => $model));
				endif;
				?>
			</ul>
		</div> */ ?>
	</div>
	<div class="row">
		<div class="span12">
			<h1>Zdjęcia</h1>
			<?php if($model->isNewRecord): ?>
				Zapisz galerię, aby dodać zdjęcia.
			<?php else: ?>root

				<?php
				$this->widget('ext.uploadify.widgets.CUploadifyWidget', array(
					'name' => 'gallery',
					'uploader' => 'uploadify_gallery',
					'uploadLimit' => 1000,
					'fileTypeDesc' => Yii::t('app', 'Image Files'),
					'fileTypeExts' => '*.jpg;*.gif;*.png;*.jpeg;*.jpe;*.jfif;*.jif',
					'buttonText' => 'Dodaj plik..',
					'buttonClass' => 'btn btn-success',
					'formData' => Array('galleryId' => $model->id),
					'onUploadSuccess' => "function(object, file, data, response){
						var json = $.parseJSON(file);

						var inpt = $('<input type=\"hidden\">').prop('name', 'Gallery[image][]').val(json.extra);
						$('#galleryContainer').append(inpt);
				}",
				));
				?>

				<div class="hiddenContainer" id="galleryContainer">
					<?php foreach($model->galleryImages as $md): ?>
						<input type="hidden" class="galleryItem" name="Gallery[image][]"
							   value="<?php echo $md->id; ?>"/>
					<?php endforeach; ?>
				</div>

				<?php
				$this->renderPartial('_thumb_sortable', Array('model' => $model, 'field' => 'galleryImages', 'canSelectThumb' => true));
				?>
			<?php endif; ?>
		</div>
	</div>

	<div class="row">
		<div class="span12">
			<div class="form-actions">
				<?php
				$this->widget('bootstrap.widgets.TbButton', Array(
					'buttonType' => 'submit',
					'type' => 'primary',
					'label' => ($model->isNewRecord ? 'Utwórz' : 'Zapisz'),
				));
				?>
			</div>
		</div>
	</div>

<?php $this->endWidget(); ?>