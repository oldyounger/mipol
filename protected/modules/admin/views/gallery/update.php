<?php
$this->breadcrumbs = Array(
	'Galerie' => Array('index'),
	$model->title,
);

?>


<h1>Edycja galerii <?php echo $model->title; ?></h1>

<?php echo $this->renderPartial('_form', Array('model' => $model)); ?>