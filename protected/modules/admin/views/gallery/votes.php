<?php
$this->breadcrumbs = Array(
	'Miejsca' => Array('index'),
	'Głosy',
);

Yii::app()->clientScript->registerScript('search', "
	$('.search-button').click(function(){
		$('.search-form').toggle();
		return false;
	});
	
	$('.search-form form').submit(function(){
		$.fn.yiiGridView.update('user-grid', {
			data: $(this).serialize()
		});
		return false;
	});
");
?>

<h1>Lista głosów</h1>

<p>
	Możesz użyć następujących warunków (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
	lub <b>=</b>) na początku każdej reguły wyszukiwania aby określić jak powinny zostać porównane wartości z bazy.
</p>

<?php
	$this->widget('bootstrap.widgets.TbGridView', Array(
		'id' => 'club-grid',
		'ajaxUpdate' => false,
		'dataProvider' => $model->search(),
		'filter' => $model,
		'columns' => Array(
			Array(
				'name' => 'id',
				'sortable' => true,
				'htmlOptions' => Array('width' => '50px'),
			),
			Array(
				'name' => 'name',
				'sortable' => true,
				'htmlOptions' => Array('width' => '175px'),
			),
			Array(
				'name' => 'votes_count',
				'value' => '$data->votes_count',
				'htmlOptions' => Array('width' => '55px'),
			),
			Array(
				'name' => 'votes_grade',
				'value' => '$data->votes_grade',
				'htmlOptions' => Array('width' => '55px'),
			),
			/*Array(
				'name' => 'clubVotesGrade',
				'htmlOptions' => Array('width' => '55px'),
			),*/
		),
	));
?>
