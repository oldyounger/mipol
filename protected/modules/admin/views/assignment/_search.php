<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', Array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
));
?>
	<?php echo $form->textFieldRow($model, 'email', Array('class' => 'span5', 'maxlength' => 128)); ?>

	<div class="form-actions">
		<?php
		$this->widget('bootstrap.widgets.TbButton', Array(
			'buttonType' => 'submit',
			'type' => 'primary',
			'label' => 'Szukaj',
		));
		?>
	</div>

<?php $this->endWidget(); ?>