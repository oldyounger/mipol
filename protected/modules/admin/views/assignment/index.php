<?php
/* @var $this AssignmentController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    Yii::t('AdminModule.main', 'Assignments'),
);

Yii::app()->clientScript->registerScript('search', "
	$('.search-button').click(function(){
		$('.search-form').toggle();
		return false;
	});

	$('.search-form form').submit(function(){
		$.fn.yiiGridView.update('assignments-grid', {
			data: $(this).serialize()
		});
		return false;
	});
");
?>

<h1><?php echo Yii::t('AdminModule.main', 'Assignments'); ?></h1>

<?php echo CHtml::link('Wyszukiwanie zaawansowane','#', Array('class' => 'search-button btn')); ?>
<div class="search-form" style="display: none;">
    <?php
    $this->renderPartial('_search', Array(
        'model' => $model,
    ));
    ?>
</div>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'assignments-grid',
    'type' => 'striped hover',
    'dataProvider' => $model->search(),
	'emptyText' => Yii::t('AdminModule.main', 'No assignments found.'),
	'template'=>"{items}\n{pager}",
    'columns' => array(
        array(
            'header' => 'Użytkownik',
			'name' => 'user',
			'type' => 'raw',
            'value' => 'CHtml::link($data->email, Yii::app()->createUrl(\'view\', Array(\'id\' => $data->id)))',
			/*'value' => function($data){
				return CHtml::link($data->email, $this->createUrl('view', Array('id' => $data->id)));
			}*/
            //'class' => 'AuthAssignmentNameColumn',
        ),
        /*array(
            'header' => 'Przypisane elementy',
			'name' => 'auth',
			'type' => 'raw',
            'value' => 'implode("<br />", $realAuths[$data->id])',*/
			/*'value' => function($data) use($realAuths){
				if(isSet($realAuths[$data->id])){
					return implode('<br />', $realAuths[$data->id]);
				}

				return '-';
			},*/
		/*	'htmlOptions' => Array(
				'class' => 'assignment-items-column',
			),
            //'class' => 'AuthAssignmentItemsColumn',
        ),*/
		Array(
			'class' => 'bootstrap.widgets.TbButtonColumn',
			'template' => '{view}' // '{view} {update} {delete}'
		),
    ),
)); ?>
