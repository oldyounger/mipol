<?php
/* @var $this AssignmentController */
/* @var $model User */
/* @var $authItemDp AuthItemDataProvider */
/* @var $formModel AddAuthItemForm */
/* @var $form TbActiveForm */
/* @var $assignmentOptions array */

$this->breadcrumbs = array(
	Yii::t('AdminModule.main', 'Assignments') => array('index'),
	CHtml::value($model, $this->module->userNameColumn),
);
?>

<h1><?php echo CHtml::encode(CHtml::value($model, $this->module->userNameColumn)); ?>
	<small><?php echo Yii::t('AdminModule.main', 'Assignments'); ?></small>
</h1>

<div class="row">
	<div class="span6">
		<h3>
			<?php echo Yii::t('AdminModule.main', 'Permissions'); ?>
			<small><?php echo Yii::t('AdminModule.main', 'Items assigned to this user'); ?></small>
		</h3>

		<?php $this->widget('bootstrap.widgets.TbGridView', Array(
			'type' => 'striped condensed hover',
			'dataProvider' => $authItemDp,
			'emptyText' => Yii::t('AdminModule.main', 'This user does not have any assignments.'),
			'hideHeader' => true,
			'template' => "{items}",
			'columns' => Array(
				Array(
					'name' => 'authDescription',
                    'value' => '$data["description"]',
					/*'value' => function($data){
						return $data['description'];
					},*/
				),
				/*Array(
					'name' => 'authRevoke',
                    'value' => 'Yii::app()->controller->widget("bootstrap.widgets.TbButton", Array(
                        "type" => "link",
                        "size" => "mini",
                        "icon" => "remove",
                        "url" => array("revoke", "itemName" => $data["name"], "userId" => $model->{Yii::app()->controller->module->userIdColumn}),
                        "htmlOptions" => array("rel" => "tooltip", "title" => Yii::t("AdminModule.main", "Revoke")),
                    ));',*/
					/*'value' => function($data) use($model){
						$this->widget('bootstrap.widgets.TbButton', Array(
							'type' => 'link',
							'size' => 'mini',
							'icon' => 'remove',
							'url' => array('revoke', 'itemName' => $data['name'], 'userId' => $model->{$this->module->userIdColumn}),
							'htmlOptions' => array('rel' => 'tooltip', 'title' => Yii::t('AdminModule.main', 'Revoke')),
						));
					},*/
				//),
			),
		)); ?>

		<?php if(!empty($assignmentOptions)): ?>

			<h4><?php echo Yii::t('AdminModule.main', 'Assign permission'); ?></h4>

			<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
				'type' => 'inline',
			)); ?>

			<?php echo $form->dropDownListRow($formModel, 'items', $assignmentOptions, array('label' => false)); ?>

			<?php $this->widget('bootstrap.widgets.TbButton', array(
				'buttonType' => 'submit',
				'label' => Yii::t('AdminModule.main', 'Assign'),
			)); ?>

			<?php $this->endWidget(); ?>

		<?php endif; ?>
	</div>
</div>