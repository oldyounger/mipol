<?php
/* @var $form TbActiveForm */
/* @var $model Configuration */
/* @var $this ConfigurationController */
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', Array(
	'id' => 'configuration-form',
	'enableAjaxValidation' => false,
	'htmlOptions' => Array('enctype' => 'multipart/form-data'),
));
?>
	<p class="help-block">Pola oznaczone <span class="required">*</span> są wymagane.</p>

	<div class="row">
		<div class="span9">
			<?php echo $form->errorSummary($model); ?>
		</div>
	</div>

	<div class="row">
		<div class="span7">
			<div class="row-fluid">
				<?php
				echo $form->textFieldRow($model, 'value', Array('class' => 'span10', 'maxlength' => 250));
				?>

				<h4>Opis</h4>
				<div class="well">
					<?php echo $model->description; ?>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="span12">
			<div class="form-actions">
				<?php
				$this->widget('bootstrap.widgets.TbButton', Array(
					'buttonType' => 'submit',
					'type' => 'primary',
					'label' => ($model->isNewRecord ? 'Utwórz' : 'Zapisz'),
				));
				?>
			</div>
		</div>
	</div>

<?php $this->endWidget(); ?>