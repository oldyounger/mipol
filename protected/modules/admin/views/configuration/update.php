<?php
$this->breadcrumbs = Array(
	'Konfiguracja' => Array('index'),
	$model->key,
);
?>

<h1>Edycja wpisu <?php echo $model->key; ?></h1>

<?php echo $this->renderPartial('_form', Array('model' => $model)); ?>