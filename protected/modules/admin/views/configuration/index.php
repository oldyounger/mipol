<?php
/** @var Configuration $model */
$this->breadcrumbs = Array(
	'Konfiguracja' => Array('index'),
	'Lista',
);
?>

<h1>Konfiguracja</h1>

<p>
	Możesz użyć następujących warunków (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
	lub <b>=</b>) na początku każdej reguły wyszukiwania aby określić jak powinny zostać porównane wartości z bazy.
</p>

<?php
	$this->widget('bootstrap.widgets.TbGridView', Array(
		'id' => 'configuration-grid',
		'ajaxUpdate' => false,
		'dataProvider' => $model->search(),
		'filter' => $model,
		'columns' => Array(
			Array(
				'name' => 'key',
				'sortable' => true,
			),
			Array(
				'name' => 'value',
				'sortable' => true,
			),
			Array(
				'class' => 'bootstrap.widgets.TbButtonColumn',
				'template' => '{update}', // '{view} {update} {delete}'
			),
		),
	));
?>
