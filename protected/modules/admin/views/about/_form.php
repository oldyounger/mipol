<?php
	/* @var $form TbActiveForm */
	/* @var $model News */
	/* @var $this CompaniesController */
    /* @var $club array */
	$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', Array(
		'id' => 'about-form',
		'enableAjaxValidation' => false,
		'htmlOptions' => Array('enctype' => 'multipart/form-data'),
	));


?>
	<p class="help-block">Pola oznaczone <span class="required">*</span> są wymagane.</p>

	<div class="row">
		<div class="span9">
			<?php echo $form->errorSummary($model); ?>
		</div>
	</div>

	<div class="row">
		<div class="span7">
			<div class="row-fluid">
				<?php
				echo $form->textFieldRow($model, 'title', Array('class' => 'span10', 'maxlength' => 250));
				echo $form->textFieldRow($model, 'url', Array('class' => 'span10', 'maxlength' => 255));

                /*echo $form->dropDownListRow($model, 'category_id',
                    CHtml::listData(Category::model()->findAll(), 'id', 'name')
                );*/

                /*echo $form->dropDownListRow($model, 'only_logged',
                    Array(0 => Yii::t('app', 'No'), 1 => Yii::t('app', 'Yes'))
                );*/

				echo $form->dropDownListRow($model, 'hidden',
					Array(0 => Yii::t('app', 'No'), 1 => Yii::t('app', 'Yes'))
				);

				echo $form->ckEditorRow($model, 'short_content', Array(
					'toolbar' => 'small',
				));

				echo $form->ckEditorRow($model, 'extra');


				echo $form->textAreaRow($model, 'description', array('rows' => 6, 'cols' => 50, 'class' => 'span8'));
				 echo $form->textAreaRow($model, 'keywords', array('rows' => 6, 'cols' => 50, 'class' => 'span8'));
				echo $form->datepickerRow($model, 'date', Array('options' => Array('dateFormat' => 'dd.mm.yy')));
				echo $form->timepickerRow($model, 'time', Array('options' => Array('showMeridian' => false)));

				//echo $form->dropDownListRow($model,'category_news',Flat::$LEVELS, Array('class' => 'span2'));

				?>
			</div>
		</div>



        <div class="span5">
			<?php echo CHtml::label('Zdjęcie wewnątrz artykułu', 'header_image_uploadifyID-button'); ?>

			<?php
			$this->widget('ext.uploadify.widgets.CUploadifyWidget', array(
				'name' => 'header_image',
				'uploadLimit' => 1,
				'fileTypeDesc' => Yii::t('app', 'Image Files'),
				'fileTypeExts' => '*.jpg;*.gif;*.png;*.jpeg;*.jpe;*.jfif;*.jif',
				'buttonText' => 'Dodaj plik..',
				'buttonClass' => 'btn btn-success',
				'onUploadSuccess' => "function(object, file, data, response){
					var json = $.parseJSON(file);
					$('.thumbInputHeaderImage').val(json.extra);
				}",
			));
			?>

			<div class="hiddenContainer">
				<input type="hidden" class="thumbInputHeaderImage" name="About[header_image]" value="<?php echo $model->header_image; ?>" />
			</div>

			<ul class="thumbnails">
				<?php
				if($model->header_image):
					$this->renderPartial('_thumb', Array('image' => Image::model()->findByPk($model->header_image), 'model' => $model));
				endif;
				?>
			</ul>



<!--			--><?php //echo CHtml::label('test', 'test_uploadifyID-button'); ?>
<!--			--><?php
//			$this->widget('ext.uploadify.widgets.CUploadifyWidget', array(
//				'name' => 'test',
//				'uploadLimit' => 1,
//				'fileTypeDesc' => Yii::t('app', 'Image Files'),
//				'fileTypeExts' => '*.jpg;*.gif;*.png;*.jpeg;*.jpe;*.jfif;*.jif',
//				'buttonText' => 'Dodaj plik..',
//				'buttonClass' => 'btn btn-success',
//				'onUploadSuccess' => "function(object, file, data, response){
//					var json = $.parseJSON(file);
//					$('.thumbInputTestImage').val(json.extra);
//				}",
//			));
//			?>
<!---->
<!--			<div class="hiddenContainer">-->
<!--				<input type="hidden" class="thumbInputTestImage" name="Immovables[test]" value="--><?php //echo $model->test; ?><!--" />-->
<!--			</div>-->
<!---->


		</div>


        <?php /*<div class="span5">
			<?php echo CHtml::label('Galeria', 'gallery_uploadifyID-button'); ?>

			<?php
			$this->widget('ext.uploadify.widgets.CUploadifyWidget', array(
				'name' => 'gallery',
				'uploadLimit' => 100,
				'fileTypeDesc' => Yii::t('app', 'Image Files'),
				'fileTypeExts' => '*.jpg;*.gif;*.png;*.jpeg;*.jpe;*.jfif;*.jif',
				'buttonText' => 'Dodaj plik..',
				'buttonClass' => 'btn btn-success',
				'onUploadSuccess' => "function(object, file, data, response){
					var json = $.parseJSON(file);

					//$('.thumbInputBackground').val(json.extra);

					var inpt = $('<input type=\"hidden\">').prop('name', 'Blog[postedImages][]').val(json.extra);
					$('#galleryContainer').append(inpt);
				}",
			));
			?>

			<div class="hiddenContainer" id="galleryContainer">
				<?php foreach($model->blogImages as $md): ?>
					<input type="hidden" class="galleryItem" name="Blog[postedImages][]" value="<?php echo $md->id; ?>" />
				<?php endforeach; ?>
			</div>

			<ul class="thumbnails">
				<?php
				foreach($model->blogImages as $md):
					$this->renderPartial('_thumb', Array('image' => $md, 'model' => $model));
				endforeach;
				?>
			</ul>
		</div> */ ?>

	</div>

<?php /*
    <div class="row">
        <div class="span12">
            <h1>Zdjęcia</h1>
            <?php if($model->isNewRecord): ?>
                Zapisz wpis, aby dodać zdjęcia.
            <?php else: ?>
                <?php
                $this->widget('ext.uploadify.widgets.CUploadifyWidget', array(
                    'name' => 'gallery',
                    'uploader' => 'uploadify',
                    'uploadLimit' => 1000,
                    'fileTypeDesc' => Yii::t('app', 'Image Files'),
                    'fileTypeExts' => '*.jpg;*.gif;*.png;*.jpeg;*.jpe;*.jfif;*.jif',
                    'buttonText' => 'Dodaj plik..',
                    'buttonClass' => 'btn btn-success',
                    'formData' => Array('galleryId' => $model->id),
                    'onUploadSuccess' => "function(object, file, data, response){
						var json = $.parseJSON(file);

						var inpt = $('<input type=\"hidden\">').prop('name', 'News[postedImages][]').val(json.extra);
						$('#galleryContainer').append(inpt);
				}",
                ));
                ?>

                <div class="hiddenContainer" id="galleryContainer">
                    <?php foreach($model->newsImages as $md): ?>
                        <input type="hidden" class="galleryItem" name="News[postedImages][]" value="<?php echo $md->image_id; ?>"/>
                    <?php endforeach; ?>
                </div>

                <?php
                $this->renderPartial('_thumb_sortable', Array('model' => $model, 'field' => 'newsImages', 'canSelectThumb' => true));
                ?>
            <?php endif; ?>
        </div>
    </div>
 */ ?>
    
	<div class="row">
		<div class="span12">
			<div class="form-actions">
				<?php
					$this->widget('bootstrap.widgets.TbButton', Array(
						'buttonType' => 'submit',
						'type' => 'primary',
						'label' => ($model->isNewRecord ? 'Utwórz' : 'Zapisz'),
					));
				?>
			</div>
		</div>
	</div>

<?php $this->endWidget(); ?>