<?php
$this->breadcrumbs = Array(
	'O nas' => Array('index'),
	$model->title,
);
?>

<h1>Edycja wpisu <?php echo $model->title; ?></h1>

<?php echo $this->renderPartial('_form', Array('model' => $model)); ?>