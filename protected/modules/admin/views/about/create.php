<?php
$this->breadcrumbs = Array(
	'O nas' => Array('index'),
	'Nowa zakładka o nas',
);
?>

<h1>Nowa zakładka</h1>

<?php echo $this->renderPartial('_form', Array('model' => $model)); ?>
