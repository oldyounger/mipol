<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset='utf-8'>

    <?php Yii::app()->bootstrap->coreCss = true; ?>
	<?php Yii::app()->bootstrap->includeAssets(); ?>
	<?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
	<?php Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl.'/css/font-awesome.min.css'); ?>


	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>
<body>
<?php
$this->widget('bootstrap.widgets.TbNavbar', Array(
	'type' => 'inverse',
	'brand' => false,
	'fluid' => false,
	'items' => Array(
		Array(
			'class' => 'bootstrap.widgets.TbMenu',
			'items' => Array(
				Array(
					'label' => 'Użytkownicy',
					'items' => Array(
						Array(
							'label' => 'Użytkownicy',
							'url' => Array('/admin/user'),
							'visible' => Yii::app()->user->checkAccess('admin.user.read'),
						),
						Array(
							'label' => 'Uprawnienia',
							'url' => Array('/admin/assignment'),
							'visible' => Yii::app()->user->checkAccess('admin.permission.read'),
						),
					),
					'visible' => true,
				),


				Array(
					'label' => 'O nas',
					'visible' => Yii::app()->user->checkAccess('admin.page.read'),
					'url' => Array('/admin/about'),

				),

                Array(
                    'label' => 'Galerie',
                    'visible' => Yii::app()->user->checkAccess('admin.gallery.read'),
                    'url' => Array('/admin/gallery'),
                ),


				Array(
					'label' => 'System',
					'items' => Array(


                        Array(
                            'label' => 'Uploader',
                            'visible' => Yii::app()->user->checkAccess('admin.slider.read'),
                            'url' => Array('/admin/uploader'),
                        ),

						Array(
							'label' => 'Konfiguracja',
							'url' => Array('/admin/configuration'),
							'visible' => Yii::app()->user->checkAccess('admin.configuration.read'),
						),
						Array(
							'label' => 'Slider',
							'url' => Array('/admin/slider'),
							'visible' => Yii::app()->user->checkAccess('admin.configuration.read'),
						),
					),
				),
				Array(
					'label' => 'Wyloguj',
					'url' => Yii::app()->user->logoutUrl,
					'visible' => !Yii::app()->user->isGuest,
				),
			),
		),
	),
));

$this->widget('application.extensions.fancybox.EFancyBox', Array(
	'target' => '.fancybox',
	'config' => Array(
		'maxWidth' => 800,
		'maxHeight' => 600,
		'fitToView' => false,
		'width' => '70%',
		'height' => '70%',
		'autoSize' => false,
		'closeClick' => false,
		'openEffect' => 'none',
		'closeEffect' => 'none'
	),
));
?>

<div class="content-container">
	<div class="container">
		<div class="row">
			<?php
			if(isSet($this->breadcrumbs)){
				$this->widget('bootstrap.widgets.TbBreadcrumbs', array(
					'homeLink' => false,
					'links' => $this->breadcrumbs,
				));
			}
			?>
		</div>

		<div class="row">
			<?php $this->widget('bootstrap.widgets.TbAlert', Array('block' => true, 'fade' => true, 'closeText' => '&times;')); ?>
		</div>

		<div class="row">
			<?php echo $content; ?>
		</div>
	</div>
</div>
</div>

</body>
</html>
