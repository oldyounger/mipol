<?php /* @var $this Controller */ ?>
<?php $this->beginContent('/layouts/admin_layout'); ?>

<div class="row">
	<div class="span12">
		<?php
			$this->widget('bootstrap.widgets.TbMenu', Array(
				'items' => $this->menu,
				'type' => 'tabs',
				'htmlOptions' => Array('class' => 'operations'),
			));
		?>
	</div>
	
	<div class="span12">
		<?php echo $content; ?>
	</div>
</div>

<?php $this->endContent(); ?>