<?php
/* @var $this OperationController|TaskController|RoleController */
/* @var $dataProvider AuthItemDataProvider */

$this->breadcrumbs = array(
	$this->capitalize($this->getTypeText(true)),
);
?>

<h1><?php echo $this->capitalize($this->getTypeText(true)); ?></h1>

<?php $this->widget('bootstrap.widgets.TbButton', array(
    'type' => 'primary',
    'label' => Yii::t('AdminModule.main', 'Add {type}', array('{type}' => $this->getTypeText())),
    'url' => array('create'),
)); ?>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'type' => 'striped hover',
    'dataProvider' => $dataProvider,
    'emptyText' => Yii::t('AdminModule.main', 'No {type} found.', array('{type}'=>$this->getTypeText(true))),
	'template'=>"{items}\n{pager}",
    'columns' => array(
		array(
			'name' => 'name',
			'type'=>'raw',
			'header' => Yii::t('AdminModule.main', 'System name'),
			'htmlOptions' => array('class'=>'item-name-column'),
			'value' => "CHtml::link(\$data->name, array('view', 'name'=>\$data->name))",
		),
		array(
			'name' => 'description',
			'header' => Yii::t('AdminModule.main', 'Description'),
			'htmlOptions' => array('class'=>'item-description-column'),
		),
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
            'template' => '{update} {delete}',
			'viewButtonLabel' => Yii::t('AdminModule.main', 'View'),
			'viewButtonUrl' => "Yii::app()->controller->createUrl('view', array('name'=>\$data->name))",
			'updateButtonLabel' => Yii::t('AdminModule.main', 'Edit'),
			'updateButtonUrl' => "Yii::app()->controller->createUrl('update', array('name'=>\$data->name))",
			'deleteButtonLabel' => Yii::t('AdminModule.main', 'Delete'),
			'deleteButtonUrl' => "Yii::app()->controller->createUrl('delete', array('name'=>\$data->name))",
			'deleteConfirmation' => Yii::t('AdminModule.main', 'Are you sure you want to delete this item?'),
		),
    ),
)); ?>
