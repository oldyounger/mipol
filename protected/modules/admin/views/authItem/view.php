<?php
/* @var $this OperationController|TaskController|RoleController */
/* @var $item CAuthItem */
/* @var $ancestorDp AuthItemDataProvider */
/* @var $descendantDp AuthItemDataProvider */
/* @var $formModel AddAuthItemForm */
/* @var $form TbActiveForm */
/* @var $childOptions array */

$this->breadcrumbs = array(
	$this->capitalize($this->getTypeText(true)) => array('index'),
	$item->description,
);

Yii::app()->clientScript->registerCss("AuthCss", "
	.privileges {
		max-height:500px;
		overflow:auto;
		margin:10px 0px;
		border:1px solid #eeeeee;
	}
	.authItems .item {
		background:#eeeeee;
		border:1px solid #999999;
		padding:5px;
		margin:5px;
		position:relative;
		height:20px;
	}

	.authItems .item input {
		position:absolute;
		left:5px;
		top:5px;
		z-index:2;
	}
	
	.authItems .item label {
		margin:0px;
		padding:0px;
		position:absolute;
		left:0px;
		padding-left:30px;
		top:0px;
		bottom:0px;
		right:0px;
		line-height:30px;
		z-index:1;
	}
	.AuthItems .authType {
		clear:both;
		display:block;
	}
	.authItems .category {
		clear:both;
		display:block;
	}
	.authItems .item label:hover {
		background-color:#cccccc;
	}
	.authItems .item input:checked + label {
		background:#d0fcc6;
	}
");

Yii::app()->clientScript->registerScript('AuthSearch', "
	$(function(){
					$('#filterAuthItems').keyup(function(){
						var str = $(this).val().toLowerCase();
						if(str.length>2)
							$('#PrivilegesList .item').each(function(){
								if($('label',this).html().toLowerCase().indexOf(str)>-1) $(this).css('display','block');
								else $(this).css('display','none');
							});
						else
							$('#PrivilegesList .item').css('display','block');
					});
				});
				");
?>

<div class="title-row clearfix">

	<h1 class="pull-left">
		<?php echo CHtml::encode($item->description); ?>
		<small><?php echo $this->getTypeText(); ?></small>
	</h1>

	<?php $this->widget('bootstrap.widgets.TbButtonGroup', array(
		'htmlOptions'=>array('class'=>'pull-right'),
		'buttons'=>array(
			array(
				'label'=>Yii::t('AdminModule.main', 'Edit'),
				'url'=>array('update', 'name'=>$item->name),
			),
			array(
				'icon'=>'trash',
				'url'=>array('delete', 'name'=>$item->name),
				'htmlOptions'=>array(
					'confirm'=>Yii::t('AdminModule.main', 'Are you sure you want to delete this item?'),
				),
			),
		),
	)); ?>

</div>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
    'data' => $item,
    'attributes' => array(
        array(
            'name' => 'name',
			'label' => Yii::t('AdminModule.main', 'System name'),
		),
		array(
			'name' => 'description',
			'label' => Yii::t('AdminModule.main', 'Description'),
		),
		/*
        array(
			'name' => 'bizrule',
			'label' => Yii::t('AdminModule.main', 'Business rule'),
		),
		array(
			'name' => 'data',
			'label' => Yii::t('AdminModule.main', 'Data'),
		),
		*/
    ),
)); ?>

<hr />

<div class="row">

	<div class="span5">

		<h4>
			<?php echo Yii::t('AdminModule.main', 'Ancestors'); ?><br/>
			<small><?php echo Yii::t('AdminModule.main', 'Permissions that inherit this item'); ?></small>
		</h4>

		<?php $this->widget('bootstrap.widgets.TbGridView', array(
			'type' => 'striped condensed hover',
			'dataProvider'=>$ancestorDp,
			'emptyText'=>Yii::t('AdminModule.main', 'This item does not have any ancestors.'),
			'template'=>"{items}",
			'hideHeader'=>true,
			'columns'=>array(
				array(
					'class'=>'AuthItemDescriptionColumn',
					'itemName'=>$item->name,
				),
				array(
					'class'=>'AuthItemTypeColumn',
					'itemName'=>$item->name,
				),
				array(
					'class'=>'AuthItemRemoveColumn',
					'itemName'=>$item->name,
				),
			),
		)); ?>

	</div>

	<div class="span5">

		<h4>
			<?php echo Yii::t('AdminModule.main', 'Descendants'); ?><br/>
			<small><?php echo Yii::t('AdminModule.main', 'Permissions granted by this item'); ?></small>
		</h4>

		<?php $this->widget('bootstrap.widgets.TbGridView', array(
			'type' => 'striped condensed hover',
			'dataProvider'=>$descendantDp,
			'emptyText'=>Yii::t('AdminModule.main', 'This item does not have any descendants.'),
			'hideHeader'=>true,
			'template'=>"{items}",
			'columns'=>array(
				/*array(
					'class'=>'AuthItemDescriptionColumn',
					'itemName'=>$item->name,
				),
				array(
					'class'=>'AuthItemTypeColumn',
					'itemName'=>$item->name,
				),
				array(
					'class'=>'AuthItemRemoveColumn',
					'itemName'=>$item->name,
				),*/
			),
		)); ?>

	</div>

</div>
<div class="row">

	<div class="authItems">

		<?php if (!empty($childOptions)): ?>

			<h4><?php echo Yii::t('AdminModule.main', 'Add child'); ?></h4>
			Wyszukaj: <input type="text" id="filterAuthItems" value="" />

			<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
				'type'=>'inline',
			)); ?>

			<?php //echo $form->dropDownListRow($formModel, 'items', $childOptions, array('label'=>false)); ?>
			<div class="privileges" id="PrivilegesList">
				<?php $lastCategory = ''; foreach ($childOptions as $key=>$one): ?>
				<div class="category">
					<strong class="authType"><?php echo $key; ?></strong>
					<?php if(is_array($one)) foreach ($one as $key=>$el): ?>
						<div class="item">
							<?php echo CHtml::checkBox('AddAuthItemForm[items][]',false,array(
								'id' => 'c_'.$key,
								'value' => $key,
							)); ?>
							<?php echo CHtml::label($el, 'c_'.$key); ?>
						</div>
					<?php endforeach; ?>
				</div>
				<?php endforeach; ?>
			</div>
			<?php $this->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>'submit',
				'label'=>Yii::t('AdminModule.main', 'Add'),
			)); ?>

			<?php $this->endWidget(); ?>

		<?php endif; ?>

	</div>

</div>