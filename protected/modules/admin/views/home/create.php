<?php
$this->breadcrumbs = Array(
	'Strona główna' => Array('index'),
	'Nowa zakładka do strony głównej',
);
?>

<h1>Nowa zakładka</h1>

<?php echo $this->renderPartial('_form', Array('model' => $model)); ?>
