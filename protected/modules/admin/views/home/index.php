<?php
$this->breadcrumbs = Array(
	'Zakładki do strony głównej' => Array('index'),
	'Nowa zakładka',
);
?>

<h1>Lista zakładek strona główna</h1>

<p>
	Możesz użyć następujących warunków (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
	lub <b>=</b>) na początku każdej reguły wyszukiwania aby określić jak powinny zostać porównane wartości z bazy.
</p>

<?php
	$this->widget('bootstrap.widgets.TbGridView', Array(
		'id' => 'news-grid',
		'ajaxUpdate' => false,
		'dataProvider' => $model->search(),
		'filter' => $model,
		'columns' => Array(
			Array(
				'name' => 'id',
				'sortable' => true,
				'htmlOptions' => Array('width' => '50px'),
			),
			Array(
				'name' => 'title',
				'sortable' => true,
				'htmlOptions' => Array('width' => '175px'),
			),
			Array(
				'name' => 'datetime',
				'sortable' => true,
				'htmlOptions' => Array('width' => '65px'),
			),
            /*Array(
                'name' => 'only_logged',
                'sortable' => true,
                'value' => '$data->only_logged ? Yii::t("app", "Yes") : Yii::t("app", "No")',
                'filter' => Array('0' => Yii::t('app', 'No'), '1' => Yii::t('app', 'Yes')),
                'htmlOptions' => Array('width' => '65px'),
            ),*/
			Array(
				'name' => 'hidden',
				'sortable' => true,
				'value' => '$data->hidden ? Yii::t("app", "Yes") : Yii::t("app", "No")',
				'filter' => Array('0' => Yii::t('app', 'No'), '1' => Yii::t('app', 'Yes')),
				'htmlOptions' => Array('width' => '65px'),
			),
			Array(
				'class' => 'bootstrap.widgets.TbButtonColumn',
				'template' => '{update} {delete}', // '{view} {update} {delete}'
			),
		),
	));
?>
