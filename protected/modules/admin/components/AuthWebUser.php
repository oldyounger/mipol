<?php
/**
 * AuthWebUser class file.
 * @author Christoffer Niska <ChristofferNiska@gmail.com>
 * @copyright Copyright &copy; Christoffer Niska 2013-
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 * @package auth.components
 */

/**
 * Web user that allows for passing access checks when enlisted as an administrator.
 *
 * @property boolean $isAdmin whether the user is an administrator.
 */
class AuthWebUser extends CWebUser
{
	public $logoutUrl;
	public $registerUrl;

	/**
	 * Initializes the component.
	 */
	public function init()
	{
		parent::init();
		$this->setIsAdmin(in_array($this->name, Yii::app()->authManager->admins));
	}

	/**
	 * Returns whether the logged in user is an administrator.
	 * @return boolean the result.
	 */
	public function getIsAdmin()
	{
		return $this->getState('__isAdmin', false);
	}

	/**
	 * Sets the logged in user as an administrator.
	 * @param boolean $value whether the user is an administrator.
	 */
	public function setIsAdmin($value)
	{
		$this->setState('__isAdmin', $value);
	}

	/**
	 * Performs access check for this user.
	 * @param string $operation the name of the operation that need access check.
	 * @param array $params name-value pairs that would be passed to business rules associated
	 * with the tasks and roles assigned to the user.
	 * @param boolean $allowCaching whether to allow caching the result of access check.
	 * @return boolean whether the operations can be performed by this user.
	 */
	public function checkAccess($operation, $params = array(), $allowCaching = false)
	{
		return parent::checkAccess($operation, $params, $allowCaching);
	}


	/**
	 * Przekierowanie na stronę logowania z linkiem zwrotnym do strony, która wywołała żądanie logowania
	 * @throws CHttpException
	 */
	public function loginRequired()
	{
		$app=Yii::app();
		$request=$app->getRequest();

		if(!$request->getIsAjaxRequest())
			$this->setReturnUrl($request->getUrl());
		elseif(isset($this->loginRequiredAjaxResponse))
		{
			echo $this->loginRequiredAjaxResponse;
			Yii::app()->end();
		}

		if(($url=$this->loginUrl)!==null)
		{
			$url = $app->createUrl($url, array('backUrl' => urlencode(Yii::app()->request->url)));
			$request->redirect($url);
		}
		else
			throw new CHttpException(403,Yii::t('yii','Login Required'));
	}









	/**
	 * Łączy alerty tego samego typu
	 * @param type $flashArray
	 * @return string|null
	 */
	private function mergeFlash($flashArray)
	{
		if($flashArray)
		{
			$value = '';
			$total = count($flashArray);
			for($i = 0; $i < $total; $i ++)
				$value .= $flashArray[$i].($i < ($total-1) ? '<br/>' : '');
			return $value;
		}
		return null;
	}

	/**
	 * Ustawienie alertu typu "key" (append)
	 * @param string $key
	 * @param string $value
	 * @param string $defaultValue
	 */
	public function setFlash($key, $value, $defaultValue = null)
	{
		if($value == $defaultValue)
			$currentState = $value;
		else
		{
			$currentState = $this->getState(self::FLASH_KEY_PREFIX.$key, array());
			$currentState[] = $value;
		}
		$this->setState(self::FLASH_KEY_PREFIX.$key,$currentState,$defaultValue);
		$counters=$this->getState(self::FLASH_COUNTERS,array());
		if($value===$defaultValue)
			unset($counters[$key]);
		else
			$counters[$key]= isset($counters[$key]) ? $counters[$key]+1 : 1;
		$this->setState(self::FLASH_COUNTERS,$counters,array());
	}

	/**
	 * Pobiera alerty jednego typu "key"
	 * @param string $key
	 * @param sring $defaultValue
	 * @param boolean $delete
	 * @return string
	 */
	public function getFlash($key, $defaultValue = null, $delete = true)
	{
		$currentState = $this->getState(self::FLASH_KEY_PREFIX.$key,$defaultValue);
		if($delete)
			$this->setFlash($key,null);
		return $this->mergeFlash($currentState);
	}


	/**
	 * Pobranie wszystkich alertów
	 * @param boolean $delete
	 * @return array
	 */
	public function getFlashes($delete = true)
	{
		$flashes=array();
		$prefix=$this->getStateKeyPrefix().self::FLASH_KEY_PREFIX;
		$keys=array_keys($_SESSION);
		$n=strlen($prefix);
		foreach($keys as $key)
		{
			if(!strncmp($key,$prefix,$n))
			{
				$flashes[substr($key,$n)] = $this->mergeFlash($_SESSION[$key]);
				if($delete)
					unset($_SESSION[$key]);
			}
		}
		if($delete)
			$this->setState(self::FLASH_COUNTERS,array());
		return $flashes;
	}


	/**
	 * Czy są jakieś alerty?
	 * @return boolean
	 */
	public function hasFlashes()
	{
		$counters = $this->getState(self::FLASH_COUNTERS,array());

		return (
				isset($counters['success'])	||
				isset($counters['info'])	||
				isset($counters['warning'])	||
				isset($counters['error'])	||
				isset($counters['danger'])
				) ? true : false;
	}
}
