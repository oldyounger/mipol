<?php
class FormSearchGraphics extends CFormModel
{
    public $building;

    public $level;

    public function attributeLabels()
    {
        return array(
            'building' => 'Budynek',
            'level' => 'Poziom',
        );
    }

    public function rules()
    {
        return array(
            array('building, level', 'required'),
        );
    }
}