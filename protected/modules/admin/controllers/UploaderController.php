<?php
class UploaderController extends ControllerAdmin
{
	public function actions()
	{
		return Array(
			'uploadify' => Array(
				'class' => 'ext.uploadify.action.CUploadifyAction',
				'model' => 'Uploader',
			),
//			'uploadify_gallery' => Array(
//				'class' => 'ext.uploadify.action.CUploadifyAction',
//				'model' => 'UploaderImage',
//			),
		);
	}

	public function beforeRender($view)
	{
		$this->menu = Array(
			Array('label' => 'Lista plików', 'icon' => 'list', 'url' => Array('index')),
			Array('label' => 'Wyślij plik', 'icon' => 'pencil', 'url' => Array('create')),
		);

		return parent::beforeRender($view);
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		if(!Yii::app()->user->checkAccess('admin.gallery.create')){
			throw new CHttpException(403, Yii::t('auth', 'Nie posiadasz wystarczających uprawnień.'));
		}

		if($user = User::getLoggedHandle()){
			$model = new Uploader;

			if(isSet($_POST['Uploader'])){
				//$cities = isSet($_POST['Event']['cities']) ? $_POST['Event']['cities'] : Array();

				$model->attributes = $_POST['Uploader'];
				if($model->save()){
					Yii::app()->user->setFlash('success', Yii::t('admin', 'Successfully added this row.'));

					$this->redirect(Array('index'));
				}
			}

			if(!Yii::app()->user->checkAccess('admin.admin_main')){
				$city = array('city_id' => $user->manager_city_id);
			}
			else{
				$city = array();
			}
		}



		$this->render('create', Array(
			'model' => $model,
			'city' => $city
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		if(!Yii::app()->user->checkAccess('admin.gallery.update')){
			throw new CHttpException(403, Yii::t('auth', 'Nie posiadasz wystarczających uprawnień.'));
		}

		$canUpdateClub = true;

		if($user = User::getLoggedHandle()){
			$model = $this->loadModel($id);
			if($model->hasImages()){ //if we have any images we cannot update club (watermark problems!)
				$canUpdateClub = false;
			}

			if(isSet($_POST['Uploader'])){
				$model->attributes = $_POST['Uploader'];
				if($model->save()){
					Yii::app()->user->setFlash('success', Yii::t('admin', 'Successfully updated this row.'));

					$this->redirect(Array('index'));
				}
			}

			if(!Yii::app()->user->checkAccess('admin.admin_main')){
				$city = array('city_id' => $user->manager_city_id);
			}
			else{
				$city = array();
			}
		}



		$this->render('update', Array(
			'model' => $model,
			'canUpdateClub' => $canUpdateClub,
			'city' => $city
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 *
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(!Yii::app()->user->checkAccess('admin.gallery.delete')){
			throw new CHttpException(403, Yii::t('auth', 'Nie posiadasz wystarczających uprawnień.'));
		}

		if(!Yii::app()->request->isPostRequest){
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isSet($_GET['ajax'])){
				Yii::app()->user->setFlash('success', Yii::t('admin', 'Successfully deleted this row.'));

				$returnUrl = (isSet($_POST['returnUrl']) ? $_POST['returnUrl'] : Array('index'));
				$this->redirect($returnUrl);
			}
		}
		else{
			throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
		}
	}

	/**
	 * Manages all models.
	 */
	public function actionIndex()
	{
		if(!Yii::app()->user->checkAccess('admin.gallery.read')){
			throw new CHttpException(403, Yii::t('auth', 'Nie posiadasz wystarczających uprawnień.'));
		}

		if($user = User::getLoggedHandle()){
			$model = new Uploader('search');
			$model->unsetAttributes();

			if(isSet($_GET['Uploader'])){
				$model->attributes = $_GET['Uploader'];
			}

			if(!Yii::app()->user->checkAccess('admin.admin_main')){
				if($user->manager_city_id == NULL){
					$user->manager_city_id = -1;
				}
				$model->manager_city_id = $user->manager_city_id;
				$model->AdminCity();
			}
		}

		$this->render('index', Array(
			'model' => $model,
		));
	}

	public function actionView($id)
	{
		if(!Yii::app()->user->checkAccess('admin.gallery.read')){
			throw new CHttpException(403, Yii::t('auth', 'Nie posiadasz wystarczających uprawnień.'));
		}

		$model = $this->loadModel($id);

		$this->render('view', Array(
			'model' => $model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 *
	 * @param integer the ID of the model to be loaded
	 *
	 * @throws CHttpException
	 * @return Uploader
	 */
	public function loadModel($id)
	{
		$model = Uploader::model()->findByPk($id);
		if($model === NULL){
			throw new CHttpException(404, 'Przykro nam, jednak strona której szukasz nie istnieje.');
		}

		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 *
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isSet($_POST['ajax']) && $_POST['ajax'] === 'gallery-form'){
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actionImageDelete($id, $parent_id, $model)
	{
		if(!Yii::app()->user->checkAccess('admin.gallery.delete')){
			throw new CHttpException(403, Yii::t('auth', 'Nie posiadasz wystarczających uprawnień.'));
		}

		$rc = new ReflectionClass($model);
		if($rc->getName() == 'Image' || ($rc->getParentClass() && $rc->getParentClass()->getName() == 'Image')){
			$className = $rc->getName();
            if($className == 'Image'){
                $mdl = Image::model();
            }
            else if($className == 'UploaderImage'){
                $mdl = UploaderImage::model();
            }

            //$mdl = $className::model();
			$image = $mdl->findByPk($id);
			if($image){
				$image->delete();
			}
		}

		$redirectUrl = $this->createUrl('update', Array('id' => $parent_id));
		$this->redirect($redirectUrl);
	}

	public function actionSetThumb($id, $parent_id, $model)
	{
		if(!Yii::app()->user->checkAccess('admin.gallery.update')){
			throw new CHttpException(403, Yii::t('auth', 'Nie posiadasz wystarczających uprawnień.'));
		}

		$redirectTo = 'update';

		$rc = new ReflectionClass($model);
		if($rc->getName() == 'UploaderImage' || ($rc->getParentClass() && $rc->getParentClass()->getName() == 'UploaderImage')){
			$className = $rc->getName();
            if($className == 'Image'){
                $mdl = Image::model();
            }
            else if($className == 'UploaderImage'){
                $mdl = UploaderImage::model();
            }
            //$mdl = $className::model();
			$image = $mdl->findByPk($id);
			if($image){
				$parent = $this->loadModel($parent_id);

				$parent->thumb_id = $id;
				$parent->save();
			}
		}

		$redirectUrl = $this->createUrl($redirectTo, Array('id' => $parent_id));
		$this->redirect($redirectUrl);
	}

	public function actionImageMovie()
	{
		$result = Array('success' => 'false');
		if(!Yii::app()->user->checkAccess('admin.gallery.update')){
			throw new CHttpException(403, Yii::t('auth', 'Nie posiadasz wystarczających uprawnień.'));
		}

		if(isSet($_POST['imageId']) && isSet($_POST['movieUrl']) && isSet($_POST['movieType'])){
			/** @var UploaderImage $model */
            $mdl = UploaderImage::model();
			$model = $mdl->findByPk((int)$_POST['imageId']);
			if($model === NULL){
				throw new CHttpException(404, 'Przykro nam, jednak strona której szukasz nie istnieje.');
			}

			$movieId = CMovie::getIDByUrl($_POST['movieType'], $_POST['movieUrl']);
			if($movieId === NULL){
				echo json_encode($result);
				Yii::app()->end();
			}

			$model->movie_id = $movieId;
			$model->movie_type = $_POST['movieType'];
			if($model->save()){
				$result['success'] = true;
				$result['url'] = CMovie::getURL($model->movie_type, $model->movie_id);
				$result['type'] = $model->movie_type;
			}

			echo json_encode($result);
			Yii::app()->end();
		}
		else{
			throw new CHttpException(400);
		}
	}

	public function actionImageDescription()
	{
		$result = Array('success' => 'false');
		if(!Yii::app()->user->checkAccess('admin.gallery.update')){
			throw new CHttpException(403, Yii::t('auth', 'Nie posiadasz wystarczających uprawnień.'));
		}

		if(isSet($_POST['imageId']) && isSet($_POST['description'])){
			/** @var UploaderImage $model */
            $mdl = UploaderImage::model();
			$model = $mdl->findByPk((int)$_POST['imageId']);
			if($model === NULL){
				throw new CHttpException(404, 'Przykro nam, jednak strona której szukasz nie istnieje.');
			}

			$model->description = $_POST['description'];
			if($model->save()){
				$result['success'] = true;
			}

			echo json_encode($result);
			Yii::app()->end();
		}
		else{
			throw new CHttpException(400);
		}
	}
}
