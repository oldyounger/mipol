<?php
class AboutController extends ControllerAdmin
{
	public function actions()
	{
		return Array(
			'uploadify' => Array(
				'class' => 'ext.uploadify.action.CUploadifyAction',
				'model' => 'Image',
				/*'resize' => Array(
                    Array('width' => 600, 'height' => 455),
                    Array('width' => 1024, 'height' => 768),
                ),*/
			),
		);
	}
	
	public function beforeRender($view)
	{
		$this->menu = Array(
			Array('label' => 'Lista zakładek o nas', 'icon' => 'list', 'url' => Array('index')),
			Array('label' => 'Nowa zakładka', 'icon' => 'pencil', 'url' => Array('create')),
		);
		
		return parent::beforeRender($view);
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 *
	 * @throws CHttpException
	 */
	public function actionCreate()
	{
		if(!Yii::app()->user->checkAccess('admin.news.create')){
			throw new CHttpException(403, Yii::t('main', 'Forbidden You don\'t have permission to access this page'));
		}

		$model = new About;
		if(isSet($_POST['About'])){
			$model->attributes = $_POST['About'];
			if($model->save()){
				Yii::app()->user->setFlash('success', Yii::t('admin', 'Successfully added this row.'));

				$this->redirect(Array('index'));
			}
		}

		$this->render('create', Array(
			'model' => $model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param integer $id: id of model
	 * @throws CHttpException
	 */
	public function actionUpdate($id)
	{
		if(!Yii::app()->user->checkAccess('admin.news.update')){
			throw new CHttpException(403, Yii::t('main', 'Forbidden You don\'t have permission to access this page'));
		}

		$model = $this->loadModel($id);
		if(isSet($_POST['About'])){
			$model->attributes = $_POST['About'];
			if($model->save()){
				Yii::app()->user->setFlash('success', Yii::t('admin', 'Successfully updated this row.'));

				$this->redirect(Array('index'));
			}
		}


        $this->render('create', Array(
            'model' => $model,
        ));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param integer $id: id of model
	 * @throws CHttpException
	 */
	public function actionDelete($id)
	{
		if(!Yii::app()->user->checkAccess('admin.news.delete')){
			throw new CHttpException(403, Yii::t('main', 'Forbidden You don\'t have permission to access this page'));
		}

		if(!Yii::app()->request->isPostRequest){ // we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isSet($_GET['ajax'])){
				Yii::app()->user->setFlash('success', Yii::t('admin', 'Successfully deleted this row.'));
				
				$returnUrl = (isSet($_POST['returnUrl']) ? $_POST['returnUrl'] : Array('index'));
				$this->redirect($returnUrl);
			}
		}
		else{
			throw new CHttpException(400, Yii::t('main', 'Invalid request. Please do not repeat this request again.'));
		}
	}

	/**
	 * Manages all models.
	 */
	public function actionIndex()
	{
		if(!Yii::app()->user->checkAccess('admin.news.read')){
			throw new CHttpException(403, Yii::t('auth', 'Nie posiadasz wystarczających uprawnień.'));
		}

		$model = new About('search');
		$model->unsetAttributes();
		
		if(isSet($_GET['About'])){
			$model->attributes = $_GET['About'];
		}

		$this->render('index', Array(
			'model' => $model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 *
	 * @param integer $id: id of model
	 * @throws CHttpException
	 * @return News
	 */
	public function loadModel($id)
	{
		$model = About::model()->findByPk($id);
		if($model === NULL){
			throw new CHttpException(404, 'Przykro nam, jednak strona której szukasz nie istnieje.');
		}
		
		return $model;
	}

    public function actionImageDelete($id, $parent_id)
    {
        if(!Yii::app()->user->checkAccess('admin.news.delete')){
            throw new CHttpException(403, Yii::t('auth', 'Nie posiadasz wystarczających uprawnień.'));
        }

        $image = Image::model()->findByPk($id);
        if($image){
            $image->delete();
        }

        $redirectUrl = $this->createUrl('update', Array('id' => $parent_id));
        $this->redirect($redirectUrl);
    }

//    public function actionImageDescription()
//    {
//        $result = Array('success' => 'false');
//        if(!Yii::app()->user->checkAccess('admin.news.update')){
//            throw new CHttpException(403, Yii::t('auth', 'Nie posiadasz wystarczających uprawnień.'));
//        }
//
//        if(isSet($_POST['imageId']) && (isSet($_POST['description']) || isSet($_POST['title']))){
//            /** @var NewsImage $model */
//            $model = NewsImage::model()->findByPk((int)$_POST['imageId']);
//            if($model === NULL){
//                throw new CHttpException(404, 'Przykro nam, jednak strona której szukasz nie istnieje.');
//            }
//
//            if(isSet($_POST['title'])){
//                $model->title = $_POST['title'];
//            }
//            if(isSet($_POST['description'])){
//                $model->description = $_POST['description'];
//            }
//            if($model->save()){
//                $result['success'] = true;
//            }
//
//            echo json_encode($result);
//            Yii::app()->end();
//        }
//        else{
//            throw new CHttpException(400);
//        }
//    }

	/**
	 * Performs the AJAX validation.
	 *
	 * @param CModel $model: model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isSet($_POST['ajax']) && $_POST['ajax'] === 'news-form'){
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
