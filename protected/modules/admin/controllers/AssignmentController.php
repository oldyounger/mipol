<?php
/**
 * AssignmentController class file.
 * @author Christoffer Niska <ChristofferNiska@gmail.com>
 * @copyright Copyright &copy; Christoffer Niska 2012-
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 * @package auth.controllers
 */

/**
 * Controller for assignment related actions.
 */
class AssignmentController extends ControllerAdmin
{
	/**
	 * Displays the a list of all the assignments.
	 */
	public function actionIndex()
	{
		if(!Yii::app()->user->checkAccess('admin.permission.read')){
			throw new CHttpException(403, Yii::t('auth', 'Nie posiadasz wystarczających uprawnień.'));
		}

		$model = new User('search');
		$model->unsetAttributes();
		if(isSet($_GET['User'])){
			$model->attributes = $_GET['User'];
		}

		$auths = $rows= Yii::app()->db->createCommand()
			->select('userid, description')
			->join(Yii::app()->authManager->itemTable, 'itemname = name')
			->from(Yii::app()->authManager->assignmentTable)
			->queryAll();

		$realAuths = Array();
		foreach($auths as $auth){
			$realAuths[$auth['userid']][] = $auth['description'];
		}

		$this->render('index', array(
			'model' => $model,
			'realAuths' => $realAuths,
		));
	}

	/**
	 * Displays the assignments for the user with the given id.
	 * @param string $id the user id.
	 * @throws CHttpException
	 */
	public function actionView($id)
	{
		if(!Yii::app()->user->checkAccess('admin.permission.read')){
			throw new CHttpException(403, Yii::t('auth', 'Nie posiadasz wystarczających uprawnień.'));
		}

		$formModel = new AddAuthItemForm();

		/* @var $am CAuthManager|AuthBehavior */
		$am = Yii::app()->getAuthManager();

		if(isset($_POST['AddAuthItemForm'])){
			$formModel->attributes = $_POST['AddAuthItemForm'];
			if($formModel->validate()){
				if(!$am->isAssigned($formModel->items, $id)){
					$am->assign($formModel->items, $id);
					if($am instanceof CPhpAuthManager){
						$am->save();
					}

					if($am instanceof ICachedAuthManager){
						$am->flushAccess($formModel->items, $id);
					}
				}
			}
		}

		/** @var User $model */
		$model = CActiveRecord::model($this->module->userClass)->findByPk($id);
		if($model === NULL){
			throw new CHttpException(404);
		}

		$auths = $rows= Yii::app()->db->createCommand()
			->select('name, description')
			->join(Yii::app()->authManager->itemTable, 'itemname = name')
			->from(Yii::app()->authManager->assignmentTable)
			->where('userid = :userId', Array('userId' => $model->id))
			->queryAll();

		$realAuths = Array();
		$authId = 0;
		foreach($auths as $auth){
			$realAuths[] = Array(
				'id' => ++$authId,
				'name' => $auth['name'],
				'description' => $auth['description'],
			);
		}

		$assignmentOptions = $this->getAssignmentOptions($id);
		if(!empty($assignmentOptions)){
			$assignmentOptions = array_merge(array('' => Yii::t('AdminModule.main', 'Select item') . ' ...'), $assignmentOptions);
		}

		$this->render('view', array(
			'model' => $model,
			'formModel' => $formModel,
			'authItemDp' => new CArrayDataProvider($realAuths),
			'assignmentOptions' => $assignmentOptions,
		));
	}

	/**
	 * Revokes an assignment from the given user.
	 * @throws CHttpException if the request is invalid.
	 */
	public function actionRevoke()
	{
		if(!Yii::app()->user->checkAccess('admin.permission.delete')){
			throw new CHttpException(403, Yii::t('auth', 'Nie posiadasz wystarczających uprawnień.'));
		}

		if(isset($_GET['itemName'], $_GET['userId'])){
			$itemName = $_GET['itemName'];
			$userId = $_GET['userId'];

			/* @var $am CAuthManager|AuthBehavior */
			$am = Yii::app()->getAuthManager();

			if($am->isAssigned($itemName, $userId)){
				$am->revoke($itemName, $userId);
				if($am instanceof CPhpAuthManager){
					$am->save();
				}

				if($am instanceof ICachedAuthManager){
					$am->flushAccess($itemName, $userId);
				}
			}

			if(!isset($_POST['ajax'])){
				$this->redirect(array('view', 'id' => $userId));
			}
		}
		else{
			throw new CHttpException(400, Yii::t('AdminModule.main', 'Invalid request.'));
		}
	}

	/**
	 * Returns a list of possible assignments for the user with the given id.
	 * @param string $userId the user id.
	 * @return array the assignment options.
	 */
	protected function getAssignmentOptions($userId)
	{
		$options = array();

		/* @var $am CAuthManager|AuthBehavior */
		$am = Yii::app()->authManager;

		$assignments = $am->getAuthAssignments($userId);
		$assignedItems = array_keys($assignments);

		/* @var $authItems CAuthItem[] */
		$authItems = $am->getAuthItems();
		foreach($authItems as $itemName => $item){
			if(!in_array($itemName, $assignedItems)){
				$options[$itemName] = $item->description;
			}
		}

		return $options;
	}
}