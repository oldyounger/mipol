<?php
class ConfigurationController extends ControllerAdmin
{
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param integer $id: id of model
	 * @throws CHttpException
	 */
	public function actionUpdate($id)
	{
		if(!Yii::app()->user->checkAccess('admin.configuration.update')){
			throw new CHttpException(403, Yii::t('main', 'Forbidden You don\'t have permission to access this page'));
		}

		$model = $this->loadModel($id);
		if(isSet($_POST['Configuration'])){
			$model->attributes = $_POST['Configuration'];
			if($model->save()){
				Yii::app()->user->setFlash('success', Yii::t('admin', 'Successfully updated this row.'));

				$this->redirect(Array('index'));
			}
		}

        $this->render('update', Array(
            'model' => $model,
        ));
	}

	/**
	 * Manages all models.
	 */
	public function actionIndex()
	{
		if(!Yii::app()->user->checkAccess('admin.configuration.read')){
			throw new CHttpException(403, Yii::t('auth', 'Nie posiadasz wystarczających uprawnień.'));
		}

		$model = new Configuration('search');
		$model->unsetAttributes();
		
		if(isSet($_GET['Configuration'])){
			$model->attributes = $_GET['Configuration'];
		}

		$this->render('index', Array(
			'model' => $model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 *
	 * @param integer $id: id of model
	 * @throws CHttpException
	 * @return Configuration
	 */
	public function loadModel($id)
	{
		$model = Configuration::model()->findByPk($id);
		if($model === NULL){
			throw new CHttpException(404, 'Przykro nam, jednak strona której szukasz nie istnieje.');
		}
		
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 *
	 * @param CModel $model: model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isSet($_POST['ajax']) && $_POST['ajax'] === 'configuration-form'){
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
