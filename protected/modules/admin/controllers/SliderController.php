<?php
class SliderController extends ControllerAdmin
{
	public function actions()
	{
		return Array(
			'uploadify' => Array(
				'class' => 'ext.uploadify.action.CUploadifyAction',
				'model' => 'Image',
			)
		);
	}

	public function beforeRender($view)
	{
		$this->menu = Array(
			Array('label' => 'Lista elementów', 'icon' => 'list', 'url' => Array('index')),
			Array('label' => 'Kolejność elementów', 'icon' => 'th', 'url' => Array('order')),
			Array('label' => 'Nowy element', 'icon' => 'pencil', 'url' => Array('create')),
		);

		return parent::beforeRender($view);
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		if(!Yii::app()->user->checkAccess('admin.slider.create')){
			throw new CHttpException(403, Yii::t('auth', 'Nie posiadasz wystarczających uprawnień.'));
		}
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		/*$insertType = Advertisement::TYPE_IMAGE;
		if($type == 'flash'){
			$insertType = Advertisement::TYPE_FLASH;
		}*/

		$model = new Slidermain();

		if(isSet($_POST['Slidermain'])){

			$model->attributes = $_POST['Slidermain'];
			if($model->save()){
				Yii::app()->user->setFlash('success', Yii::t('admin', 'Successfully added this row.'));
				$this->redirect(Array('index'));
			}
		}

		$this->render('create', Array(
			'model' => $model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		if(!Yii::app()->user->checkAccess('admin.slider.update')){
			throw new CHttpException(403, Yii::t('auth', 'Nie posiadasz wystarczających uprawnień.'));
		}

		$model = $this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isSet($_POST['Slidermain'])){

			$model->attributes = $_POST['Slidermain'];
			if($model->save()){
				Yii::app()->user->setFlash('success', Yii::t('admin', 'Successfully updated this row.'));

				$this->redirect(Array('index'));
			}
		}

		$this->render('update', Array(
			'model' => $model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(!Yii::app()->user->checkAccess('admin.slider.delete')){
			throw new CHttpException(403, Yii::t('auth', 'Nie posiadasz wystarczających uprawnień.'));
		}

		if(Yii::app()->request->isPostRequest){
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isSet($_GET['ajax'])){
				Yii::app()->user->setFlash('success', Yii::t('admin', 'Successfully deleted this row.'));

				$returnUrl = (isSet($_POST['returnUrl']) ? $_POST['returnUrl'] : Array('index'));
				$this->redirect($returnUrl);
			}
		}
		else{
			throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
		}
	}

	/**
	 * Manages all models.
	 */
	public function actionIndex()
	{
		if(!Yii::app()->user->checkAccess('admin.slider.read')){
			throw new CHttpException(403, Yii::t('auth', 'Nie posiadasz wystarczających uprawnień.'));
		}

		$model = new Slidermain('search');
		$model->unsetAttributes();

		if(isSet($_GET['Slidermain'])){
			$model->attributes = $_GET['Slidermain'];
		}

		$this->render('index', Array(
			'model' => $model,
		));
	}

	public function actionOrder()
	{
		if(!Yii::app()->user->checkAccess('admin.slider.update')){
			throw new CHttpException(403, Yii::t('auth', 'Nie posiadasz wystarczających uprawnień.'));
		}

		if(isSet($_POST['Slidermain']) && isSet($_POST['Slidermain']['image_sort'])){
			Slidermain::updateOrder($_POST['Slidermain']['image_sort']);

			$returnUrl = (isSet($_POST['returnUrl']) ? $_POST['returnUrl'] : Array('index'));
			$this->redirect($returnUrl);
		}

		$this->render('order', Array(
			'carouselData' => Slidermain::getCarouselData(),
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model = Slidermain::model()->findByPk($id);
		if($model === NULL){
			throw new CHttpException(404, 'Przykro nam, jednak strona której szukasz nie istnieje.');
		}

		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isSet($_POST['ajax']) && $_POST['ajax'] === 'slider-form'){
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actionImageDelete($id, $parent_id)
	{
		if(!Yii::app()->user->checkAccess('admin.slider.delete')){
			throw new CHttpException(403, Yii::t('auth', 'Nie posiadasz wystarczających uprawnień.'));
		}

		$image = Image::model()->findByPk($id);
		if($image){
			$image->delete();
		}

		$redirectUrl = $this->createUrl('update', Array('id' => $parent_id));
		$this->redirect($redirectUrl);
	}

	public function actionFileDelete($id, $parent_id)
	{
		if(!Yii::app()->user->checkAccess('admin.slider.delete')){
			throw new CHttpException(403, Yii::t('auth', 'Nie posiadasz wystarczających uprawnień.'));
		}

		$file = File::model()->findByPk($id);
		if($file){
			$file->delete();
		}

		$redirectUrl = $this->createUrl('update', Array('id' => $parent_id));
		$this->redirect($redirectUrl);
	}
}
