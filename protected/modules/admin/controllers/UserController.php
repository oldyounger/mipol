<?php

class UserController extends ControllerAdmin
{


	public function beforeRender($view)
	{
		$this->menu = Array(
			Array('label' => 'Lista użytkowników', 'url' => Array('index')),
			Array('label' => 'Nowy użytkownik', 'url' => Array('create')),
		);

		return parent::beforeRender($view);
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 *
	 * @throws CHttpException
	 */
	public function actionCreate()
	{
		if(!Yii::app()->user->checkAccess('admin.user.create')){
			throw new CHttpException(403, Yii::t('main', 'Forbidden You don\'t have permission to access this page'));
		}

		$model = new User;
		if(isSet($_POST['User'])){
			$model->attributes = $_POST['User'];
			if($model->save()){
				$this->redirect(Array('index'));
			}
		}

		$this->render('create', Array(
			'model' => $model,
		));
	}

	/**
	 * Updates a particular model.
	 *
	 * @param integer $id
	 *
	 * @throws CHttpException
	 */
	public function actionUpdate($id)
	{
		if(!Yii::app()->user->checkAccess('admin.user.update')){
			throw new CHttpException(403, Yii::t('main', 'Forbidden You don\'t have permission to access this page'));
		}

		$model = $this->loadModel($id);
		if(isSet($_POST['User'])){
			$model->attributes = $_POST['User'];
			if($model->save()){
				$this->redirect(Array('index'));
			}
		}

		$this->render('update', Array(
			'model' => $model,
		));
	}

	/**
	 * Deletes a particular model.
	 *
	 * @param integer $id
	 *
	 * @throws CHttpException
	 */
	public function actionDelete($id)
	{
		if(!Yii::app()->user->checkAccess('admin.user.delete')){
			throw new CHttpException(403, Yii::t('main', 'Forbidden You don\'t have permission to access this page'));
		}

		if(Yii::app()->request->isPostRequest){ //we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isSet($_GET['ajax'])){
				$this->redirect(isSet($_POST['returnUrl']) ? $_POST['returnUrl'] : Array('index'));
			}
		}
		else{
			throw new CHttpException(400, Yii::t('main', 'Invalid request. Please do not repeat this request again.'));
		}
	}

	/**
	 * Manages all models.
	 */
	public function actionIndex()
	{
		if(!Yii::app()->user->checkAccess('admin.user.read')){
			throw new CHttpException(403, Yii::t('main', 'Forbidden You don\'t have permission to access this page'));
		}

		$model = new User('search');
		$model->unsetAttributes();
		if(isSet($_GET['User'])){
			$model->attributes = $_GET['User'];
		}

		$this->render('index', Array(
			'model' => $model,
		));
	}

	/**
	 * Loads user by id
	 *
	 * @param integer $id
	 *
	 * @throws CHttpException
	 * @return User
	 */
	public function loadModel($id)
	{
		$model = User::model()->findByPk($id);
		if($model === NULL){
			throw new CHttpException(404, Yii::t('main', 'The requested page does not exist.'));
		}

		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 *
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isSet($_POST['ajax']) && $_POST['ajax'] === 'user-form'){
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
