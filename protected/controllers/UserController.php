<?php
class UserController extends Controller
{

	public $homeMenu = false;
	public $otherMenu = true;

	/**
	 * @return Array action filters
	 */
	public function filters()
	{
		return Array(
			'accessControl',
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return Array access control rules
	 */
	public function accessRules()
	{
		return Array(
			/*Array('deny',
				'actions' => array('account'),
				'users' => array('?'),
			),
			Array('allow',
				'actions' => array('account'),
				'users' => array('@'),
			),*/
		);
	}

	/**
	 * Logging in
	 */
	public function actionLogin()
	{

		$this->bodyClass = 'login_body';


		$model = new FormLogin;

		// collect user input data
		if(isSet($_POST['FormLogin'])){
			$model->attributes = $_POST['FormLogin'];
			if($model->validate() && $model->login()){
				UserLoginHistory::successful(Yii::app()->user->getId());
			}
			else{
				UserLoginHistory::failure($model->email);
			}
		}

		if(!Yii::app()->request->isAjaxRequest){
			if(!Yii::app()->user->isGuest){
				$backUrl = Yii::app()->homeUrl;
				if(isSet($_GET['backUrl'])){
					$backUrl = urldecode($_GET['backUrl']);
				}

				$this->redirect($backUrl);

				Yii::app()->end();
			}

			$this->render('login', Array(
				'model' => $model,
			));
		}
	}

	/**
	 * Logging out
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();

		if(!Yii::app()->request->isAjaxRequest){
			$this->redirect(Yii::app()->homeUrl);
		}
	}

	/**
	 * Executed beforeRender. Yii method
	 *
	 * @param string $view
	 * @return boolean
	 */
	public function beforeRender($view)
	{
		//Yii::app()->getClientScript()->registerScriptFile(Yii::app()->baseUrl.'/js/user.js');

		return parent::beforeRender($view);
	}
}
