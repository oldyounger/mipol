<?php
class HexagonController extends Controller
{
    public $activeMenu = 'activity';
    public $contentClass = 'activity';


    public $homeMenu = false;
    public $fastContact = true;
    public $otherMenu = true;

//    public function actionIndex()
//    {
//
//
//        /*if($model->only_logged && User::getLoggedHandle() == false){
//            $this->showError404();
//        }*/
//
//        $this->render('index', Array(
//        ));
//    }


    public function actionIndex()
    {

        $model = Hexagon::model()->findAll();
        $premises_site = Home::model()->findByAttributes(Array('url'=>'premises'));

        $cMail = new CMail();
        $form = new FormContact;
        if(isSet($_POST['FormContact'])){


            $form->attributes = $_POST['FormContact'];
            $params = Array(
                'name' => $form->name,
                'surname' => $form->surname,
                'email' => $form->email,
                'content' => $form->content,

            );
            if($form->validate() && $cMail->sendFromTemplate(SystemMailTemplate::TEMPLATE_CONTACT_FORM, 'biuro@columbuscapital.pl', $form->email, $params, $form->email)){

                Yii::app()->user->setFlash('contact','Dziękujemy za wysłanie wiadomości.');
                $this->refresh();
            }
            else{
                Yii::app()->user->setFlash('contact','Błąd podczas wysyłania wiadomości. Prosimy spróbować ponownie.');
                $this->refresh();
            }
        }

        $this->render('index',Array('model'=>$model,'model2'=>$form,'premises_site'=>$premises_site));
    }










    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     *
     * @param string $url: url of news
     * @throws CHttpException
     * @return News
     */
    public function loadModel($url)
    {
        $model = About::model()->findByAttributes(Array('url' => $url));
        if(!$model){
            $this->showError404();
        }

        return $model;
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     *
     * @param string $url: url of news
     * @throws CHttpException
     * @return News
     */
    public function loadModel2($url)
    {
        $model = Immovables::model()->findByAttributes(Array('url' => $url));
        if(!$model){
            $this->showError404();
        }

        return $model;
    }

}