<?php
class SiteController extends Controller
{


    public $show_get =  true;

    public $homeMenu = true;
    public $fastContact = false;
    public $otherMenu = false;





        /**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
        /*$search = new Search;
        $u = $search->getRooms();
        var_dump($u);die;*/

		$this->homePage = true;
        $this->contentClass = 'home';

        $home_site = Home::model()->findByAttributes(Array('url'=>'hexagon'));
        $about_site = Home::model()->findByAttributes(Array('url'=>'about'));
        $premises_site = Home::model()->findByAttributes(Array('url'=>'premises'));
        $gallery = Gallery::model()->findAllByAttributes(Array('home'=>1),Array('limit'=>2,'order'=>'date DESC'));


        $cMail = new CMail();
        $form = new FormContact;
        if(isSet($_POST['FormContact'])){


            $form->attributes = $_POST['FormContact'];
            $params = Array(
                'name' => $form->name,
                'surname' => $form->surname,
                'email' => $form->email,
                'content' => $form->content,

            );
            if($form->validate() && $cMail->sendFromTemplate(SystemMailTemplate::TEMPLATE_CONTACT_FORM, 'noneluke@gmail.com', $form->email, $params, $form->email)){

                Yii::app()->user->setFlash('contact','Dziękujemy za wysłanie wiadomości.');
                $this->refresh();
            }
            else{
                Yii::app()->user->setFlash('contact','Błąd podczas wysyłania wiadomości. Prosimy spróbować ponownie.');
                $this->refresh();
            }
        }


        if(($seo=Configuration::getValue('SEO'))!== null){
            $this->pageDescription = $seo;
        }

        if(Configuration::getValue('keywords')!== null && Configuration::getValue('keywords') !== ' ' && Configuration::getValue('keywords') !== 'null'){
            $this->pageKeywords = trim(Configuration::getValue('keywords'));
        }
        else{
            $this->pageKeywords='Hexagon';
        }



            $this->render('home' ,Array('model2'=>$form,'home_site'=>$home_site,'about_site'=>$about_site,'premises_site'=>$premises_site,'gallery'=>$gallery
            ));






	}


    public function actionCopy()
    {
        echo 'dis';die();
        /** @var Flat[] $flats */
        $flats = Flat::model()->findAll();
        foreach($flats as $flat){
            if($flat->pdf_file !== NULL){
                $fileName = 'th_' . $flat->pdf_file->file . '.png';

                $img = new Image;

                $fPath = $img->getDirectoryPath() . $fileName . DIRECTORY_SEPARATOR . $fileName;
                if(!file_exists($fPath)){
                    echo $fPath;die;
                }
                $size = getimagesize($fPath);

                $img->file = $fileName;
                $img->name = $fileName;
                $img->size = 65535;
                $img->width = $size[0];
                $img->height = $size[1];
                $img->uniqueDir = $fileName;
                $img->mime = 'image/png';
                $img->addedTime = new CDbExpression('NOW()');
                $img->delete = 0;
                $img->save();

                //$img = Image::model()->findByAttributes(array('file' => $fileName));

                $flat->image_id = $img->id;

                $flat->save();
            }
        }

        echo 'all is ok';
    }

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error = Yii::app()->errorHandler->error){
			if(Yii::app()->request->isAjaxRequest){
				echo $error['message'];
			}
			else{
				$this->render('error', $error);
			}
		}
	}
}