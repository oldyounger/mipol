<?php
class OfferController extends Controller
{
    public $activeMenu = 'activity';
    public $contentClass = 'activity';


    public $siteTitle = 'Wynajem';
    public $homeMenu = false;
    public $fastContact = true;
    public $otherMenu = true;




    public function actionIndex()
    {

        $model = Localscategory::model()->findAll(Array('limit'=>3));


        $cMail = new CMail();
        $form = new FormContact;
        if(isSet($_POST['FormContact'])){


            $form->attributes = $_POST['FormContact'];
            $params = Array(
                'name' => $form->name,
                'surname' => $form->surname,
                'email' => $form->email,
                'content' => $form->content,

            );
            if($form->validate() && $cMail->sendFromTemplate(SystemMailTemplate::TEMPLATE_CONTACT_FORM, 'noneluke@gmail.com', $form->email, $params, $form->email)){

                Yii::app()->user->setFlash('contact','Dziękujemy za wysłanie wiadomości.');
                $this->refresh();
            }
            else{
                Yii::app()->user->setFlash('contact','Błąd podczas wysyłania wiadomości. Prosimy spróbować ponownie.');
                $this->refresh();
            }
        }

        $this->render('index',Array('model'=>$model,'model2'=>$form));
    }

    public function actionShow($categoryId)
    {

        if($categoryId == 1)
        {
            $model = Offer::model()->findAllByAttributes(Array('category_id'=>$categoryId));
        }
        else{

            $model = Offer::model()->findAllByAttributes(Array('category_id'=>$categoryId),Array('limit'=>1,'order'=>'datetime DESC'));
        }

        $category = Localscategory::model()->findByAttributes(Array('id'=>$categoryId));



        $gallery_id = $category->category_gallery;

       if($gallery_id !== '0') {
           $gallery_url = Gallery::model()->findByAttributes(Array('id' => $gallery_id));
           $imagesList = $gallery_url->getImagesPaginated(false);
           $galleryImages = $imagesList['items'];


       }

        else{
            $galleryImages = false;
        }



//        $gallery_url =  $gallery->url;
//        if($gallery !== 0 ) {
//
//            $gallery_array = Gallery::model()->findByAttributes(Array('url' => $gallery_url));
//            $imagesList = $gallery_array->getImagesPaginated(false);
//
//            $galleryImages = $imagesList['items'];
//        }
//
//       else{
//           $galleryImages = false;
//       }









        $cMail = new CMail();
        $form = new FormContact;
        if(isSet($_POST['FormContact'])){

            $form->attributes = $_POST['FormContact'];
            $params = Array(
                'name' => $form->name,
                'surname' => $form->surname,
                'email' => $form->email,
                'content' => $form->content,

            );
            if($form->validate() && $cMail->sendFromTemplate(SystemMailTemplate::TEMPLATE_CONTACT_FORM, 'biuro@columbuscapital.pl', $form->email, $params, $form->email)){

                Yii::app()->user->setFlash('contact','Dziękujemy za wysłanie wiadomości.');
                $this->refresh();
            }
            else{
                Yii::app()->user->setFlash('contact','Błąd podczas wysyłania wiadomości. Prosimy spróbować ponownie.');
                $this->refresh();
            }
        }

        $this->render('show',Array('model'=>$model,'category'=>$category,'galleryImages'=>$galleryImages));
    }











}