<?php
class RelationinvestorController extends Controller
{
    public $activeMenu = 'activity';
    public $contentClass = 'activity';

    public $show_get =  true;
    public $homeMenu = false;
    public $fastContact = true;
    public $otherMenu = true;

//    public function actionIndex()
//    {
//
//
//        /*if($model->only_logged && User::getLoggedHandle() == false){
//            $this->showError404();
//        }*/
//
//        $this->render('index', Array(
//        ));
//    }






    public function moreShow($count,$getmore = null)
    {

        if ($getmore == null) {

            if(count($count)<5)
            {
                $this->show_get=false;
            }

        }

        else{

            if(count($count)< $getmore)
            {
                $this->show_get=false;
            }
        }

        return $this->show_get;

    }

    public function actionIndex()
    {

        $cMail = new CMail();
        $form = new FormContact;
        if(isSet($_POST['FormContact'])){


            $form->attributes = $_POST['FormContact'];
            $params = Array(
                'name' => $form->name,
                'surname' => $form->surname,
                'email' => $form->email,
                'content' => $form->content,

            );
            if($form->validate() && $cMail->sendFromTemplate(SystemMailTemplate::TEMPLATE_CONTACT_FORM, 'biuro@columbuscapital.pl', $form->email, $params, $form->email)){

                Yii::app()->user->setFlash('contact','Dziękujemy za wysłanie wiadomości.');
                $this->refresh();
            }
            else{
                Yii::app()->user->setFlash('contact','Błąd podczas wysyłania wiadomości. Prosimy spróbować ponownie.');
                $this->refresh();
            }
        }



        if(isset($_GET['show_more']))
        {
            $model = Relationinvestor::model()->findAll(array('order'=>'id DESC','limit'=>$_GET['show_more']));
        }

        else{
            $model = Relationinvestor::model()->findAll(array('order'=>'id DESC','limit'=>5));
        }


        $this->moreShow($model,isset($_GET['show_more']) ? $_GET['show_more'] : null );


        $category = Relationcategory::model()->findAll();
        //$model = Relationinvestor::model()->findAll();

//        $model = About::model()->findAll();
        $this->render('index',Array('category'=>$category,'model'=>$model,'model2'=>$form));
    }


    public function actionshowcategory($url)
    {



//        if(isset($_GET['show_more']))
//        {
//            $model = Relationinvestor::model()->findAll(array('order'=>'id DESC','limit'=>$_GET['show_more']));
//        }
//
//        else{
//            $model = Relationinvestor::model()->findAll(array('order'=>'id DESC','limit'=>5));
//        }
//
//
//        $this->moreShow($model,isset($_GET['show_more']) ? $_GET['show_more'] : null );


        $cMail = new CMail();
        $form = new FormContact;
        if(isSet($_POST['FormContact'])){


            $form->attributes = $_POST['FormContact'];
            $params = Array(
                'name' => $form->name,
                'surname' => $form->surname,
                'email' => $form->email,
                'content' => $form->content,

            );
            if($form->validate() && $cMail->sendFromTemplate(SystemMailTemplate::TEMPLATE_CONTACT_FORM, 'biuro@columbuscapital.pl', $form->email, $params, $form->email)){

                Yii::app()->user->setFlash('contact','Dziękujemy za wysłanie wiadomości.');
                $this->refresh();
            }
            else{
                Yii::app()->user->setFlash('contact','Błąd podczas wysyłania wiadomości. Prosimy spróbować ponownie.');
                $this->refresh();
            }
        }


        $category = Relationcategory::model()->findAll();



 //       if(isset($_GET['show_more']))
   //     {
         //   $model = Relationinvestor::model()->findAllByAttributes(array(array('category_news' => $url),'order'=>'id DESC','limit'=>$_GET['show_more']));
  //      }

//        else{
            $model = Relationinvestor::model()->findAllByAttributes(array('category_news' => $url),array('order'=>'datetime DESC'));
  //      }


      //  $this->moreShow($model,isset($_GET['show_more']) ? $_GET['show_more'] : null );



        $category_name = Categoryrelation::model()->findByAttributes(Array('id' => $url));

//        $model = About::model()->findAll();
        $this->render('showcategory',Array('category'=>$category,'model'=>$model,'category_name'=>$category_name,'model2'=>$form));
    }








    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     *
     * @param string $url: url of news
     * @throws CHttpException
     * @return News
     */
    public function loadModel($url)
    {
        $model = About::model()->findByAttributes(Array('url' => $url));
        if(!$model){
            $this->showError404();
        }

        return $model;
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     *
     * @param string $url: url of news
     * @throws CHttpException
     * @return News
     */
    public function loadModel2($url)
    {
        $model = Immovables::model()->findByAttributes(Array('url' => $url));
        if(!$model){
            $this->showError404();
        }

        return $model;
    }

    /**
     * Get flat image (2d render)
     */
    public function actionPDF($id)
    {
        $pdf = File::model()->findByAttributes(Array('id'=>$id));


        return $pdf;

    }
}