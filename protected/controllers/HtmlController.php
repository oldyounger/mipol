<?php
class HtmlController extends Controller
{
	public $activeMenu = 'html-page';
	public $fastContact = true;
	/**
	 * HTML page specific page
	 *
	 * @param string $pageUrl
	 */
	public function actionPage($pageUrl)


	{

		$cMail = new CMail();
		$form = new FormContact;
		if(isSet($_POST['FormContact'])){


			$form->attributes = $_POST['FormContact'];
			$params = Array(
				'name' => $form->name,
				'surname' => $form->surname,
				'email' => $form->email,
				'content' => $form->content,

			);
			if($form->validate() && $cMail->sendFromTemplate(SystemMailTemplate::TEMPLATE_CONTACT_FORM, 'biuro@columbuscapital.pl', $form->email, $params, $form->email)){

				Yii::app()->user->setFlash('contact','Dziękujemy za wysłanie wiadomości.');
				$this->refresh();
			}
			else{
				Yii::app()->user->setFlash('contact','Błąd podczas wysyłania wiadomości. Prosimy spróbować ponownie.');
				$this->refresh();
			}
		}
		$model = $this->loadModel($pageUrl);

		$this->activeMenu = 'page/' . $model->url;

		$this->pageTitle = $model->name;

		if($model->description){
			$this->pageDescription = $model->description;
		}

		if($model->keywords){
			$this->pageKeywords = $model->keywords;
		}

		if($model->show_slider){
			$this->showSlider = true;
		}

        if(Yii::app()->request->isAjaxRequest){
            $this->renderPartial('page', Array(
                'model' => $model,
                'renderFooter' => true,
            ));
        }
        else{
		    $this->render('page', Array(
                'model' => $model,
                'renderFooter' => false,
				'model2'=>$form
            ));
        }
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 *
	 * @param string $url: url of page
	 * @throws CHttpException
	 * @return Page
	 */
	public function loadModel($url)
	{
		/** @var Page $model */
		$model = Page::model()->findByAttributes(Array('url' => $url));
		if(!$model || $model->internal){
			$this->showError404();
		}

		return $model;
	}
}