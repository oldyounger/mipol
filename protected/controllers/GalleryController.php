<?php
class GalleryController extends Controller
{
    public $activeMenu = 'activity';
    public $contentClass = 'activity';


    public $siteTitle = 'Galeria';
    public $homeMenu = false;
    public $fastContact = true;
    public $otherMenu = true;


    /**
     * Gets previous image of specified image
     *
     * @param Gallery $galleryID
     *
     * @return GalleryImage
     */
    public function getPreviousGallery($galleryId)
    {
        $criteria = new CDbCriteria;
       // $criteria->compare('gallery_id', $this->id);
        $criteria->addCondition('`id` < ' . $galleryId);
        $criteria->order = '`id` DESC';


        return Gallery::model()->find($criteria);
    }
    /**
     * Gets previous image of specified image
     *
     * @param Gallery $galleryID
     *
     * @return GalleryImage
     */
    public function getNextGallery($galleryId)
    {
        $criteria = new CDbCriteria;
        // $criteria->compare('gallery_id', $this->id);
        $criteria->addCondition('`id` > ' . $galleryId);
        $criteria->order = '`id` ASC';


        return Gallery::model()->find($criteria);
    }



    public function actionIndex($galleryId)
    {



        $prev = $this->getPreviousGallery($galleryId);
        $next = $this->getNextGallery($galleryId);




        var_dump($prev);

        $gallery = Gallery::model()->findByAttributes(Array('id' => $galleryId));
        $imagesList = $gallery->getImagesPaginated(false);
        $galleryImages = $imagesList['items'];



        $cMail = new CMail();
        $form = new FormContact;
        if(isSet($_POST['FormContact'])){

            $form->attributes = $_POST['FormContact'];
            $params = Array(
                'name' => $form->name,
                'surname' => $form->surname,
                'email' => $form->email,
                'content' => $form->content,

            );
            if($form->validate() && $cMail->sendFromTemplate(SystemMailTemplate::TEMPLATE_CONTACT_FORM, 'noneluke@gmail.com', $form->email, $params, $form->email)){

                Yii::app()->user->setFlash('contact','Dziękujemy za wysłanie wiadomości.');
                $this->refresh();
            }
            else{
                Yii::app()->user->setFlash('contact','Błąd podczas wysyłania wiadomości. Prosimy spróbować ponownie.');
                $this->refresh();
            }
        }

        $this->render('index',Array('galleryImages'=>$galleryImages,'prev'=>$prev,'next'=>$next));
    }











}