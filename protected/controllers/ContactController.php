<?php
class ContactController extends Controller
{
	public $activeMenu = 'contact';
	public $showSlider = true;

    public $contentClass = 'contact-page';

	public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the user registration page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
        );
    }

	/**
	 * Shows contact page.
	 */
	public function actionIndex()
	{



        $this->render('index', Array(

        ));
	}


}