<?php
/**
 * Class CMovie
 *
 * @author Kamil Sokołowski
 * @date 2014-02-24
 */
class CMovie
{
	const MOVIE_NONE = 0;
	const MOVIE_YOUTUBE = 1;
	const MOVIE_VIMEO = 2;

	public static $TYPES = Array(
		self::MOVIE_YOUTUBE => 'YouTube',
		self::MOVIE_VIMEO => 'Vimeo',
	);

	/**
	 * Creates HTML based on movie type
	 *
	 * @param integer $movieType
	 * @param string $movieId
	 * @return string
	 */
	public static function getHTML($movieType, $movieId)
	{
		if($movieType == self::MOVIE_YOUTUBE){
			return '<iframe width="786" height="504" frameborder="0" allowfullscreen="allowfullscreen" src="//www.youtube.com/embed/' . $movieId . '"></iframe>';
		}
		else if($movieType == self::MOVIE_VIMEO){
			return '<iframe src="//player.vimeo.com/video/' . $movieId . '?color=ff9933" width="786" height="504" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
		}

		return NULL;
	}

	/**
	 * Creates URL based on movie type
	 *
	 * @param integer $movieType
	 * @param string $movieId
	 * @return string
	 */
	public static function getURL($movieType, $movieId)
	{
		if($movieType == self::MOVIE_YOUTUBE){
			return 'http://youtube.com/watch?v=' . $movieId;
		}
		else if($movieType == self::MOVIE_VIMEO){
			return 'http://vimeo.com/' . $movieId;
		}

		return NULL;
	}

	/**
	 * Extracts ID from URL, based on type
	 *
	 * @param integer $movieType
	 * @param string $movieUrl
	 * @return string
	 */
	public static function getIDByUrl($movieType, $movieUrl)
	{
		$matches = Array();
		if($movieType == self::MOVIE_YOUTUBE){
			if(preg_match('@(?:https?://)?(?:www\.)?youtu(?:\.be|be\.com)/(?:watch\?v=)?([a-zA-Z0-9\-_]+)@i', $movieUrl, $matches)){
				return $matches[1];
			}
		}
		else if($movieType == self::MOVIE_VIMEO){ //TODO
			if(preg_match('@(?:https?://)?(?:player\.)?vimeo\.com(?:/video)?/(\d+)@i', $movieUrl, $matches)){
				return $matches[1];
			}
		}

		return NULL;
	}
}