<?php
/**
 * Class AjaxAPI
 *
 * it is module for easier communication with ajax..
 * created because I'm tired with writing the same thing.. result blabla
 *
 * @author Kamil Sokołowski
 * @date 19 Nov 2013
 */
class AjaxAPI
{
	const STATUS_FAILED = 'STATUS_FAILED';
	const STATUS_OK = 'STATUS_OK';

	private $responseFields = Array(
		'result' => self::STATUS_FAILED
	);

	private static $instance = NULL;

	public function __construct()
	{
		//
	}

	/**
	 * Singleton implementation
	 * @return AjaxAPI
	 */
	public static function &getInstance()
	{
		if(self::$instance === NULL){
			self::$instance = new AjaxAPI();
		}

		return self::$instance;
	}

	public function setResult($message)
	{
		$this->responseFields['result'] = $message;
	}

	public function getResult($format = 'json')
	{
		if($format == 'json'){
			return json_encode($this->responseFields);
		}

		return false;
	}

	public function printResult($format = 'json', $end = true)
	{
		echo $this->getResult($format);

		if($end){
			Yii::app()->end();
		}
	}

	public function setResponseField($key, $value)
	{
		if($key == 'result' || $key == 'message'){
			throw new Exception('Usage of reserved AjaxAPI field: '.$key);
		}

		$this->responseFields[$key] = $value;
	}

	public function getResponseField($key)
	{
		if(isSet($responseFields[$key])){
			return $this->responseFields[$key];
		}

		return NULL;
	}

	public function setResultInfo($message)
	{
		$this->responseFields['message'] = $message;
	}

	public function getResultInfo()
	{
		if(isSet($this->responseFields['message'])){
			return $this->responseFields['message'];
		}

		return NULL;
	}
}