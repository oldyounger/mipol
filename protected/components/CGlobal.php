<?php
class CGlobal
{
	/**
	 * Konwersja kodowania tekstu
	 * @param string $string
	 * @param string $fromEncoding
	 * @return string
	 */
	public static function getUTF8($string, $fromEncoding = '')
	{
		if ($fromEncoding != ''){
			return @iconv($fromEncoding, 'UTF-8', $string);
		}

		$fromEncoding = self::detectEncoding($string);
		if ($fromEncoding != 'UTF-8'){
			return @iconv($fromEncoding, 'UTF-8', $string);
		}

		return $string;
	}

	/**
	 * Wykrywanie kododwania kompatybilne z windows-1250
	 * @link http://www.blogbox.com.pl/blog/mateusz-matipl-kaminski/post/mb-detect-encoding-nie-wspiera-windows-1250 author
	 * @param string $string
	 * @return string
	 */
	public static function detectEncoding($string)
	{
		if (strlen($string) === strlen(@iconv('UTF-8', 'UTF-8//IGNORE', $string))){
			return 'UTF-8';
		}
		else{
			$win2utf = array(
				"\xb9" => "\xc4\x85", "\xa5" => "\xc4\x84", "\xe6" => "\xc4\x87", "\xc6" => "\xc4\x86",
				"\xea" => "\xc4\x99", "\xca" => "\xc4\x98", "\xb3" => "\xc5\x82", "\xa3" => "\xc5\x81",
				"\xf3" => "\xc3\xb3", "\xd3" => "\xc3\x93", "\x9c" => "\xc5\x9b", "\x8c" => "\xc5\x9a",
				"\x9f" => "\xc5\xba", "\xaf" => "\xc5\xbb", "\xbf" => "\xc5\xbc", "\xac" => "\xc5\xb9",
				"\xf1" => "\xc5\x84", "\xd1" => "\xc5\x83", "\x8f" => "\xc5\xb9");

			$countWin = 0;
			$found = 0;
			foreach ($win2utf as $win => $utf){
				$pos = strpos($string, $win);
				if ($pos !== false){
					$found++;
				}

				if ($pos && substr(@iconv('WINDOWS-1250', 'UTF-8', $string), $pos + $countWin, 2) == $utf){
					$countWin++;

					return 'WINDOWS-1250';
				}
			}

			if ($countWin > 0 && $found == $countWin){
				return 'WINDOWS-1250';
			}
			if ($found){
				return 'ISO-8859-2';
			}

			return 'UTF-8';
		}
	}

	/**
	 * Przetwarza przesłany tekst na tekst złożony tylko ze znaków ASCII
	 * @param string $input
	 * @param boolean $strip
	 * @return string
	 */
	public static function getPureAsciiString($input, $strip = true)
	{
		$output = iconv('UTF-8', 'ASCII//TRANSLIT//IGNORE', $input);
		if ($strip){
			$output = preg_replace('#([^A-Za-z0-9_])#', '', $output);
		}

		return $output;
	}

	/**
	 * Wyciąga liczbę z tekstu
	 * @param string $input
	 * @return float
	 */
	public static function getNumberFromString($input)
	{
		return (float)CGlobal::getPureString(str_replace(',', '.', $input), '0-9\.\-');
	}

	/**
	 * Usuwa wszystkie znaki, które nie znajdują się w leaveChars
	 * @param string $input
	 * @param string $leaveChars
	 * @return string
	 */
	public static function getPureString($input, $leaveChars = '0-9\.\-')
	{
		return preg_replace('#([^' . $leaveChars . ']+)#', '', $input);
	}

	/**
	 * Zapisuje komunikat/zmienną/tablicę do pliku
	 * @param mixed $mixed
	 * @param boolean $_append
	 * @return nothing
	 */
	public static function file_r($mixed, $_append = true)
	{
		$mode = $_append ? 'a+' : 'w';
		$file = fopen(ROOT_DIR . '/file_r.txt', $mode);
		if($file){
			if(is_array($mixed) || is_object($mixed)){
				fwrite($file, var_export($mixed, true));
			}
			else{
				fwrite($file, $mixed);
			}
			fwrite($file, "\n");
			fclose($file);
		}
	}

	/**
	 * Generuje MD5 z pliku
	 * @param string $filepath pełna ścieżka do pliku
	 * @return string MD5
	 */
	public static function md5file($filepath)
	{
		if (file_exists($filepath)){
			$result = explode(' ', exec('md5sum -b ' . escapeshellarg($filepath)));

			return $result[0];
		}

		return '';
	}

	/**
	 * Generuje unikatowy ciąg znaków
	 * @return string
	 */
	public static function unique()
	{
		return date('ymdHis') . rand(10000, 99999) . rand(10, 99);
	}

	/**
	 * Generuje unikatowy hash
	 * @return string
	 */
	public static function uniqueHash()
	{
		return hash('sha256', date('ymdHis') . rand(10000, 99999) . rand(10, 99) . rand(10, 99));
	}

	/**
	 * Generuje unikatowy hash
	 * @return string
	 */
	public static function simpleHash()
	{
		return hash('sha1', date('ymdHis') . '__NERAVIA_CMS_SALT9__' . rand(10000, 99999) . rand(10, 99) . rand(10, 99));
	}

	/**
	 * Generates alphanumeric random password
	 *
	 * @param int $length
	 * @return string
	 */
	public static function randomPassword($length = 10)
	{
		$digits = 'QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm01234567890123456789'; //more digits
		if($length > strlen($digits)){ //in case we want longer password than length
			$times = ceil($length / strlen($digits));

			$digits = str_repeat($digits, $times);
		}

		return substr(str_shuffle($digits), 0, $length);
	}

	/**
	 * Wybiera jedno pole z tablicy obiektow i wrzuca je do tablicy zwracanej,
	 * niezastapione przy tworzeniu kwerend z warunkiem IN (X,X,X)
	 * @param array $objects tablica obiektow
	 * @param string $idField pole do uzupelnienia tablicy wyjsciowej
	 * @return array
	 */
	public static function objectsToIdsArray($objects, $idField)
	{
		$return = array();
		foreach ($objects as $object){
			$return[] = $object->$idField;
		}

		return $return;
	}

	public static function removeEmptyArrayItems($array)
	{
		return array_filter($array, array("CGlobal", "_removeEmptyArrayItems"));
	}

	private static function _removeEmptyArrayItems($item)
	{
		return !empty($item);
	}

	/**
	 * Gets content by cURL
	 * @param string $url
	 * @return mixed
	 */
	public static function curl_get_contents($url)
	{
		$curl = curl_init($url);

		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);

		$data = curl_exec($curl);
		curl_close($curl);

		return $data;
	}


	/**
	 * Extracts file extension from a file. Returns NULL if can't find
	 * @param string $fileName
	 * @return string|null
	 */
	public static function getFileExtension($fileName)
	{
		$ppInfo = pathinfo($fileName);
		if(isSet($ppInfo['extension'])){
			return $ppInfo['extension'];
		}

		return NULL;
	}

	/**
	 * Parses post date (for use in calendar date range)
	 *
	 * @param Array/boolean $date: date range, passed back by reference
	 * @param $dateRangePickerStart: for use in calendar widget, passed back by reference
	 * @param $dateRangePickerEnd: for use in calendar widget, passed back by reference
	 * @return nothing
	 */
	public static function parsePostDateRange(&$date, &$dateRangePickerStart, &$dateRangePickerEnd)
	{
		if(isSet($_GET['date']) && preg_match('@^(\d{4})-(\d{2})-(\d{2})$@', $_GET['date'])){
			$stamp = strtotime($_GET['date']);
			$date = date('Y-m-d', $stamp);
		}
		else if(isSet($_GET['date_start']) && isSet($_GET['date_end']) && preg_match('@^(\d{4})-(\d{2})-(\d{2})$@', $_GET['date_start']) && preg_match('@^(\d{4})-(\d{2})-(\d{2})$@', $_GET['date_end'])){
			$stamp_start = strtotime($_GET['date_start']);
			$date_start = date('Y-m-d', $stamp_start);

			$stamp_end = strtotime($_GET['date_end']);
			$date_end = date('Y-m-d', $stamp_end);

			$date = Array('start' => $date_start, 'end' => $date_end);
			$dateRangePickerStart = date('d.m.Y', $stamp_start);
			$dateRangePickerEnd = date('d.m.Y', $stamp_end);
		}
	}

	/**
	 * Parses post date (for use in calendar date)
	 *
	 * @return string/boolean
	 */
	public static function parsePostDate()
	{
		if(isSet($_GET['date']) && preg_match('@^(\d{4})-(\d{2})-(\d{2})$@', $_GET['date'])){
			$stamp = strtotime($_GET['date']);
			return date('Y-m-d', $stamp);
		}

		return false;
	}
}