<?php
class CTools
{
	/**
	 * Removes \n and \r from string
	 *
	 * @param string $str
	 * @return string
	 */
	public static function fixSingleQuote($str)
	{
		return str_replace("'", "\\'", $str);
	}

	/**
	 * Removes \n and \r from string
	 *
	 * @param string $str
	 * @return string
	 */
	public static function removeNewLine($str)
	{
		return preg_replace('@[\r\n]@', '', $str);
	}

	/* replaces [quote][/quote] with blockquote. simple, we don't support quote stacking */
	public static function parseQuotes($str, $allowOneNewLine = false)
	{
		$quote = preg_replace('@\[quote\](.*?)\[/quote\]@is', '<blockquote>$1</blockquote>', htmlspecialchars($str));

		if($allowOneNewLine){
			$quote = nl2br(preg_replace('@(\s*)(\r\n|\n|\r){2,}(\s*)@is', '$2', $quote));
		}

		return $quote;
	}

    public static function parseQuotesAndLinks($str, $allowOneNewLine = false)
    {
        $quote = preg_replace('@\[quote\](.*?)\[/quote\]@is', '<blockquote>$1</blockquote>', htmlspecialchars($str));

        if($allowOneNewLine){
            $quote = nl2br(preg_replace('@(\s*)(\r\n|\n|\r){2,}(\s*)@is', '$2', $quote));
        }

        $quote = preg_replace('/(?:https?:\/\/)?(?:www\.)?youtu(?:\.be|be\.com)\/(?:watch\?v=)?([a-zA-Z0-9]+)/', '__YOUTUBE_MOVIE_$1__', $quote);

        $text = preg_replace("/(?i)\b((?:https?:\/\/|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}\/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'\".,<>?«»“”‘’]))/", '<a href="$1" class="blockClickBehaviour" target="blank">$1</a>', $quote);
        $text = str_replace("href=\"www.","href=\"http://www.",$text);

        $text = preg_replace('/__YOUTUBE_MOVIE_([a-zA-Z0-9]+)__/', '<iframe class="youtube" width="100%" src="//www.youtube.com/embed/$1?rel=0" frameborder="0" allowfullscreen></iframe>', $text);

        return htmlspecialchars_decode($text);
    }

	public static function parseLinks($str)
	{
		$text = preg_replace("/(?i)\b((?:https?:\/\/|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}\/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'\".,<>?«»“”‘’]))/", '<a href="$1" class="blockClickBehaviour" target="blank">$1</a>', $str);
		$text = str_replace("href=\"www.","href=\"http://www.",$text);

		return $text;
	}

	public static function stripQuotes($str)
	{
		$quote = preg_replace('@\[quote\](.*?)\[/quote\]@is', '$1', htmlspecialchars($str));

		return $quote;
	}

    public static function createBreadcrumbs(Array $breadcrumbs)
    {
        /*
                    <li>
                        <a href="#">Strona Główna</a>
                    </li>
                    <li>
                        <a href="#">Kontakt</a>
                    </li>*/

        $ret = '';
        foreach($breadcrumbs as $br){
            $ret .= '
            <li>
                <a href="' . $br[1] . '">' . $br[0] . '</a>
            </li>
            ';
        }

        return $ret;
    }
}