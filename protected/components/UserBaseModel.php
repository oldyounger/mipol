<?php
class UserBaseModel extends CActiveRecord
{
	public $currentPassword;

	public function afterFind()
	{
		$this->currentPassword = $this->password;
		$this->password = null;

		parent::afterFind();
	}

	public function beforeSave()
	{
		if(empty($this->password)){
			$this->password = $this->currentPassword;
		}
		else{
			$this->password = UserBaseModel::crypt($this->password);
		}

		return parent::beforeSave();
	}

	/**
	 * Czy podane w parametrze hasło jest identyczne z hasłem użytkownika?
	 * @param string $password
	 * @param string $userPassword
	 * @return boolean
	 */
	public static function checkPassword($password, $userPassword)
	{
		//	echo_r(UserIdentity::crypt('admin'));
		//	echo_r(UserIdentity::crypt($password, $userPassword));
		if($userPassword == UserBaseModel::crypt($password, $userPassword)){
			return true;
		}

		return false;
	}


	/**
	 * Zwraca hash wpisanego hasła
	 * @param string $password
	 * @param string $salt
	 * @return string
	 */
	public static function crypt($password, $salt = '')
	{
		if($salt == ''){
			$salt = UserBaseModel::salt();
		}

		return crypt($password, $salt);
	}


	/**
	 * Generuje salt dla funkcji crypt() - wybiera najmocniejszy obsługiwany algorytm
	 * obsługiwaną funkcję hash'owania
	 * @param int $param
	 * @return string
	 */
	public static function salt($param = 0)
	{
		$salt = '';
		/*if(CRYPT_BLOWFISH)
		{
			$salt = '$2a$';
			if(version_compare(PHP_VERSION, '5.3.7') >= 0)
				$salt = '$2y$';

			if($param < 4) $param = 4;
			if($param > 31) $param = 31;

			$salt .= str_pad($param,2,'0',STR_PAD_LEFT).'$';
			$salt .= substr( md5(microtime().rand(100,999)), 0, 22 ).'$';
		}
		elseif(CRYPT_SHA256)
		{
			$salt = '$5$';
			if($param < 1000) $param = 5000;

			$salt .= 'rounds='.$param.'$';
			$salt .= substr( md5(microtime().rand(100,999)), 0, 16 ).'$';
		}
		elseif(CRYPT_MD5)
		{
			$salt = '$1$';
			$salt .= substr( md5(microtime().rand(100,999)), 0, 12 ).'$';
		}*/
		
		if(CRYPT_MD5){
			$salt = '$1$';
			$salt .= substr( md5(microtime().rand(100,999)), 0, 12 ).'$';
		}
		else{
			throw new CException('MD5 is not available in this system.');
		}
		
		return $salt;
	}
}