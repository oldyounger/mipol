<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class CString
{
	public static function cleanString($fromString, $maxLength = -1, $prepend = null, $default = null, $spaceSign = '-', $clearDot = false)
	{
		$translate = array(
			'А' => 'A', 'а' => 'a', 'Б' => 'B',	'б' => 'b', 'В' => 'W', 'в' => 'w', 'Г' => 'G', 'г' => 'g',	'Д' => 'D', 'д' => 'd', 'Е' => 'E',
			'е' => 'e', 'Ё' => 'O', 'ё' => 'o', 'Ж' => 'Z', 'ж' => 'z', 'З' => 'Z', 'з' => 'z', 'И' => 'Y', 'и' => 'y', 'Й' => 'J', 'й' => 'j',
			'К' => 'K', 'к' => 'k', 'Л' => 'L', 'л' => 'l', 'М' => 'M', 'м' => 'm', 'Н' => 'N', 'н' => 'n', 'О' => 'O', 'о' => 'o', 'П' => 'P',
			'п' => 'p', 'Р' => 'R', 'р' => 'r', 'С' => 'S', 'с' => 's', 'Т' => 'T', 'т' => 't', 'У' => 'U', 'у' => 'u', 'Ф' => 'F', 'ф' => 'f',
			'Х' => 'CH', 'х' => 'ch', 'Ц' => 'C', 'ц' => 'c', 'Ч' => 'CZ', 'ч' => 'cz', 'Ш' => 'SZ', 'ш' => 'sz', 'Щ' => 'SZCZ', 'щ' => 'szcz',
			'Э' => 'E', 'э' => 'e', 'Ю' => 'U', 'ю' => 'u', 'Я' => 'A', 'я' => 'a', 'Ā' => 'AA', 'ā' => 'aa', 'Č' => 'CH', 'č' => 'ch', 'Ē' => 'EE',
			'ē' => 'ee', 'Ģ' => 'GJ', 'ģ' => 'gj', 'Ī' => 'II', 'ī' => 'ii', 'Ķ' => 'KJ', 'ķ' => 'kj', 'Ļ' => 'LJ', 'ļ' => 'lj', 'Ņ' => 'NJ',
			'ņ' => 'nj', 'Š' => 'SH', 'š' => 'sh', 'Ū' => 'UU', 'ū' => 'uu', 'Ž' => 'ZH', 'ž' => 'zh', 'Ą' => 'A', 'ą' => 'a', 'Ć' => 'C', 'ć' => 'c',
			'Ę' => 'E', 'ę' => 'e', 'Ł' => 'L', 'ł' => 'l', 'Ń' => 'N', 'ń' => 'ń', 'Ó' => 'O', 'ó' => 'o', 'Ś' => 'S', 'ś' => 's', 'Ź' => 'Z',
			'ź' => 'z', 'Ż' => 'Z',	'ż' => 'z'
		);

		$fromString = strtr($fromString, $translate);
		$fromString = preg_replace('#[ /]+#', $spaceSign, preg_replace('/[^a-zA-Z0-9'.(!$clearDot ? '\.' : NULL).'\/ _-]*/', '', trim($fromString)));

		if($maxLength > 0)
			$fromString = substr($fromString, 0, $maxLength);

		$fromString = strtolower($fromString);

		if($prepend !== null && preg_match('#^([0-9]+)$#', $fromString))
			$fromString = $prepend.$fromString;

		if($fromString == '' && $default !== null)
			$fromString = $default;

		return $fromString;
	}

	public static function cleanFilename($filename)
	{
		return CString::cleanString($filename, -1, null, null, '_');
	}
}
