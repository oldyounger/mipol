<?php
/**
 * CMail
 * sends mail both in raw and formatted (with template) mode
 */
class CMail extends CComponent
{
	protected $_lastError;

	/**
	 * returns last error
	 * @return string
	 */
	public function getLastError()
	{
		return $this->_lastError;
	}

	/**
	 * send mail from template
	 *
	 * @param mixed $templateIdOrSystemName jezeli podamy tu zmienna z numerem - szablon bedzie wyszukany po id
	 * w innym przypadku bedzie szukany wg nazwy systemowej
	 * @param string $email adres email odbiorcy
	 * @param string $name nazwa odbiorcy
	 * @param array $params pary nazwa=>wartosc dla zmiennych w szablonach
	 * @param string $replyTo: mail na ktory odpowiadamy
	 * @return boolean sukces wysylki szablony email
	 */
	public function sendFromTemplate($templateIdOrSystemName, $email, $name, $params = array(), $replyTo = NULL)
	{
		$template = SystemMailTemplate::model()->getWithParams($templateIdOrSystemName, $params);
		if($template){
			return $this->send($email, $name, $template['subject'], $template['body'], $replyTo);
		}

		return false;
	}

	/**
	 * Sends email
	 *
	 * @param string $email: reciever email
	 * @param string $name: reciever name
	 * @param string $subject: topic of message
	 * @param string $message: content of message
	 * @param string $replyTo: reply to mail
	 * @return boolean if sending was successful
	 */
	public function send($email, $name, $subject, $message, $replyTo = NULL)
	{
		$this->_bodyHtml($message); //if there is no charset, put whole message in HTML Document

		Yii::import('application.extensions.phpmailer.JPhpMailer');

		$mail = new JPhpMailer;
		$mail->IsSMTP();
		$mail->CharSet = 'utf-8';
		$mail->Host = Yii::app()->params['smtp_mail_auth']['host'];
		$mail->SMTPAuth = Yii::app()->params['smtp_mail_auth']['smtp_auth'];
		$mail->SMTPSecure = Yii::app()->params['smtp_mail_auth']['smtp_secure'];
		if(isSet(Yii::app()->params['smtp_mail_auth']['port']) && mb_strlen(Yii::app()->params['smtp_mail_auth']['port']) > 0){
			$mail->Port = Yii::app()->params['smtp_mail_auth']['port'];
		}

		$mail->Username = Yii::app()->params['smtp_mail_auth']['user_name'];
		$mail->Password = Yii::app()->params['smtp_mail_auth']['password'];
		$mail->SetFrom(Yii::app()->params['smtp_mail_auth']['send_from_email'], Yii::app()->params['smtp_mail_auth']['send_from_name']);

		if($replyTo !== NULL){
			$mail->ClearReplyTos();
			$mail->AddReplyTo($replyTo);
		}

		$mail->Subject = $subject;
		$mail->AltBody = strip_tags(preg_replace('/\<br(\s*)?\/?\>/i', "\n", $message)); //change <br/> to newline and strip other tags
		$mail->MsgHTML($message);
		$mail->AddAddress($email, $name);
		$return = $mail->Send();
		$this->_lastError = $mail->ErrorInfo;

		return $return;
	}

	/**
	 * packs content in a HTML body
	 * @param $body body to wrap in
	 * @return boolean
	 */
	protected function _bodyHtml(&$body)
	{
		if(strpos($body, "charset") !== false){ //if got charset, don't touch
			return true;
		}

		Yii::import('application.widgets.MailTemplateWidget.MailTemplateWidget');
		$widget = new MailTemplateWidget();
		$body = $widget->wrap($body);

		return true;
	}
}
