<?php
class DataPaths
{
	const PATH_REPORT = 'report';

	protected static $_initialized = false;

	protected static $paths = array();

	public static function get($pathName)
	{
		if(!self::$_initialized){
			self::initilize();
		}
		if(!isset(self::$paths[$pathName])){
			throw new CException("Brak podanej ścieżki w systemie: $pathName");
		}
		else{
			return self::$paths[$pathName];
		}
	}

	protected static function initilize()
	{
		self::$_initialized = true;
		$main = Yii::getPathOfAlias('application') . '/../';
		self::$paths = array(
			'report' => $main . 'report/',
		);
	}
}