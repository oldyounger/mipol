<?php
class ActiveRecord extends CActiveRecord
{
	/**
	 * Nadpisuje URL przed zapisem do bazy
	 * @return type
	 */
	public function beforeSave()
	{
		if($this->hasAttribute('url')){
			if(mb_strlen($this->url) == 0){
				//$this->url = $this->createUrl($this->name);
				$field = NULL;
				//title or name is required
				if(isSet($this->name)){
					$field = $this->name;
				}
				else{
					$field = $this->title;
				}

				$this->url = CUniqueURL::getURL($field, get_called_class(), 'url');
			}

//			var_dump($this->url);die;
			/*else{
				$this->url = $this->createUrl($this->url);
			}*/
		}

		return parent::beforeSave();
	}

	/**
	 * Tworzy URL z przekazanego tekstu
	 * @param string $fromString
	 * @return string
	 */
	/*public function createUrl($fromString)
	{
		$url = CString::cleanString($fromString, $this->tableSchema->getColumn('name')->size, 'url-', 'brak-nazwy');
		$criteria = new CDbCriteria();
		$criteria->addCondition("`url` = '{$url}'");
		if(!$this->isNewRecord){
			$criteria->addNotInCondition($this->tableSchema->primaryKey, array($this->primaryKey));
		}
		$command = Yii::app()->db->commandBuilder->createFindCommand($this->tableSchema, $criteria);

		if($command->execute()){
			$url .= '-' . rand(1000, 9999);
		}

		return $url;
	}*/
}
