<?php
class UserIdentity extends CUserIdentity
{
	const ERROR_NOT_ACTIVATED = 3;

	// logged in user ID
	private $_userId;

	/**
	 * Authenticates a user.
	 *
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
		$user = User::model()->findByAttributes(Array('email' => $this->username));
		if(!$user){
			$this->errorCode = self::ERROR_USERNAME_INVALID;
		}
		elseif($user->active == 0){
			$this->errorCode = self::ERROR_NOT_ACTIVATED;
		}
		elseif(!UserBaseModel::checkPassword($this->password, $user->currentPassword)){
			$this->errorCode = self::ERROR_PASSWORD_INVALID;
		}
		else{
			$this->_userId = $user->getPrimaryKey();

			$this->setState('active', $user->active);
			$this->errorCode = self::ERROR_NONE;
		}

		return !$this->errorCode;
	}

	public function authenticateWithoutPasswordAndActive()
	{
		$user = User::model()->findByAttributes(array('email' => $this->username));
		if(!$user){
			$this->errorCode = self::ERROR_USERNAME_INVALID;
		}
		else{
			$this->_userId = $user->getPrimaryKey();

			$this->setState('active', $user->active);
			$this->errorCode = self::ERROR_NONE;
		}

		return !$this->errorCode;
	}


	public function getId()
	{
		return $this->_userId;
	}
}