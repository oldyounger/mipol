<?php
class CUniqueURL
{
	public static function getURL($name, $modelName, $fieldURL = 'url', $extraInfoToSearch = Array())
	{
		$fixedName = CString::cleanString($name, 130, NULL, NULL, '-', true);

		$infoToSearch = array_merge($extraInfoToSearch, Array(
			$fieldURL => $fixedName
		));

        $mdl = NULL;
        if($modelName == 'Category'){
            $mdl = Category::model();
        }
        else if($modelName == 'Gallery'){
            $mdl = Gallery::model();
        }
        else if($modelName == 'News'){
            $mdl = News::model();
        }
        else if($modelName == 'Page'){
            $mdl = Page::model();
        }

		$count = $mdl->countByAttributes($infoToSearch);

		if($count > 0){
			$tempName = NULL;

			$foundAvailableUrl = false;
			$i = 0;
			do {
				$tempName = $fixedName . '-' . ++$i;

				$infoToSearch[$fieldURL] = $tempName;
				$count = $mdl->countByAttributes($infoToSearch);

				if($count == 0){
					$foundAvailableUrl = true;
				}
			}
			while(!$foundAvailableUrl);

			return $tempName;
		}

		return $fixedName;
	}
}