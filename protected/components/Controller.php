<?php
class Controller extends CController
{
	/**
	 * @var User
	 */
	protected $_user;

	public $layout = '//layouts/column1_layout';
	public $leftView = '//layouts/left_column';

	/** @var string: used in menu highlighting */
	public $activeMenu;

	public $menu = Array();
	public $breadcrumbs = Array();
	public $pageTitle = '';
	public $pageDescription = '';
	public $pageKeywords = '';

	public $customBackground = false;
	public $customBackgroundColor = false;
	public $homePage = false;
	public $contentWrapperWithoutMargin = false;
	public $containerWithMargin = false;

    public $contentClass = NULL;

	//public $showSlider = false;

    public $footerContent = NULL;
    public $specialFooter;
    public $showFooter = true;
    public $bodyClass;

	public function __construct($id, $module = NULL)
	{
		parent::__construct($id, $module);
	}

	/**
	 * Initial function, sets current theme
	 */
	public function init()
	{
		$this->setCurrentTheme();
	}

    public function beforeRender($view)
    {
        $criteria = new CDbCriteria;
        $criteria->compare('internal', 0);
        $this->menu = Page::model()->findAll($criteria);

        return parent::beforeRender($view);
    }

	/**
	 * Shows error 404 with nice message
	 *
	 * @param string $message: message to override, will be translated (if there is available translation in messages/LANG/http.php
	 * @throws CHttpException
	 */
	public function showError404($message = "We're so sorry but the page you are looking for does not exists.")
	{
		throw new CHttpException(404, Yii::t('http', $message));
	}

	/**
	 * Shows error 403 with nice message
	 *
	 * @param string $message: message to override, will be translated (if there is available translation in messages/LANG/http.php
	 * @throws CHttpException
	 */
	public function showError403($message = "We're so sorry but you don't have access to this page.")
	{
		throw new CHttpException(403, Yii::t('http', $message));
	}

	/**
	 * Sets current theme
	 * TODO: multi layout. by now it is forced to 'white'
	 */
	protected function setCurrentTheme()
	{
		$selectedLayout = 'white';

		/*$selectedLayout = 'auto';
		if ($user = User::getLoggedHandle()){ //user is logged, check its layout
			$selectedLayout = $user->getSelectedTheme();
		}

		if ($selectedLayout == 'auto'){
			//8:00 AM - 7:00 PM = white
			//rest = black
			$currentHour = date('G', time(NULL));
			if ($currentHour >= 8 && $currentHour <= 19){ //white
				$selectedLayout = 'white';
			}
			else{ //black
				$selectedLayout = 'black';
			}
		}*/

		Yii::app()->theme = $selectedLayout;
	}

	/**
	 * Redirects to login page with backUrl set as $_GET parameter
	 */
	public function redirectToLoginPage()
	{
		$returnPath = Yii::app()->getRequest()->getRequestUri();

		$redirectUrl = $this->createUrl('user/login', Array('backUrl' => $returnPath));
		$this->redirect($redirectUrl);
	}

	/**
	 * Gets logged user, if not found it redirect to login page
	 *
	 * @param boolean $onlyActive: check if user is active
	 * @return User
	 */
	public function getLoggedUser($onlyActive = true)
	{
		if($user = User::getLoggedHandle($onlyActive)){
			return $user;
		}

		//REDIRECT_TO_LOGIN
		$this->redirectToLoginPage();
	}

	/**
	 * Checks if user is logged, if yes it throws error 404
	 *
	 * @param boolean $onlyActive: check if user is active
	 */
	public function requireUserNotLogged($onlyActive = false)
	{
		if($user = User::getLoggedHandle($onlyActive)){ //if user is logged, throw error
			$this->showError403();
		}
	}
}