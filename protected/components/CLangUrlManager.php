<?php
class CLangUrlManager extends CUrlManager
{
	/*private function isRouteLangSensitive($route)
	{
		return in_array($route, Yii::app()->params['languageSensitiveUrl']);
	}*/

	public function createUrl($route, $params = Array(), $ampersand = '&')
	{
		//TODO: choose language by user (stage 2)
		//Yii::app()->setLanguage('en');
		if(!isSet($params['lang'])){ //if param lang was not forced, add current language
			$params['lang'] = Yii::app()->getLanguage();
		}

		Yii::app()->setLanguage($params['lang']);

		return parent::createUrl($route, $params, $ampersand);

		/*if(isSet($_POST['_lang'])){
			Yii::app()->setLanguage($_POST['_lang']);
			$params['lang'] = Yii::app()->language;
		}
		elseif(!isSet($route['language'])){
			$params['lang'] = Yii::app()->language;
		}
		else{
			Yii::app()->setLanguage($params['lang']);
		}

		return parent::createUrl($route, $params, $ampersand);*/
	}
}

?>