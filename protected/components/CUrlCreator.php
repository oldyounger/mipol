<?php

/**
 * CUrlCreator
 */
class CUrlCreator extends CComponent{

    public function makeUrlFromName($name, $pkName, ActiveRecord $model, $urlFieldName){
        $url = $this->_changeUrlToName($name);
        $pkWhere = "";
        if($model->primaryKey){
            //jeżeli obecny już ma taką nazwę to jest ok
            if($model->count("{$pkName} = {$model->primaryKey} AND ( $urlFieldName = :url OR $urlFieldName REGEXP '^{$url}\-\d+')", array(":url"=>$url)))
                return $model->$urlFieldName;

            $pkWhere = "{$pkName} != {$model->primaryKey} AND ";
        }
        //trzeba ustalić jaki suffix dodać aby było ok
        $alreadyTaken = $model->findAll("$pkWhere $urlFieldName  like '{$url}%'");
        if(count($alreadyTaken) == 0)
            return $url;
        
        $no = 1;
        foreach($alreadyTaken as $item){
            $match = array();
            preg_match("#{$url}\-(\d+)#i",$item->$urlFieldName, $match);
            if(isset($match[1])){
                if($no < ($match[1]+1)){
                    $no = (int)$match[1]+1;
                }
            }
        }
        return $url."-$no"; 
    }

    protected function _changeUrlToName($name){
        //podmiana polskich znaków
        $url = str_replace(
            array("ę","ó","ą","ś","ł","ż","ź","ć","ń","Ę","Ó","Ą","Ś","Ł","Ż","Ź","Ć","Ń"),
            array("e","o","a","s","l","z","x","c","n","E","O","A","S","L","Z","X","C","N"),
        $name);

        $url = preg_replace("#[^\w0-9 \-_\\\/]#i","",$url);
        $url = preg_replace("#[ _\\\/]#i","-",$url);

        return urlencode($url);
    }

}
