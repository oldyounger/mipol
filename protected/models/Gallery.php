<?php
/**
 * This is the model class for table "gallery".
 *
 * The followings are the available columns in table 'gallery':
 * @property integer $id
 * @property integer $thumb_id
 * @property integer $header_image
 * @property string $title
 * @property string $url
 * @property integer $type
 * @property datetime $date
 * @property integer $view_count
 * @property integer $hidden
 * @property integer $home
 *
 * The followings are the available model relations:
 * @property Image $headerImage
 * @property GalleryImage $thumb
 * @property GalleryImage[] $galleryImages
 */
class Gallery extends CActiveRecord
{
	const PAGE_SIZE = 9;

	const TYPE_PHOTO = 1;
	const TYPE_MOVIE = 2;

	public static $VALID_TYPES = Array(
		self::TYPE_PHOTO,
		self::TYPE_MOVIE,
	);

	public static $VALID_TYPES_NAME = Array(
		self::TYPE_PHOTO => 'zdjecia',
		self::TYPE_MOVIE => 'filmy',
	);

	public $date_formatted;
	public $image_sort;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{gallery}}';
	}

	/**
	 * @return Array validation rules for model attributes.
	 */
	public function rules()
	{
		return Array(
			Array('title, type, date, date_formatted,hidden', 'required'),
			Array('hidden, type, thumb_id, header_image, view_count,home', 'numerical', 'integerOnly' => true),
			Array('title', 'length', 'max' => 128),
			Array('url', 'length', 'max' => 134),

			Array('date_formatted, image_sort', 'safe'),

			Array('id, thumb_id, header_image, title, date, view_count, hidden', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return Array relational rules.
	 */
	public function relations()
	{
		return Array(
			'headerImage' => Array(self::BELONGS_TO, 'Image', 'header_image'),
			'thumb' => Array(self::BELONGS_TO, 'GalleryImage', 'thumb_id'),
			'galleryImages' => Array(self::HAS_MANY, 'GalleryImage', 'gallery_id', 'order' => 'galleryImages.order ASC, galleryImages.id ASC'),
		);
	}

	/**
	 * @return Array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return Array(
			'id' => 'ID',
			'thumb_id' => 'Miniaturka',
			'type' => 'Typ',
			'header_image' => 'Zdjęcie tytułowe',
			'title' => 'Tytuł',
			'url' => 'URL',
			'date' => 'Data',
			'author' => 'Autor',
			'home' => 'Umieść na stronie głównej',
			'description' => 'Opis galerii',
			'view_count' => 'Ilość wyświetleń',
			'hidden' => 'Ukryta',
			'date_formatted' => 'Data',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria = new CDbCriteria;
		$criteria->compare('id', $this->id);
		$criteria->compare('thumb_id', $this->thumb_id);
		$criteria->compare('type', $this->type);
		$criteria->compare('header_image', $this->header_image);
		$criteria->compare('title', $this->title, true);
		$criteria->compare('url', $this->url, true);
		$criteria->compare('date', $this->date);
		$criteria->compare('view_count', $this->view_count);
		$criteria->compare('hidden', $this->hidden);

		return new CActiveDataProvider($this, Array(
			'criteria' => $criteria,
		));
	}

	public function beforeValidate()
	{
		if(!parent::beforeValidate()){
			return false;
		}

		$this->date = date('Y-m-d', CDateTimeParser::parse($this->date_formatted, Yii::app()->locale->getDateFormat('short')));

		//NULL fields
		if(!Image::model()->findByPk($this->header_image)){
			$this->header_image = NULL;
		}

		return true;
	}

    public function beforeSave()
    {
        if($this->hasAttribute('url')){
            if(mb_strlen($this->url) == 0){
                //$this->url = $this->createUrl($this->name);
                $field = NULL;
                //title or name is required
                if(isSet($this->name)){
                    $field = $this->name;
                }
                else{
                    $field = $this->title;
                }

                $this->url = CUniqueURL::getURL($field, 'Gallery', 'url');
            }

            //			var_dump($this->url);die;
            /*else{
                $this->url = $this->createUrl($this->url);
            }*/
        }

        return parent::beforeSave();
    }

	public function afterSave()
	{
		//we must update order of images
		if(isSet($this->image_sort) && is_array($this->image_sort) && count($this->image_sort) > 0){
			$order = 0;
			foreach($this->image_sort as $img){
				$galleryImage = GalleryImage::model()->findByAttributes(Array(
					'id' => $img,
					'gallery_id' => $this->id,
				));

				if($galleryImage !== NULL){
					$galleryImage->order = $order++;

					$galleryImage->save();
				}
			}
		}
	}

	public function afterFind()
	{
		$this->date_formatted = Yii::app()->dateFormatter->format(Yii::app()->locale->getDateFormat('short'), $this->date);
	}

	/**
	 * Gets images with pagination
	 *
	 * @param boolean /integer $page
	 *
	 * @return Array
	 */
	public function getImagesPaginated($page = false)
	{
		$criteria = new CDbCriteria;
		$criteria->compare('gallery_id', $this->id);
		$criteria->order = '`order` ASC, `id` ASC';

		$pages = NULL;
		if($page !== false){ //use pagination :)
			$itemCount = GalleryImage::model()->count($criteria);

			$pages = new CPagination($itemCount);
			$pages->setPageSize(self::PAGE_SIZE_SINGLE);
			$pages->setCurrentPage($page);
			$pages->applyLimit($criteria); // the trick is here!
		}

		return Array(
			'items' => GalleryImage::model()->findAll($criteria),
			'pages' => $pages,
		);
	}

	/**
	 * Get images count in gallery
	 *
	 * @return integer
	 */
	public function getImagesCount()
	{
		return GalleryImage::model()->countByAttributes(Array(
			'gallery_id' => $this->id
		));
	}

	/**
	 * Gets image by id
	 *
	 * @param $imageId
	 * @return GalleryImage
	 */
	public function getImage($imageId)
	{
		return GalleryImage::model()->findByAttributes(Array(
			'id' => (int)$imageId,
			'gallery_id' => $this->id,
		));
	}

	/**
	 * Gets first image in gallery
	 *
	 * @return GalleryImage
	 */
	public function getFirstImage()
	{
		$criteria = new CDbCriteria;
		$criteria->compare('gallery_id', $this->id);
		$criteria->order = '`order` ASC';

		return GalleryImage::model()->find($criteria);
	}

	/**
	 * Gets previous image of specified image
	 *
	 * @param Image $image
	 *
	 * @return GalleryImage
	 */
	public function getPreviousImage(Image $image)
	{
		$criteria = new CDbCriteria;
		$criteria->compare('gallery_id', $this->id);
		//$criteria->compare('`order`', ($image->order - 1));
		$criteria->addCondition('`order` <= ' . ($image->order - 1));
		$criteria->order = '`order` DESC';
		$criteria->limit = 1;

		return GalleryImage::model()->find($criteria);
	}

	/**
	 * Gets next image of specified image
	 *
	 * @param Image $image
	 *
	 * @return GalleryImage
	 */
	public function getNextImage(Image $image)
	{
		$criteria = new CDbCriteria;
		$criteria->compare('gallery_id', $this->id);
		$criteria->addCondition('`order` >= ' . ($image->order + 1));
		$criteria->limit = 1;

		return GalleryImage::model()->find($criteria);
	}

	/**
	 * Checks if gallery has ANY image
	 *
	 * @return boolean
	 */
	public function hasImages()
	{
		return (bool)GalleryImage::model()->countByAttributes(Array(
			'gallery_id' => $this->id,
		));
	}

	/**
	 * Increases view counter
	 *
	 * @param integer $count : how many views we should add
	 *
	 * @return nothing
	 */
	public function increaseViewCount($count = 1)
	{
		$this->view_count += max(0, (int)$count);

		Yii::app()->db->createCommand()
			->update($this->tableName(), Array('view_count' => $this->view_count), 'id = :id', Array(':id' => $this->id));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 *
	 * @param string $className active record class name.
	 *
	 * @return Gallery the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Gets galleries
	 *
	 * @param boolean/integer $type: type of gallery
	 * @param boolean/integer $page: page id, defaults to false (without pages)
	 *
	 * @throws Exception
	 * @return Gallery[]
	 */
	public static function findAllByType($type = false, $page = false)
	{
		$criteria = new CDbCriteria;
		$criteria->alias = 'Gallery';

        if($type !== false){
            $criteria->compare('Gallery.type', $type);
        }

		$criteria->compare('Gallery.hidden', 0); //basic filtering, we don't want hidden galleries
		$criteria->addCondition('thumb.id IS NOT NULL');
		$criteria->with = 'thumb';

		$criteria->order = 'Gallery.date DESC'; //newest ones first
		$criteria->with = Array('thumb');

		$pages = NULL;
		if($page !== false){ //use pagination :)
			$itemCount = self::model()->count($criteria);

			$pages = new CPagination($itemCount);
			$pages->setPageSize(self::PAGE_SIZE);
			$pages->setCurrentPage($page);
			$pages->applyLimit($criteria); // the trick is here!
		}

		return Array(
			'items' => self::model()->findAll($criteria),
			'pages' => $pages,
		);
	}
}
