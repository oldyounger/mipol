<?php
/**
 * This is the model class for table "UserLoginHistory".
 *
 * The followings are the available columns in table 'UserLoginHistory':
 * @property string $id
 * @property string $user_id
 * @property string $ip
 * @property string $date
 * @property integer $success
 *
 * The followings are the available model relations:
 * @property User $user
 */
class UserLoginHistory extends CActiveRecord
{
	public $email;
	public $firstname;
	public $lastname;

	/**
	* Returns the static model of the specified AR class.
	* @param string $className active record class name.
	* @return UserLoginHistory the static model class
	*/
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	* @return string the associated database table name
	*/
	public function tableName()
	{
		return '{{user_login_history}}';
	}

	/**
	* @return array validation rules for model attributes.
	*/
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return Array(
			Array('user_id, success', 'required'),
			Array('success', 'numerical', 'integerOnly' => true),
			Array('user_id', 'length', 'max' => 10),
			Array('ip', 'length', 'max' => 15),
			Array('ip, date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			Array('id, user_id, ip, date, success, email, firstname, lastname', 'safe', 'on' => 'search'),
		);
	}

	/**
	* @return array relational rules.
	*/
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return Array(
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	/**
	* @return array customized attribute labels (name=>label)
	*/
	public function attributeLabels()
	{
		return Array(
			'id' => 'ID',
			'user_id' => 'Użytkownik',
			'ip' => 'IP',
			'date' => 'Data',
			'success' => 'Powodzenie',
			'email' => 'Email',
			'firstname' => 'Imię',
			'lastname' => 'Nazwisko',
		);
	}

	/**
	* Retrieves a list of models based on the current search/filter conditions.
	* @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	*/
	public function search()
	{               
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('userLoginHistoryId', $this->userLoginHistoryId, true);
		$criteria->compare('t.id', $this->user_id, true);
		$criteria->compare('ip', $this->ip, true);
		$criteria->compare('date', $this->date, true);
		$criteria->compare('success', $this->success);

		$criteria->compare('user.email', $this->email);
		$criteria->compare('user.firstname', $this->firstname);
		$criteria->compare('user.lastname', $this->lastname);

		$criteria->with = Array('user');

		return new CActiveDataProvider($this, Array(
			'criteria'=>$criteria,
			'sort' => Array(
				'attributes' => Array(
					'*',
					'email' => Array(
						'asc' => "user.email",
						'desc' => "user.email DESC",
					),
					'firstname' => Array(
						'asc' => "user.firstname",
						'desc' => "user.firstname DESC",
					),
					'lastname' => Array(
						'asc' => "user.lastname",
						'desc' => "user.lastname DESC",
					)
				),
				'defaultOrder' => "`date` DESC"
			)
		));
	}

	public function beforeSave()
	{
		if($this->isNewRecord){
			$this->date = new CDbExpression("NOW()");
			$this->ip = $_SERVER['REMOTE_ADDR'];
		}
		
		return parent::beforeSave();
	}

	public static function failure($email)
	{
		$user = User::model()->findByAttributes(Array(
			'email' => $email
		));
		
		if($user){
			$loginHistory = new UserLoginHistory();
			$loginHistory->user_id = $user->id;
			$loginHistory->success = 0;
			$loginHistory->save();
		}
	}

	public static function successful($user_id)
	{
		$loginHistory = new UserLoginHistory();
		$loginHistory->user_id = $user_id;
		$loginHistory->success = 1;
		$loginHistory->save();
	}

	/**
	* Zwraca dane do wykresu wg parametrow
	* @param array $params parametry?
	* @return ChartData
	*/
	public function getForChart($params = Array())
	{
		$data = new ChartData();

		$rawData = Yii::app()->db->createCommand()
			->from("UserLoginHistory")
			->select("SUM(IF(success = 1,1,0)) as successful, SUM(IF(success = 0,1,0)) as failure, date(`date`) as `date`")
			->group(array('date(`date`)'))
			->where("`date` >= (DATE_SUB(CURDATE(),INTERVAL 30 DAY))")
			->queryAll();

		$data->mesureName = "Ilość";
		$data->title = "Logowania na przestrzeni ostatnich 30 dni";
		$data->cols = array(0 => "Udane", 1 => "Nieudane");
		
		foreach($rawData as $i => $row){
			$data->rows[$i] = $row['date'];
			$data->data[$i] = Array(
				0 => (int)$row['successful'],
				1 => -(int)$row['failure']
			);
		}
		
		return $data;
	}
}