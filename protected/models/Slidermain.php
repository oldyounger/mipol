<?php
/**
 * This is the model class for table "slider_main".
 *
 * The followings are the available columns in table 'slider_main':
 * @property string $id
 * @property string $title
 * @property string $content
 * @property string $image_id
 * @property string $link
 * @property integer $hidden
 * @property integer $order
 *
 * The followings are the available model relations:
 * @property Image $image
 */
class Slidermain extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{slider_main}}';
	}

	/**
	 * @return Array validation rules for model attributes.
	 */
	public function rules()
	{
		return Array(
			Array('title, hidden', 'required'),
			Array('image_id, hidden, order', 'numerical', 'integerOnly' => true),
			Array('title', 'length', 'max' => 255),
            Array('link', 'length', 'max' => 512),
			Array('id, title,image_id, link, hidden,content, order', 'safe', 'on' => 'search'),
			Array('id, title,image_id, link, hidden,content, order', 'safe', 'on' => 'insert,update'),

		);
	}

	/**
	 * @return Array relational rules.
	 */
	public function relations()
	{
		return Array(
			'image' => Array(self::BELONGS_TO, 'Image', 'image_id'),
		);
	}

	/**
	 * @return Array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return Array(
			'id' => 'ID',
			'title' => 'Tytuł',
			'image_id' => 'Zdjęcie',
			'link' => 'Link',
			'hidden' => 'Ukryty',
			'order' => 'Kolejność',
			'content' => 'Treść',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria = new CDbCriteria;
		$criteria->compare('id', $this->id);
		$criteria->compare('title', $this->title, true);
		$criteria->compare('image_id', $this->image_id);
		$criteria->compare('link', $this->link, true);
		$criteria->compare('hidden', $this->hidden);
		$criteria->compare('order', $this->order);
		$criteria->compare('content', $this->content);
		return new CActiveDataProvider($this, Array(
			'criteria' => $criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Slidermain the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Updates order of a slider
	 *
	 * @param array $orderArray
	 * @return void
	 */
	public static function updateOrder($orderArray)
	{
		$order = 0;
		if(is_array($orderArray)){
			foreach($orderArray as $slide){
				/** @var Slidermain $slideModel */
				$slideModel = self::model()->findByAttributes(Array(
					'id' => $slide,
				));

				if($slideModel !== NULL){
					$slideModel->order = $order++;

					$slideModel->save();
				}
			}
		}
	}

    /**
     * Get all not hidden slides
     *
     * @return Slidermain[]
     */
    public static function getAllNotHidden()
    {
        $criteria = new CDbCriteria;
        $criteria->alias = 'Slidermain';
        $criteria->compare('Slidermain.hidden', 0);
        $criteria->order = '`order` ASC';

        $criteria->with = Array('image');

        return Slidermain::model()->findAll($criteria);
    }

	/**
	 * Gets array to use in carousel
	 *
	 * @return Array
	 */
	public static function getCarouselData()
	{
		$ret = Array();

		$mainImageOptions = Array(
			'resize' => Array(
				'width' => 600,
				'height' => 380,
				'fit' => 'fill',
				'scale' => 'any',
			),
			'crop' => Array(
				'width' => 600,
				'height' => 380
			)
		);

		$thumbImageOptions = Array(
			'resize' => Array(
				'width' => 120,
				'height' => 60,
				'fit' => 'fill',
				'scale' => 'any',
			),
			'crop' => Array(
				'width' => 120,
				'height' => 60
			)
		);

		$criteria = new CDbCriteria;
		$criteria->alias = 'Slidermain';
		$criteria->compare('Slidermain.hidden', 0);
		$criteria->order = '`order` ASC';

		$criteria->with = Array('image');

		/** @var Slidermain[] $all */
		$all = self::model()->findAll($criteria);
		foreach($all as $one){
			$arr = Array(
				'label' => $one->title,
				'caption' => $one->description,
				'id' => $one->id,
				'thumbs' => Array(
					'next' => 5,
					'allowFromStart' => true,
				),
			);

			if($one->image !== NULL){
				$arr['image'] = $one->image->getImageUrl($mainImageOptions);
				$arr['thumb'] = $one->image->getImageUrl($thumbImageOptions);
			}
			else{ //no image, we cannot add this..
				continue;
			}

			if(mb_strlen($one->link) > 0){ //has got a link
				$arr['button'] = Array(
					'caption' => 'Zobacz więcej',
					'link' => $one->link
				);

				$arr['label'] = CHtml::link($arr['label'], $one->link);
			}

			$ret[] = $arr;
		}

		return $ret;
	}

	/**
	 * Gets array to use in UnsliderWidget
	 *
	 * @return Array
	 */
	public static function getUnsliderData()
	{
		$ret = Array();

		$thumbImageOptions = Array(
			'resize' => Array(
				'width' => 557,
				'height' => 268,
				'fit' => 'fill',
				'scale' => 'any',
			),
			'crop' => Array(
				'width' => 557,
				'height' => 268
			)
		);

		$criteria = new CDbCriteria;
		$criteria->alias = 'Slidermain';
		$criteria->compare('Slidermain.hidden', 0);
		$criteria->order = '`order` ASC';

		$criteria->with = Array('image');

		/** @var Slidermain[] $all */
		$all = self::model()->findAll($criteria);
		foreach($all as $one){
			if($one->image === NULL){ //image does not exists
				continue;
			}

			$arr = Array(
				'title' => $one->title,
				'description' => $one->description,
				'image' => $one->image->getImageUrl($thumbImageOptions),
			);

			$ret[] = $arr;
		}

		return $ret;
	}
}
