<?php
/**
 * This is the model class for table "gallery_image".
 *
 * The followings are the available columns in table 'avatar':
 * @property integer $id
 * @property integer $gallery_id
 * @property integer $order
 * @property integer $view_count
 * @property boolean $hidden
 * @property string $movie_id
 * @property integer $movie_type
 * @property string $description
 * @property string $file
 * @property string $name
 * @property integer $size
 * @property integer $width
 * @property integer $height
 * @property string $uniqueDir
 * @property string $mime
 * @property string $addedTime
 * @property integer $delete
 *
 * The followings are the available model relations:
 * @property Gallery $galleryThumb
 * @property GalleryComment[] $galleryComments
 * @property Gallery $gallery
 *
 * @property integer $galleryPinCount
 */
class GalleryImage extends Image
{
	const PIN_COUNT_MAX = 100; //this will be enough for normal photos, not spamming ones.

	public static $uploadDirectory = 'galleries';

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return GalleryImage
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{gallery_image}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return Array(
			Array('gallery_id, order, view_count, hidden, file, name, size, width, height, uniqueDir, mime', 'required'),
			Array('gallery_id, order, view_count, hidden, size, width, height, movie_type', 'numerical', 'integerOnly' => true),
			Array('file, name, mime', 'length', 'max' => 128),
			Array('uniqueDir', 'length', 'max' => 128),
			Array('movie_id', 'length', 'max' => 30),
			Array('description', 'safe'),
			Array('id, gallery_id, order, view_count, hidden, movie_id, movie_type, file, name, size, width, height, uniqueDir, mime', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return Array(
			'galleryThumb' => Array(self::HAS_ONE, 'Gallery', 'thumb_id'),
			'galleryComments' => Array(self::HAS_MANY, 'GalleryComment', 'image_id'),
			'gallery' => Array(self::BELONGS_TO, 'Gallery', 'gallery_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return Array(
			'id' => 'ID',
			'gallery_id' => 'Galeria',
			'order' => 'Kolejność',
			'view_count' => 'Ilość wyświetleń',
			'hidden' => 'Ukryty',
			'delete' => 'Youtube ID',
			'file' => 'Plik',
			'name' => 'Nazwa oryginalna',
			'size' => 'Rozmiar',
			'width' => 'Szerokość',
			'height' => 'Wysokość',
			'uniqueDir' => 'Katalog',
			'mime' => 'Typ MIME',
			'addedTime' => 'Czas dodania',
			'delete' => 'Usunąć?',
			'movie_id' => 'ID filmu',
			'movie_type' => 'Typ filmu',
			'description' => 'Opis',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('gallery_id', $this->gallery_id);
		$criteria->compare('order', $this->order);
		$criteria->compare('view_count', $this->view_count);
		$criteria->compare('hidden', $this->hidden);
		$criteria->compare('movie_id', $this->movie_id, true);
		$criteria->compare('movie_type', $this->movie_type);
		$criteria->compare('description', $this->description, true);

		$criteria->compare('file', $this->file, true);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('size', $this->size);
		$criteria->compare('width', $this->width);
		$criteria->compare('height', $this->height);
		$criteria->compare('uniqueDir', $this->uniqueDir);
		$criteria->compare('mime', $this->mime, true);

		return new CActiveDataProvider($this, Array(
			'criteria' => $criteria,
		));
	}

	/**
	 * Gets directory path
	 * @return string
	 */
	public function getDirectoryPath()
	{
		return ROOT_DIR . DIRECTORY_SEPARATOR . self::getDirectory() . DIRECTORY_SEPARATOR . $this->gallery_id . DIRECTORY_SEPARATOR . $this->uniqueDir . DIRECTORY_SEPARATOR;
	}

	/**
	 * Gets image path
	 *
	 * @return string: pth
	 */
	public function getImagePath()
	{
		return $this->getDirectoryPath() . $this->file;
	}

	/**
	 * Gets image HTML path (used in URL)
	 *
	 * @return string: path
	 */
	public function getImageHtmlPath()
	{
		return Yii::app()->getBaseUrl(false) . '/' . self::getDirectory() . '/' . $this->gallery_id . '/' . $this->uniqueDir . '/' . $this->file;
	}

	/**
	 * Gets thumb path
	 *
	 * @param $thumbFile: thumb name
	 * @return string: path
	 */
	public function getThumbPath($thumbFile)
	{
		return $this->getDirectoryPath() . $thumbFile;
	}

	/**
	 * Gets thumb HTML path (used in URL)
	 *
	 * @param $thumbFile: thumb name
	 * @return string: path
	 */
	public function getThumbHtmlPath($thumbFile)
	{
		return Yii::app()->getBaseUrl(false) . '/' . self::getDirectory() . '/' . $this->gallery_id . '/' . $this->uniqueDir . '/' . $thumbFile;
	}

	/**
	 * Gets comment list of gallery image for use in CommentsListWidget
	 * @return Array with records
	 */
	public function getCommentsListForWidget()
	{
		$ret = Array();
		foreach($this->galleryComments as $comment){
			$ret[] = Array(
				'nick' => $comment->user->nick,
				'date' => $comment->date,
				'content' => $comment->content,
				'id' => $comment->id,
				'author_id' => $comment->user->id,
			);
		}

		return $ret;
	}

	/**
	 * Gets a filename for download (galleryName-idPhoto.EXT)
	 */
	public function getDownloadFileName()
	{
		$extension = $this->getExtension();

		return $this->gallery->url . '__' . $this->id . '.' . $extension;
	}

	public function getExtension()
	{
		$extensions = Array(
			'image/jpeg' => 'jpg',
			'image/png' => 'png',
			'image/gif' => 'gif',
			'image/bmp' => 'bmp',
		);

		if(isSet($extensions[$this->mime])){
			return $extensions[$this->mime];
		}

		return 'bin';
	}

	/**
	 * Increases view counter
	 * @param integer $count: how many views we should add
	 * @return void
	 */
	public function increaseViewCount($count = 1)
	{
		$this->view_count += max(0, (int)$count);

		Yii::app()->db->createCommand()
			->update($this->tableName(), Array('view_count' => $this->view_count), 'id = :id', Array(':id' => $this->id));
	}

	/**
	 * Performs upload of GALLERY image (or moving)
	 *
	 * @param string $name
	 * @param string $type
	 * @param string $tmpName
	 * @param int $error
	 * @param int $size
	 * @param bool $normalUpload: true = move_uploaded_file, false = rename (normal moving in system)
	 * @param Array $requestInfo: dump of $_REQUEST table
	 * @return mixed
	 * @throws Exception
	 */
	public static function uploadFile($name, $type, $tmpName, $error, $size, $normalUpload = true, $requestInfo = Array())
	{
		try{
			if(!isSet($requestInfo['galleryId'])){
				throw new Exception('Gallery ID is not set. Cannot upload Gallery Image.');
			}

			self::$uploadDirectory = 'galleries' . DIRECTORY_SEPARATOR . (int)$requestInfo['galleryId'];

			$imagesRoot = ROOT_DIR . DIRECTORY_SEPARATOR . self::$uploadDirectory;
			if(!is_dir($imagesRoot) && !mkdir($imagesRoot, 0777)){
				throw new Exception('Could not create directory "'.self::$uploadDirectory.'" for gallery image.');
			}

			@chmod($imagesRoot, 0777);

			return self::uploadFilePostPhp52IsShit($name, $type, $tmpName, $error, $size, $normalUpload, $requestInfo);
		}
		catch(Exception $e){
			CGlobal::file_r('GALLERY IMAGE EXCEPTION '.$e);
		}
	}

	/**
	 * Saves record to database
	 *
	 * @param $name
	 * @param $uniqueDir
	 * @param $cleanUniqueNameExt
	 * @param $imageinfo
	 * @param $size
	 * @param $extension
	 * @param $requestInfo
	 * @return integer
	 * @throws Exception
	 */
	public static function saveToDatabase($name, $uniqueDir, $cleanUniqueNameExt, $imageinfo, $size, $extension, $requestInfo)
	{
		$model = new GalleryImage();

		/*$order = GalleryImage::model()->countByAttributes(Array(
			'gallery_id' => (int)$requestInfo['galleryId'],
		));*/
		//SELECT `order`+1 FROM `gallery_image` WHERE `gallery_id` = 49 ORDER BY `order` DESC LIMIT 1
		$order = Yii::app()->db->createCommand()
			->select('order')
			->from(self::model()->tableName())
			->where('`gallery_id` = :gallery_id', Array(':gallery_id' => (int)$requestInfo['galleryId']))
			->limit(1)
			->order('order DESC')
			->queryScalar();

		if($order === false){
			$order = 0;
		}
		else{
			$order++; //increase by 1
		}

		//we must apply a watermark somehow..
		//Open: $uniqueDir / file
		//then merge with NN logo & club logo (if available)

		//we can't use getImagePath() here ;(
		/*$imageUrl = self::$uploadDirectory . DIRECTORY_SEPARATOR . $uniqueDir . DIRECTORY_SEPARATOR . $cleanUniqueNameExt;
		$img = WideImage::load($imageUrl);

		$wantedWidth = 0.25 * $img->getWidth(); //25% of width
		$wantedHeight = 0.10 * $img->getHeight(); //10% of height

		$nnLogo = WideImage::load(Yii::app()->basePath . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR .  'watermark_logo.png');
		$nnLogo = $nnLogo->resize($wantedWidth, $wantedHeight, 'inside', 'down');

		$new = $img->merge($nnLogo, '3%', 'bottom - 3%', 100); //bottom left + 30px padding

		$new->saveToFile($imageUrl);*/

		$model->gallery_id = (int)$requestInfo['galleryId'];
		$model->order = $order;
		$model->view_count = 0;
		$model->hidden = 0;

		$model->mime		= $extension;
		$model->size		= $size;
		$model->width		= $imageinfo[0];
		$model->height		= $imageinfo[1];
		$model->file		= $cleanUniqueNameExt;
		$model->name		= $name;
		$model->uniqueDir	= $uniqueDir;
		$model->addedTime	= date('Y-m-d H:i:s');
		$model->delete		= 0;

		if(!$model->save()){
			throw new Exception('Could not save Gallery Image model.');
		}

		return $model->getPrimaryKey();
	}

    /**
     * Returns the directory to upload in
     * @return string
     */
    public static function getDirectory()
    {
        return self::$uploadDirectory;
    }

    /**
     * Performs upload of image (or moving)
     *
     * @param string $name
     * @param string $type
     * @param string $tmpName
     * @param int $error
     * @param int $size
     * @param bool $normalUpload: true = move_uploaded_file, false = rename (normal moving in system)
     * @param Array $requestInfo: dump of $_REQUEST table
     * @return mixed
     * @throws Exception
     */
    public static function uploadFilePostPhp52IsShit($name, $type, $tmpName, $error, $size, $normalUpload = true, $requestInfo = Array())
    {
        $allowedExtensions = Array(
            'jpg'	=> 'image/jpeg',
            'jpeg'	=> 'image/jpeg',
            'png'	=> 'image/png',
            'gif'	=> 'image/gif',
        );

        $pathinfo = pathinfo($name);

        /** FIX BUG 1: extension was case sensitive - files *.JPG was not allowed **/
        $pathinfo['extension'] = mb_strtolower($pathinfo['extension']);

        if(!array_key_exists($pathinfo['extension'], $allowedExtensions)){
            throw new Exception('File extension not allowed.');
        }

        $imagesRoot = realpath(ROOT_DIR . DIRECTORY_SEPARATOR . self::getDirectory());

        if($imagesRoot == '' || !is_dir($imagesRoot)){
            throw new Exception('Missing image root directory.');
        }

        $cleanUniqueName = CString::cleanFilename($pathinfo['filename']);
        $cleanUniqueNameExt = $cleanUniqueName . '.' . $pathinfo['extension'];

        // unique catalog for image and it's thumbnails in format "clean-file-name___2013_10_10___12_55_12___1123"
        $uniqueDir = substr($cleanUniqueName, 0, 97) . date('__Y_m_d___H_i_s___') . rand(1000,9999);
        $uniquePath = $imagesRoot . DIRECTORY_SEPARATOR . $uniqueDir;

        if(!mkdir($uniquePath, 0777)){
            throw new Exception('Could not create directory "'.$uniqueDir.'" for image.');
        }

        @chmod($uniquePath, 0777);

        if($normalUpload && !@move_uploaded_file($tmpName, $uniquePath . DIRECTORY_SEPARATOR . $cleanUniqueNameExt)){
            throw new Exception('Could not move uploaded file.');
        }
        else if(!$normalUpload && !@rename($tmpName, $uniquePath . DIRECTORY_SEPARATOR . $cleanUniqueNameExt)){
            throw new Exception('Could not move uploaded file by rename.');
        }

        $imageinfo = getimagesize($uniquePath . DIRECTORY_SEPARATOR . $cleanUniqueNameExt);

        $saveExtension = $allowedExtensions[$pathinfo['extension']];
        return self::saveToDatabase($name, $uniqueDir, $cleanUniqueNameExt, $imageinfo, $size, $saveExtension, $requestInfo);
    }

    /**
     * Shorthand function for getting image URL
     *
     * @param $id
     * @param array $options
     * @return string
     */
    public static function url($id, $options = Array())
    {
        $image = self::model()->findByPk($id);
        if($image !== NULL){
            return $image->getImageUrl($options);
        }
        else{ //does not exists, blank image
            return 'data:image/gif;base64,R0lGODlhAQABAIAAAAUEBAAAACwAAAAAAQABAAACAkQBADs=';
        }
    }

    /**
     * gets url to image
     * @param Array $options options of resizing ($fit 'inside', 'outside', 'fill', $scale 'down', 'up', 'any')
     * @param boolean $asArray: shall we return result as array or path ? defaults to false (return path)
     * @param boolean $returnHtmlPath: shall we return html path ? defaults to true, if false return HDD path
     * @return mixed depends on $asArray
     */
    public function getImageUrl($options = Array(), $asArray = false, $returnHtmlPath = true)
    {
        $baseFilePath = $this->getImagePath();

        //var_dump($baseFilePath);

        if(!file_exists($baseFilePath)){ //empty image file to prevent X
            if($returnHtmlPath){
                return 'data:image/gif;base64,R0lGODlhAQABAIAAAAUEBAAAACwAAAAAAQABAAACAkQBADs=';
            }
            else{
                return false;
            }
        }

        //echo 'ex';die;

        if(!$options && $returnHtmlPath){ //plain image without options (full size)
            return $this->getImageHtmlPath();
        }
        else if(!$options){
            return $this->getImagePath();
        }

        $options = $this->getImageOptions($options);

        $thumbFile		= $this->getThumbName($this->file, $options);
        $thumbFilePath	= $this->getThumbPath($thumbFile);

        if(!file_exists($thumbFilePath)){
            $image = WideImage::load($baseFilePath);
            if(isSet($options['resize'])){
                $image = $image->resize($options['resize']['width'], $options['resize']['height'], $options['resize']['fit'], $options['resize']['scale']);
            }

            if(isSet($options['crop'])){
                $image = $image->crop($options['crop']['top'], $options['crop']['left'], $options['crop']['width'], $options['crop']['height']);
            }

            //Compress based on extension
            $ext = WideImage_MapperFactory::determineFormat($thumbFilePath);
            if($ext === 'PNG'){
                $image->saveToFile($thumbFilePath, 9); //max compression
            }
            else if($ext == 'JPEG'){
                $image->saveToFile($thumbFilePath, 90);
            }
            else{ //other types we cannot convert
                $image->saveToFile($thumbFilePath);
            }
        }

        if($asArray){
            return Array(
                'uniqueDir' => $this->uniqueDir,
                'fileName' => $thumbFile,
                'filePath' => $thumbFilePath,
            );
        }

        if(!$returnHtmlPath){
            return $this->getThumbPath($thumbFile);
        }

        return $this->getThumbHtmlPath($thumbFile);
    }
}
