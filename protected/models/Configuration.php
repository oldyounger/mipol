<?php
/**
 * This is the model class for table "configuration".
 *
 * The followings are the available columns in table 'configuration':
 * @property string $key
 * @property string $value
 * @property string $description
 * @property string $date_changed
 */
class Configuration extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{configuration}}';
	}

	/**
	 * @return Array validation rules for model attributes.
	 */
	public function rules()
	{
		return Array(
			Array('key, value', 'required'),
			Array('key', 'length', 'max' => 64),
			Array('key, value, description, date_changed', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return Array relational rules.
	 */
	public function relations()
	{
		return Array(
			//
		);
	}

	/**
	 * @return Array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return Array(
			'key' => 'Klucz',
			'value' => 'Wartość',
			'description' => 'Opis',
			'date_changed' => 'Ostatnia zmiana',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('key', $this->key, true);
		$criteria->compare('value', $this->value, true);
		$criteria->compare('description', $this->description, true);
		$criteria->compare('date_changed', $this->date_changed, true);

		return new CActiveDataProvider($this, Array(
			'criteria' => $criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Configuration the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Gets value for specified key in configuration. If not found, then returns NULL
	 *
	 * @param $key: key to check
	 * @return string|NULL
	 */
	public static function getValue($key)
	{
		/** @var Configuration $model */
		$model = self::model()->findByPk($key);
		if($model !== NULL){
			return $model->value;
		}

		return NULL;
	}
}
