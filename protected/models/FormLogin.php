<?php
/**
 * FormLogin class.
 * FormLogin is the data structure for keeping
 * user login form data.
 */
class FormLogin extends CFormModel
{
	public $email;
	public $password;
	public $rememberMe;
	private $_identity;

	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
		return Array(
			Array('email, password', 'required'),
			Array('rememberMe', 'boolean'),
			Array('password', 'authenticate'),
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return Array(
			'rememberMe' => 'Zapamiętaj mnie',
			'email' => 'E-mail',
			'password' => 'Hasło',
		);
	}

	/**
	 * Authenticates the password.
	 * This is the 'authenticate' validator as declared in rules().
	 */
	public function authenticate($attribute, $params)
	{
		if(!$this->hasErrors()){
			$this->_identity = new UserIdentity($this->email, $this->password);
			if(!$this->_identity->authenticate()){
				if($this->_identity->errorCode == UserIdentity::ERROR_NOT_ACTIVATED){
					$this->addError('password', Yii::t('register', 'Your account is not activated.'));
				}
				else{
					$this->addError('password', Yii::t('register', 'Incorrect e-mail or password.'));
				}
			}
		}
	}

	/**
	 * Logs in the user using the given username and password in the model.
	 * @return boolean whether login is successful
	 */
	public function login()
	{
		if ($this->_identity === NULL){
			$this->_identity = new UserIdentity($this->email, $this->password);
			$this->_identity->authenticate();
		}

		if ($this->_identity->errorCode === UserIdentity::ERROR_NONE){
			$duration = $this->rememberMe ? 3600 * 24 * 30 : 0; // 30 days
			Yii::app()->user->login($this->_identity, $duration);

			setCookie('show-start', 'no', (time(NULL) + 30 * 86400), '/');
			setCookie('cityCookie', '0', (time(NULL) - 30 * 86400), '/');

			return true;
		}
		else{
			return false;
		}
	}
}
