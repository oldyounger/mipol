<?php

/**
 * This is the model class for table "contact_form_answer".
 *
 * The followings are the available columns in table 'contact_form_answer':
 * @property integer $id
 * @property integer $contact_form_id
 * @property string $email
 * @property string $content
 * @property string $datetime
 * @property integer $sent
 *
 * The followings are the available model relations:
 * @property ContactForm $contactForm
 */
class ContactFormAnswer extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{contact_form_answer}}';
	}

	/**
	 * @return Array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return Array(
			Array('contact_form_id, email, content, datetime, sent', 'required'),
			Array('sent, contact_form_id', 'numerical', 'integerOnly' => true),
			Array('email', 'length', 'max' => 128),
			Array('id, contact_form_id, email, content, datetime, sent', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return Array relational rules.
	 */
	public function relations()
	{
		return Array(
			'contactForm' => Array(self::BELONGS_TO, 'ContactForm', 'contact_form_id'),
		);
	}

	/**
	 * @return Array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return Array(
			'id' => 'ID',
			'contact_form_id' => 'ID Zapytania',
			'email' => 'E-mail',
			'content' => 'Treść',
			'datetime' => 'Data',
			'sent' => 'Wysłany',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('contact_form_id', $this->contact_form_id);
		$criteria->compare('email', $this->email, true);
		$criteria->compare('content', $this->content, true);
		$criteria->compare('datetime', $this->datetime, true);
		$criteria->compare('sent', $this->sent);

		return new CActiveDataProvider($this, Array(
			'criteria' => $criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 *
	 * @param string $className active record class name.
	 *
	 * @return ContactFormAnswer the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
}
