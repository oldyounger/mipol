<?php
/**
 * FormModal class.
 * FormModal is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class FormModal extends CFormModel
{
    public $name;
    public $email;
    public $telephone;
    public $flat_id;
    public $send_copy;

    /**
     * Declares the validation rules.
     */
    public function rules()
    {
        return Array(
            Array('name, email, telephone, flat_id', 'required'),
            Array('email, name', 'length', 'max' => 128),
            Array('telephone', 'length', 'max' => 50),
            Array('flat_id', 'numerical', 'integerOnly' => true),
            Array('send_copy', 'boolean'),
            Array('email', 'email'),
        );
    }

    /**
     * Declares customized attribute labels.
     * If not declared here, an attribute would have a label that is
     * the same as its name with the first letter in upper case.
     */
    public function attributeLabels()
    {
        return Array(
            'name' => 'Imię i nazwisko',
            'email' => 'E-mail',
            'telephone' => 'Telefon',
            'flat_id' => 'Mieszkanie',
            'send_copy' => 'Wyślij kopię',
        );
    }

    /**
     * Sends an email and adds record to DB
     *
     * @return boolean
     */
    public function sendEmail()
    {
        $entry = new ContactForm;
        $entry->name = $this->name;
        $entry->telephone = $this->telephone;
        $entry->title = 'Kontakt - mieszkanie';
        $entry->content = '-';
        $entry->email = $this->email;
        $entry->type = ContactForm::TYPE_FLAT;
        $entry->flat_id = $this->flat_id;
        $entry->datetime = date('Y-m-d H:i:s', time(NULL));
        $entry->type = ContactForm::TYPE_FLAT;

        if($entry->save()){
            $entry->sendEmail();

            if($this->send_copy){
                $entry->sendCopyEmail();
            }

            return true;
        }

        return false;
    }
}