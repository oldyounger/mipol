<?php
/**
 * This is the model class for table "SystemMailTemplate".
 *
 * The followings are the available columns in table 'SystemMailTemplate':
 *
 * @property string $templateId
 * @property string $name
 * @property string $subject
 * @property string $body
 */
class SystemMailTemplate extends CActiveRecord
{
	/**
	 * Systemowa nazwa szablonu aktywacji konta po rejestracji
	 */
	const TEMPLATE_REGISTER_ACTIVATION = 'register.activation';

	/**
	 * Systemowa nazwa szablon zapomnianego hasla
	 */
	const TEMPLATE_FORGOTTEN_PASSWORD = 'user.forgottenPassword';
	const TEMPLATE_FORGOTTEN_PASSWORD_GENERATED = 'user.forgottenPasswordGenerated';

	const TEMPLATE_NEWSLETTER_ACTIVATION = 'user.newsletterActivation';

	const TEMPLATE_CONTACT_FORM = 'user.contactForm';

    const TEMPLATE_CONTACT_FORM_FAST = 'user.contactFormFast';

    const TEMPLATE_CONTACT_FORM_FLAT = 'user.contactFormFlat';

    const TEMPLATE_CONTACT_FORM_COPY = 'user.contactFormCopy';
    const TEMPLATE_CONTACT_FORM_FAST_COPY = 'user.contactFormFastCopy';
    const TEMPLATE_CONTACT_FORM_FLAT_COPY = 'user.contactFormFlatCopy';

    const TEMPLATE_CONTACT_FORM_LANDING = 'user.contactFormLanding';
    const TEMPLATE_CONTACT_FORM_LANDING_COPY = 'user.contactFormLandingCopy';

    const TEMPLATE_CONTACT_FORM_LANDING_APARTMENTS = 'user.contactFormLandingApartments';
    const TEMPLATE_CONTACT_FORM_LANDING_APARTMENTS_COPY = 'user.contactFormLandingApartmentsCopy';

    const TEMPLATE_CONTACT_FORM_LANDING_FLATS = 'user.contactFormLandingFlats';
    const TEMPLATE_CONTACT_FORM_LANDING_FLATS_COPY = 'user.contactFormLandingFlatsCopy';

    const TEMPLATE_CONTACT_FORM_LANDING_JOURNEY = 'user.contactFormLandingJourney';
    const TEMPLATE_CONTACT_FORM_LANDING_JOURNEY_COPY = 'user.contactFormLandingJourneyCopy';

    const TEMPLATE_CONTACT_FORM_LANDING_WEB = 'user.contactFormLandingWeb';
    const TEMPLATE_CONTACT_FORM_LANDING_WEB_COPY = 'user.contactFormLandingWebCopy';

	/**
	 * Returns the static model of the specified AR class.
	 *
	 * @param string $className active record class name.
	 * @return SystemMailTemplate the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{systemmailtemplate}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			Array('name, subject, body', 'required'),
			Array('name', 'length', 'max' => 64),
			Array('subject', 'length', 'max' => 250),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			Array('templateId, name, subject, body', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return Array(
			//
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return Array(
			'templateId' => 'Szablon',
			'name' => 'Nazwa systemowa',
			'subject' => 'Temat wiadomości',
			'body' => 'Treść wiadomości',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('templateId', $this->templateId, true);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('subject', $this->subject, true);
		$criteria->compare('body', $this->body, true);

		return new CActiveDataProvider($this, Array(
			'criteria' => $criteria,
		));
	}

	public function getWithParams($templateIdOrSystemName, $params)
	{
		$criteria = new CDbCriteria;
		if(is_numeric($templateIdOrSystemName)){
			$criteria->compare('templateId', $templateIdOrSystemName);
		}
		else{
			$criteria->compare('name', $templateIdOrSystemName);
		}

		$template = $this->find($criteria);

		if(!$template){
			return false;
		}

		$templateArr = Array(
			'subject' => $template->subject,
			'body' => $template->body,
		);

		$this->_insertParams($templateArr['subject'], $params);
		$this->_insertParams($templateArr['body'], $params);

		return $templateArr;
	}

	protected function _insertParams(&$text, &$params)
	{
		foreach($params as $name => $value){
			$text = str_replace("%$name%", $value, $text);
		}

		//ekstra zamiana stalych parametrow
		$text = str_replace("%site_url%", Yii::app()->getBaseUrl(true) . "/", $text);
	}
}