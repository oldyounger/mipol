<?php
/**
 * This is the model class for table "Page".
 *
 * The followings are the available columns in table 'Page':
 * @property int $id
 * @property string $name
 * @property string $url
 * @property string $description
 * @property string $keywords
 * @property string $content
 * @property string $stamp
 * @property integer $internal
 * @property integer $show_slider
 * @property integer $gallery_id
 * @property integer $test
 *
 * The followings are the available model relations:
 * @property Gallery $gallery
 */
class Page extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Page the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{page}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return Array(
			Array('name', 'required'),
			Array('name', 'length', 'max' => 128),
			Array('internal, show_slider, gallery_id', 'numerical', 'integerOnly' => true),
			Array('url', 'length', 'max' => 134),
			Array('url', 'required', 'on' => 'update'),
			Array('url', 'unique', 'caseSensitive' => false, 'on'=> 'update'),
			Array('description, keywords, content', 'safe', 'on'=> 'insert, update'),
			Array('id, name, url, description, keywords, content, stamp, gallery_id', 'safe', 'on' => 'search'),
		);
	}


	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return Array(
		    'gallery' => Array(self::BELONGS_TO, 'Gallery', 'gallery_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Tytuł',
			'url' => 'Adres URL',
			'description' => 'Opis SEO',
			'keywords' => 'Słowa kluczowe SEO',
			'content' => 'Treść',
			'stamp' => 'Stamp',
			'internal' => 'Wewnętrzna',
			'show_slider' => 'Pokaż slider',
            'gallery_id' => 'Galeria',
            'test' => 'test',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria = new CDbCriteria;
		$criteria->compare('id', $this->id);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('url', $this->url);
		$criteria->compare('description', $this->description, true);
		$criteria->compare('keywords', $this->keywords, true);
		$criteria->compare('content', $this->content, true);
		$criteria->compare('internal', $this->internal);
		$criteria->compare('show_slider', $this->show_slider);
        $criteria->compare('gallery_id', $this->gallery_id);
        $criteria->compare('test', $this->test);

		return new CActiveDataProvider($this, Array(
			'criteria' => $criteria,
		));
	}

    public function beforeSave()
    {
        if($this->hasAttribute('url')){
            if(mb_strlen($this->url) == 0){
                //$this->url = $this->createUrl($this->name);
                $field = NULL;
                //title or name is required
                if(isSet($this->name)){
                    $field = $this->name;
                }
                else{
                    $field = $this->title;
                }

                $this->url = CUniqueURL::getURL($field, 'Page', 'url');
            }

            //			var_dump($this->url);die;
            /*else{
                $this->url = $this->createUrl($this->url);
            }*/
        }

        if(Gallery::model()->findByPk($this->gallery_id) === NULL){
            $this->gallery_id = NULL;
        }

        return parent::beforeSave();
    }

	/**
	 * Return URL to page
	 *
	 * @param boolean $absolute include http://... ?
	 * @return string
	 */
	public function getUrl($absolute = false)
	{
		if($absolute){ //TODO
			return '/pl/' . $this->url . '.html';
		}
		else{
			return '/pl/' . $this->url . '.html';
		}
	}


	/**
	 * Returns Link <a> to page
	 *
	 * @param boolean $absolute
	 * @param string $label
	 * @param string $target
	 * @return string link
	 */
	public function getLink($absolute = false, $label = 'self::name', $target = '_self')
	{
		$url = $this->getUrl($absolute);

		if($label == 'self::name'){
			$label = $this->name;
		}
		else if($label == 'self::url'){
			$label = $url;
		}

		$htmlOptions = Array();
		if($target != '_self'){
			$htmlOptions['target'] = $target;
		}

		return CHtml::link(CHtml::encode($label), $url, $htmlOptions);
	}

	/**
	 * Gets page content by URL
	 *
	 * @param string $url
	 * @return string
	 */
	public static function getContent($url)
	{
		/** @var Page $page */
		$page = self::model()->findByAttributes(Array(
			'url' => $url,
		));

		if($page !== NULL){
			return $page->content;
		}

		return NULL;
	}
}