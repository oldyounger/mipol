<?php
/**
 * This is the model class for table "contact_form".
 *
 * The followings are the available columns in table 'contact_form':
 * @property integer $id
 * @property string $email
 * @property string $name
 * @property string $surname
 * @property string $telephone
 * @property string $title
 * @property string $content
 * @property string $datetime
 * @property string $hours
 * @property integer $type
 * @property integer $readed
 *
 * The followings are the available model relations:
 * @property ContactFormAnswer[] $contactFormAnswers
 */
class ContactForm extends CActiveRecord
{
	const TYPE_NORMAL = 0;
	const TYPE_FAST = 1;
    const TYPE_FLAT = 2;
    const TYPE_LANDING = 3;
    const TYPE_LANDING_APARTMENTS = 4;
    const TYPE_LANDING_FLATS = 5;
    const TYPE_LANDING_JOURNEY = 6;
    const TYPE_LANDING_WEB = 7;

	public static $VALID_TYPES = Array(
		self::TYPE_NORMAL,
		self::TYPE_FAST,
        self::TYPE_FLAT,
        self::TYPE_LANDING,
        self::TYPE_LANDING_APARTMENTS,
        self::TYPE_LANDING_FLATS,
        self::TYPE_LANDING_JOURNEY,
        self::TYPE_LANDING_WEB,
	);

	public static $VALID_TYPES_NAMES = Array(
		self::TYPE_NORMAL => 'podstrona',
		self::TYPE_FAST => 'szybki',
        self::TYPE_FLAT => 'mieszkanie',
        self::TYPE_LANDING => 'landing jubileuszowy',
        self::TYPE_LANDING_APARTMENTS => 'landing apartamenty',
        self::TYPE_LANDING_FLATS => 'landing mieszkania',
        self::TYPE_LANDING_JOURNEY => 'landing przygoda',
        self::TYPE_LANDING_WEB => 'landing przygoda strona',
    );

    public $flat_id;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{contact_form}}';
	}

	/**
	 * @return Array validation rules for model attributes.
	 */
	public function rules()
	{
		return Array(
			Array('email, name, title, type, datetime, content', 'required'),
			Array('readed, type', 'numerical', 'integerOnly' => true),
			Array('email, name, surname, title', 'length', 'max' => 128),
			Array('telephone', 'length', 'max' => 50),
            Array('hours', 'length', 'max' => 10),
			Array('id, email, name, surname, type, telephone, title, content, datetime, readed', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return Array relational rules.
	 */
	public function relations()
	{
		return Array(
			'contactFormAnswers' => Array(self::HAS_MANY, 'ContactFormAnswer', 'contact_form_id'),
		);
	}

	/**
	 * @return Array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return Array(
			'id' => 'ID',
			'email' => 'E-mail',
			'name' => 'Imię',
			'surname' => 'Nazwisko',
			'telephone' => 'Telefon',
			'title' => 'Tytuł',
			'content' => 'Treść',
			'datetime' => 'Data',
			'readed' => 'Przeczytany',
            'type' => 'Typ',
            'hours' => 'Godziny kontaktu',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('email', $this->email, true);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('surname', $this->surname, true);
		$criteria->compare('telephone', $this->telephone, true);
		$criteria->compare('title', $this->title, true);
		$criteria->compare('content', $this->content, true);
		$criteria->compare('datetime', $this->datetime, true);
		$criteria->compare('readed', $this->readed);
        $criteria->compare('type', $this->type);
        $criteria->compare('hours', $this->hours);

		return new CActiveDataProvider($this, Array(
			'criteria' => $criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 *
	 * @param string $className active record class name.
	 *
	 * @return ContactForm the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Sends e-mail to administrator
	 *
	 * @return boolean
	 */
	public function sendEmail($ignoreFlat = false)
	{
        if($ignoreFlat){
            $params = Array(
                'name' => $this->name,
                'email' => $this->email,
                'content' => $this->content,
                'surname' => $this->surname,
                'telephone' => $this->telephone,
                'title' => $this->title,
                'datetime' => $this->datetime,
                'hours' => $this->hours,
                'type' => self::$VALID_TYPES_NAMES[$this->type],
                'flat_id' => $this->flat_id,
            );
        }
        else{
            /** @var Flat $flat */
            $flat = Flat::model()->findByPk($this->flat_id);
            if($flat === NULL){
                return false;
            }

            $params = Array(
                'name' => $this->name,
                'email' => $this->email,
                'content' => $this->content,
                'surname' => $this->surname,
                'telephone' => $this->telephone,
                'title' => $this->title,
                'datetime' => $this->datetime,
                'hours' => $this->hours,
                'type' => self::$VALID_TYPES_NAMES[$this->type],
                'flat_id' => $this->flat_id,

                'state' => $flat->getStateAsString(),
                'flat_name' => $flat->name,
                'building' => $flat->building,
                'rooms' => $flat->rooms,
                'area' => str_replace('.', ',', $flat->area) . ' m²',
                'balcony' => $flat->getBalconyAsString(),
                'level' => $flat->level,
                'available' => $flat->getAvailableAsString(),
            );
        }

		$replyTo = $this->email;

        $destinationEmailKey = 'contact_email';

        $cMail = new CMail();

        if($this->type == self::TYPE_FAST){
            return $cMail->sendFromTemplate(SystemMailTemplate::TEMPLATE_CONTACT_FORM_FAST, Configuration::getValue($destinationEmailKey), 'Administrator', $params, $replyTo);
        }
        else if($this->type == self::TYPE_FLAT){
            return $cMail->sendFromTemplate(SystemMailTemplate::TEMPLATE_CONTACT_FORM_FLAT, Configuration::getValue($destinationEmailKey), 'Administrator', $params, $replyTo);
        }
        else if($this->type == self::TYPE_LANDING){
            return $cMail->sendFromTemplate(SystemMailTemplate::TEMPLATE_CONTACT_FORM_LANDING, Configuration::getValue($destinationEmailKey), 'Administrator', $params, $replyTo);
        }
        else if($this->type == self::TYPE_LANDING_APARTMENTS){
            return $cMail->sendFromTemplate(SystemMailTemplate::TEMPLATE_CONTACT_FORM_LANDING_APARTMENTS, Configuration::getValue($destinationEmailKey), 'Administrator', $params, $replyTo);
        }
        else if($this->type == self::TYPE_LANDING_FLATS){
            return $cMail->sendFromTemplate(SystemMailTemplate::TEMPLATE_CONTACT_FORM_LANDING_FLATS, Configuration::getValue($destinationEmailKey), 'Administrator', $params, $replyTo);
        }
        else if($this->type == self::TYPE_LANDING_JOURNEY){
            return $cMail->sendFromTemplate(SystemMailTemplate::TEMPLATE_CONTACT_FORM_LANDING_JOURNEY, Configuration::getValue($destinationEmailKey), 'Administrator', $params, $replyTo);
        }
        else if($this->type == self::TYPE_LANDING_WEB){
            return $cMail->sendFromTemplate(SystemMailTemplate::TEMPLATE_CONTACT_FORM_LANDING_WEB, Configuration::getValue($destinationEmailKey), 'Administrator', $params, $replyTo);
        }

		return $cMail->sendFromTemplate(SystemMailTemplate::TEMPLATE_CONTACT_FORM, Configuration::getValue($destinationEmailKey), 'Administrator', $params, $replyTo);
	}

    /**
     * Sends e-mail to sender
     *
     * @return boolean
     */
    public function sendCopyEmail($ignoreFlat = false)
    {
        if($ignoreFlat){
            $params = Array(
                'name' => $this->name,
                'email' => $this->email,
                'content' => $this->content,
                'surname' => $this->surname,
                'telephone' => $this->telephone,
                'title' => $this->title,
                'datetime' => $this->datetime,
                'hours' => $this->hours,
                'type' => self::$VALID_TYPES_NAMES[$this->type],
                'flat_id' => $this->flat_id
            );
        }
        else{
            /** @var Flat $flat */
            $flat = Flat::model()->findByPk($this->flat_id);
            if($flat === null){
                return false;
            }

            $params = Array(
                'name' => $this->name,
                'email' => $this->email,
                'content' => $this->content,
                'surname' => $this->surname,
                'telephone' => $this->telephone,
                'title' => $this->title,
                'datetime' => $this->datetime,
                'hours' => $this->hours,
                'type' => self::$VALID_TYPES_NAMES[$this->type],
                'flat_id' => $this->flat_id,
                'state' => $flat->getStateAsString(),
                'flat_name' => $flat->name,
                'building' => $flat->building,
                'rooms' => $flat->rooms,
                'area' => str_replace('.', ',', $flat->area) . ' m²',
                'balcony' => $flat->getBalconyAsString(),
                'level' => $flat->level,
                'available' => $flat->getAvailableAsString(),
            );
        }

        $replyTo = $this->email;

        $cMail = new CMail();

        if($this->type == self::TYPE_FAST){
            echo 'not implemented';die;
            return $cMail->sendFromTemplate(SystemMailTemplate::TEMPLATE_CONTACT_FORM_FAST_COPY, $this->email, 'Administrator', $params, $replyTo);
        }
        else if($this->type == self::TYPE_FLAT){
            return $cMail->sendFromTemplate(SystemMailTemplate::TEMPLATE_CONTACT_FORM_FLAT_COPY, $this->email, 'Administrator', $params, $replyTo);
        }
        else if($this->type == self::TYPE_LANDING){
            return $cMail->sendFromTemplate(SystemMailTemplate::TEMPLATE_CONTACT_FORM_LANDING_COPY, $this->email, 'Administrator', $params, $replyTo);
        }
        else if($this->type == self::TYPE_LANDING_APARTMENTS){
            return $cMail->sendFromTemplate(SystemMailTemplate::TEMPLATE_CONTACT_FORM_LANDING_APARTMENTS_COPY, $this->email, 'Administrator', $params, $replyTo);
        }
        else if($this->type == self::TYPE_LANDING_FLATS){
            return $cMail->sendFromTemplate(SystemMailTemplate::TEMPLATE_CONTACT_FORM_LANDING_FLATS_COPY, $this->email, 'Administrator', $params, $replyTo);
        }
        else if($this->type == self::TYPE_LANDING_JOURNEY){
            return $cMail->sendFromTemplate(SystemMailTemplate::TEMPLATE_CONTACT_FORM_LANDING_JOURNEY_COPY, $this->email, 'Administrator', $params, $replyTo);
        }
        else if($this->type == self::TYPE_LANDING_WEB){
            return $cMail->sendFromTemplate(SystemMailTemplate::TEMPLATE_CONTACT_FORM_LANDING_WEB_COPY, $this->email, 'Administrator', $params, $replyTo);
        }

        echo 'not implemented';die;
        return $cMail->sendFromTemplate(SystemMailTemplate::TEMPLATE_CONTACT_FORM_COPY, $this->email, 'Administrator', $params, $replyTo);
    }
}
