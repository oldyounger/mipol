<?php
/**
 * This is the model class for table "Image".
 *
 * The followings are the available columns in table 'Image':
 * @property integer $id
 * @property string $file
 * @property string $name
 * @property integer $size
 * @property integer $width
 * @property integer $height
 * @property string $uniqueDir
 * @property string $mime
 * @property string $addedTime
 * @property integer $delete
 */
class Image extends CActiveRecord implements IFileUpload
{
	const IMAGE_CROP		= 0x0001;
	const IMAGE_RESIZE		= 0x0002;
	const IMAGE_CENTER		= 0x0004;

	const FILTER_BW			= 0x1001;
	const FILTER_NEGATIVE	= 0x1002;

	public static $uploadDirectory = 'images';

	/**
	* Returns the static model of the specified AR class.
	* @param string $className active record class name.
	* @return Image the static model class
	*/
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Returns the directory to upload in
	 * @return string
	 */
	public static function getDirectory()
	{
		return self::$uploadDirectory;
	}

	/**
	* @return string the associated database table name
	*/
	public function tableName()
	{
		return '{{image}}';
	}

	/**
	* @return array validation rules for model attributes.
	*/
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return Array(
			Array('file, name, size, width, height, uniqueDir, mime', 'required'),
			Array('size, width, height', 'numerical', 'integerOnly' => true),
			Array('file, name, mime', 'length', 'max' => 128),
			Array('uniqueDir', 'length', 'max' => 128),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			Array('id, file, name, size, width, height, uniqueDir, mime', 'safe', 'on' => 'search'),
		);
	}

	/**
	* @return array relational rules.
	*/
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return Array(
			//
		);
	}

	/**
	* @return array customized attribute labels (name=>label)
	*/
	public function attributeLabels()
	{
		return Array(
			'id' => 'ID',
			'file' => 'Plik',
			'name' => 'Nazwa oryginalna',
			'size' => 'Rozmiar',
			'width' => 'Szerokość',
			'height' => 'Wysokość',
			'uniqueDir' => 'Katalog',
			'mime' => 'Typ MIME',
			'addedTime' => 'Czas dodania',
			'delete' => 'Usunąć?',
		);
	}

	/**
	* usunięcie wszystkich plików należących do tego modelu
	*/
	public function beforeDelete()
	{
		$path = $this->getDirectoryPath();
		if(is_dir($path)){
			$dh = opendir($path);
			if($dh){
				while($file = readdir($dh)){
					if($file != '..' && $file != '.' && is_file($path.$file) && strpos($file, DIRECTORY_SEPARATOR) === false){
						@unlink($path.$file);
					}
				}
				
				closedir($dh);
				@rmdir($path);
			}
		}
		
		return parent::beforeDelete();
	}

	/**
	* Retrieves a list of models based on the current search/filter conditions.
	* @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	*/
	public function search()
	{
		// Warning: Please modify the following code to remove attrthat
		// should not be searched.
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('file', $this->file, true);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('size', $this->size);
		$criteria->compare('width', $this->width);
		$criteria->compare('height', $this->height);
		$criteria->compare('uniqueDir', $this->uniqueDir);
		$criteria->compare('mime', $this->mime, true);

		return new CActiveDataProvider($this, Array(
			'criteria' => $criteria,
		));
	}

	/**
	 * Gets directory path
	 * @return string
	 */
	public function getDirectoryPath()
	{
		return ROOT_DIR . DIRECTORY_SEPARATOR . self::getDirectory() . DIRECTORY_SEPARATOR . $this->uniqueDir . DIRECTORY_SEPARATOR;
	}

	/**
	 * Gets image path
	 *
	 * @return string: pth
	 */
	public function getImagePath()
	{
		return $this->getDirectoryPath() . $this->file;
	}

	/**
	 * Gets image HTML path (used in URL)
	 *
	 * @return string: path
	 */
	public function getImageHtmlPath()
	{
		return Yii::app()->getBaseUrl(false) . '/' . self::getDirectory() . '/' . $this->uniqueDir . '/' . $this->file;
	}

	/**
	 * Gets thumb path
	 *
	 * @param $thumbFile: thumb name
	 * @return string: path
	 */
	public function getThumbPath($thumbFile)
	{
		return $this->getDirectoryPath() . $thumbFile;
	}

	/**
	 * Gets thumb HTML path (used in URL)
	 *
	 * @param $thumbFile: thumb name
	 * @return string: path
	 */
	public function getThumbHtmlPath($thumbFile)
	{
		return Yii::app()->getBaseUrl(false) . '/' . self::getDirectory() . '/' . $this->uniqueDir . '/' . $thumbFile;
	}

    /**
     * Get file extension
     * @return string png jpeg gif & other
     */
    public function getExtension()
    {
        $lastDotPos = mb_strrpos($this->file, '.');
        if($lastDotPos !== false){
            return mb_strtolower(mb_substr($this->file, $lastDotPos + 1));
        }

        return 'unk';
    }

	/**
	 * gets url to image
	 * @param Array $options options of resizing ($fit 'inside', 'outside', 'fill', $scale 'down', 'up', 'any')
	 * @param boolean $asArray: shall we return result as array or path ? defaults to false (return path)
	 * @param boolean $returnHtmlPath: shall we return html path ? defaults to true, if false return HDD path
	 * @return mixed depends on $asArray
	 */
	public function getImageUrl($options = Array(), $asArray = false, $returnHtmlPath = true)
	{
		$baseFilePath = $this->getImagePath();

		if(!file_exists($baseFilePath)){ //empty image file to prevent X
			if($returnHtmlPath){
				return 'data:image/gif;base64,R0lGODlhAQABAIAAAAUEBAAAACwAAAAAAQABAAACAkQBADs=';
			}
			else{
				return false;
			}
		}

		if(!$options && $returnHtmlPath){ //plain image without options (full size)
			return $this->getImageHtmlPath();
		}
		else if(!$options){
			return $this->getImagePath();
		}

		$options = $this->getImageOptions($options);

		$thumbFile		= $this->getThumbName($this->file, $options);
		$thumbFilePath	= $this->getThumbPath($thumbFile);

		if(!file_exists($thumbFilePath)){
			$image = WideImage::load($baseFilePath);
			if(isSet($options['resize'])){
				$image = $image->resize($options['resize']['width'], $options['resize']['height'], $options['resize']['fit'], $options['resize']['scale']);
			}

			if(isSet($options['crop'])){
				$image = $image->crop($options['crop']['top'], $options['crop']['left'], $options['crop']['width'], $options['crop']['height']);
			}

            //Compress based on extension
            $ext = WideImage_MapperFactory::determineFormat($thumbFilePath);
            if($ext === 'PNG'){
                $image->saveToFile($thumbFilePath, 9); //max compression
            }
            else if($ext == 'JPEG'){
                $image->saveToFile($thumbFilePath, 90);
            }
            else{ //other types we cannot convert
			    $image->saveToFile($thumbFilePath);
            }
		}

		if($asArray){
			return Array(
				'uniqueDir' => $this->uniqueDir,
				'fileName' => $thumbFile,
				'filePath' => $thumbFilePath,
			);
		}

		if(!$returnHtmlPath){
			return $this->getThumbPath($thumbFile);
		}

		return $this->getThumbHtmlPath($thumbFile);
	}

	/**
	 * Merges image options (resize, crop)
	 *
	 * @param Array $options
	 * @return Array
	 */
	public function getImageOptions(Array $options)
	{
		if(isSet($options['resize'])){
			$options['resize'] = array_merge(Array(
				'width' => NULL,
				'height' => NULL,
				'fit' => 'inside',
				'scale' => 'any'
			), $options['resize']);
		}

		if(isSet($options['crop'])){
			$options['crop'] = array_merge(Array(
				'left' => 'center',
				'top' => 'middle',
				'width' => 100,
				'height' => 100
			), $options['crop']);
		}

		return $options;
	}

	/**
	 * Gets thumb name
	 *
	 * @param $fileName: file name
	 * @param $o: resize parameteres
	 * @return string
	 */
    public function getThumbName($fileName, $o)
	{
		$name  = isSet($o['resize'])	? "_resize_{$o['resize']['width']}x{$o['resize']['height']}_{$o['resize']['fit']}_{$o['resize']['scale']}"	: "";
		$name .= isSet($o['crop'])		? "_crop_{$o['crop']['top']}x{$o['crop']['left']}_{$o['crop']['width']}_{$o['crop']['height']}"	: "";
		$name .= '_'.$fileName;
		
		return $name;
	}

	/**
	 * Performs upload of image (or moving)
	 *
	 * @param string $name
	 * @param string $type
	 * @param string $tmpName
	 * @param int $error
	 * @param int $size
	 * @param bool $normalUpload: true = move_uploaded_file, false = rename (normal moving in system)
	 * @param Array $requestInfo: dump of $_REQUEST table
	 * @return mixed
	 * @throws Exception
	 */
	public static function uploadFile($name, $type, $tmpName, $error, $size, $normalUpload = true, $requestInfo = Array())
	{
		$allowedExtensions = Array(
			'jpg'	=> 'image/jpeg',
			'jpeg'	=> 'image/jpeg',
			'png'	=> 'image/png',
			'gif'	=> 'image/gif',
		);

		$pathinfo = pathinfo($name);

		/** FIX BUG 1: extension was case sensitive - files *.JPG was not allowed **/
		$pathinfo['extension'] = mb_strtolower($pathinfo['extension']);

		if(!array_key_exists($pathinfo['extension'], $allowedExtensions)){
			throw new Exception('File extension not allowed.');
		}

		$imagesRoot = realpath(ROOT_DIR . DIRECTORY_SEPARATOR . self::getDirectory());

		if($imagesRoot == '' || !is_dir($imagesRoot)){
			throw new Exception('Missing image root directory.');
		}

		$cleanUniqueName = CString::cleanFilename($pathinfo['filename']);
		$cleanUniqueNameExt = $cleanUniqueName . '.' . $pathinfo['extension'];

		// unique catalog for image and it's thumbnails in format "clean-file-name___2013_10_10___12_55_12___1123"
		$uniqueDir = substr($cleanUniqueName, 0, 97) . date('__Y_m_d___H_i_s___') . rand(1000,9999);
		$uniquePath = $imagesRoot . DIRECTORY_SEPARATOR . $uniqueDir;

		if(!mkdir($uniquePath, 0777)){
			throw new Exception('Could not create directory "'.$uniqueDir.'" for image.');
		}

		@chmod($uniquePath, 0777);

		if($normalUpload && !@move_uploaded_file($tmpName, $uniquePath . DIRECTORY_SEPARATOR . $cleanUniqueNameExt)){
			throw new Exception('Could not move uploaded file.');
		}
		else if(!$normalUpload && !@rename($tmpName, $uniquePath . DIRECTORY_SEPARATOR . $cleanUniqueNameExt)){
			throw new Exception('Could not move uploaded file by rename.');
		}

		$imageinfo = getimagesize($uniquePath . DIRECTORY_SEPARATOR . $cleanUniqueNameExt);

		$saveExtension = $allowedExtensions[$pathinfo['extension']];
		return self::saveToDatabase($name, $uniqueDir, $cleanUniqueNameExt, $imageinfo, $size, $saveExtension, $requestInfo);
	}

	/**
	 * Saves record to database
	 *
	 * @param $name
	 * @param $uniqueDir
	 * @param $cleanUniqueNameExt
	 * @param $imageinfo
	 * @param $size
	 * @param $extension
	 * @param $requestInfo
	 * @return integer
	 * @throws Exception
	 */
	public static function saveToDatabase($name, $uniqueDir, $cleanUniqueNameExt, $imageinfo, $size, $extension, $requestInfo)
	{
		$model = new Image;

		$model->mime		= $extension;
		$model->size		= $size;
		$model->width		= $imageinfo[0];
		$model->height		= $imageinfo[1];
		$model->file		= $cleanUniqueNameExt;
		$model->name		= $name;
		$model->uniqueDir	= $uniqueDir;
		$model->addedTime	= date('Y-m-d H:i:s');
		$model->delete		= 0;

		if(!$model->save()){
			throw new Exception('Could not save Image model.');
		}

		return $model->getPrimaryKey();
	}

	/**
	 * Downloads a file (ex. from Facebook)
	 *
	 * @param $url: url to download form
	 * @throws Exception
	 * @return mixed
	 */
	public static function downloadFile($url)
	{
		$allowedExtensions = Array(
			'jpg'	=> 'image/jpeg',
			'jpeg'	=> 'image/jpeg',
			'png'	=> 'image/png',
			'gif'	=> 'image/gif',
		);
		
		$pathinfo = pathinfo($url);

		if(!array_key_exists($pathinfo['extension'], $allowedExtensions)){
			throw new Exception('File extension not allowed.');
		}

		$imagesRoot = realpath(ROOT_DIR . DIRECTORY_SEPARATOR . self::getDirectory());

		if($imagesRoot == '' || !is_dir($imagesRoot)){
			throw new Exception('Missing image root directory.');
		}

		$cleanUniqueName = CString::cleanFilename($pathinfo['filename']);
		$cleanUniqueNameExt = $cleanUniqueName . '.' . $pathinfo['extension'];

		// unique catalog for image and it's thumbnails in format "clean-file-name___2013_10_10___12_55_12___1123"
		$uniqueDir = NULL;
		$uniquePath = NULL;
		do{
			$uniqueDir = substr($cleanUniqueName,0,97).date('__Y_m_d___H_i_s___').rand(10000000,99999999);
			$uniquePath = $imagesRoot . DIRECTORY_SEPARATOR . $uniqueDir;
		}
		while(is_dir($uniquePath));

		if(!mkdir($uniquePath, 0777)){
			throw new Exception('Could not create directory "'.$uniqueDir.'" for image.');
		}

		@chmod($uniquePath, 0777);

		if(!$file = CGlobal::curl_get_contents($url)){
			throw new Exception('Could not download file.');
		}
		
		if(!@file_put_contents($uniquePath . DIRECTORY_SEPARATOR . $cleanUniqueNameExt, $file)){
			throw new Exception('Could not save file.');
		}
		
		unset($file);
		
		$imageinfo = getimagesize($uniquePath . DIRECTORY_SEPARATOR . $cleanUniqueNameExt);

		$modelClassName = get_called_class();
		$model = new $modelClassName;

		$model->mime		= $allowedExtensions[$pathinfo['extension']];
		$model->size		= filesize($uniquePath . DIRECTORY_SEPARATOR . $cleanUniqueNameExt);
		$model->width		= $imageinfo[0];
		$model->height		= $imageinfo[1];
		$model->file		= $cleanUniqueNameExt;
		$model->name		= $cleanUniqueNameExt;
		$model->uniqueDir	= $uniqueDir;
		$model->addedTime	= date('Y-m-d H:i:s');
		$model->delete		= 0;
		
		if(!$model->save()){
			throw new Exception('Could not save Image model.');
		}

		return $model->getPrimaryKey();
	}

	/**
	 * Shorthand function for getting image URL
	 *
	 * @param $id
	 * @param array $options
	 * @return string
	 */
	public static function url($id, $options = Array())
	{
		$image = self::model()->findByPk($id);
		if($image !== NULL){
			return $image->getImageUrl($options);
		}
		else{ //does not exists, blank image
			return 'data:image/gif;base64,R0lGODlhAQABAIAAAAUEBAAAACwAAAAAAQABAAACAkQBADs=';
		}
	}
}