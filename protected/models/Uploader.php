<?php
/**
 * This is the model class for table "File".
 *
 * The followings are the available columns in table 'File':
 * @property string $id
 * @property string $file
 * @property string $name
 * @property integer $size
 * @property string $uniqueDir
 * @property string $mime
 * @property string $addedTime
 * @property int $delete
 */
class Uploader extends CActiveRecord implements IFileUpload
{
	public static $uploadDirectory = 'upload';

	/**
	* Returns the static model of the specified AR class.
	* @param string $className active record class name.
	* @return Image the static model class
	*/
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Returns the directory to upload in
	 * @return string
	 */
	public static function getDirectory()
	{
		return self::$uploadDirectory;
	}

	/**
	* @return string the associated database table name
	*/
	public function tableName()
	{
		return '{{uploader}}';
	}

	/**
	* @return array validation rules for model attributes.
	*/
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return Array(
			Array('file, name, size, uniqueDir, mime', 'required'),
			Array('size', 'numerical', 'integerOnly' => true),
			Array('file, name, mime', 'length', 'max' => 128),
			Array('uniqueDir', 'length', 'max' => 128),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			Array('id, file, name, size, uniqueDir, mime', 'safe', 'on' => 'search'),
		);
	}

	/**
	* @return array relational rules.
	*/
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return Array(
			//
		);
	}

	/**
	* @return array customized attribute labels (name=>label)
	*/
	public function attributeLabels()
	{
		return Array(
			'id' => 'ID',
			'file' => 'Plik',
			'name' => 'Nazwa oryginalna',
			'size' => 'Rozmiar',
			'uniqueDir' => 'Plik',
			'mime' => 'Typ pliku',
			'addedTime' => 'Czas dodania',
			'delete' => 'Usunąć?',
		);
	}

	/**
	* usunięcie wszystkich plików należących do tego modelu
	*/
	public function beforeDelete()
	{
		$path = ROOT_DIR . DIRECTORY_SEPARATOR . self::getDirectory() . DIRECTORY_SEPARATOR . $this->uniqueDir . DIRECTORY_SEPARATOR;
		if(is_dir($path)){
			$dh = opendir($path);
			if($dh){
				while($file = readdir($dh)){
					if($file != '..' && $file != '.' && is_file($path.$file) && strpos($file, DIRECTORY_SEPARATOR) === false){
						@unlink($path.$file);
					}
				}

				closedir($dh);
				@rmdir($path);
			}
		}

		return parent::beforeDelete();
	}

	/**
	* Retrieves a list of models based on the current search/filter conditions.
	* @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	*/
	public function search()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('file', $this->file, true);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('size', $this->size);
		$criteria->compare('uniqueDir', $this->uniqueDir);
		$criteria->compare('mime', $this->mime, true);

		return new CActiveDataProvider($this, Array(
			'criteria' => $criteria,
		));
	}

	public function getFileUrl()
	{
		$baseFilePath = ROOT_DIR . DIRECTORY_SEPARATOR . self::getDirectory() . DIRECTORY_SEPARATOR . $this->uniqueDir . DIRECTORY_SEPARATOR . $this->file;

		if(!file_exists($baseFilePath)){ //empty url
			return NULL;
		}

		return Yii::app()->getBaseUrl(false) . DIRECTORY_SEPARATOR . self::getDirectory() . DIRECTORY_SEPARATOR . $this->uniqueDir . DIRECTORY_SEPARATOR . $this->file;
	}

    /**
     * Gets directory path
     * @return string
     */
    public function getDirectoryPath()
    {
        return ROOT_DIR . DIRECTORY_SEPARATOR . self::getDirectory() . DIRECTORY_SEPARATOR . $this->uniqueDir . DIRECTORY_SEPARATOR;
    }

    /**
     * Gets file path
     *
     * @return string: path
     */
    public function getFilePath()
    {
        return $this->getDirectoryPath() . $this->file;
    }

	public static function uploadFile($name, $type, $tmpName, $error, $size)
	{
		/*$allowedExtensions = Array(
			'jpg'	=> 'image/jpeg',
			'jpeg'	=> 'image/jpeg',
			'png'	=> 'image/png',
			'gif'	=> 'image/gif',
		);*/

		//get MIME type
		if(function_exists('finfo_open')){
			$finfo = finfo_open(FILEINFO_MIME_TYPE);
			$mimeType = finfo_file($finfo, $tmpName);
			finfo_close($finfo);
		}
		else{
			$mimeType = self::mime_content_type($tmpName);
		}

		$pathinfo = pathinfo($name);

		/*if(!array_key_exists($pathinfo['extension'], $allowedExtensions)){
			throw new Exception('File extension not allowed.');
		}*/

		$filesRoot = realpath(ROOT_DIR . DIRECTORY_SEPARATOR . self::getDirectory());

		if($filesRoot == '' || !is_dir($filesRoot)){
			throw new Exception('Missing file root directory.');
		}

		$yearDir = date('Y');
		$cleanUniqueName = CString::cleanFilename($pathinfo['filename']);
		$cleanUniqueNameExt = $cleanUniqueName.'.'.$pathinfo['extension'];

		$diryear = $filesRoot . DIRECTORY_SEPARATOR . $yearDir;
		if(!is_dir($diryear))
		{
			mkdir($diryear,0777);
			@chmod($diryear, 0777);
		}
		// unique catalog for file in format "clean-file-name___2013_10_10___12_55_12___1123"
		$uniqueDir = date('m');
		$uniquePath = $filesRoot . DIRECTORY_SEPARATOR .$yearDir. DIRECTORY_SEPARATOR .$uniqueDir;


		$uniquePathThumbnail =  DIRECTORY_SEPARATOR.self::getDirectory().DIRECTORY_SEPARATOR .$yearDir. DIRECTORY_SEPARATOR .$uniqueDir;


		if(!is_dir($uniquePath))
		{

			mkdir($uniquePath, 0777);
			@chmod($uniquePath, 0777);
		}





		if(!@move_uploaded_file($tmpName, $uniquePath . DIRECTORY_SEPARATOR . $cleanUniqueNameExt)){
			throw new Exception('Could not move uploaded file.');
		}

		$modelClassName = get_called_class();
		$model = new $modelClassName;

		$model->mime		= $mimeType;
		$model->size		= $size;
		$model->file		= $cleanUniqueNameExt;
		$model->name		= $name;
		$model->uniqueDir	= $uniquePathThumbnail.DIRECTORY_SEPARATOR.$cleanUniqueNameExt;
		$model->addedTime	= date('Y-m-d H:i:s');
		$model->delete		= 0;

		if(!$model->save()){
			throw new Exception('Could not save File model.');
		}

		return $model->getPrimaryKey();
	}

	public static function downloadFile($url)
	{
		//TODO: this function is not working
		throw new Exception('FIXME');

		$allowedExtensions = Array(
			'jpg'	=> 'image/jpeg',
			'jpeg'	=> 'image/jpeg',
			'png'	=> 'image/png',
			'gif'	=> 'image/gif',
		);

		$pathinfo = pathinfo($url);

		if(!array_key_exists($pathinfo['extension'], $allowedExtensions)){
			throw new Exception('File extension not allowed.');
		}

		$imagesRoot = realpath(ROOT_DIR . DIRECTORY_SEPARATOR . self::getDirectory());

		if($imagesRoot == '' || !is_dir($imagesRoot)){
			throw new Exception('Missing image root directory.');
		}

		$cleanUniqueName = CString::cleanFilename($pathinfo['filename']);
		$cleanUniqueNameExt = $cleanUniqueName . '.' . $pathinfo['extension'];

		// unique catalog for image and it's thumbnails in format "clean-file-name___2013_10_10___12_55_12___1123"
		$uniqueDir = NULL;
		$uniquePath = NULL;
		do{
			$uniqueDir = substr($cleanUniqueName,0,97).date('__Y_m_d___H_i_s___').rand(10000000,99999999);
			$uniquePath = $imagesRoot . DIRECTORY_SEPARATOR . $uniqueDir;
		}
		while(is_dir($uniquePath));

		if(!mkdir($uniquePath, 0777)){
			throw new Exception('Could not create directory "'.$uniqueDir.'" for image.');
		}

		@chmod($uniquePath, 0777);
		
		if(!$file = file_get_contents($url)){
			throw new Exception('Could not download file.');
		}
		
		if(!@file_put_contents($uniquePath . DIRECTORY_SEPARATOR . $cleanUniqueNameExt, $file)){
			throw new Exception('Could not save file.');
		}
		
		unset($file);
		
		$imageinfo = getimagesize($uniquePath . DIRECTORY_SEPARATOR . $cleanUniqueNameExt);

		$model = new Image;
		$model->mime		= $allowedExtensions[$pathinfo['extension']];
		$model->size		= filesize($uniquePath . DIRECTORY_SEPARATOR . $cleanUniqueNameExt);
		$model->width		= $imageinfo[0];
		$model->height		= $imageinfo[1];
		$model->file		= $cleanUniqueNameExt;
		$model->name		= $cleanUniqueNameExt;
		$model->uniqueDir	= $uniqueDir;
		$model->addedTime	= date('Y-m-d H:i:s');
		$model->delete		= 0;
		
		if(!$model->save()){
			throw new Exception('Could not save Image model.');
		}

		return $model->getPrimaryKey();
	}

    private static function mime_content_type($filename)
    {
        $mime_types = array(

            'txt' => 'text/plain',
            'htm' => 'text/html',
            'html' => 'text/html',
            'php' => 'text/html',
            'css' => 'text/css',
            'js' => 'application/javascript',
            'json' => 'application/json',
            'xml' => 'application/xml',
            'swf' => 'application/x-shockwave-flash',
            'flv' => 'video/x-flv',

            // images
            'png' => 'image/png',
            'jpe' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
            'gif' => 'image/gif',
            'bmp' => 'image/bmp',
            'ico' => 'image/vnd.microsoft.icon',
            'tiff' => 'image/tiff',
            'tif' => 'image/tiff',
            'svg' => 'image/svg+xml',
            'svgz' => 'image/svg+xml',

            // archives
            'zip' => 'application/zip',
            'rar' => 'application/x-rar-compressed',
            'exe' => 'application/x-msdownload',
            'msi' => 'application/x-msdownload',
            'cab' => 'application/vnd.ms-cab-compressed',

            // audio/video
            'mp3' => 'audio/mpeg',
            'qt' => 'video/quicktime',
            'mov' => 'video/quicktime',

            // adobe
            'pdf' => 'application/pdf',
            'psd' => 'image/vnd.adobe.photoshop',
            'ai' => 'application/postscript',
            'eps' => 'application/postscript',
            'ps' => 'application/postscript',

            // ms office
            'doc' => 'application/msword',
            'rtf' => 'application/rtf',
            'xls' => 'application/vnd.ms-excel',
            'ppt' => 'application/vnd.ms-powerpoint',

            // open office
            'odt' => 'application/vnd.oasis.opendocument.text',
            'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
        );

        $ext = strtolower(array_pop(explode('.', $filename)));
        if(array_key_exists($ext, $mime_types)){
            return $mime_types[$ext];
        }
        elseif(function_exists('finfo_open')){
            $finfo = finfo_open(FILEINFO_MIME);
            $mimetype = finfo_file($finfo, $filename);
            finfo_close($finfo);

            return $mimetype;
        }
        else{
            return 'application/octet-stream';
        }
    }
}