<?php
/**
 * This is the model class for table "news".
 *
 * The followings are the available columns in table 'news':
 * @property string $id
 * @property integer $category_id
 * @property string $datetime
 * @property string $url
 * @property string $title
 * @property string $short_content
 * @property string $keywords
 * @property string $description
 * @property string $short_title
 * @property integer $view_count
 * @property boolean $hidden
 * @property integer $header_image
 * @property integer $only_logged
 *
 * The followings are the available model relations:
 * @property Category $category
 * @property Image $headerImage
 * @property Image $thumbnailImage
 * @property NewsImage[] $newsImages
 */
class Offer extends CActiveRecord
{
	const PAGE_SIZE = 99;

	public $date;
	public $time;

    public $postedImages;
    public $postedImagesSort;


        
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{offer}}';
	}

	/**
	 * @return Array validation rules for model attributes.
	 */
	public function rules()
	{
		return Array(
			Array('title,hidden,url,category_id', 'required'),
			Array('view_count, hidden, header_image, category_id, only_logged', 'numerical', 'integerOnly' => true),
			Array('url', 'length', 'max' => 255),
			Array('title', 'length', 'max' => 250),
			Array('view_count', 'length', 'max' => 11),
			Array('id, datetime, url, title, description,keywords,short_content,view_count, hidden', 'safe', 'on' => 'search'),
			Array('date,time,postedImages, postedImagesSort,description, keywords,extra,short_content', 'safe', 'on' => 'insert,update'),

		);
	}

	/**
	 * @return Array relational rules.
	 */
	public function relations()
	{
		return Array(
            'headerImage' => Array(self::BELONGS_TO, 'Image', 'header_image'),
//            'newsImages' => Array(self::HAS_MANY, 'NewsImage', 'news_id', 'order' => 'newsImages.order ASC, newsImages.id ASC'),
		);
	}

	/**
	 * @return Array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return Array(
			'id' => 'ID',
			'datetime' => 'Data',
			'url' => 'URL',
			'title' => 'Tytuł',
			'short_content' => 'Treść',
			'view_count' => 'Ilość odsłon',
			'extra' => 'Opis na zdjęciu',
			'hidden' => 'Ukryty',
            'category_id' => 'Kategoria',
            'only_logged' => 'Tylko dla zalogowanych',
            'header_image' => 'Miniaturka artykułu',
			'description' => 'Opis SEO',
			'keywords' => 'Słowa kluczowe SEO',
			'date' => 'Data',
			'time' => 'Godzina',



		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('datetime', $this->datetime, true);
		$criteria->compare('url', $this->url);
		$criteria->compare('title', $this->title, true);
        $criteria->compare('category_id', $this->category_id);
		$criteria->compare('short_content', $this->short_content);
		$criteria->compare('view_count', $this->view_count);
		$criteria->compare('hidden', $this->hidden);
        $criteria->compare('only_logged', $this->only_logged);
		$criteria->compare('description', $this->description, true);
		$criteria->compare('keywords', $this->keywords, true);

		return new CActiveDataProvider($this, Array(
			'criteria' => $criteria,
		));
	}

	/**
	 * Published scope
	 *
	 * @return News
	 */
	public function published()
	{
		$this->getDbCriteria()->mergeWith(Array(
			'alias' => 'Offer',
			'condition' => 'Offer.hidden = 0',
		));

		return $this;
	}

	public function afterFind()
	{
		$dt = explode(' ', $this->datetime);
		if(isSet($dt[0]) && isSet($dt[1])){
			$dateFixed = implode('.', array_reverse(explode('-', $dt[0])));
			$timeFixed = mb_substr($dt[1], 0, mb_strrpos($dt[1], ':00'));

			$this->date = $dateFixed;
			$this->time = $timeFixed;
		}
	}

    public function afterSave()
    {
        parent::afterSave();

        //var_dump($this->postedImages);die;
//        if(is_array($this->postedImages)){
//            $maxOrderCriteria = new CDbCriteria;
//            $maxOrderCriteria->order = '`order` DESC';
//            $maxOrderCriteria->compare('news_id', $this->id);
//
//            $maxOrder = 0;
//
//            /** @var NewsImage $maxOrderImage */
//            $maxOrderImage = NewsImage::model()->find($maxOrderCriteria);
//            if($maxOrderImage !== NULL){
//                $maxOrder = $maxOrderImage->order + 1;
//            }
//
//            foreach($this->postedImages as $postedImage){
//                $existingImage = NewsImage::model()->findByAttributes(Array(
//                    'news_id' => $this->id,
//                    'image_id' => $postedImage,
//                ));
//
//                if($existingImage !== NULL){ //found existing record
//                    continue;
//                }
//
//                //not found, add it
//                $newImage = new NewsImage;
//                $newImage->news_id = $this->id;
//                $newImage->image_id = $postedImage;
//                $newImage->order = $maxOrder++;
//                $newImage->description = '';
//
//                $newImage->save();
//            }
//        }

        if(!is_array($this->postedImages)){
            $this->postedImages = Array();
        }

//        foreach($this->newsImages as $newsImage){
//            if(!in_array($newsImage->image_id, $this->postedImages)){ //we got this image in DB, but not in POST. remove
//                $newsImage->delete();
//            }
//        }

        //We can finally update order of them!
        if(isSet($this->postedImagesSort) && is_array($this->postedImagesSort) && count($this->postedImagesSort) > 0){
            $order = 0;
            foreach($this->postedImagesSort as $img){
                /** @var NewsImage $newsImage */
                $newsImage = NewsImage::model()->findByAttributes(Array(
                    'image_id' => $img,
                    'news_id' => $this->id,
                ));

                if($newsImage !== NULL){
                    $newsImage->order = $order++;

                    $newsImage->save();
                }
            }
        }
    }

	public function beforeValidate()
	{
		if(!parent::beforeValidate()){
			return false;
		}

		$error = false;

		//parse date
		if(!preg_match('@^(\d{2})\.(\d{2})\.(\d{4})$@', $this->date)){
			$this->addError('file', Yii::t('app', 'Date format is invalid.'));
			$error = true;
		}

		if(!preg_match('@^(\d{2}):(\d{2})$@', $this->time)){
			$this->addError('file', Yii::t('app', 'Time format is invalid.'));
			$error = true;
		}

		$fixedDate = date('Y-m-d', strtotime($this->date));

		$this->datetime = $fixedDate.' '.$this->time.':00';

        //NULL fields
        if(!Image::model()->findByPk($this->header_image)){
            $this->header_image = NULL;
        }




		if($error){
			return false;
		}

		return true;
	}

    public function beforeSave()
    {
        if($this->hasAttribute('url')){
            if(mb_strlen($this->url) === 0){
                //$this->url = $this->createUrl($this->name);
                $field = NULL;
                //title or name is required
                if(isSet($this->name)){
                    $field = $this->name;
                }
                else{
                    $field = $this->title;
                }

                $this->url = CUniqueURL::getURL($field, 'News', 'url');
            }

            //			var_dump($this->url);die;
            /*else{
                $this->url = $this->createUrl($this->url);
            }*/
        }

        return parent::beforeSave();
    }

	/**
	 * Increases view counter
	 *
	 * @param integer $count: how many views we should add
	 * @return void
	 */
	public function increaseViewCount($count = 1)
	{
		$this->view_count += max(0, (int)$count);

		Yii::app()->db->createCommand()
			->update($this->tableName(), Array('view_count' => $this->view_count), 'id = :id', Array(':id' => $this->id));
	}

	/**
	 * Gets comment list of news for use in CommentsListWidget
	 * @return Array with records
	 */
	public function getCommentsListForWidget()
	{
		$ret = Array();
		/*foreach($this->newsComments as $comment){
			$ret[] = Array(
				'nick' => $comment->user->nick,
				'date' => $comment->date,
				'content' => $comment->content,
				'id' => $comment->id,
				'author_id' => $comment->user->id,
			);
		}*/

		return $ret;
	}

    /**
     * Gets images ordered by filename
     *
     * @return NewsImage[]
     */
    public function getImages()
    {
        $criteria = new CDbCriteria;
        $criteria->alias = 'NewsImage';
        $criteria->join = 'INNER JOIN ' . Image::model()->tableName() . ' AS Image ON NewsImage.image_id = Image.id';
        $criteria->compare('NewsImage.news_id', $this->id);
        $criteria->order = 'Image.name ASC';

        return NewsImage::model()->findAll($criteria);
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return News the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Finds all entries ordering by date (date added)
     *
     * @param string $categoryUrl
	 * @param boolean/integer $page current page
     * @param string $query query to search
     * @param boolean $includeForLogged
	 * @return Array with items and pages (if wanted)
	 */
	public static function findAllByCategoryPaginationOrderByDate($categoryUrl = NULL, $page = false, $query = NULL, $includeForLogged = false)
	{
		$criteria = new CDbCriteria;
		$criteria->alias = 'News';
        if($categoryUrl !== NULL){
            $criteria->select = 'News.*';
            $criteria->join = 'INNER JOIN ' . Category::model()->tableName() . ' AS Category ON Category.id = News.category_id';
            $criteria->compare('Category.url', $categoryUrl);
        }

        if($query !== NULL){
            $criteria->addSearchCondition('News.title', $query);
            $criteria->addSearchCondition('News.content', $query, true, 'OR');
            $criteria->addSearchCondition('News.short_content', $query, true, 'OR');
        }

        if(!$includeForLogged){ //if not include, then use criteria just for 0
            $criteria->compare('News.only_logged', 0);
        }

		$criteria->order = 'News.datetime DESC';
		$criteria->limit = 10;

		$pages = NULL;
		if($page !== false){ //use pagination :)
			$itemCount = self::model()->published()->count($criteria);

			$pages = new CPagination($itemCount);
			$pages->setPageSize(self::PAGE_SIZE);
			$pages->setCurrentPage($page);
			$pages->applyLimit($criteria);  // the trick is here!
		}

		return Array(
			'items' => self::model()->published()->findAll($criteria),
			'pages' => $pages,
		);
	}
}

