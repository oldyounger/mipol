<?php
/**
 * FormContact class.
 * FormContact is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class FormContact extends CFormModel
{
	public $name;
	public $email;
	public $surname;
	public $telephone;
	//public $title;
	public $content;
	//public $verifyCode;

    public $hours;
    public $type = ContactForm::TYPE_NORMAL;

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return Array(
			Array('name, email, content', 'required'),
			Array('email, name, surname', 'length', 'max' => 128),
			Array('telephone', 'length', 'max' => 50),
            Array('hours', 'length', 'max' => 10),
			Array('email', 'email'),
			//Array('verifyCode', 'captcha', 'allowEmpty' => !CCaptcha::checkRequirements()),
		);
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return Array(
			'name' => 'Imię',
			'email' => 'E-mail',
			'surname' => 'Nazwisko',
			'telephone' => 'Telefon',
			//'title' => 'Tytuł wiadomości',
			'content' => 'Treść wiadomości',
			//'verifyCode' => 'Kod weryfikujący',
		);
	}

    /**
     * Set contact form type
     *
     * @param $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

	/**
	 * Sends an email and adds record to DB
	 *
	 * @return boolean
	 */
	public function sendEmail($ignoreFlat = false, $sendCopy = false)
	{
		$entry = new ContactForm;
		$entry->name = $this->name;
		$entry->surname = $this->surname;
		$entry->telephone = $this->telephone;
		//$entry->title = $this->title;
        $entry->title = 'Kontakt - podstrona';
		$entry->content = $this->content;
		$entry->email = $this->email;
        $entry->type = $this->type;
        $entry->hours = $this->hours;
		$entry->datetime = date('Y-m-d H:i:s', time(NULL));

		if($entry->save()){
			$entry->sendEmail($ignoreFlat);

            if($sendCopy){
                $entry->sendCopyEmail($ignoreFlat);
            }

			return true;
		}

		return false;
	}
}