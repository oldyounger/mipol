<?php
/**
 * This is the model class for table "User".
 *
 * The followings are the available columns in table 'User':
 * @property integer $id
 * @property string $email
 * @property string $password
 * @property string $firstname
 * @property string $lastname
 * @property integer $active
 * @property string $hash
 *
 * The followings are the available model relations:
 * @property UserLoginHistory[] $userLoginHistories
 */
class User extends UserBaseModel
{
	public static $lastUserId = NULL; //used in performance tuning
	public static $lastUserHandle = false;

	public $repassword;

	/**
	* Returns the static model of the specified AR class.
	* @param string $className active record class name.
	* @return User the static model class
	*/
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	* @return string the associated database table name
	*/
	public function tableName()
	{
		return '{{user}}';
	}

	/**
	* @return array validation rules for model attributes.
	*/
	public function rules()
	{
		return Array(
			Array('email', 'required'),
			Array('password, repassword', 'required', 'on' => 'insert'),
			Array('repassword, active', 'safe', 'on' => 'insert, update'),
			Array('email, hash', 'length', 'max' => 128),
			Array('email', 'unique', 'allowEmpty' => false, 'caseSensitive' => false),
			Array('password', 'length', 'max' => 64),
			Array('password', 'compare', 'compareAttribute' => 'repassword', 'message' => 'Hasło w obu polach musi być identyczne.', 'on' => 'insert'), //validate only on insert
			Array('firstname, lastname', 'length', 'max' => 32),
			Array('active', 'boolean'),

			Array('id, email, firstname, lastname, active', 'safe', 'on' => 'search'),
		);
	}

	/**
	* @return Array relational rules.
	*/
	public function relations()
	{
		return Array(
			'userLoginHistories' => Array(self::HAS_MANY, 'UserLoginHistory', 'user_id'),
		);
	}

	/**
	* @return Array customized attribute labels (name=>label)
	*/
	public function attributeLabels()
	{
		return Array(
			'id' => 'ID',
			'email' => 'E-mail',
			'password' => 'Hasło',
			'repassword' => 'Powtórz hasło',
			'firstname' => 'Imię',
			'lastname' => 'Nazwisko',
			'active' => 'Aktywny',
			'hash' => 'Hash',
		);
	}

	/**
	* Retrieves a list of models based on the current search/filter conditions.
	* @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	*/
	public function search()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('email', $this->email, true);
		$criteria->compare('firstname', $this->firstname, true);
		$criteria->compare('lastname', $this->lastname, true);

		return new CActiveDataProvider($this, Array(
			'criteria' => $criteria,
		));
	}

	public function beforeValidate()
	{
		if(!parent::beforeValidate()){
			return false;
		}

		return true;
	}

	public function afterFind()
	{
		parent::afterFind();
	}

	public function beforeSave()
	{
		/*if(!$this->_canSave){
			throw new CException("Nie można zapisać tego obiektu.");
		}*/

		if($this->hash == NULL){ //generate new hash only if deleted previous one
			do{
				$hash = $this->salt();
			}
			while($this->find("hash = :hash", Array('hash' => $hash)));

			$this->hash = $hash;
		}

		return parent::beforeSave();
	}

	/**
	 * Activates user by hash
	 * @param string $hash
	 * @return boolean
	 */
	public function activate($hash)
	{
		/** @var User $user */
		$user = $this->find("hash = :hash", Array(":hash" => $hash));
		if($user === NULL){
			return false;
		}

		$user->hash = '';
		$user->active = 1;
		$user->save();

		//$user->logout();
		if(Yii::app()->user->id){
			Yii::app()->user->logout();
		}
		
		return true;
	}

	/**
	 * Logs an user without using password & active (used in auto logging in after registration to perform avatar upload)
	 * @param boolean $rememberUser
	 * @return boolean
	 */
	public function login($rememberUser = false)
	{
		$identity = new UserIdentity($this->email, $this->password);
		$identity->authenticateWithoutPasswordAndActive();

		if($identity->errorCode === UserIdentity::ERROR_NONE){
			//setCookie('show-start', 'no', (time(NULL) + 30 * 86400), '/');

			$duration = $rememberUser ? 3600 * 24 * 30 : 0; // 30 days
			Yii::app()->user->login($identity, $duration);

			UserLoginHistory::successful($this->id);
			return true;
		}
		else{
			return false;
		}
	}

	/**
	 * Gets user's name
	 *
	 * @return string
	 */
	public function getName()
	{
		return $this->firstname . ' ' . $this->lastname;
	}

	/**
     * Returns object of currently logged user (if any)
	 * @param boolean $onlyActive: check if user is active
     * @return User|false
     */
	public static function getLoggedHandle($onlyActive = true)
	{
		if($onlyActive && (!isSet(Yii::app()->user->active) || !Yii::app()->user->active)){
			return false;
		}

		//NOTE: stable code
		/*$user = User::model()->findByPk(Yii::app()->user->id);
		if($user){
			$user->updateOnlineStatus();
			//$user->password = 'XXXXXXXXXXX'; //we don't have password anyway
			//$user->disableSave();

			return $user;
		}

		return false;*/

		//NOTE: this may be unstable!
		if(self::$lastUserId != Yii::app()->user->id){ //yii user id has changed, refresh handle
			self::$lastUserId = Yii::app()->user->id;

			/** @var User $user */
			$user = User::model()->findByPk(Yii::app()->user->id);
			self::$lastUserHandle = $user;

			if($user){
				//$user->password = 'XXXXXXXXXXX'; //we don't have password anyway
				//$user->disableSave();

				return $user;
			}
		}

		return self::$lastUserHandle;
	}

	/**
	 * Gets ID of current logged user (if any)
	 * @param bool $onlyActive
	 * @return integer|false
	 */
	public static function getLoggedID($onlyActive = true)
	{
		if($onlyActive && (!isSet(Yii::app()->user->active) || !Yii::app()->user->active)){
			return NULL;
		}

		return Yii::app()->user->id;
	}
}