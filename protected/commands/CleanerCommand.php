<?php
class CleanerCommand extends CConsoleCommand
{
	public function run($args)
	{
		//TODO: test this before using!!

		//STEP FIRST: delete all login histories by specified alghoritm:
		//delete if older than 3 months & this is not last login of user

		echo 'STARTING ALL'.PHP_EOL;

		echo 'STARTING: STEP 1'.PHP_EOL;
		$deletedHistoriesCount = 0;

		$criteria = new CDbCriteria;
		$criteria->addCondition('DATE_SUB( CURDATE( ) , INTERVAL 7 DAY ) >= `date`');
		$criteria->order = '`user_id` ASC';
		$criteria->group = '`user_id`';

		$oldHistories = UserLoginHistory::model()->findAll($criteria);
		foreach($oldHistories as $oldHistory){
			/** @var $oldHistory UserLoginHistory */
			//delete all but first (groupped by)

			$newestCriteria = new CDbCriteria;
			$newestCriteria->compare('user_id', $oldHistory->user_id);
			$newestCriteria->addCondition('DATE_SUB( CURDATE( ) , INTERVAL 7 DAY ) >= `date`');
			$newestCriteria->order = '`date` DESC';

			$newestHistory = UserLoginHistory::model()->find($newestCriteria);
			if($newestHistory !== NULL){
				$deleteCriteria = new CDbCriteria;
				$deleteCriteria->compare('user_id', $oldHistory->user_id);
				$deleteCriteria->addCondition("`id` != '".(int)$newestHistory->id."' AND DATE_SUB( CURDATE( ) , INTERVAL 7 DAY ) >= `date`");

				$deletedHistoriesCount += UserLoginHistory::model()->deleteAll($deleteCriteria);
			}
		}

		echo 'FINISHED: STEP 1 - DELETED '.(int)$deletedHistoriesCount.' ROWS'.PHP_EOL;

		//STEP SECOND: clean old image token (older than 1 week)
		echo 'STARTING: STEP 2'.PHP_EOL;
		$deleteTempTokenCriteria = new CDbCriteria;
		$deleteTempTokenCriteria->addCondition('DATE_SUB( CURDATE( ) , INTERVAL 7 DAY ) >= `datetime`');
		$deletedTempTokenCount = TempImageToken::model()->deleteAll($deleteTempTokenCriteria);

		echo 'FINISHED: STEP 2 - DELETED '.(int)$deletedTempTokenCount.' ROWS'.PHP_EOL;

		//STEP THIRD: clean old temp images (older than 1 week)
		echo 'STARTING: STEP 3'.PHP_EOL;
		$deleteTempImageCriteria = new CDbCriteria;
		$deleteTempImageCriteria->addCondition('DATE_SUB( CURDATE( ) , INTERVAL 7 DAY ) >= `addedTime`');
		$deletedTempImagesCount = TempImage::model()->deleteAll($deleteTempImageCriteria);

		echo 'FINISHED: STEP 3 - DELETED '.(int)$deletedTempImagesCount.' ROWS'.PHP_EOL;

		//STEP FOURTH: clean invalid user activities (object_id does not exists)
		echo 'STARTING: STEP 4'.PHP_EOL;

		$lastCleanDate = System::getLastCleanDate();
		$activitiesIdsToDelete = Array();
		$activitiesCheckCriteria = new CDbCriteria;
		$activitiesCheckCriteria->addCondition("`date` >= '" . $lastCleanDate . "'");

		$activitiesToCheck = UserActivity::model()->findAll($activitiesCheckCriteria);
		foreach($activitiesToCheck as $activity){
			/** @var $activity UserActivity */
			if($activity->getRelatedObject() === NULL){ //if related object does not exists, we must delete this row
				$activitiesIdsToDelete[] = $activity->id;
			}
		}

		$deletedActivitiesCount = 0;
		if(count($activitiesIdsToDelete) > 0){ //we got anything to delete
			$activitiesDeleteCriteria = new CDbCriteria;
			$activitiesDeleteCriteria->addInCondition('id', $activitiesIdsToDelete);

			$deletedActivitiesCount = UserActivity::model()->deleteAll($activitiesDeleteCriteria);
		}

		System::updateLastCleanDate();

		echo 'FINISHED: STEP 4 - DELETED '.(int)$deletedActivitiesCount.' ROWS'.PHP_EOL;

		echo 'FINISHED ALL'.PHP_EOL;
	}
}