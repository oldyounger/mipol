<?php
class AdvertiseWidget extends CWidget
{
	public $type;
	public $file;
	public $image;
	public $url;
	public $size = Array('width' => 0, 'height' => 0);
	
	public function init()
	{
		//
	}
	
	public function run()
	{
		if($this->type == Advertisement::TYPE_IMAGE && $this->image !== NULL){
			$cImage = CHtml::image(
					$this->image->getImageUrl(Array(
						'resize' => Array(
							'width' => $this->size['width'],
							'height' => $this->size['height'],
							'fit' => 'fill'
						),
						'crop' => Array(
							'width' => $this->size['width'],
							'height' => $this->size['height']
						)
					)),
					Yii::t('advertisement', 'Advertisement')
			);
			
			if(mb_strlen($this->url) > 0){
				echo CHtml::link($cImage, $this->url);
			}
			else{
				echo $cImage;
			}
		}
		else if($this->type == Advertisement::TYPE_FLASH && $this->file !== NULL){
			$this->widget('ext.swfobject.ESwfObjectWidget', Array(
				'tag' => 'div',
				'htmlOptions' => Array(),
				'swfUrl' => $this->file->getFileUrl(),
				'width' => $this->size['width'],
				'height' => $this->size['height'],
				'version' => '9',
				'params' => Array('wMode' => 'transparent'),
				'scriptPosition' => CClientScript::POS_READY
			));
		}
	}
}