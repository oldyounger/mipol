<?php
/**
 * WMailTemplate
 */
class MailTemplateWidget extends CWidget
{
	public function wrap($body, $template = 'default')
	{
		return $this->render($template, Array(
			'body' => $body
		), true);
	}
}
