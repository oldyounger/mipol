<div class="comment-list-container comment-list-tiny clearfix" id="addComment-<?php echo $moduleId; ?>" data-name="<?php echo $elementName; ?>" data-deleteall="<?php echo $this->canDeleteAll ? 'true' : 'false'; ?>" data-path="<?php echo $jsUrlName; ?>" data-stamp="<?php echo $lastCommentStamp; ?>">
	<?php if(Yii::app()->user->isGuest): ?>
		<div class="please-log-in">
			Musisz się <a href="#" class="loginRedirectBox">zalogować</a>, żeby móc napisać komentarz.
		</div>
	<?php else: ?>
	<?php endif; ?>

	<div class="comment-box">
		<?php if(count($comments) > 0): ?>
			<?php foreach($comments as $comment): ?>
				<div class="comment clearfix">
					<div class="avatar">
						<?php echo CHtml::image($comment['avatar'], $comment['nick']); ?>
					</div>
					<div class="comment-content">
						<h5><a href="<?php echo $this->controller->createUrl('userProfile/index', Array('userNick' => $comment['nick'])); ?>"><?php echo $comment['nick']; ?></a> | <?php $this->widget('application.widgets.DateDiffWidget', Array('compare' => $comment['date'])); ?></h5>

						<div class="comment-content-internal">
							<?php echo CTools::parseQuotesAndLinks($comment['content'], true); ?>
						</div>
					</div>

					<div class="quote-button">
						<i class="icon-custom-quote icon-small icon-purple"></i>
						<?php $user_id = User::getLoggedID(); ?>
						<?php if($this->canDeleteAll || ($user_id !== NULL && $comment['author_id'] == $user_id)): ?>
							<i class="icon-custom-times icon-small icon-purple" data-id="<?php echo $comment['id']; ?>"></i>
						<?php endif; ?>
					</div>
				</div>
			<?php endforeach; ?>
		<?php else: ?>
			<div class="no-messages">
				Brak komentarzy. Bądź pierwszy, który skomentuje!
			</div>
		<?php endif; ?>
	</div>

	<?php if(!Yii::app()->user->isGuest): ?>
		<div class="comment-form clearfix">
			<button type="button" class="btn btn-mini btn-link pull-right onClickToggleComment">Dodaj komentarz</button>
			<div class="comment-post-box hidden">
				<form action="#" method="post" class="addCommentForm">
					<label for="comment-area">Treść komentarza:</label>
					<textarea id="comment-area" name="FormComment[content]"></textarea>

					<button type="submit" class="btn btn-mini btn-yellow pull-right">Dodaj komentarz</button>
				</form>
			</div>
		</div>
	<?php endif; ?>
</div>