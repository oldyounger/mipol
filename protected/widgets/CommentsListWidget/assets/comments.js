FORM_COMMENT_LOCKED = false;

$(document).ready(function(){
	/* COMMENT MODULE */
	function refreshComments()
	{
		$('.comment-list-container').each(function(){
			var pStamp = $(this).data('stamp');
			var commentBox = $(this).find('.comment-box');
			var parentObj = $(this);
			var infoContainer = $(this).closest('.comment-list-container');
			var canDeleteAll = $(this).data('deleteall');

			$.ajax({
				url: CONFIG.urls[infoContainer.data('path')].replace('__NAME__', infoContainer.data('name')),
				type: 'GET',
				data: {'comments-list': Math.floor((Math.random()*10000)+1), 'canDeleteAll': canDeleteAll},
				dataType: 'html'
			}).done(function(data){
				var dataObj = $(data);
				var cBox = dataObj.find('.comment-box').html();

				var cStamp = dataObj.data('stamp');
				if(cStamp != pStamp){ //update
					parentObj.data('stamp', cStamp);

					if(parentObj.hasClass('comment-list-tiny')){ //tiny, scrollable layout, just update
                        commentBox.html(cBox);
					}
					else{ //normal, we must prevent jumping of site
                        commentBox.css({
							'max-height': commentBox.height() + 'px',
							'overflow': 'hidden'
						}).animate({
							opacity: 0
						}, {
							duration: 200,
							done: function(){
								$(this).html(cBox);

								$(this).animate({
									opacity: 1,
									'max-height': '10000px'
								}, {
									duration: 200
								});
							}
						});
					}
				}
				else{ //update without animation (only timing changed)
                    commentBox.html(cBox);
				}
			}).fail(function(){
				//do nothing if failed
			});
		});
	}

	$(document).on('submit', '.addCommentForm', function(){
		if(FORM_COMMENT_LOCKED){
			Application.notifyAlert(Application.getTranslation('REQUEST_IN_PROGRESS'));
		}
		else if(Application.isUserLogged()){
			FORM_COMMENT_LOCKED = true;

			var thisObj = $(this);
			var infoContainer = $(this).closest('.comment-list-container');
			$.ajax({
				url: CONFIG.urls[infoContainer.data('path')].replace('__NAME__', infoContainer.data('name')),
				type: 'POST',
				data: $(this).serialize(),
				dataType: 'json'
			}).done(function(response){
				var responseObj = new AjaxAPI(response);
				if(responseObj.isSuccess()){
					Application.notifySuccess(Application.getTranslation('SUCCESSFULLY_ADDED_COMMENT'));

					thisObj.find('textarea,input').val(''); //clear inputs
					refreshComments();
				}
				else{
					Application.notifyError(responseObj.getResultInfo(true));
				}
			}).fail(function(){
				Application.showModalRequestFailed();
			}).always(function(){
				FORM_COMMENT_LOCKED = false;
			});
		}
		else if(Application.isUserLogged() && !Application.isUserActive()){
			Application.showModalPleaseActivate();
		}
		else{
			Application.showModalPleaseLogIn();
		}

		return false;
	});

	$(document).on('click', '.comment-list-container .quote-button .icon-custom-quote', function(){
		var internalObj = $(this).parent().siblings('.comment-content').find('.comment-content-internal');

		var textToInsert = '[quote]' + $.trim(internalObj.text()) + '[/quote]\n';

		var textArea = $(this).closest('.comment-list-container').find('.addCommentForm textarea');
		textArea.val(textArea.val() + textToInsert);
	});

	$(document).on('click', '.comment-list-container .quote-button .icon-custom-times', function(){
		var parentObj = $(this);
		var infoContainer = $(this).closest('.comment-list-container');
		bootbox.confirm('Czy na pewno chcesz usunąć ten komentarz?', function(ret){
			if(ret == true && FORM_COMMENT_LOCKED){
				Application.notifyAlert(Application.getTranslation('REQUEST_IN_PROGRESS'));
			}
			else if(ret == true){
				FORM_COMMENT_LOCKED = true;
				$.ajax({
					url: CONFIG.urls[infoContainer.data('path')].replace('__NAME__', infoContainer.data('name')),
					type: 'POST',
					data: {'delete': 'true', 'comment_id': parentObj.data('id')},
					dataType: 'json'
				}).done(function(response){
					var responseObj = new AjaxAPI(response);
					if(responseObj.isSuccess()){
						Application.notifySuccess(Application.getTranslation('SUCCESSFULLY_REMOVED_COMMENT'));

						refreshComments();
					}
					else{
						Application.notifyError(responseObj.getResultInfo(true));
					}
				}).always(function(){
					FORM_COMMENT_LOCKED = false;
				});
			}
		});
	});

	//refresh comments every 60s
	setInterval(function(){
		refreshComments();
	}, 60*1000);

	/* TINY */
	$(document).on('click', '.onClickToggleComment', function(){
		var postBox = $(this).siblings('.comment-post-box');
		if(postBox.hasClass('hidden')){
			postBox.slideDown().removeClass('hidden');
		}
		else{
			postBox.slideUp().addClass('hidden');
		}

		return false;
	})
	/* COMMENT MODULE END */
});