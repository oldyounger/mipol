<?php
class CommentsListWidget extends CWidget
{
	public $commentList;
	public $jsUrlName;
	public $url;

	public $layout = 'normal';

	public $registerOnlyScripts = false;
	public $disableInput = false;
	public $canDeleteAll = false;

	public static $moduleId = 0;

	public function init()
	{
		$this->registerClientScript();
	}

	protected function registerClientScript()
	{
		$file = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'assets/';
		$path = Yii::app()->getAssetManager()->publish($file);

		$cs = Yii::app()->clientScript;

		$cs->registerLessFile($path . '/comments.less');
		$cs->registerScriptFile($path . '/comments.js');
	}

	public function run()
	{
		if($this->registerOnlyScripts){ //we wanted only scripts
			return;
		}

		//TODO: make pagination (?)

		$lastCommentStamp = 0;

		$ob = end($this->commentList);
		if(isSet($ob['date'])){
			$lastCommentStamp = $ob['date'];
		}

		$this->render($this->layout, Array(
			'comments' => $this->commentList,
			'elementName' => $this->url,
			'jsUrlName' => $this->jsUrlName,
			'moduleId' => ++self::$moduleId,
			'lastCommentStamp' => $lastCommentStamp,
			'canDeleteAll' => $this->canDeleteAll,
		));
	}
}