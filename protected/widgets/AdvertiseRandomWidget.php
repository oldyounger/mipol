<?php
class AdvertiseRandomWidget extends CWidget
{
	public $place;
	
	public function init()
	{
		//
	}
	
	public function run()
	{
		$mainAdv = AdvertisementPlace::getRandomEntry($this->place);

		if($mainAdv !== NULL){
			$this->widget('application.widgets.AdvertiseWidget', Array(
				'type' => $mainAdv['type'],
				'file' => $mainAdv['file'],
				'image' => $mainAdv['image'],
				'url' => $mainAdv['url'],
				'size' => $mainAdv['size'],
			));
		}
	}
}