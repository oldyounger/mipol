<?php
class DateDiffWidget extends CWidget
{
	public $now = 'now';
	public $compare;

	public $extended = false;

	public function init()
	{
		//
	}

	public function run()
	{
		//$dob = date("Y-m-d H:i:s", $this->now);
		$nowObject = new DateTime($this->now);

		//$cmpob = date("Y-m-d H:i:s", $this->compare);
		$compareObject = new DateTime($this->compare);

		$diff = $compareObject->diff($nowObject);

		if($this->extended){
			if($compareObject > $nowObject){ //future
				if($diff->y > 0){
					//za N lat
					echo Yii::t('app', 'in {count} years', Array('{count}' => $diff->y));
				}
				else if($diff->m > 0){
					//w tym roku
					echo Yii::t('app', 'this year');
				}
				else if($diff->d > 7){
					//w tym miesiącu
					echo Yii::t('app', 'this month');
				}
				else if($diff->d > 0){
					//w tym tygodniu
					echo Yii::t('app', 'this week');
				}
				else if($diff->h > 0){
					//dzisiaj
					echo Yii::t('app', 'today');
				}
				else if($diff->i >= 5){
					//za chwilę
					echo Yii::t('app', 'in a moment');
				}
				else if($diff->i < 5){
					//teraz
					echo Yii::t('app', 'now');
				}
			}
			else{ //past
				if($diff->y > 1){
					echo $diff->y.' '.Yii::t('app', 'years ago');
					//N lat temu
				}
				else if($diff->y == 1){
					//rok temu
					echo Yii::t('app', 'year ago');
				}
				else if($diff->m > 1){
					//N miesięcy temu
					echo $diff->m.' '.Yii::t('app', 'months ago');
				}
				else if($diff->m == 1){
					echo Yii::t('app', 'month ago');
				}
				else if($diff->d > 1){
					//N dni temu
					echo $diff->d.' '.Yii::t('app', 'days ago');
				}
				else if($diff->d == 1){
					echo Yii::t('app', 'day ago');
				}
				else if($diff->h > 0){
					//N h temu
					echo $diff->h.' '.Yii::t('app', 'hours ago');
				}
				else if($diff->i > 0){
					//N min temu
					echo $diff->i.' '.Yii::t('app', 'minutes ago');
				}
				else if($diff->i == 0){
					//przed chwilą
					echo Yii::t('app', 'just now');
				}
			}
		}
		else{ //basic
			if($compareObject > $nowObject){ //future
				if($diff->days > 1){
					echo $compareObject->format('d.m.Y');
				}
				else if($diff->days == 1){
					echo Yii::t('app', 'tomorrow');
				}
				else if($diff->h > 0){
					//dzisiaj
					echo Yii::t('app', 'today');
				}
				else if($diff->i >= 5){
					//za chwilę
					echo Yii::t('app', 'in a moment');
				}
				else if($diff->i < 5){
					//teraz
					echo Yii::t('app', 'now');
				}
			}
			else{ //past
				if($diff->days > 1){
					echo $compareObject->format('d.m.Y');
				}
				else if($diff->days == 1){
					echo Yii::t('app', 'yesterday');
				}
				else if($diff->h > 0){
					//N h temu
					echo $diff->h.' '.Yii::t('app', 'hours ago');
				}
				else if($diff->i > 0){
					//N min temu
					echo $diff->i.' '.Yii::t('app', 'minutes ago');
				}
				else if($diff->i == 0){
					//przed chwilą
					echo Yii::t('app', 'just now');
				}
			}
		}
	}
}