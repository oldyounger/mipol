<?php
class DateRangePicker extends CWidget
{
	public $name = 'dateRangePicker';
	public $value = '';
	public $htmlOptions = array();
	public $options = array();

	public $callback = '';

	public $asInput = true;

	public function init()
	{
		$this->registerClientScript();
		parent::init();
	}

	public function run()
	{
		$elId = CHtml::getIdByName($this->name);

		if($this->asInput){
			echo CHtml::textField($this->name, $this->value, $this->htmlOptions);
		}
		else{
			$arr = $this->htmlOptions;
			$arr['id'] = $elId;

			echo CHtml::openTag('div', $arr);
			echo CHtml::closeTag('div');
		}

		$options = '';
		if($this->options){
			$options = CJSON::encode($this->options);
		}

		$callback = '';
		if($this->callback){
			if(mb_strlen($options) > 0){
				$callback .= ', ';
			}
			else{
				$callback .= '{}, ';
			}

			$callback .= CJavaScript::encode($this->callback);
		}

		$cs = Yii::app()->clientScript->registerScript($elId, "
			$('#" . $elId . "').daterangepicker($options $callback);
		");
	}

	protected function registerClientScript()
	{
		$file = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'assets/';

		$cs = Yii::app()->clientScript;
		$cs->registerCssFile(Yii::app()->getAssetManager()->publish($file . 'daterangepicker.css'));
		$cs->registerScriptFile(Yii::app()->getAssetManager()->publish($file . 'daterangepicker.js'));
		$cs->registerScriptFile(Yii::app()->getAssetManager()->publish($file . 'moment.js'));

		$langFile = $file . 'lang/moment.' . Yii::app()->language . '.min.js';
		if(file_exists($langFile)){
			$cs->registerScriptFile(Yii::app()->getAssetManager()->publish($langFile));
		}
		$langFile2 = $file . 'lang/daterangepicker.' . Yii::app()->language . '.js';
		if(file_exists($langFile2)){
			$cs->registerScriptFile(Yii::app()->getAssetManager()->publish($langFile2));
		}
	}
}