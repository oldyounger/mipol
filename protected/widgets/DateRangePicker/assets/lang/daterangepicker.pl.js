!function($) {
	$.daterangepicker.locale = {
		applyLabel: 'Zastosuj',
		clearLabel: "Wyczyść",
		fromLabel: 'Od',
		toLabel: 'Do',
		weekLabel: 'W',
	};

}(window.jQuery);
