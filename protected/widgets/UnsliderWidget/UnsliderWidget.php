<?php
class UnsliderWidget extends CWidget
{
	public $items = Array();
	public $speed = 500;
	public $delay = 2000;

	protected $elementId = NULL;
	public static $moduleId = 0;

	public function init()
	{
		$this->elementId = 'yw-unslider-' . ++self::$moduleId;

		$this->registerClientScript();
	}

	protected function registerClientScript()
	{
		$file = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR;
		$path = Yii::app()->getAssetManager()->publish($file);

		$cs = Yii::app()->clientScript;

		$cs->registerScriptFile($path . '/unslider.js');
	}

	public function run()
	{
		Yii::app()->clientScript->registerScript($this->elementId, <<<EOT
			$(window).load(function(){
				$('.banner').unslider({
					speed: {$this->speed},
					delay: {$this->delay},
					keys: false,
					fluid: false,
					dots: false,
				});
			});
EOT
);

		Yii::app()->clientScript->registerCss($this->elementId, <<<EOT
			#{$this->elementId} {
				position: relative;
				overflow: auto;
			}

			#{$this->elementId} li {
				list-style: none;
			}

 			#{$this->elementId} ul li {
 				float: left;
 			}
EOT
);

		$this->render('slider');
	}
}