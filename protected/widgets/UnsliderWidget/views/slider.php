<?php /** @var UnsliderWidget $this */ ?>

<div class="banner" id="<?php echo $this->elementId; ?>">
	<ul>
		<?php foreach($this->items as $slide): ?>
			<?php $this->render('slide', Array(
				'item' => $slide,
			)); ?>
		<?php endforeach; ?>
	</ul>
</div>