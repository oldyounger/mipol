<?php
class BootstrapSelect extends CWidget
{
	public $id = NULL;
	public $name;
	public $items;
	public $selected = '';

	/* list of available JS options in bootstrap select:
		http://silviomoreto.github.io/bootstrap-select/#options
	*/
	public $options = Array();

	public $htmlOptions = Array();

	public static $moduleId = 0;

	public function init()
	{
		$this->registerClientScript();
	}

	protected function registerClientScript()
	{
		$file = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'assets/';
		$path = Yii::app()->getAssetManager()->publish($file);

		$cs = Yii::app()->clientScript;

		$cs->registerCssFile($path . '/bootstrap-select.css');
		$cs->registerScriptFile($path . '/bootstrap-select.js');
	}

	public function run()
	{
		$elementId = NULL;
		if($this->id !== NULL){ //if manual id is posted, use it!
			$elementId = $this->id;
		}
		else{
			$elementId = 'ytbsel-'.self::$moduleId++;
		}

		$this->htmlOptions['id'] = $elementId;

		$options = '';
		if($this->options){
			$options = CJSON::encode($this->options);
		}

		echo CHtml::dropDownList($this->name, $this->selected, $this->items, $this->htmlOptions);

		Yii::app()->clientScript->registerScript($elementId, "
			$('#" . $elementId . "').selectpicker($options);
		");
	}
}