/**
 * Created by Bld on 2016-02-17.
 */
$(window).scroll(function() {
    if($( window ).width()>=991) {
        var scroll = $(window).scrollTop();
        var button_height = $("header#mainNav").height();
        if ( (scroll >= button_height*2) ) {
            $("header#mainNav").addClass("scrollMenu");
        }
        else{
            $("header#mainNav").removeClass("scrollMenu");
        }
    }
});


//jQuery for page scrolling feature - requires jQuery Easing plugin
$(function() {
    $('a.page-scroll').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
    });
});


//show more
$(document).ready(function () {
    var elCount = $(".newsBoxHolder .newsElement").size();
    var howManyElShow = 6;
    var showMoreButton = $('#showMoreNewsElements');
    $('.newsBoxHolder .newsElement:lt(' + howManyElShow + ')').show();
    showMoreButton.click(function (e) {
        e.preventDefault();
        howManyElShow = (howManyElShow + 3 <= elCount) ? howManyElShow + 3 : elCount;
        $('.newsBoxHolder .newsElement:lt(' + howManyElShow + ')').show();
        (howManyElShow == elCount) ? showMoreButton.hide() : '';
    });

});


//imgMap coords responsive
$(document).ready(function() {
    $('img[usemap]').rwdImageMaps();

    $("#MapImg area").on("click", function(e){
        e.preventDefault();
        var category = $(this).attr("data-category");
        $("#productElements").load("products/"+category);

    });
});

