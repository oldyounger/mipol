/**
 * Created by Bld on 2015-09-17.
 */

google.maps.event.addDomListener(window, 'load', init);
/*google.maps.event.addDomListener(window, 'load', initiate_geolocation);*/
function init() {
    // Basic options for a simple Google Map
    // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
    var my = new google.maps.LatLng(49.946573,19.849861);
    var image = ('images/marker.png');


    var mapOptions = {
        // How zoomed in you want the map to start at (always required)
        scrollwheel: false,
        zoom: 14,
        center:my,
        mapTypeControl:true,
        panControl: true,
        zoomControl: true,
        mapTypeControlOptions: {
            style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
            position: google.maps.ControlPosition.LEFT_BOTTOM
        },          // The latitude and longitude to center the map (always required)
        zoomControlOptions: {
            style: google.maps.ZoomControlStyle.LARGE,
            position: google.maps.ControlPosition.LEFT_BOTTOM
        },
        panControlOptions: {
            position: google.maps.ControlPosition.LEFT_BOTTOM
        },
        styles: [
            {
                featureType:'water',
                stylers:[
                    {color:'#a8a8a8'},
                    {visibility:'on'}
                ]
            },
            {
                featureType:'landscape',
                stylers:[
                    {color:'#ebebeb'}
                ]
            },
            {
                featureType:'road',
                stylers:[
                    {saturation:-100},
                    {lightness:45}
                ]
            },
            {
                featureType:'road.highway',
                stylers:[
                    {visibility:'simplified'}
                ]
            },
            {
                featureType:'road.arterial',
                elementType:'labels.icon',
                stylers:[
                    {visibility:'off'}
                ]
            },
            {
                featureType:'administrative',
                elementType:'labels.text.fill',
                stylers:[
                    {color:'#444444'}
                ]
            },
            {
                featureType:'transit',
                stylers:[
                    {visibility:'off'}
                ]
            },
            {
                featureType:'poi',
                stylers:[
                    {visibility:'off'}
                ]
            }
        ]

        // How you would like to style the map.
        // This is where you would paste any style found on Snazzy Maps.
    };

    // Get the HTML DOM element that will contain your map
    // We are using a div with id="map" seen below in the <body>
    var mapElement = document.getElementById('map');

    // Create the Google Map using out element and options defined above
    var map = new google.maps.Map(mapElement, mapOptions);
    var triangleCoords = [
        new google.maps.LatLng(25.774252, -80.190262)
    ];

    // Construct the polygon.
    bermudaTriangle = new google.maps.Polygon({
        paths: triangleCoords,
        strokeColor: '#FF0000',
        strokeOpacity: 1,
        strokeWeight: 1,
        fillColor: '#FF0000',
        fillOpacity: 0
    });

    bermudaTriangle.setMap(map);
    var infowindow = new google.maps.InfoWindow();

    var marker = new google.maps.Marker({
        position: my,
        map: map,
        icon: image
//                                    zoom: 14
    });

    google.maps.event.addListener(marker, 'click', (function(marker) {
        return function() {
            infowindow.setContent("Mipol");
            infowindow.open(map, marker);
        }
    })(marker));
    infowindow.setContent("Mipol");
    infowindow.open(map, marker);
    google.maps.event.addListener(map, 'click', function() {
        infowindow.close();
    });

    function geolocalize() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(geolocalizeSuccess, geolocalizEerror, {enableHighAccuracy: true});
        } else {
            error('not supported');
        }

    }

    function geolocalizEerror(msg) {
        var s = document.querySelector('#status');
        s.innerHTML = typeof msg == 'string' ? msg : "failed";
        s.className = 'fail';
    }

    function geolocalizeSuccess(position) {
        var latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        var infowindow = new google.maps.InfoWindow();
        var myMarker = new google.maps.Marker({
            position: latlng,
            map: map,
            title: 'Twoja pozycja',
            draggable: true,
            icon: 'images/geolocalization.png',
            zoom: 14
        });
        google.maps.event.addListener(myMarker, 'click', (function(myMarker) {
            return function() {
                infowindow.setContent("Twoja Pozycja");
                infowindow.open(map, myMarker);
            }
        })(myMarker));

        google.maps.event.addListener(map, 'click', function() {
            infowindow.close();
        });
    }

    google.maps.event.addDomListener(window, 'load', init);
    geolocalize();
    var markers = [];

    function addmarker(lati,longi,title,content) {

        var infowindow = new google.maps.InfoWindow();
        var marker;

        marker = new google.maps.Marker({
            position: new google.maps.LatLng(lati,longi),
            draggable: false,
            map: map,
            icon: image,
            title: title
        });

        google.maps.event.addListener(marker, 'click', (function(marker) {
            return function() {
                infowindow.setContent(content);
                infowindow.open(map, marker);
            }
        })(marker));
        infowindow.setContent(content);
        google.maps.event.addListener(map, 'click', function() {
            infowindow.close();
        });
        markers.push(marker);

    }

}