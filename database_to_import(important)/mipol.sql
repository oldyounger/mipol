-- phpMyAdmin SQL Dump
-- version 4.2.12deb2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Czas generowania: 25 Lut 2016, 11:44
-- Wersja serwera: 5.5.40-1
-- Wersja PHP: 5.6.4-1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Baza danych: `mipol`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ofi_about`
--

CREATE TABLE IF NOT EXISTS `ofi_about` (
`id` int(11) unsigned NOT NULL,
  `category_id` int(11) unsigned NOT NULL,
  `datetime` datetime NOT NULL,
  `url` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `title` varchar(250) COLLATE utf8_polish_ci NOT NULL,
  `short_content` text COLLATE utf8_polish_ci NOT NULL,
  `view_count` int(11) unsigned NOT NULL,
  `hidden` tinyint(1) unsigned NOT NULL,
  `header_image` int(11) unsigned DEFAULT NULL,
  `only_logged` tinyint(1) unsigned NOT NULL,
  `description` text COLLATE utf8_polish_ci,
  `keywords` text COLLATE utf8_polish_ci,
  `extra` text COLLATE utf8_polish_ci
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `ofi_about`
--

INSERT INTO `ofi_about` (`id`, `category_id`, `datetime`, `url`, `title`, `short_content`, `view_count`, `hidden`, `header_image`, `only_logged`, `description`, `keywords`, `extra`) VALUES
(1, 0, '2015-11-18 16:45:00', 'kim-jestesmy', 'Kim jesteśmy', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium adipisci deleniti dignissimos maiores, neque nostrum quaerat sunt voluptatem! Aliquid animi, commodi culpa dicta dolores dolorum harum illum labore laborum magni mollitia officiis placeat, quibusdam recusandae repellendus saepe sed, sint vero. Commodi expedita maxime nobis repellat similique sit vitae? Consectetur, cumque. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium adipisci deleniti dignissimos maiores, neque nostrum quaerat sunt voluptatem! Aliquid animi, commodi culpa dicta dolores dolorum harum illum labore laborum magni mollitia officiis placeat, quibusdam recusandae repellendus saepe sed, sint vero. Commodi expedita maxime nobis repellat similique sit vitae? Consectetur, cumque.</p>\r\n', 0, 0, 1277, 0, '', '', NULL),
(2, 0, '2015-11-18 16:45:00', 'gwarancja-jakosci', 'Gwaranacja Jakości', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium adipisci deleniti dignissimos maiores, neque nostrum quaerat sunt voluptatem! Aliquid animi, commodi culpa dicta dolores dolorum harum illum labore laborum magni mollitia officiis placeat, quibusdam recusandae repellendus saepe sed, sint vero. Commodi expedita maxime nobis repellat similique sit vitae? Consectetur, cumque. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium adipisci deleniti dignissimos maiores, neque nostrum quaerat sunt voluptatem! Aliquid animi, commodi culpa dicta dolores dolorum harum illum labore laborum magni mollitia officiis placeat, quibusdam recusandae repellendus saepe sed, sint vero. Commodi expedita maxime nobis repellat similique sit vitae? Consectetur, cumque.</p>\r\n', 0, 0, 1278, 0, '', '', '<span>&quot;</span>Jakość to coś, co zadowala, a nawet zachwyca klient&oacute;w.<span>&quot;</span>\r\n\r\n<p>- William Deming</p>\r\n');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ofi_auth_assignment`
--

CREATE TABLE IF NOT EXISTS `ofi_auth_assignment` (
  `itemname` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `userid` int(11) unsigned NOT NULL,
  `bizrule` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `data` text CHARACTER SET utf8 COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `ofi_auth_assignment`
--

INSERT INTO `ofi_auth_assignment` (`itemname`, `userid`, `bizrule`, `data`) VALUES
('admin.admin_main', 1, NULL, 'N;'),
('admin.admin_main', 2, NULL, 'N;');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ofi_auth_item`
--

CREATE TABLE IF NOT EXISTS `ofi_auth_item` (
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `bizrule` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `data` text CHARACTER SET utf8 COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `ofi_auth_item`
--

INSERT INTO `ofi_auth_item` (`name`, `type`, `description`, `bizrule`, `data`) VALUES
('admin.admin_main', 2, 'Administrator główny', '', NULL),
('admin.category.create', 0, 'Admin - Kategorie - Dodawanie', NULL, NULL),
('admin.category.delete', 0, 'Admin - Kategorie - Usuwanie', NULL, NULL),
('admin.category.read', 0, 'Admin - Kategorie - Dodawanie', NULL, NULL),
('admin.category.update', 0, 'Admin - Kategorie - Aktualizowanie', NULL, NULL),
('admin.configuration.read', 0, 'Admin - Konfiguracja - Oglądanie', NULL, NULL),
('admin.configuration.update', 0, 'Admin - Konfiguracja - Aktualizowanie', NULL, NULL),
('admin.contact.answer', 0, 'Admin - Formularz Kontaktowy - Odpowiadanie', NULL, NULL),
('admin.contact.delete', 0, 'Admin - Formularz Kontaktowy - Usuwanie', NULL, NULL),
('admin.contact.read', 0, 'Admin - Formularz Kontaktowy - Oglądanie', NULL, NULL),
('admin.flat.create', 0, 'Admin - Mieszkania - Dodawanie', NULL, NULL),
('admin.flat.delete', 0, 'Admin - Mieszkania - Usuwanie', NULL, NULL),
('admin.flat.read', 0, 'Admin - Mieszkania - Dodawanie', NULL, NULL),
('admin.flat.update', 0, 'Admin - Mieszkania - Aktualizowanie', NULL, NULL),
('admin.gallery.create', 0, 'Admin - Galerie - Dodawanie', NULL, NULL),
('admin.gallery.delete', 0, 'Admin - Galerie - Usuwanie', NULL, NULL),
('admin.gallery.read', 0, 'Admin - Galerie - Dodawanie', NULL, NULL),
('admin.gallery.update', 0, 'Admin - Galerie - Aktualizowanie', NULL, NULL),
('admin.invest.create', 0, 'Admin - Invest - Dodawanie', NULL, NULL),
('admin.invest.delete', 0, 'Admin - Invest - Usuwanie', NULL, NULL),
('admin.invest.read', 0, 'Admin - Invest - Czytanie', NULL, NULL),
('admin.invest.update', 0, 'Admin - Invest - Update', NULL, NULL),
('admin.news.create', 0, 'Admin - News - Dodawanie', NULL, NULL),
('admin.news.delete', 0, 'Admin - News - Usuwanie', NULL, NULL),
('admin.news.read', 0, 'Admin - News - Dodawanie', NULL, NULL),
('admin.news.update', 0, 'Admin - News - Aktualizowanie', NULL, NULL),
('admin.page.create', 0, 'Admin - Strona HTML - Dodawanie', NULL, NULL),
('admin.page.delete', 0, 'Admin - Strona HTML - Usuwanie', NULL, NULL),
('admin.page.read', 0, 'Admin - Strona HTML - Oglądanie', NULL, NULL),
('admin.page.update', 0, 'Admin - Strona HTML - Aktualizowanie', NULL, NULL),
('admin.permission.create', 0, 'Admin - Uprawnienia - Dodawanie', NULL, NULL),
('admin.permission.delete', 0, 'Admin - Uprawnienia - Usuwanie', NULL, NULL),
('admin.permission.read', 0, 'Admin - Uprawnienia - Oglądanie', NULL, NULL),
('admin.permission.update', 0, 'Admin - Uprawnienia - Aktualizowanie', NULL, NULL),
('admin.recommendation.create', 0, 'Admin - Rekomendacje - Dodawanie', NULL, NULL),
('admin.recommendation.delete', 0, 'Admin - Rekomendacje - Usuwanie', NULL, NULL),
('admin.recommendation.read', 0, 'Admin - Rekomendacje - Oglądanie', NULL, NULL),
('admin.recommendation.update', 0, 'Admin - Rekomendacje - Aktualizowanie', NULL, NULL),
('admin.slider.create', 0, 'Admin - Slider - Dodawanie', NULL, NULL),
('admin.slider.delete', 0, 'Admin - Slider - Usuwanie', NULL, NULL),
('admin.slider.read', 0, 'Admin - Slider - Oglądanie', NULL, NULL),
('admin.slider.update', 0, 'Admin - Slider - Aktualizowanie', NULL, NULL),
('admin.user.create', 0, 'Admin - Użytkownicy - Dodawanie', NULL, NULL),
('admin.user.delete', 0, 'Admin - Użytkownicy - Usuwanie', NULL, NULL),
('admin.user.read', 0, 'Admin - Użytkownicy - Oglądanie', NULL, NULL),
('admin.user.update', 0, 'Admin - Użytkownicy - Aktualizacja', NULL, NULL),
('site.guest', 2, 'Gość', 'return Yii::app()->user->getIsGuest();', NULL),
('site.user', 2, 'Użytkownik (zalogowany)', 'return !Yii::app()->user->getIsGuest();', NULL),
('site.user.activation', 0, 'Strona - Aktywacja', 'NULL', NULL),
('site.user.active', 2, 'Użytkownik (zalogowany i aktywny)', 'return !Yii::app()->user->getIsGuest() && isSet(Yii::app()->user->active) && Yii::app()->user->active;', NULL),
('site.user.login', 0, 'Strona - Logowanie', NULL, NULL),
('site.user.logout', 0, 'Strona - Wylogowanie', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ofi_auth_item_child`
--

CREATE TABLE IF NOT EXISTS `ofi_auth_item_child` (
  `parent` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `ofi_auth_item_child`
--

INSERT INTO `ofi_auth_item_child` (`parent`, `child`) VALUES
('admin.admin_main', 'admin.category.create'),
('admin.admin_main', 'admin.category.delete'),
('admin.admin_main', 'admin.category.read'),
('admin.admin_main', 'admin.category.update'),
('admin.admin_main', 'admin.configuration.read'),
('admin.admin_main', 'admin.configuration.update'),
('admin.admin_main', 'admin.contact.answer'),
('admin.admin_main', 'admin.contact.delete'),
('admin.admin_main', 'admin.contact.read'),
('admin.admin_main', 'admin.flat.create'),
('admin.admin_main', 'admin.flat.delete'),
('admin.admin_main', 'admin.flat.read'),
('admin.admin_main', 'admin.flat.update'),
('admin.admin_main', 'admin.gallery.create'),
('admin.admin_main', 'admin.gallery.delete'),
('admin.admin_main', 'admin.gallery.read'),
('admin.admin_main', 'admin.gallery.update'),
('admin.admin_main', 'admin.invest.create'),
('admin.admin_main', 'admin.invest.delete'),
('admin.admin_main', 'admin.invest.read'),
('admin.admin_main', 'admin.invest.update'),
('admin.admin_main', 'admin.news.create'),
('admin.admin_main', 'admin.news.delete'),
('admin.admin_main', 'admin.news.read'),
('admin.admin_main', 'admin.news.update'),
('admin.admin_main', 'admin.page.create'),
('admin.admin_main', 'admin.page.delete'),
('admin.admin_main', 'admin.page.read'),
('admin.admin_main', 'admin.page.update'),
('admin.admin_main', 'admin.permission.create'),
('admin.admin_main', 'admin.permission.delete'),
('admin.admin_main', 'admin.permission.read'),
('admin.admin_main', 'admin.permission.update'),
('admin.admin_main', 'admin.recommendation.create'),
('admin.admin_main', 'admin.recommendation.delete'),
('admin.admin_main', 'admin.recommendation.read'),
('admin.admin_main', 'admin.recommendation.update'),
('admin.admin_main', 'admin.slider.create'),
('admin.admin_main', 'admin.slider.delete'),
('admin.admin_main', 'admin.slider.read'),
('admin.admin_main', 'admin.slider.update'),
('admin.admin_main', 'admin.user.create'),
('admin.admin_main', 'admin.user.delete'),
('admin.admin_main', 'admin.user.read'),
('admin.admin_main', 'admin.user.update'),
('site.user', 'site.guest'),
('admin.admin_main', 'site.user'),
('site.guest', 'site.user.activation'),
('site.guest', 'site.user.login'),
('site.user', 'site.user.logout');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ofi_category`
--

CREATE TABLE IF NOT EXISTS `ofi_category` (
`id` int(11) unsigned NOT NULL,
  `name` varchar(128) COLLATE utf8_polish_ci NOT NULL,
  `url` varchar(134) COLLATE utf8_polish_ci NOT NULL,
  `hidden` tinyint(1) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ofi_configuration`
--

CREATE TABLE IF NOT EXISTS `ofi_configuration` (
  `key` varchar(64) COLLATE utf8_polish_ci NOT NULL,
  `value` text COLLATE utf8_polish_ci NOT NULL,
  `description` text COLLATE utf8_polish_ci NOT NULL,
  `date_changed` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `ofi_configuration`
--

INSERT INTO `ofi_configuration` (`key`, `value`, `description`, `date_changed`) VALUES
('email', '#', 'email', '2015-11-17 10:36:17'),
('facebook_url', '#', 'Hiperłącze do portalu Facebook', '2015-11-17 10:36:01'),
('keywords', 'null', 'Słowa kluczowe', '2015-06-17 08:02:49'),
('linkedin_url', '#', 'Hiperłącze do LinkedIn.', '2015-07-13 12:18:29'),
('phone', '#', 'Numer telefonu', '2015-11-17 10:36:11'),
('SEO', 'null', 'Opis SEO', '2015-07-13 14:06:52'),
('twitter_url', '#', 'Hiperłącze do portalu Twitter', '2015-07-13 12:19:16'),
('viemo_url', '#', 'Hiperłącze do portalu Vimeo', '2015-11-17 10:36:01');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ofi_file`
--

CREATE TABLE IF NOT EXISTS `ofi_file` (
`id` int(11) unsigned NOT NULL,
  `file` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'nazwa pliku na serwerze',
  `name` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'oryginalna nazwa pliku',
  `size` smallint(5) unsigned NOT NULL COMMENT 'rozmiar w bajtach',
  `uniqueDir` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'podkatalog miniatur',
  `mime` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'typ mime grafiki',
  `addedTime` datetime NOT NULL COMMENT 'czas utworzenia',
  `delete` tinyint(1) unsigned NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=640 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `ofi_file`
--

INSERT INTO `ofi_file` (`id`, `file`, `name`, `size`, `uniqueDir`, `mime`, `addedTime`, `delete`) VALUES
(453, '2-01.c-01c.pdf', '2-01.C-01c.pdf', 65535, '2-01.c-01c__2015_06_25___12_37_18___7115', 'application/pdf', '2015-06-25 12:37:18', 0),
(454, '2-02.c-02c.pdf', '2-02.C-02c.pdf', 65535, '2-02.c-02c__2015_06_25___15_40_11___3275', 'application/pdf', '2015-06-25 15:40:11', 0),
(455, '2-03.b-01e.pdf', '2-03.B-01e.pdf', 65535, '2-03.b-01e__2015_06_25___15_40_50___3294', 'application/pdf', '2015-06-25 15:40:50', 0),
(456, '2-04.a-01c.pdf', '2-04.A-01c.pdf', 65535, '2-04.a-01c__2015_06_25___15_41_37___1554', 'application/pdf', '2015-06-25 15:41:37', 0),
(457, '2-05.c-04c.pdf', '2-05.C-04c.pdf', 65535, '2-05.c-04c__2015_06_25___15_41_43___8467', 'application/pdf', '2015-06-25 15:41:43', 0),
(458, '2-06.c-01a.pdf', '2-06.C-01a.pdf', 65535, '2-06.c-01a__2015_06_25___15_41_48___4715', 'application/pdf', '2015-06-25 15:41:48', 0),
(459, '2-07.c-02a.pdf', '2-07.C-02a.pdf', 65535, '2-07.c-02a__2015_06_25___15_41_50___1998', 'application/pdf', '2015-06-25 15:41:50', 0),
(460, '2-08.b-01a.pdf', '2-08.B-01a.pdf', 65535, '2-08.b-01a__2015_06_25___15_41_54___5962', 'application/pdf', '2015-06-25 15:41:54', 0),
(461, '2-09.a-01a.pdf', '2-09.A-01a.pdf', 65535, '2-09.a-01a__2015_06_25___15_41_58___9208', 'application/pdf', '2015-06-25 15:41:58', 0),
(462, '2-10.c-04a.pdf', '2-10.C-04a.pdf', 65535, '2-10.c-04a__2015_06_25___15_42_07___6126', 'application/pdf', '2015-06-25 15:42:07', 0),
(463, '2-11.c-01a.pdf', '2-11.C-01a.pdf', 65535, '2-11.c-01a__2015_06_25___15_42_55___6627', 'application/pdf', '2015-06-25 15:42:55', 0),
(464, '2-12.c-02a.pdf', '2-12.C-02a.pdf', 65535, '2-12.c-02a__2015_06_25___15_43_03___1470', 'application/pdf', '2015-06-25 15:43:03', 0),
(465, '2-13.b-01a.pdf', '2-13.B-01a.pdf', 65535, '2-13.b-01a__2015_06_25___15_43_08___2316', 'application/pdf', '2015-06-25 15:43:08', 0),
(466, '2-14.a-01a.pdf', '2-14.A-01a.pdf', 65535, '2-14.a-01a__2015_06_25___15_43_12___8279', 'application/pdf', '2015-06-25 15:43:12', 0),
(467, '2-15.c-04a.pdf', '2-15.C-04a.pdf', 65535, '2-15.c-04a__2015_06_25___15_43_18___4925', 'application/pdf', '2015-06-25 15:43:18', 0),
(468, '2-16.c-01a.pdf', '2-16.C-01a.pdf', 65535, '2-16.c-01a__2015_06_25___15_43_22___5861', 'application/pdf', '2015-06-25 15:43:22', 0),
(469, '2-17.c-02a.pdf', '2-17.C-02a.pdf', 65535, '2-17.c-02a__2015_06_25___15_43_26___1780', 'application/pdf', '2015-06-25 15:43:26', 0),
(470, '2-18.b-01a.pdf', '2-18.B-01a.pdf', 65535, '2-18.b-01a__2015_06_25___15_43_30___5107', 'application/pdf', '2015-06-25 15:43:30', 0),
(471, '2-19.a-01a.pdf', '2-19.A-01a.pdf', 65535, '2-19.a-01a__2015_06_25___15_43_34___1145', 'application/pdf', '2015-06-25 15:43:34', 0),
(472, '2-20.c-04a.pdf', '2-20.C-04a.pdf', 65535, '2-20.c-04a__2015_06_25___15_43_39___4313', 'application/pdf', '2015-06-25 15:43:39', 0),
(473, '2-12.c-02a.pdf', '2-12.C-02a.pdf', 65535, '2-12.c-02a__2015_06_25___15_44_15___6150', 'application/pdf', '2015-06-25 15:44:15', 0),
(474, '2-21.c-11.pdf', '2-21.C-11.pdf', 65535, '2-21.c-11__2015_06_25___15_45_02___5541', 'application/pdf', '2015-06-25 15:45:03', 0),
(475, '2-22.b-16.pdf', '2-22.B-16.pdf', 65535, '2-22.b-16__2015_06_25___15_45_05___9336', 'application/pdf', '2015-06-25 15:45:05', 0),
(476, '2-23.d-04.pdf', '2-23.D-04.pdf', 65535, '2-23.d-04__2015_06_25___15_45_11___8292', 'application/pdf', '2015-06-25 15:45:11', 0),
(477, '2-24.c-04a.pdf', '2-24.C-04a.pdf', 65535, '2-24.c-04a__2015_06_25___15_45_13___3959', 'application/pdf', '2015-06-25 15:45:13', 0),
(478, '2-25.a-01a.pdf', '2-25.A-01a.pdf', 65535, '2-25.a-01a__2015_06_25___15_45_18___3189', 'application/pdf', '2015-06-25 15:45:18', 0),
(479, '2-26.b-01b.pdf', '2-26.B-01b.pdf', 65535, '2-26.b-01b__2015_06_25___15_45_23___6985', 'application/pdf', '2015-06-25 15:45:23', 0),
(480, '2-27.a-01a.pdf', '2-27.A-01a.pdf', 65535, '2-27.a-01a__2015_06_25___15_45_27___5921', 'application/pdf', '2015-06-25 15:45:27', 0),
(481, '2-28.c-04a.pdf', '2-28.C-04a.pdf', 65535, '2-28.c-04a__2015_06_25___15_45_32___5831', 'application/pdf', '2015-06-25 15:45:32', 0),
(482, '2-29.c-04a.pdf', '2-29.C-04a.pdf', 65535, '2-29.c-04a__2015_06_25___15_45_37___7166', 'application/pdf', '2015-06-25 15:45:37', 0),
(483, '2-30.a-01a.pdf', '2-30.A-01a.pdf', 65535, '2-30.a-01a__2015_06_25___15_45_48___5903', 'application/pdf', '2015-06-25 15:45:48', 0),
(484, '2-21.c-11.pdf', '2-21.C-11.pdf', 65535, '2-21.c-11__2015_06_25___15_46_08___5675', 'application/pdf', '2015-06-25 15:46:08', 0),
(485, '2-24.c-04a.pdf', '2-24.C-04a.pdf', 65535, '2-24.c-04a__2015_06_25___15_46_24___1697', 'application/pdf', '2015-06-25 15:46:24', 0),
(486, '2-31.b-01b.pdf', '2-31.B-01b.pdf', 65535, '2-31.b-01b__2015_06_25___15_47_12___9930', 'application/pdf', '2015-06-25 15:47:12', 0),
(487, '2-32.a-01a.pdf', '2-32.A-01a.pdf', 65535, '2-32.a-01a__2015_06_25___15_47_16___9990', 'application/pdf', '2015-06-25 15:47:16', 0),
(488, '2-33.c-04a.pdf', '2-33.C-04a.pdf', 65535, '2-33.c-04a__2015_06_25___15_47_21___1318', 'application/pdf', '2015-06-25 15:47:21', 0),
(489, '2-34.c-04a.pdf', '2-34.C-04a.pdf', 65535, '2-34.c-04a__2015_06_25___15_47_24___4308', 'application/pdf', '2015-06-25 15:47:24', 0),
(490, '2-35.a-01a.pdf', '2-35.A-01a.pdf', 65535, '2-35.a-01a__2015_06_25___15_47_29___7605', 'application/pdf', '2015-06-25 15:47:29', 0),
(491, '2-36.b-01b.pdf', '2-36.B-01b.pdf', 65535, '2-36.b-01b__2015_06_25___15_47_34___6186', 'application/pdf', '2015-06-25 15:47:34', 0),
(492, '2-37.a-01a.pdf', '2-37.A-01a.pdf', 65535, '2-37.a-01a__2015_06_25___15_47_38___8099', 'application/pdf', '2015-06-25 15:47:38', 0),
(493, '2-38.c-04a.pdf', '2-38.C-04a.pdf', 65535, '2-38.c-04a__2015_06_25___15_47_43___6530', 'application/pdf', '2015-06-25 15:47:43', 0),
(494, '2-39.c-04a.pdf', '2-39.C-04a.pdf', 65535, '2-39.c-04a__2015_06_25___15_47_48___2590', 'application/pdf', '2015-06-25 15:47:48', 0),
(495, '2-40.a-01a.pdf', '2-40.A-01a.pdf', 65535, '2-40.a-01a__2015_06_25___15_47_52___4488', 'application/pdf', '2015-06-25 15:47:52', 0),
(496, '2-41.b-01b.pdf', '2-41.B-01b.pdf', 65535, '2-41.b-01b__2015_06_25___15_49_04___4820', 'application/pdf', '2015-06-25 15:49:04', 0),
(497, '2-42.a-01a.pdf', '2-42.A-01a.pdf', 65535, '2-42.a-01a__2015_06_25___15_49_08___3706', 'application/pdf', '2015-06-25 15:49:08', 0),
(498, '2-43.c-04a.pdf', '2-43.C-04a.pdf', 65535, '2-43.c-04a__2015_06_25___15_49_12___9980', 'application/pdf', '2015-06-25 15:49:12', 0),
(499, '2-44.d-05.pdf', '2-44.D-05.pdf', 65535, '2-44.d-05__2015_06_25___15_49_17___2564', 'application/pdf', '2015-06-25 15:49:17', 0),
(500, '2-45.b-16.pdf', '2-45.B-16.pdf', 65535, '2-45.b-16__2015_06_25___15_49_26___2981', 'application/pdf', '2015-06-25 15:49:26', 0),
(501, '2-46.d-04.pdf', '2-46.D-04.pdf', 65535, '2-46.d-04__2015_06_25___15_49_31___2486', 'application/pdf', '2015-06-25 15:49:31', 0),
(502, '2-47.c-04a.pdf', '2-47.C-04a.pdf', 65535, '2-47.c-04a__2015_06_25___15_49_37___5899', 'application/pdf', '2015-06-25 15:49:37', 0),
(503, '2-48.a-01a.pdf', '2-48.A-01a.pdf', 65535, '2-48.a-01a__2015_06_25___15_49_41___7763', 'application/pdf', '2015-06-25 15:49:41', 0),
(504, '2-49.b-01b.pdf', '2-49.B-01b.pdf', 65535, '2-49.b-01b__2015_06_25___15_49_47___4358', 'application/pdf', '2015-06-25 15:49:47', 0),
(505, '2-50.a-01a.pdf', '2-50.A-01a.pdf', 65535, '2-50.a-01a__2015_06_25___15_49_53___1842', 'application/pdf', '2015-06-25 15:49:53', 0),
(506, '2-51.c-05.pdf', '2-51.C-05.pdf', 65535, '2-51.c-05__2015_06_25___15_51_15___7249', 'application/pdf', '2015-06-25 15:51:15', 0),
(507, '2-52.c-04a.pdf', '2-52.C-04a.pdf', 65535, '2-52.c-04a__2015_06_25___15_51_21___2752', 'application/pdf', '2015-06-25 15:51:21', 0),
(508, '2-53.a-01a.pdf', '2-53.A-01a.pdf', 65535, '2-53.a-01a__2015_06_25___15_51_24___7245', 'application/pdf', '2015-06-25 15:51:24', 0),
(509, '2-54.b-01b.pdf', '2-54.B-01b.pdf', 65535, '2-54.b-01b__2015_06_25___15_51_28___7634', 'application/pdf', '2015-06-25 15:51:28', 0),
(510, '2-55.a-01a.pdf', '2-55.A-01a.pdf', 65535, '2-55.a-01a__2015_06_25___15_51_32___2815', 'application/pdf', '2015-06-25 15:51:32', 0),
(511, '2-57.c-04a.pdf', '2-57.C-04a.pdf', 65535, '2-57.c-04a__2015_06_25___15_51_42___6619', 'application/pdf', '2015-06-25 15:51:42', 0),
(512, '2-58.a-01a.pdf', '2-58.A-01a.pdf', 65535, '2-58.a-01a__2015_06_25___15_51_47___7374', 'application/pdf', '2015-06-25 15:51:47', 0),
(513, '2-59.b-01b.pdf', '2-59.B-01b.pdf', 65535, '2-59.b-01b__2015_06_25___15_51_55___7906', 'application/pdf', '2015-06-25 15:51:55', 0),
(514, '2-56.c-05.pdf', '2-56.C-05.pdf', 65535, '2-56.c-05__2015_06_25___15_52_11___4078', 'application/pdf', '2015-06-25 15:52:11', 0),
(515, '2-60.a-01a.pdf', '2-60.A-01a.pdf', 65535, '2-60.a-01a__2015_06_25___15_53_55___1383', 'application/pdf', '2015-06-25 15:53:55', 0),
(516, '2-61.c-05.pdf', '2-61.C-05.pdf', 65535, '2-61.c-05__2015_06_25___15_54_37___6645', 'application/pdf', '2015-06-25 15:54:37', 0),
(517, '2-62.c-04a.pdf', '2-62.C-04a.pdf', 65535, '2-62.c-04a__2015_06_25___15_54_40___6897', 'application/pdf', '2015-06-25 15:54:40', 0),
(518, '2-63.a-01a.pdf', '2-63.A-01a.pdf', 65535, '2-63.a-01a__2015_06_25___15_54_44___4430', 'application/pdf', '2015-06-25 15:54:44', 0),
(519, '2-64.b-01b.pdf', '2-64.B-01b.pdf', 65535, '2-64.b-01b__2015_06_25___15_54_47___6293', 'application/pdf', '2015-06-25 15:54:47', 0),
(520, '2-65.a-01a.pdf', '2-65.A-01a.pdf', 65535, '2-65.a-01a__2015_06_25___15_54_51___6071', 'application/pdf', '2015-06-25 15:54:51', 0),
(521, '2-66.c-05.pdf', '2-66.C-05.pdf', 65535, '2-66.c-05__2015_06_25___15_54_54___7578', 'application/pdf', '2015-06-25 15:54:54', 0),
(522, '2-67.d-04.pdf', '2-67.D-04.pdf', 65535, '2-67.d-04__2015_06_25___15_54_58___7939', 'application/pdf', '2015-06-25 15:54:58', 0),
(523, '2-68.b-16.pdf', '2-68.B-16.pdf', 65535, '2-68.b-16__2015_06_25___15_55_02___4684', 'application/pdf', '2015-06-25 15:55:02', 0),
(524, '2-69.c-12.pdf', '2-69.C-12.pdf', 65535, '2-69.c-12__2015_06_25___15_55_07___3606', 'application/pdf', '2015-06-25 15:55:07', 0),
(525, '2-70.c-06.pdf', '2-70.C-06.pdf', 65535, '2-70.c-06__2015_06_25___15_55_19___6405', 'application/pdf', '2015-06-25 15:55:19', 0),
(526, '2-71.a-01a.pdf', '2-71.A-01a.pdf', 65535, '2-71.a-01a__2015_06_25___15_57_50___5538', 'application/pdf', '2015-06-25 15:57:50', 0),
(527, '2-72.b-01a.pdf', '2-72.B-01a.pdf', 65535, '2-72.b-01a__2015_06_25___15_57_53___5068', 'application/pdf', '2015-06-25 15:57:53', 0),
(528, '2-73.b-02.pdf', '2-73.B-02.pdf', 65535, '2-73.b-02__2015_06_25___15_57_56___9259', 'application/pdf', '2015-06-25 15:57:56', 0),
(529, '2-74.b-03.pdf', '2-74.B-03.pdf', 65535, '2-74.b-03__2015_06_25___15_58_02___7888', 'application/pdf', '2015-06-25 15:58:02', 0),
(530, '2-75.c-06.pdf', '2-75.C-06.pdf', 65535, '2-75.c-06__2015_06_25___15_58_04___1428', 'application/pdf', '2015-06-25 15:58:04', 0),
(531, '2-76.a-01a.pdf', '2-76.A-01a.pdf', 65535, '2-76.a-01a__2015_06_25___15_58_07___4971', 'application/pdf', '2015-06-25 15:58:07', 0),
(532, '2-77.b-01a.pdf', '2-77.B-01a.pdf', 65535, '2-77.b-01a__2015_06_25___15_58_11___1789', 'application/pdf', '2015-06-25 15:58:11', 0),
(533, '2-78.c-02a.pdf', '2-78.C-02a.pdf', 65535, '2-78.c-02a__2015_06_25___15_58_15___6691', 'application/pdf', '2015-06-25 15:58:15', 0),
(534, '2-79.c-01a.pdf', '2-79.C-01a.pdf', 65535, '2-79.c-01a__2015_06_25___15_58_19___8130', 'application/pdf', '2015-06-25 15:58:19', 0),
(535, '2-80.c-06.pdf', '2-80.C-06.pdf', 65535, '2-80.c-06__2015_06_25___15_58_23___1204', 'application/pdf', '2015-06-25 15:58:23', 0),
(536, '2-81.a-01a.pdf', '2-81.A-01a.pdf', 65535, '2-81.a-01a__2015_06_25___15_59_22___6758', 'application/pdf', '2015-06-25 15:59:22', 0),
(537, '2-82.b-01a.pdf', '2-82.B-01a.pdf', 65535, '2-82.b-01a__2015_06_25___15_59_25___6826', 'application/pdf', '2015-06-25 15:59:25', 0),
(538, '2-83.c-02a.pdf', '2-83.C-02a.pdf', 65535, '2-83.c-02a__2015_06_25___15_59_28___8199', 'application/pdf', '2015-06-25 15:59:28', 0),
(539, '2-84.c-01a.pdf', '2-84.C-01a.pdf', 65535, '2-84.c-01a__2015_06_25___15_59_32___8978', 'application/pdf', '2015-06-25 15:59:32', 0),
(540, '2-85.c-06.pdf', '2-85.C-06.pdf', 65535, '2-85.c-06__2015_06_25___15_59_35___2167', 'application/pdf', '2015-06-25 15:59:35', 0),
(541, '2-86.a-01a.pdf', '2-86.A-01a.pdf', 65535, '2-86.a-01a__2015_06_25___15_59_39___9651', 'application/pdf', '2015-06-25 15:59:39', 0),
(542, '2-87.b-01a.pdf', '2-87.B-01a.pdf', 65535, '2-87.b-01a__2015_06_25___15_59_42___2627', 'application/pdf', '2015-06-25 15:59:42', 0),
(543, '2-88.c-02a.pdf', '2-88.C-02a.pdf', 65535, '2-88.c-02a__2015_06_25___15_59_46___2587', 'application/pdf', '2015-06-25 15:59:46', 0),
(544, '2-89.c-01a.pdf', '2-89.C-01a.pdf', 65535, '2-89.c-01a__2015_06_25___15_59_50___7376', 'application/pdf', '2015-06-25 15:59:50', 0),
(545, '2-90.d-06.pdf', '2-90.D-06.pdf', 65535, '2-90.d-06__2015_06_25___15_59_53___5566', 'application/pdf', '2015-06-25 15:59:53', 0),
(546, '2-91.b-16.pdf', '2-91.B-16.pdf', 65535, '2-91.b-16__2015_06_25___16_00_34___1656', 'application/pdf', '2015-06-25 16:00:34', 0),
(547, '2-92.c-11.pdf', '2-92.C-11.pdf', 65535, '2-92.c-11__2015_06_25___16_00_37___1250', 'application/pdf', '2015-06-25 16:00:37', 0),
(548, 'bud.2_ar-2.03_rzut_2_pietra.pdf', 'BUD.2_AR-2.03_RZUT 2 PIĘTRA.pdf', 65535, 'bud.2_ar-2.03_rzut_2_pietra__2015_06_30___12_28_04___3143', 'application/pdf', '2015-06-30 12:28:04', 0),
(549, 'bud.2_ar-2.01_rzut_parteru.pdf', 'BUD.2_AR-2.01_RZUT PARTERU.pdf', 65535, 'bud.2_ar-2.01_rzut_parteru__2015_06_30___14_48_24___5584', 'application/pdf', '2015-06-30 14:48:24', 0),
(550, 'bud.2_ar-2.03_rzut_2_pietra.pdf', 'BUD.2_AR-2.03_RZUT 2 PIĘTRA.pdf', 65535, 'bud.2_ar-2.03_rzut_2_pietra__2015_06_30___15_32_35___8758', 'application/pdf', '2015-06-30 15:32:35', 0),
(551, 'bud.2_ar-2.01_rzut_parteru.pdf', 'BUD.2_AR-2.01_RZUT PARTERU.pdf', 65535, 'bud.2_ar-2.01_rzut_parteru__2015_06_30___16_22_14___7903', 'application/pdf', '2015-06-30 16:22:14', 0),
(552, 'bud.2_ar-2.01_rzut_parteru.pdf', 'BUD.2_AR-2.01_RZUT PARTERU.pdf', 65535, 'bud.2_ar-2.01_rzut_parteru__2015_07_01___09_35_34___6609', 'application/pdf', '2015-07-01 09:35:34', 0),
(553, 'bud.2_ar-2.04_rzut_3_pietra.pdf', 'BUD.2_AR-2.04_RZUT 3 PIĘTRA.pdf', 65535, 'bud.2_ar-2.04_rzut_3_pietra__2015_07_01___13_48_46___4079', 'application/pdf', '2015-07-01 13:48:46', 0),
(554, '3-01.c-04b.pdf', '3-01.C-04b.pdf', 65535, '3-01.c-04b__2015_07_06___16_54_27___5389', 'application/pdf', '2015-07-06 16:54:27', 0),
(555, '3-02.a-01b.pdf', '3-02.A-01b.pdf', 65535, '3-02.a-01b__2015_07_06___16_54_31___4943', 'application/pdf', '2015-07-06 16:54:31', 0),
(556, '3-03.b-01c.pdf', '3-03.B-01c.pdf', 65535, '3-03.b-01c__2015_07_06___16_54_36___5131', 'application/pdf', '2015-07-06 16:54:36', 0),
(557, '3-04.c-02b.pdf', '3-04.C-02b.pdf', 65535, '3-04.c-02b__2015_07_06___16_54_38___2917', 'application/pdf', '2015-07-06 16:54:38', 0),
(558, '3-05.c-01b.pdf', '3-05.C-01b.pdf', 65535, '3-05.c-01b__2015_07_06___16_54_44___5965', 'application/pdf', '2015-07-06 16:54:44', 0),
(559, '3-06.c-04a.pdf', '3-06.C-04a.pdf', 65535, '3-06.c-04a__2015_07_06___16_54_46___5740', 'application/pdf', '2015-07-06 16:54:46', 0),
(560, '3-06.c-04a.pdf', '3-06.C-04a.pdf', 65535, '3-06.c-04a__2015_07_06___16_54_48___5939', 'application/pdf', '2015-07-06 16:54:48', 0),
(561, '3-07.a-01a.pdf', '3-07.A-01a.pdf', 65535, '3-07.a-01a__2015_07_06___16_54_52___4880', 'application/pdf', '2015-07-06 16:54:52', 0),
(562, '3-09.c-02a.pdf', '3-09.C-02a.pdf', 65535, '3-09.c-02a__2015_07_06___16_54_55___3103', 'application/pdf', '2015-07-06 16:54:55', 0),
(563, '3-10.c-01a.pdf', '3-10.C-01a.pdf', 65535, '3-10.c-01a__2015_07_06___16_55_02___9700', 'application/pdf', '2015-07-06 16:55:02', 0),
(564, '3-17.a-01a.pdf', '3-17.A-01a.pdf', 65535, '3-17.a-01a__2015_07_06___16_56_50___3657', 'application/pdf', '2015-07-06 16:56:50', 0),
(565, '3-18.b-01a.pdf', '3-18.B-01a.pdf', 65535, '3-18.b-01a__2015_07_06___16_56_56___1362', 'application/pdf', '2015-07-06 16:56:56', 0),
(566, '3-19.c-02a.pdf', '3-19.C-02a.pdf', 65535, '3-19.c-02a__2015_07_06___16_56_59___6709', 'application/pdf', '2015-07-06 16:56:59', 0),
(567, '3-20.c-01a.pdf', '3-20.C-01a.pdf', 65535, '3-20.c-01a__2015_07_06___16_57_02___6299', 'application/pdf', '2015-07-06 16:57:02', 0),
(568, '3-11.c-04a.pdf', '3-11.C-04a.pdf', 65535, '3-11.c-04a__2015_07_06___16_57_07___9957', 'application/pdf', '2015-07-06 16:57:07', 0),
(569, '3-12.a-01a.pdf', '3-12.A-01a.pdf', 65535, '3-12.a-01a__2015_07_06___16_57_09___9757', 'application/pdf', '2015-07-06 16:57:09', 0),
(570, '3-13.b-01a.pdf', '3-13.B-01a.pdf', 65535, '3-13.b-01a__2015_07_06___16_57_15___2521', 'application/pdf', '2015-07-06 16:57:15', 0),
(571, '3-14.c-02a.pdf', '3-14.C-02a.pdf', 65535, '3-14.c-02a__2015_07_06___16_57_18___5359', 'application/pdf', '2015-07-06 16:57:18', 0),
(572, '3-15.c-01a.pdf', '3-15.C-01a.pdf', 65535, '3-15.c-01a__2015_07_06___16_57_25___4468', 'application/pdf', '2015-07-06 16:57:25', 0),
(573, '3-16.c-04a.pdf', '3-16.C-04a.pdf', 65535, '3-16.c-04a__2015_07_06___16_57_31___8587', 'application/pdf', '2015-07-06 16:57:31', 0),
(574, '3-21.d-04.pdf', '3-21.D-04.pdf', 65535, '3-21.d-04__2015_07_06___16_59_04___6736', 'application/pdf', '2015-07-06 16:59:04', 0),
(575, '3-22.b-16.pdf', '3-22.B-16.pdf', 65535, '3-22.b-16__2015_07_06___16_59_08___7877', 'application/pdf', '2015-07-06 16:59:08', 0),
(576, '3-23.c-11.pdf', '3-23.C-11.pdf', 65535, '3-23.c-11__2015_07_06___16_59_10___1466', 'application/pdf', '2015-07-06 16:59:10', 0),
(577, '3-24.d-02b.pdf', '3-24.D-02b.pdf', 65535, '3-24.d-02b__2015_07_06___16_59_20___1619', 'application/pdf', '2015-07-06 16:59:20', 0),
(578, '3-25.a-01b.pdf', '3-25.A-01b.pdf', 65535, '3-25.a-01b__2015_07_06___16_59_22___9073', 'application/pdf', '2015-07-06 16:59:22', 0),
(579, '3-26.b-01d.pdf', '3-26.B-01d.pdf', 65535, '3-26.b-01d__2015_07_06___16_59_28___6879', 'application/pdf', '2015-07-06 16:59:28', 0),
(580, '3-27.a-01b.pdf', '3-27.A-01b.pdf', 65535, '3-27.a-01b__2015_07_06___16_59_30___3703', 'application/pdf', '2015-07-06 16:59:30', 0),
(581, '3-28.c-04b.pdf', '3-28.C-04b.pdf', 65535, '3-28.c-04b__2015_07_06___16_59_38___4054', 'application/pdf', '2015-07-06 16:59:38', 0),
(582, '3-29.d-02a.pdf', '3-29.D-02a.pdf', 65535, '3-29.d-02a__2015_07_06___16_59_41___6511', 'application/pdf', '2015-07-06 16:59:41', 0),
(583, '3-30.a-01a.pdf', '3-30.A-01a.pdf', 65535, '3-30.a-01a__2015_07_06___16_59_44___1985', 'application/pdf', '2015-07-06 16:59:44', 0),
(584, '3-31.b-01b.pdf', '3-31.B-01b.pdf', 65535, '3-31.b-01b__2015_07_06___17_01_06___3801', 'application/pdf', '2015-07-06 17:01:06', 0),
(585, '3-32.a-01a.pdf', '3-32.A-01a.pdf', 65535, '3-32.a-01a__2015_07_06___17_01_09___4257', 'application/pdf', '2015-07-06 17:01:09', 0),
(586, '3-33.c-04a.pdf', '3-33.C-04a.pdf', 65535, '3-33.c-04a__2015_07_06___17_01_14___4086', 'application/pdf', '2015-07-06 17:01:14', 0),
(587, '3-34.d-02a.pdf', '3-34.D-02a.pdf', 65535, '3-34.d-02a__2015_07_06___17_01_20___6307', 'application/pdf', '2015-07-06 17:01:20', 0),
(588, '3-35.a-01a.pdf', '3-35.A-01a.pdf', 65535, '3-35.a-01a__2015_07_06___17_01_23___5296', 'application/pdf', '2015-07-06 17:01:23', 0),
(589, '3-36.b-01b.pdf', '3-36.B-01b.pdf', 65535, '3-36.b-01b__2015_07_06___17_01_29___1685', 'application/pdf', '2015-07-06 17:01:29', 0),
(590, '3-37.a-01a.pdf', '3-37.A-01a.pdf', 65535, '3-37.a-01a__2015_07_06___17_01_35___6036', 'application/pdf', '2015-07-06 17:01:35', 0),
(591, '3-38.c-04a.pdf', '3-38.C-04a.pdf', 65535, '3-38.c-04a__2015_07_06___17_01_42___2954', 'application/pdf', '2015-07-06 17:01:42', 0),
(592, '3-39.d-02a.pdf', '3-39.D-02a.pdf', 65535, '3-39.d-02a__2015_07_06___17_01_47___2923', 'application/pdf', '2015-07-06 17:01:47', 0),
(593, '3-41.b-01b.pdf', '3-41.B-01b.pdf', 65535, '3-41.b-01b__2015_07_06___17_03_05___2709', 'application/pdf', '2015-07-06 17:03:05', 0),
(594, '3-42.a-01a.pdf', '3-42.A-01a.pdf', 65535, '3-42.a-01a__2015_07_06___17_03_13___2022', 'application/pdf', '2015-07-06 17:03:13', 0),
(595, '3-43.c-04a.pdf', '3-43.C-04a.pdf', 65535, '3-43.c-04a__2015_07_06___17_03_18___3939', 'application/pdf', '2015-07-06 17:03:18', 0),
(596, '3-44.d-07.pdf', '3-44.D-07.pdf', 65535, '3-44.d-07__2015_07_06___17_03_29___6865', 'application/pdf', '2015-07-06 17:03:29', 0),
(597, '3-45.b-16.pdf', '3-45.B-16.pdf', 65535, '3-45.b-16__2015_07_06___17_03_40___2145', 'application/pdf', '2015-07-06 17:03:40', 0),
(598, '3-46.d-04.pdf', '3-46.D-04.pdf', 65535, '3-46.d-04__2015_07_06___17_03_48___2603', 'application/pdf', '2015-07-06 17:03:48', 0),
(599, '3-47.c-08b.pdf', '3-47.C-08b.pdf', 65535, '3-47.c-08b__2015_07_06___17_03_53___9272', 'application/pdf', '2015-07-06 17:03:53', 0),
(600, '3-48.b-05b.pdf', '3-48.B-05b.pdf', 65535, '3-48.b-05b__2015_07_06___17_04_01___4690', 'application/pdf', '2015-07-06 17:04:01', 0),
(601, '3-49.c-07b.pdf', '3-49.C-07b.pdf', 65535, '3-49.c-07b__2015_07_06___17_04_11___9381', 'application/pdf', '2015-07-06 17:04:11', 0),
(602, '3-50.c-08a.pdf', '3-50.C-08a.pdf', 65535, '3-50.c-08a__2015_07_06___17_04_12___6923', 'application/pdf', '2015-07-06 17:04:12', 0),
(603, '3-51.b-05a.pdf', '3-51.B-05a.pdf', 65535, '3-51.b-05a__2015_07_06___17_05_12___5297', 'application/pdf', '2015-07-06 17:05:12', 0),
(604, '3-52.c-07a.pdf', '3-52.C-07a.pdf', 65535, '3-52.c-07a__2015_07_06___17_05_20___5565', 'application/pdf', '2015-07-06 17:05:20', 0),
(605, '3-53.c-08a.pdf', '3-53.C-08a.pdf', 65535, '3-53.c-08a__2015_07_06___17_05_23___6198', 'application/pdf', '2015-07-06 17:05:23', 0),
(606, '3-54.b-05a.pdf', '3-54.B-05a.pdf', 65535, '3-54.b-05a__2015_07_06___17_05_28___9897', 'application/pdf', '2015-07-06 17:05:28', 0),
(607, '3-55.c-07a.pdf', '3-55.C-07a.pdf', 65535, '3-55.c-07a__2015_07_06___17_05_34___7135', 'application/pdf', '2015-07-06 17:05:34', 0),
(608, '3-56.c-08a.pdf', '3-56.C-08a.pdf', 65535, '3-56.c-08a__2015_07_06___17_05_40___6921', 'application/pdf', '2015-07-06 17:05:40', 0),
(609, '3-57.b-05a.pdf', '3-57.B-05a.pdf', 65535, '3-57.b-05a__2015_07_06___17_05_45___1563', 'application/pdf', '2015-07-06 17:05:45', 0),
(610, '3-58.c-07a.pdf', '3-58.C-07a.pdf', 65535, '3-58.c-07a__2015_07_06___17_05_51___6573', 'application/pdf', '2015-07-06 17:05:51', 0),
(611, '3-59.b-17.pdf', '3-59.B-17.pdf', 65535, '3-59.b-17__2015_07_06___17_05_58___8345', 'application/pdf', '2015-07-06 17:05:58', 0),
(612, '3-60.c-13.pdf', '3-60.C-13.pdf', 65535, '3-60.c-13__2015_07_06___17_06_04___6003', 'application/pdf', '2015-07-06 17:06:04', 0),
(613, '3-61.c-01b.pdf', '3-61.C-01b.pdf', 65535, '3-61.c-01b__2015_07_06___17_07_10___6476', 'application/pdf', '2015-07-06 17:07:10', 0),
(614, '3-62.c-02b.pdf', '3-62.C-02b.pdf', 65535, '3-62.c-02b__2015_07_06___17_07_18___6220', 'application/pdf', '2015-07-06 17:07:18', 0),
(615, '3-63.b-01c.pdf', '3-63.B-01c.pdf', 65535, '3-63.b-01c__2015_07_06___17_07_23___5582', 'application/pdf', '2015-07-06 17:07:23', 0),
(616, '3-64.a-01b.pdf', '3-64.A-01b.pdf', 65535, '3-64.a-01b__2015_07_06___17_07_30___6363', 'application/pdf', '2015-07-06 17:07:30', 0),
(617, '3-65.d-02b.pdf', '3-65.D-02b.pdf', 65535, '3-65.d-02b__2015_07_06___17_07_35___7075', 'application/pdf', '2015-07-06 17:07:35', 0),
(618, '3-66.c-01a.pdf', '3-66.C-01a.pdf', 65535, '3-66.c-01a__2015_07_06___17_07_36___1806', 'application/pdf', '2015-07-06 17:07:36', 0),
(619, '3-67.c-02a.pdf', '3-67.C-02a.pdf', 65535, '3-67.c-02a__2015_07_06___17_07_42___2237', 'application/pdf', '2015-07-06 17:07:42', 0),
(620, '3-69.a-01a.pdf', '3-69.A-01a.pdf', 65535, '3-69.a-01a__2015_07_06___17_07_47___4430', 'application/pdf', '2015-07-06 17:07:47', 0),
(621, '3-68.b-01a.pdf', '3-68.B-01a.pdf', 65535, '3-68.b-01a__2015_07_06___17_07_48___2268', 'application/pdf', '2015-07-06 17:07:48', 0),
(622, '3-70.d-02a.pdf', '3-70.D-02a.pdf', 65535, '3-70.d-02a__2015_07_06___17_08_00___3754', 'application/pdf', '2015-07-06 17:08:00', 0),
(623, '3-71.c-01a.pdf', '3-71.C-01a.pdf', 65535, '3-71.c-01a__2015_07_06___17_09_03___3073', 'application/pdf', '2015-07-06 17:09:03', 0),
(624, '3-72.c-02a.pdf', '3-72.C-02a.pdf', 65535, '3-72.c-02a__2015_07_06___17_09_08___7080', 'application/pdf', '2015-07-06 17:09:08', 0),
(625, '3-73.b-01a.pdf', '3-73.B-01a.pdf', 65535, '3-73.b-01a__2015_07_06___17_09_14___4222', 'application/pdf', '2015-07-06 17:09:14', 0),
(626, '3-74.a-01a.pdf', '3-74.A-01a.pdf', 65535, '3-74.a-01a__2015_07_06___17_09_18___8765', 'application/pdf', '2015-07-06 17:09:18', 0),
(627, '3-75.d-02a.pdf', '3-75.D-02a.pdf', 65535, '3-75.d-02a__2015_07_06___17_09_27___8777', 'application/pdf', '2015-07-06 17:09:27', 0),
(628, '3-76.c-01a.pdf', '3-76.C-01a.pdf', 65535, '3-76.c-01a__2015_07_06___17_09_34___3609', 'application/pdf', '2015-07-06 17:09:34', 0),
(629, '3-77.c-02a.pdf', '3-77.C-02a.pdf', 65535, '3-77.c-02a__2015_07_06___17_09_40___8817', 'application/pdf', '2015-07-06 17:09:40', 0),
(630, '3-78.b-01a.pdf', '3-78.B-01a.pdf', 65535, '3-78.b-01a__2015_07_06___17_09_45___5966', 'application/pdf', '2015-07-06 17:09:45', 0),
(631, '3-79.a-01a.pdf', '3-79.A-01a.pdf', 65535, '3-79.a-01a__2015_07_06___17_09_48___8837', 'application/pdf', '2015-07-06 17:09:48', 0),
(632, '3-80.d-02a.pdf', '3-80.D-02a.pdf', 65535, '3-80.d-02a__2015_07_06___17_09_57___2114', 'application/pdf', '2015-07-06 17:09:57', 0),
(633, '3-81.c-11.pdf', '3-81.C-11.pdf', 65535, '3-81.c-11__2015_07_06___17_10_59___2936', 'application/pdf', '2015-07-06 17:10:59', 0),
(634, '3-82.b-16.pdf', '3-82.B-16.pdf', 65535, '3-82.b-16__2015_07_06___17_11_04___3556', 'application/pdf', '2015-07-06 17:11:04', 0),
(635, '3-83.d-08.pdf', '3-83.D-08.pdf', 65535, '3-83.d-08__2015_07_06___17_11_12___4493', 'application/pdf', '2015-07-06 17:11:12', 0),
(636, '3-08.b-01a.pdf', '3-08.B-01a.pdf', 65535, '3-08.b-01a__2015_07_07___09_28_59___9531', 'application/pdf', '2015-07-07 09:28:59', 0),
(637, '3-40.a-01a.pdf', '3-40.A-01a.pdf', 65535, '3-40.a-01a__2015_07_07___09_42_03___4258', 'application/pdf', '2015-07-07 09:42:03', 0),
(638, 'columbus_energy_osiaga_wysoki_zysk_w_3kw_2015.pdf', 'Columbus_Energy_osiaga_wysoki_zysk_w_3kw_2015.pdf', 65535, 'columbus_energy_osiaga_wysoki_zysk_w_3kw_2015__2015_10_30___10_13_34___5432', 'application/pdf', '2015-10-30 10:13:34', 0),
(639, 'stigroup_di.pdf', 'STIGROUP_DI.pdf', 65535, 'stigroup_di__2015_11_03___09_43_44___7785', 'application/pdf', '2015-11-03 09:43:44', 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ofi_gallery`
--

CREATE TABLE IF NOT EXISTS `ofi_gallery` (
`id` int(11) unsigned NOT NULL,
  `thumb_id` int(11) unsigned DEFAULT NULL COMMENT 'id of gallery_image',
  `header_image` int(11) unsigned DEFAULT NULL,
  `title` varchar(128) COLLATE utf8_polish_ci NOT NULL,
  `url` varchar(134) COLLATE utf8_polish_ci NOT NULL,
  `type` tinyint(1) NOT NULL COMMENT '1 - zdjecia, 2 - video',
  `date` date NOT NULL,
  `view_count` int(11) unsigned NOT NULL,
  `hidden` tinyint(1) unsigned NOT NULL,
  `category` text COLLATE utf8_polish_ci NOT NULL,
  `home` int(11) NOT NULL DEFAULT '0',
  `description` text COLLATE utf8_polish_ci
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `ofi_gallery`
--

INSERT INTO `ofi_gallery` (`id`, `thumb_id`, `header_image`, `title`, `url`, `type`, `date`, `view_count`, `hidden`, `category`, `home`, `description`) VALUES
(1, 123, NULL, 'tsdfasfas', 'tsdfasfas', 1, '2015-11-03', 0, 0, '', 1, ''),
(4, 123, NULL, 'tsdfasfas', 'tsdfasfas2', 1, '2015-11-03', 0, 0, '', 1, ''),
(6, 123, NULL, 'tsdfasfas', 'tsdfasfas2', 1, '2015-11-03', 0, 0, '', 1, ''),
(7, 123, NULL, 'tsdfasfas', 'tsdfasfas2', 1, '2015-11-03', 0, 0, '', 1, '');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ofi_gallery_image`
--

CREATE TABLE IF NOT EXISTS `ofi_gallery_image` (
`id` int(11) unsigned NOT NULL,
  `gallery_id` int(11) unsigned NOT NULL,
  `order` int(11) unsigned NOT NULL,
  `view_count` int(11) unsigned NOT NULL,
  `hidden` tinyint(1) unsigned NOT NULL,
  `movie_id` varchar(30) COLLATE utf8_polish_ci NOT NULL,
  `movie_type` tinyint(1) unsigned NOT NULL COMMENT '0 - no movie, 1 - youtube, 2 - vimeo',
  `description` text COLLATE utf8_polish_ci NOT NULL,
  `file` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'nazwa pliku na serwerze',
  `name` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'oryginalna nazwa pliku',
  `size` smallint(5) unsigned NOT NULL COMMENT 'rozmiar w bajtach',
  `width` smallint(5) unsigned NOT NULL COMMENT 'szerokość w px',
  `height` smallint(5) unsigned NOT NULL COMMENT 'wysokość w px',
  `uniqueDir` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'podkatalog miniatur',
  `mime` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'typ mime grafiki',
  `addedTime` datetime NOT NULL COMMENT 'czas utworzenia',
  `delete` tinyint(1) unsigned NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=127 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `ofi_gallery_image`
--

INSERT INTO `ofi_gallery_image` (`id`, `gallery_id`, `order`, `view_count`, `hidden`, `movie_id`, `movie_type`, `description`, `file`, `name`, `size`, `width`, `height`, `uniqueDir`, `mime`, `addedTime`, `delete`) VALUES
(120, 1, 0, 0, 0, '', 0, '', 'foyer.jpg', 'foyer.jpg', 65535, 961, 452, 'foyer__2015_11_17___14_56_12___7356', 'image/jpeg', '2015-11-17 14:56:12', 0),
(121, 2, 0, 0, 0, '', 0, '', 'foyer.jpg', 'foyer.jpg', 65535, 961, 452, 'foyer__2015_11_17___14_56_30___9749', 'image/jpeg', '2015-11-17 14:56:30', 0),
(122, 3, 0, 0, 0, '', 0, '', 'openspace.jpg', 'openSpace.jpg', 65535, 959, 900, 'openspace__2015_11_17___14_57_00___7470', 'image/jpeg', '2015-11-17 14:57:00', 0),
(123, 1, 1, 0, 0, '', 0, '', 'hexagon.jpg', 'hexagon.jpg', 65535, 961, 448, 'hexagon__2015_11_17___14_57_35___8263', 'image/jpeg', '2015-11-17 14:57:35', 0),
(124, 4, 0, 0, 0, '', 0, '', 'flightstous.jpg', 'flightsToUs.jpg', 65535, 1920, 900, 'flightstous__2015_11_20___14_05_36___3243', 'image/jpeg', '2015-11-20 14:05:36', 0),
(125, 7, 0, 0, 0, '', 0, '', 'hexagon.jpg', 'hexagon.jpg', 65535, 961, 448, 'hexagon__2015_11_20___14_40_34___9806', 'image/jpeg', '2015-11-20 14:40:34', 0),
(126, 6, 0, 0, 0, '', 0, '', 'flightstous.jpg', 'flightsToUs.jpg', 65535, 1920, 900, 'flightstous__2015_11_20___14_40_48___1770', 'image/jpeg', '2015-11-20 14:40:48', 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ofi_image`
--

CREATE TABLE IF NOT EXISTS `ofi_image` (
`id` int(11) unsigned NOT NULL,
  `file` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'nazwa pliku na serwerze',
  `name` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'oryginalna nazwa pliku',
  `size` smallint(5) unsigned NOT NULL COMMENT 'rozmiar w bajtach',
  `width` smallint(5) unsigned NOT NULL COMMENT 'szerokość w px',
  `height` smallint(5) unsigned NOT NULL COMMENT 'wysokość w px',
  `uniqueDir` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'podkatalog miniatur',
  `mime` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'typ mime grafiki',
  `addedTime` datetime NOT NULL COMMENT 'czas utworzenia',
  `delete` tinyint(1) unsigned NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=1291 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `ofi_image`
--

INSERT INTO `ofi_image` (`id`, `file`, `name`, `size`, `width`, `height`, `uniqueDir`, `mime`, `addedTime`, `delete`) VALUES
(1246, 'hexagon.jpg', 'hexagon.jpg', 65535, 961, 448, 'hexagon__2015_11_17___10_48_38___8836', 'image/jpeg', '2015-11-17 10:48:38', 0),
(1247, 'mainhexagon.jpg', 'mainHexagon.jpg', 65535, 1920, 900, 'mainhexagon__2015_11_17___10_48_56___5455', 'image/jpeg', '2015-11-17 10:48:56', 0),
(1248, 'flightstous.jpg', 'flightsToUs.jpg', 65535, 1920, 900, 'flightstous__2015_11_17___11_10_21___6660', 'image/jpeg', '2015-11-17 11:10:21', 0),
(1249, 'offerpremises.jpg', 'offerPremises.jpg', 65535, 1920, 900, 'offerpremises__2015_11_17___11_17_38___6937', 'image/jpeg', '2015-11-17 11:17:38', 0),
(1250, 'mainhexagon.jpg', 'mainHexagon.jpg', 65535, 1920, 900, 'mainhexagon__2015_11_17___14_55_14___6395', 'image/jpeg', '2015-11-17 14:55:14', 0),
(1251, 'offerpremises.jpg', 'offerPremises.jpg', 65535, 1920, 900, 'offerpremises__2015_11_17___14_55_38___5892', 'image/jpeg', '2015-11-17 14:55:38', 0),
(1252, 'hexagonoffice.jpg', 'hexagonOffice.jpg', 65535, 960, 900, 'hexagonoffice__2015_11_17___14_59_44___8432', 'image/jpeg', '2015-11-17 14:59:44', 0),
(1253, 'hexagonoffice.jpg', 'hexagonOffice.jpg', 65535, 960, 900, 'hexagonoffice__2015_11_17___15_00_54___6153', 'image/jpeg', '2015-11-17 15:00:54', 0),
(1254, 'foyer.jpg', 'foyer.jpg', 65535, 961, 452, 'foyer__2015_11_17___15_02_42___4088', 'image/jpeg', '2015-11-17 15:02:42', 0),
(1255, 'flightstous.jpg', 'flightsToUs.jpg', 65535, 1920, 900, 'flightstous__2015_11_17___15_05_51___6503', 'image/jpeg', '2015-11-17 15:05:51', 0),
(1256, 'flightstous.jpg', 'flightsToUs.jpg', 65535, 1920, 900, 'flightstous__2015_11_17___15_06_49___9366', 'image/jpeg', '2015-11-17 15:06:49', 0),
(1257, 'flightstous.jpg', 'flightsToUs.jpg', 65535, 1920, 900, 'flightstous__2015_11_17___15_10_16___2465', 'image/jpeg', '2015-11-17 15:10:16', 0),
(1258, 'flightstous.jpg', 'flightsToUs.jpg', 65535, 1920, 900, 'flightstous__2015_11_17___15_13_19___2723', 'image/jpeg', '2015-11-17 15:13:19', 0),
(1259, 'foyer.jpg', 'foyer.jpg', 65535, 961, 452, 'foyer__2015_11_17___15_14_15___8859', 'image/jpeg', '2015-11-17 15:14:15', 0),
(1260, 'foyer.jpg', 'foyer.jpg', 65535, 961, 452, 'foyer__2015_11_17___15_15_03___1755', 'image/jpeg', '2015-11-17 15:15:03', 0),
(1261, 'foyer.jpg', 'foyer.jpg', 65535, 961, 452, 'foyer__2015_11_17___15_16_24___7885', 'image/jpeg', '2015-11-17 15:16:24', 0),
(1262, 'foyer.jpg', 'foyer.jpg', 65535, 961, 452, 'foyer__2015_11_17___15_19_11___1595', 'image/jpeg', '2015-11-17 15:19:11', 0),
(1263, 'foyer.jpg', 'foyer.jpg', 65535, 961, 452, 'foyer__2015_11_17___15_20_35___1854', 'image/jpeg', '2015-11-17 15:20:35', 0),
(1264, 'geolocalization.png', 'geolocalization.png', 2465, 65, 70, 'geolocalization__2015_11_17___15_22_12___1297', 'image/png', '2015-11-17 15:22:12', 0),
(1265, 'foyer.jpg', 'foyer.jpg', 65535, 961, 452, 'foyer__2015_11_17___15_25_29___7816', 'image/jpeg', '2015-11-17 15:25:29', 0),
(1266, 'foyer.jpg', 'foyer.jpg', 65535, 961, 452, 'foyer__2015_11_17___15_26_02___4306', 'image/jpeg', '2015-11-17 15:26:02', 0),
(1267, 'foyer.jpg', 'foyer.jpg', 65535, 961, 452, 'foyer__2015_11_17___15_26_34___1756', 'image/jpeg', '2015-11-17 15:26:34', 0),
(1268, 'foyer.jpg', 'foyer.jpg', 65535, 961, 452, 'foyer__2015_11_17___15_26_56___1162', 'image/jpeg', '2015-11-17 15:26:56', 0),
(1269, 'foyer.jpg', 'foyer.jpg', 65535, 961, 452, 'foyer__2015_11_17___16_04_43___7526', 'image/jpeg', '2015-11-17 16:04:43', 0),
(1270, 'foyer.jpg', 'foyer.jpg', 65535, 961, 452, 'foyer__2015_11_17___16_06_00___7681', 'image/jpeg', '2015-11-17 16:06:00', 0),
(1271, 'foyer.jpg', 'foyer.jpg', 65535, 961, 452, 'foyer__2015_11_17___16_06_29___3068', 'image/jpeg', '2015-11-17 16:06:29', 0),
(1272, 'geolocalization.png', 'geolocalization.png', 2465, 65, 70, 'geolocalization__2015_11_17___16_06_48___3938', 'image/png', '2015-11-17 16:06:48', 0),
(1273, 'hexagonoffice.jpg', 'hexagonOffice.jpg', 65535, 960, 900, 'hexagonoffice__2015_11_17___16_10_44___1655', 'image/jpeg', '2015-11-17 16:10:44', 0),
(1274, 'convenienceandcomfort.jpg', 'convenienceAndComfort.jpg', 65535, 960, 900, 'convenienceandcomfort__2015_11_17___16_14_03___5542', 'image/jpeg', '2015-11-17 16:14:03', 0),
(1275, 'excellentlocation.jpg', 'excellentLocation.jpg', 65535, 960, 900, 'excellentlocation__2015_11_17___16_14_41___6701', 'image/jpeg', '2015-11-17 16:14:41', 0),
(1276, 'perfectoffer.jpg', 'perfectOffer.jpg', 65535, 960, 900, 'perfectoffer__2015_11_17___16_15_11___2833', 'image/jpeg', '2015-11-17 16:15:11', 0),
(1277, 'whoweare.jpg', 'whoWeAre.jpg', 65535, 960, 900, 'whoweare__2015_11_18___16_46_28___1736', 'image/jpeg', '2015-11-18 16:46:28', 0),
(1278, 'qualityassurance.jpg', 'qualityAssurance.jpg', 65535, 960, 900, 'qualityassurance__2015_11_18___16_48_30___1228', 'image/jpeg', '2015-11-18 16:48:30', 0),
(1279, 'flightstous.jpg', 'flightsToUs.jpg', 65535, 1920, 900, 'flightstous__2015_11_19___11_29_25___3852', 'image/jpeg', '2015-11-19 11:29:25', 0),
(1280, 'flightstous.jpg', 'flightsToUs.jpg', 65535, 1920, 900, 'flightstous__2015_11_19___11_34_37___4382', 'image/jpeg', '2015-11-19 11:34:37', 0),
(1281, 'foyer.jpg', 'foyer.jpg', 65535, 961, 452, 'foyer__2015_11_19___11_36_09___4075', 'image/jpeg', '2015-11-19 11:36:09', 0),
(1282, 'flightstous.jpg', 'flightsToUs.jpg', 65535, 1920, 900, 'flightstous__2015_11_19___11_57_44___3325', 'image/jpeg', '2015-11-19 11:57:44', 0),
(1284, 'officespace.jpg', 'officeSpace.jpg', 26941, 370, 340, 'officespace__2015_11_19___12_09_12___4257', 'image/jpeg', '2015-11-19 12:09:12', 0),
(1285, 'commercialpremises.jpg', 'commercialPremises.jpg', 33046, 370, 340, 'commercialpremises__2015_11_19___12_09_36___8042', 'image/jpeg', '2015-11-19 12:09:36', 0),
(1286, 'parkingplace.jpg', 'parkingPlace.jpg', 31104, 370, 340, 'parkingplace__2015_11_19___12_10_17___5800', 'image/jpeg', '2015-11-19 12:10:17', 0),
(1287, 'projection.png', 'projection.png', 65535, 1170, 500, 'projection__2015_11_20___09_40_19___1152', 'image/png', '2015-11-20 09:40:19', 0),
(1288, 'projection.png', 'projection.png', 65535, 1170, 500, 'projection__2015_11_20___13_18_35___6692', 'image/png', '2015-11-20 13:18:35', 0),
(1289, 'slider.jpg', 'slider.jpg', 65535, 1920, 900, 'slider__2016_02_25___11_01_53___7002', 'image/jpeg', '2016-02-25 11:01:53', 0),
(1290, 'slider.jpg', 'slider.jpg', 65535, 1920, 900, 'slider__2016_02_25___11_03_26___5469', 'image/jpeg', '2016-02-25 11:03:26', 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ofi_locals_category`
--

CREATE TABLE IF NOT EXISTS `ofi_locals_category` (
`id` int(11) unsigned NOT NULL,
  `name` varchar(128) COLLATE utf8_polish_ci NOT NULL,
  `url` varchar(134) COLLATE utf8_polish_ci NOT NULL,
  `hidden` tinyint(1) unsigned NOT NULL,
  `header_image` int(11) NOT NULL,
  `category_gallery` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `ofi_locals_category`
--

INSERT INTO `ofi_locals_category` (`id`, `name`, `url`, `hidden`, `header_image`, `category_gallery`) VALUES
(1, 'Powierzchnie biurowe', 'powierzchnie-biurowe', 0, 1284, 0),
(2, 'Lokale usługowe', 'lokale-uslugowe', 0, 1285, 0),
(3, 'Miejsce parkingowe', 'miejsce-parkingowe', 0, 1286, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ofi_news`
--

CREATE TABLE IF NOT EXISTS `ofi_news` (
`id` int(11) unsigned NOT NULL,
  `category_id` int(11) unsigned NOT NULL,
  `datetime` datetime NOT NULL,
  `url` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `title` varchar(250) COLLATE utf8_polish_ci NOT NULL,
  `content` longtext COLLATE utf8_polish_ci NOT NULL,
  `short_content` text COLLATE utf8_polish_ci NOT NULL,
  `view_count` int(11) unsigned NOT NULL,
  `hidden` tinyint(1) unsigned NOT NULL,
  `header_image` int(11) unsigned DEFAULT NULL,
  `thumbnail` int(11) unsigned DEFAULT NULL,
  `only_logged` tinyint(1) unsigned NOT NULL,
  `description` text COLLATE utf8_polish_ci,
  `keywords` text COLLATE utf8_polish_ci
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `ofi_news`
--

INSERT INTO `ofi_news` (`id`, `category_id`, `datetime`, `url`, `title`, `content`, `short_content`, `view_count`, `hidden`, `header_image`, `thumbnail`, `only_logged`, `description`, `keywords`) VALUES
(8, 0, '2011-11-27 14:15:00', 'wyznaczenie-pierwszego-dnia-notowania-akcji-spolki-sti-group-sa', 'Wyznaczenie pierwszego dnia notowania akcji spółki STI GROUP S.A.', '<p>Zarząd STI GROUP S.A. z siedzibą w Częstochowie informuje, że Uchwałą nr 533/2011 Zarządu Giełdy Papier&oacute;w Wartościowych w Warszawie S.A. z dnia 26 kwietnia 2011 r. określono dzień 4 maja 2011 r. jako dzień pierwszego notowania w Alternatywnym Systemie Obrotu na rynku NewConnect akcji zwykłych na okaziciela serii A Sp&oacute;łki STI GROUP S.A. w ilości 10.000.000 akcji (słownie: dziesięć milion&oacute;w akcji) o wartości nominalnej 1,00 PLN każda, oraz akcji zwykłych na okaziciela serii B Sp&oacute;łki STI GROUP S.A. w ilości 5.002.400 akcji (słownie: pięć milion&oacute;w dwa tysiące czterysta akcji) o wartości nominalnej 1,00 PLN każda, oznaczonych przez Krajowy Depozyt Papier&oacute;w Wartościowych S.A. kodem &quot;PLSTIGR00012&quot;. Akcje będą notowane na rynku kierowanym zleceniami w systemie notowań ciągłych pod nazwą skr&oacute;coną &quot;STIGROUP&quot; i oznaczeniem &quot;STI&quot;.</p>\r\n\r\n<p>Podstawa prawna: &sect; 3 ust. 1 Załącznika nr 3 do Regulaminu Alternatywnego Systemu Obrotu ,,Informacje bieżące i okresowe przekazywane w alternatywnym systemie obrotu na rynku NewConnect&quot;.</p>\r\n', '<p>Zarząd STI GROUP S.A. z siedzibą w Częstochowie informuje, że Uchwałą nr 533/2011 Zarządu Giełdy Papier&oacute;w Wartościowych w Warszawie S.A. z dnia 26 kwietnia 2011 r. określono dzień 4 maja 2011 r. jako dzień pierwszego notowania</p>\r\n', 0, 0, NULL, NULL, 0, '', ''),
(9, 0, '2011-08-02 14:15:00', 'zawarcie-istotnej-umowy', 'Zawarcie istotnej umowy', '<p>Zarząd STI GROUP S.A. z siedzibą w Częstochowie, informuje, że realizując założenia biznesowe zmierzające do rozwoju Sp&oacute;łki, w dniu 1 sierpnia 2011 roku podpisał Umowę o świadczenie usług z firmą PMR Ltd. sp. z o.o. z siedzibą w Krakowie, kt&oacute;rej przedmiotem jest pozyskanie Inwestor&oacute;w wspierających dalsze dofinansowanie Emitenta i realizowanych przez niego projekt&oacute;w.</p>\r\n\r\n<p>Postawa Prawna:</p>\r\n\r\n<p>&sect; 3 ust. 1. Załącznika Nr 3 do Regulaminu Alternatywnego Systemu Obrotu &quot;Informacje bieżące i okresowe przekazywane w alternatywnym systemie obrotu na rynku NewConnect&quot;.</p>\r\n', '<p>Zarząd STI GROUP S.A. z siedzibą w Częstochowie, informuje, że realizując założenia biznesowe zmierzające do rozwoju Sp&oacute;łki, w dniu 1 sierpnia 2011 roku podpisał Umowę o świadczenie usług z firmą PMR Ltd. sp. z o.o</p>\r\n', 0, 0, NULL, NULL, 0, '', ''),
(10, 0, '2011-08-02 14:15:00', 'zawarcie-istotnej-umowy-1', 'Zawarcie istotnej umowy', '<p>Zarząd STI GROUP S.A. z siedzibą w Częstochowie informuje, że w dniu 2 sierpnia 2011 roku podpisał umowę z Gminą Miastem Częstochowa, przedmiotem kt&oacute;rej jest patronat medialny gazety internetowej wCzestochowie.pl, kt&oacute;rej właścicielem jest Emitent nad wydarzeniem &quot;19 Dni Częstochowy&quot;.</p>\r\n\r\n<p>STI GROUP S.A. przekazuje niniejszą informację z uwagi na fakt, iż jest to realizacja założeń, o kt&oacute;rych Sp&oacute;łka informowała w dokumencie informacyjnym w kwestii pozyskiwania dużych kontrakt&oacute;w z wiodącymi instytucjami promocyjno - eventowymi w regionie, dzięki czemu internetowa gazeta wCzestochowie.pl zyskuje nowych czytelnik&oacute;w, reklamodawc&oacute;w oraz partner&oacute;w biznesowych. Działania te mają bezpośredni wpływ na wzmocnienie potencjału Emitenta oraz co jest z tym związane zwiększenie przychod&oacute;w z racji świadczonych usług.</p>\r\n\r\n<p>Podstawa prawna: &sect; 3 ust. 1 Załącznika Nr 3 do Regulaminu ASO &quot;Informacje Bieżące i Okresowe w Alternatywnym Systemie Obrotu&quot;</p>\r\n', '<p>Zarząd STI GROUP S.A. z siedzibą w Częstochowie informuje, że w dniu 2 sierpnia 2011 roku podpisał umowę z Gminą Miastem Częstochowa, przedmiotem kt&oacute;rej jest patronat medialny gazety internetowej wCzestochowie.pl</p>\r\n', 0, 0, NULL, NULL, 0, '', ''),
(11, 0, '2011-08-05 14:15:00', 'zawarcie-istotnej-umowy-2', 'Zawarcie istotnej umowy', '<p>Zarząd STI GROUP S.A. z siedzibą w Częstochowie informuje, że w dniu 5 sierpnia 2011 roku podpisał umowę z GMITRUK PROMOTION - Andrzej Robert Gmitruk z siedzibą w Warszawie, przedmiotem kt&oacute;rej jest patronat medialny gazety internetowej wCzestochowie.pl, kt&oacute;rej właścicielem jest Emitent, nad galą boksu zawodowego Częstochowa BOXING CUP</p>\r\n\r\n<p>STI GROUP S.A. przekazuje niniejszą informację z uwagi na fakt, iż jest to realizacja założeń, o kt&oacute;rych Sp&oacute;łka informowała w dokumencie informacyjnym w kwestii pozyskiwania dużych kontrakt&oacute;w z wiodącymi instytucjami promocyjno - eventowymi w regionie, dzięki czemu internetowa gazeta wCzestochowie.pl zyskuje nowych czytelnik&oacute;w, reklamodawc&oacute;w oraz partner&oacute;w biznesowych. Działania te mają bezpośredni wpływ na wzmocnienie potencjału Emitenta oraz co jest z tym związane zwiększenie przychod&oacute;w z racji świadczonych usług.&nbsp;</p>\r\n\r\n<p>Podstawa prawna: &sect; 3 ust. 1 Załącznika Nr 3 do Regulaminu ASO &quot;Informacje Bieżące i Okresowe w Alternatywnym Systemie Obrotu&quot;&nbsp;</p>\r\n', '<p>Zarząd STI GROUP S.A. z siedzibą w Częstochowie informuje, że w dniu 5 sierpnia 2011 roku podpisał umowę z GMITRUK PROMOTION - Andrzej Robert Gmitruk</p>\r\n', 0, 0, NULL, NULL, 0, '', ''),
(12, 0, '2011-11-16 14:15:00', 'raport-z-zakoczenia-subskrypcji-akcji-serii-c', 'Raport z zakończenia subskrypcji akcji serii C', '<p>Raport z zakończenia subskrypcji akcji serii C.</p>\r\n\r\n<p>Zarząd STI GROUP S.A. w związku z zakończeniem subskrypcji akcji serii C Emitenta, emitowanych w trybie subskrypcji prywatnej z pozbawieniem prawa poboru dotychczasowych akcjonariuszy na podstawie &sect; 7 Statutu Sp&oacute;łki, podaje do publicznej wiadomości następujące informacje:</p>\r\n\r\n<p>1. Data rozpoczęcia i zakończenia subskrypcji:</p>\r\n\r\n<p>Subskrypcja akcji miała charakter subskrypcji prywatnej i następowała poprzez złożenie oferty przez Emitenta i jej przyjęcia przez oznaczonego adresata (art. 431 &sect;2 pkt 1 Kodeksu sp&oacute;łek handlowych).<br />\r\nOtwarcie subskrypcji nastąpiło w dniu 14 listopada 2011 r. Zakończenie subskrypcji nastąpiło w dniu 15 listopada 2011r.</p>\r\n\r\n<p>2. Data przydziału akcji:</p>\r\n\r\n<p>15 listopada 2011 rok.&nbsp;</p>\r\n\r\n<p>3. Liczba instrument&oacute;w finansowych objętych subskrypcją:</p>\r\n\r\n<p>Subskrypcja prywatna obejmowała 1 000 000 (jeden milion) sztuk akcji zwykłych na okaziciela serii C o wartości nominalnej 1,00 zł (jeden złoty) każda akcja.</p>\r\n\r\n<p>4. Stopa redukcji zapis&oacute;w na akcje w poszczeg&oacute;lnych transzach:</p>\r\n\r\n<p>Subskrypcja została przeprowadzona bez żadnych redukcji.</p>\r\n\r\n<p>5. Liczba instrument&oacute;w finansowych, kt&oacute;re zostały przydzielone w ramach przeprowadzonej subskrypcji:</p>\r\n\r\n<p>W ramach przeprowadzonej subskrypcji prywatnej zostało objętych 1 000 000 (jeden milion) sztuk akcji zwykłych na okaziciela serii C.</p>\r\n\r\n<p>6. Cena, po jakiej instrumenty finansowe były nabywane (obejmowane):</p>\r\n\r\n<p>Akcje serii C zostały objęte po cenie emisyjnej r&oacute;wnej 1,00 zł (jeden złotych) za jedną akcję.</p>\r\n\r\n<p>7. Liczba os&oacute;b, kt&oacute;re złożyły zapisy na instrumenty finansowe objęte subskrypcją&nbsp;<br />\r\nw poszczeg&oacute;lnych transzach:</p>\r\n\r\n<p>Akcje serii C zostały zaoferowane przez Zarząd w trybie subskrypcji prywatnej jednemu inwestorowi, z kt&oacute;rym została zawarta umowa objęcia akcji. Akcje obejmowane były w zamian za wkłady pieniężne.</p>\r\n\r\n<p>8. Liczba os&oacute;b, kt&oacute;rym przydzielono instrumenty finansowe w ramach przeprowadzonej subskrypcji:</p>\r\n\r\n<p>Umowy objęcia akcji serii C została zawarta z jednym inwestorem (osoba fizyczna).</p>\r\n\r\n<p>9. Nazwy (firmy) subemitent&oacute;w, kt&oacute;rzy objęli instrumenty finansowe w ramach wykonywania um&oacute;w o subemisję, z określeniem liczby instrument&oacute;w finansowych, kt&oacute;re objęli, wraz z faktyczną ceną jednostki instrumentu finansowego (cena emisyjna lub sprzedaży, po odliczeniu wynagrodzenia za objęcie jednostki instrumentu finansowego, w wykonaniu umowy subemisji, nabytej przez subemitenta.</p>\r\n\r\n<p>Akcje serii C nie były obejmowane przez subemitent&oacute;w. Nie została zawarta żadna umowa o subemisję.</p>\r\n\r\n<p>10. Łączne określenie wysokości koszt&oacute;w, kt&oacute;re zostały zaliczone do koszt&oacute;w emisji, ze wskazaniem wysokości koszt&oacute;w według ich tytuł&oacute;w</p>\r\n\r\n<p>Łączne koszty emisji akcji serii C wyniosły 24 000 zł, przy czym w podziale na koszty:<br />\r\na) przygotowania i przeprowadzenia oferty - 10 000 zł,<br />\r\nb) sporządzenia dokumentu informacyjnego, z uwzględnieniem koszt&oacute;w doradztwa - 10 000 zł,<br />\r\nc) opłaty notarialne - 4 000,00 zł<br />\r\nd) wynagrodzenia subemitent&oacute;w - 0 zł (zgodnie z pkt. 9 raportu nie dotyczy),<br />\r\ne) promocji oferty - 0 zł.<br />\r\nKoszty emisji zostaną ujęte w pozycji pozostałe koszty operacyjne, jako koszt niestanowiący kosztu uzyskania przychodu.</p>\r\n\r\n<p>Podstawa prawna : &sect;4 ust.1 Załącznika nr 3 do Regulaminu ASO - &quot;Informacje Bieżące i Okresowe przekazywane w Alternatywnym Systemie Obrotu na rynku NewConnect&quot;.</p>\r\n', '<p>Zarząd STI GROUP S.A. w związku z zakończeniem subskrypcji akcji serii C Emitenta, emitowanych w trybie subskrypcji prywatnej z pozbawieniem prawa poboru dotychczasowych akcjonariuszy na podstawie &sect; 7 Statutu Sp&oacute;łki, podaje informacje do publicznej wiadomości.</p>\r\n', 0, 0, NULL, NULL, 0, '', ''),
(13, 0, '2013-05-21 14:15:00', 'przyjecie-rezygnacji-z-funkcji-prezesa-zarzadu', 'Przyjęcie rezygnacji z funkcji Prezesa Zarządu', '<article class="left-span1 span11 column">\r\n<p>Zarząd Sp&oacute;łki STI GROUP S.A. z siedzibą w Częstochowie (&quot;Sp&oacute;łka&quot;) informuje, iż w dniu 20 maja 2013 roku, Rada Nadzorcza Sp&oacute;łki podjęła Uchwałę Nr 2/RN/2013, mocą kt&oacute;rej przyjęto rezygnację Prezesa Zarządu Piotra Rurarza ze stanowiska Prezesa Zarządu Sp&oacute;łki.&nbsp;<br />\r\n<br />\r\nPonadto na tym samym posiedzeniu Rady Nadzorczej Sp&oacute;łki Rada Nadzorca Sp&oacute;łki podjęła uchwałę Nr 3/RN 2013 w sprawie wyboru nowego Zarządu w osobie Piotra Brewczyńskiego, kt&oacute;ry objął funkcję Prezesa Zarządu Sp&oacute;łki.<br />\r\n<br />\r\nPodstawa prawna:<br />\r\n&sect; 3 ust. 2 punkt 11 Załącznika Nr 3 do Regulaminu Alternatywnego Systemu Obrotu - Informacje bieżące i okresowe przekazywane w alternatywnym systemie obrotu na rynku NewConnect.<br />\r\n<br />\r\nOsoby reprezentujące Sp&oacute;łkę: Piotr Brewczyński - Prezes Zarządu Sp&oacute;łki</p>\r\n</article>\r\n', '<p>Przyjęcie rezygnacji z funkcji Prezesa Zarządu Piotra Rurarza oraz powołanie Prezesa Zarządu w osobie Piotra Brewczyńskiego</p>\r\n', 0, 0, NULL, NULL, 0, '', ''),
(14, 0, '2013-07-05 14:15:00', 'zawarcie-istotnej-umowy-z-wierzycielem-spolki', 'Zawarcie istotnej umowy z wierzycielem spółki', '<p>Zarząd Sp&oacute;łki STI GROUP S.A. (&quot;Sp&oacute;łka, &quot;Emitent&quot;) informuje, iż dnia 5 lipca 2013 roku, Prezes Zarządu Sp&oacute;łki Emitenta działając w imieniu i na rzecz Emitenta, zgodnie z wyrażoną zgodą i na podstawie Uchwały Zwyczajnego Walnego Zgromadzenia STI Group S.A. o numerze 14 z dnia 30 czerwca, na kt&oacute;rym to Walne Zgromadzenie Akcjonariuszy wyraziło zgodę na zawarcie powyższej umowy, jak r&oacute;wnież r&oacute;wnież na podstawie &sect; 25 pkt 12 statutu Sp&oacute;łki STI Group S.A. zawarł Umowę Zastawniczą z Wierzycielem Sp&oacute;łki.</p>\r\n\r\n<p>Przedmiotem powyższej umowy zastawniczej jest ustanowienie zastawu na zorganizowanej części przedsiębiorstwa prowadzonego przez STI GROUP S.A. z siedzibą w Częstochowie.</p>\r\n\r\n<p>Celem zawarcia powyższej umowy jest ustanowienie zabezpieczenia na rzecz Wierzyciela Emitenta, Pana Piotra Rurarza, na zorganizowanej części przedsiębiorstwa prowadzonego przez STI GROUP S.A. z siedzibą w Częstochowie. Zastaw rejestrowy będzie zabezpieczał wierzytelność Zastawnika (Wierzyciela) w stosunku do Zastawcy (Emitenta) z tytułu udzielonych Emitentowi pożyczek wraz z umownymi odsetkami.</p>\r\n\r\n<p>Spłata wierzytelności z tytułu umowy pożyczki ma nastąpić w terminie do dnia 31 sierpnia 2013 roku. W przypadku braku spłaty Wierzycielowi przez Emitenta długu w wyznaczonym terminie, własność przedmiotu zastawu przejdzie na Zastawnika (art. 22. Ust. 1 pkt 3 i art. 22 ust. 2 pkt ustawy o zastawie rejestrowym i rejestrze zastaw&oacute;w).</p>\r\n\r\n<p>Ponadto Emitent i Wierzyciel uzgodnili, iż najwyższa suma zabezpieczenia wierzytelności, zgodnie z art. 3 ust. 2 pkt 4 ustawy o zastawie rejestrowym i rejestrze zastaw&oacute;w wynosi 1.500.000,00 zł (p&oacute;łtorej miliona złotych).</p>\r\n\r\n<p>Koszty zawarcia powyższej umowy oraz koszty wpisu zastawu rejestrowego do rejestru zastaw&oacute;w zobowiązany jest ponieść Emitent (Zastawca). Pozostałe postanowienia umowy zastawniczej nie odbiegają od warunk&oacute;w powszechnie stosowanych dla danego typu um&oacute;w.&nbsp;</p>\r\n\r\n<p>Podstawa prawna:<br />\r\n&sect; 3 ust. 2 pkt 2 załącznika nr 3 do Regulaminu Alternatywnego Systemu<br />\r\nObrotu - &quot;Informacje Bieżące i Okresowe w Alternatywnym Systemie<br />\r\nObrotu na rynku NewConnect&quot;.</p>\r\n', '<p>Przedmiotem umowy zastawniczej jest ustanowienie zastawu na zorganizowanej części przedsiębiorstwa prowadzonego przez STI GROUP S.A. z siedzibą w Częstochowie.</p>\r\n', 0, 0, NULL, NULL, 0, '', ''),
(15, 0, '2013-11-14 14:15:00', 'informacja-o-uzyskaniu-dofinansowania-do-projektu', 'Informacja o uzyskaniu dofinansowania do projektu', '<p>Zarząd Sp&oacute;łki STI GROUP S.A. (&quot;Sp&oacute;łka, &quot;Emitent&quot;) informuje, iż w dniu 13 listopada 2013 roku uzyskał informację o przyznanym Sp&oacute;łce dofinansowaniu projektu. Emitent uzyskał dofinansowanie do projektu &quot;WND-RPSL.01.02.04-00-C29/11-02&quot;, gdzie wartość dofinansowania wynosi 669.000,00 zł, co stanowi 60% dofinansowania projektu. Zarząd nie zawarł jeszcze umowy dotacyjnej i nie ma pewności, że do zawarcia takiej umowy dojdzie w roku bieżącym min. z racji na obecne możliwości uruchomienia środk&oacute;w własnych Emitenta. Zarząd STI Group SA poczyni kroki, aby skorzystać z przyznanej dotacji.&nbsp;</p>\r\n\r\n<p>Emitent złożył wniosek o przyznanie dofinansowania w dniu 22 listopada 2011 roku.</p>\r\n\r\n<p>Podstawa prawna:<br />\r\n&sect; 3 ust. 1 załącznika nr 3 do Regulaminu Alternatywnego Systemu Obrotu.</p>\r\n', '<p>Emitent uzyskał dofinansowanie do projektu &quot;WND-RPSL.01.02.04-00-C29/11-02&quot;, gdzie wartość dofinansowania wynosi 669.000,00 zł, co stanowi 60% dofinansowania projektu.<br />\r\n&nbsp;</p>\r\n', 0, 0, NULL, NULL, 0, '', ''),
(16, 0, '2014-01-27 14:30:00', 'umorzenie-przez-wierzyciela-w-calosci-dlugu-sti-group-sa', 'Umorzenie przez wierzyciela w całości długu STI GROUP S.A.', '<p>Zarząd STI GROUP S.A. z siedzibą w Częstochowie (&quot;Sp&oacute;łka&quot;, &quot;Emitent&quot;) informuje, że w dniu 27 stycznia 2014 roku, Pan Piotr Rurarz, będący Zastawnikiem w Sp&oacute;łce STI GROUP Sp&oacute;łka Akcyjna, kt&oacute;ry udzielił pożyczek Sp&oacute;łce w łącznej kwocie 1.232.500,00 zł (słownie: jeden milion dwieście trzydzieści dwa tysiące pięćset) złotych wraz z umownymi odsetkami, kt&oacute;re na dzień 27 stycznia 2014 roku wyniosły 250.298,98 zł (dwieście pięćdziesiąt tysięcy dwieście dziewięćdziesiąt osiem złotych 98/100) z terminem zwrotu wierzytelności do 31 sierpnia 2013 r. oraz zważywszy, że wierzytelności Pana Piotra Rurarza wobec Emitenta wynikające z Um&oacute;w Pożyczek zostały zabezpieczone zastawem rejestrowym ustanowionym na podstawie umowy o ustanowienie zastawu rejestrowego z dnia 5 lipca 2013 r., oraz wierzytelności te, nie zostały zaspokojone choćby w części, a sytuacja finansowa Sp&oacute;łki nie uległa poprawie i uniemożliwia jej dokonanie spłat wierzytelności, Pan Piotr Rurarz zrzekł się przedmiotowego zabezpieczenia wyżej wskazanych wierzytelności. W związku z czym wygasł Zastaw Rejestrowy.</p>\r\n\r\n<p>Zarząd STI GROUP S.A. informuje jednocześnie, iż na mocy podpisanej pomiędzy stronami w dniu 27 stycznia 2014 roku Umowy Pan Piotr Rurarz wyraził zgodę na wykreślenie Zastawu Rejestrowego z rejestru zastaw&oacute;w i umorzył Sp&oacute;łce STI GROUP Sp&oacute;łka Akcyjna, wpisanej do rejestru przedsiębiorc&oacute;w Krajowego Rejestru Sądowego pod numerem 0000373608, należne mu zobowiązania wynikające z udzielonych przeze niego pożyczek. Mając na uwadze zawartą Umowę STI GROUP S.A. w wyniku Oświadczenia Pana Piotra Rurarza w całości zostało zwolnione z długu. Wartość umorzonej należności gł&oacute;wnej to 1.232.500,00 zł (słownie: jeden milion dwieście trzydzieści dwa tysiące pięćset) złotych Wartość umorzonych odsetek to 250.298,98 zł (dwieście pięćdziesiąt tysięcy dwieście dziewięćdziesiąt osiem złotych 98/100). Zarząd Emitenta w osobie Prezesa Zarządu Sp&oacute;łki STI GROUP S.A. zgodnie z art. 508 kodeksu cywilnego przyjął zwolnienie z długu.</p>\r\n\r\n<p>podstawa prawna:</p>\r\n\r\n<p>&sect; 3 pkt.1 Załącznika nr 3 do Regulaminu Alternatywnego Systemu Obrotu &quot;Informacje bieżące i okresowe przekazywane w alternatywnym systemie obrotu na rynku NewConnect&quot;</p>\r\n', '<p>Zarząd STI GROUP S.A. informuje, iż na mocy podpisanej pomiędzy stronami w dniu 27 stycznia 2014 roku Umowy Pan Piotr Rurarz wyraził zgodę na wykreślenie Zastawu Rejestrowego z rejestru zastaw&oacute;w i umorzył Sp&oacute;łce STI GROUP Sp&oacute;łka Akcyjna.</p>\r\n', 0, 0, NULL, NULL, 0, '', ''),
(17, 0, '2014-01-30 14:30:00', 'powolanie-prokurentow-spolki', 'Powołanie Prokurentów Spółki', '<p>Zarząd Sp&oacute;łki STI GROUP S.A. z siedzibą w Częstochowie (&quot;Emitent&quot;, &quot;Sp&oacute;łka&quot;) informuje, iż Zarząd uchwałą nr 1 z dnia 29 stycznia 2014 r. powołuję jako prokurent&oacute;w samoistnych następujące osoby:&nbsp;<br />\r\n- Pana Piotra Brewczyńskiego<br />\r\n- Pana Januarego Ciszewskiego<br />\r\nRada Nadzorcza Sp&oacute;łki wyraziła zgodnie z &sect; 19 ust. 3 pkt 10 Statutu Sp&oacute;łki zgodę na powołanie wskazanych os&oacute;b jako Prokurent&oacute;w Sp&oacute;łki uchwałą nr 4 z dnia 29 stycznia 2014 r. Rady Nadzorczej Sp&oacute;łki.&nbsp;</p>\r\n\r\n<p><br />\r\nPodstawa prawna: &sect;3 ust. 1 Załącznika Nr 3 do Regulaminu Alternatywnego Systemu Obrotu &quot;Informacje bieżące i okresowe przekazywane w alternatywnym systemie obrotu na rynku NewConnect&quot;.</p>\r\n', '<p>Zarząd Sp&oacute;łki STI GROUP S.A. informuje, iż Zarząd uchwałą nr 1 z dnia 29 stycznia 2014 r. powołuję jako prokurent&oacute;w samoistnych następujące osoby: Pana Piotra Brewczyńskiego oraz Pana Januarego Ciszewskiego.</p>\r\n', 0, 0, NULL, NULL, 0, '', ''),
(18, 0, '2014-01-30 14:30:00', 'powolanie-prezesa-zarzadu-spolki', 'Powołanie Prezesa Zarządu Spółki', '<p>Zarząd Sp&oacute;łki STI GROUP S.A. z siedzibą w Częstochowie (&quot;Sp&oacute;łka&quot;,&quot;Emitent&quot;) informuje, iż Rada Nadzorcza na posiedzeniu w dniu 29 stycznia 2014 r. uchwałą nr 3 w oparciu o &sect; 19 ust. ust. 2 pkt 4 Statutu Sp&oacute;łki wybiera Pana Bogdana Sadeckiego do sprawowania od dnia 29 stycznia 2014 r. funkcji Prezesa Zarządu Sp&oacute;łki.&nbsp;</p>\r\n\r\n<p>W chwili obecnej Zarząd Emitenta jest jednoosobowy.&nbsp;</p>\r\n\r\n<p>W załączeniu do niniejszego raportu bieżącego przekazuje się informacje, o kt&oacute;rych mowa art. 18 ustawy Kodeks Sp&oacute;łek Handlowych oraz &sect; 3 ust. 2 pkt. 11 Załącznika nr 3 do Regulaminu ASO w zw. z &sect; 10 pkt. 20 Załącznika nr 1 do Regulaminu ASO.&nbsp;</p>\r\n\r\n<p>Podstawa prawna: &sect;3 ust. 2 pkt 11) Załącznika Nr 3 do Regulaminu Alternatywnego Systemu Obrotu &quot;Informacje bieżące i okresowe przekazywane w alternatywnym systemie obrotu na rynku NewConnect&quot;.</p>\r\n', '<p>Zarząd Sp&oacute;łki STI GROUP S.A. informuje, iż Rada Nadzorcza na posiedzeniu w dniu 29 stycznia 2014 r. wybrała Pana Bogdana Sadeckiego do sprawowania od dnia 29 stycznia 2014 r. funkcji Prezesa Zarządu Sp&oacute;łki.</p>\r\n', 0, 0, NULL, NULL, 0, '', ''),
(19, 0, '2014-02-06 14:30:00', 'podpisanie-listu-intencyjnego', 'Podpisanie listu intencyjnego', '<p>Zarząd STI GROUP S.A. z siedzibą w Częstochowie (zwanej dalej &quot;Sp&oacute;łką&quot; lub &quot;Emitentem)&quot; informuje, iż w dniu 6 lutego 2014 roku otrzymał podpisany z &quot;NATURA CENTRUM&quot; Sp&oacute;łka z ograniczoną odpowiedzialnością z siedzibą w Warszawie list intencyjny, przedmiotem kt&oacute;rego jest zawarcie umowy o wykonywaniu szeroko pojętej usługi doradztwa przy pozyskaniu finansowania na zrealizowanie pierwszego na terenie Polski i Europy projektu inwestycyjnego &quot;Rodzinny Kurort Aktywnego Wypoczynku&quot; w bezpośrednim sąsiedztwie Puszczy Osieckiej (otulina Mazowieckiego Parku Krajobrazowego) z modelową nazwą miejscowości Polanki Zdr&oacute;j. Wartość projektu inwestycyjnego, przy kt&oacute;rym Emitent ma wystąpić jako doradca zgodnie z przedstawionymi dokumentami określona została na kwotę 450.000.000 zł (słownie: czterysta pięćdziesiąt milion&oacute;w złotych). Szczeg&oacute;ły transakcji nie stanowią treści niniejszej informacji z uwagi na klauzulę poufności. Emitent o realizacji transakcji poinformuje oddzielnym raportem bieżącym.&nbsp;</p>\r\n\r\n<p>Emitent podaje niniejszą informacje, gdyż w opinii Zarządu, transakcja ta wpłynie pozytywnie zar&oacute;wno na przychody nie tylko w postaci wynagrodzenia należnego na podstawie wspomnianej umowy na usługi doradcze, ale ze względu na możliwą długofalową wsp&oacute;łpracę przy przedmiotowym unikatowym w Polsce projekcie inwestycyjnym.&nbsp;</p>\r\n\r\n<p>Podstawa prawna: &sect; 3 ust. 1 Załącznika Nr 3 do Regulaminu Alternatywnego Systemu Obrotu &quot;Informacje bieżące i okresowe przekazywane w alternatywnym systemie obrotu na rynku NewConnect&quot;.</p>\r\n', '<p>Zarząd STI GROUP S.A. informuje, iż w dniu 6 lutego 2014 roku otrzymał podpisany z &quot;NATURA CENTRUM&quot; Sp&oacute;łka z ograniczoną odpowiedzialnością z siedzibą w Warszawie list intencyjny.</p>\r\n', 0, 0, NULL, NULL, 0, '', ''),
(20, 0, '2014-03-05 14:30:00', 'zawarcie-istotnej-umowy-z-madkom-sa', 'Zawarcie istotnej umowy z MADKOM S.A.', '<p>Zarząd STI GROUP S.A. z siedzibą w Częstochowie (zwanej dalej &quot;Sp&oacute;łką&quot; lub &quot;Emitentem&quot;) Jako Zleceniobiorca informuje, iż w dniu 4 marca 2014 roku podpisał ze Sp&oacute;łką MADKOM S.A. z siedzibą w Gdyni umowę, przedmiotem kt&oacute;rej jest świadczenie usług doradczych na rzecz MADKOM S.A. - Zleceniodawcy. Szczeg&oacute;ły umowy nie stanowią treści niniejszej informacji z uwagi na klauzulę poufności.&nbsp;</p>\r\n\r\n<p>Emitent podaje niniejszą informacje, gdyż podpisanie przedmiotowej umowy ma zapewnić Emitentowi kolejne źr&oacute;dło przychodu. W opinii Zarządu, transakcja ta bowiem wpłynie pozytywnie zar&oacute;wno na przychody nie tylko w postaci wynagrodzenia należnego Sp&oacute;łce na podstawie wspomnianej umowy na usługi doradcze, ale ze względu na możliwą długofalową wsp&oacute;łpracę wsp&oacute;łpracę z MADKOM S.A. przez Emitenta.</p>\r\n\r\n<p>Emitent uznał tę informację za istotną z uwagi na jej wpływ na sytuację gospodarczą, majątkową i finansową emitenta oraz jego perspektywy rozwoju, gdyż wynagrodzenie Sp&oacute;łki z wskazanej umowy przekracza 20 % wartości przychod&oacute;w za cztery kwartały 2013 r., kt&oacute;re zgodnie z RZiS wyniosły 114.343,08 zł.&nbsp;</p>\r\n\r\n<p>Informacja jest istotna r&oacute;wnież na fakt , że przychody wynikają z działalności, kt&oacute;rej dotychczas Sp&oacute;łka nie wykonywała. Zgodnie z już realizowanym rozwojem oferty Emitenta, Sp&oacute;łka będzie prowadzić działalność w zakresie usług doradczych na rzecz innych podmiot&oacute;w. O planach w zakresie rozszerzenia działalności Zarząd Emitenta informował w raporcie za IV kwartał 2013 w punkcie 11.10.</p>\r\n\r\n<p>Podstawa prawna: &sect; 3 ust. 1 Załącznika Nr 3 do Regulaminu Alternatywnego Systemu Obrotu &quot;Informacje bieżące i okresowe przekazywane w alternatywnym systemie obrotu na rynku NewConnect&quot;.</p>\r\n', '<p>Zarząd STI GROUP S.A. Jako Zleceniobiorca informuje, iż w dniu 4 marca 2014 roku podpisał ze Sp&oacute;łką MADKOM S.A. z siedzibą w Gdyni umowę, przedmiotem kt&oacute;rej jest świadczenie usług doradczych na rzecz MADKOM S.A.<br />\r\n&nbsp;</p>\r\n', 0, 0, NULL, NULL, 0, '', ''),
(21, 0, '2014-03-11 14:30:00', 'zawarcie-z-akcjonariuszem-dorota-denis---brewczyska-umowy-lock-up', 'Zawarcie z akcjonariuszem Dorotą Denis - Brewczyńską umowy lock up', '<p>Zarząd STI GROUP S.A. z siedzibą w Częstochowie (&quot;Sp&oacute;łka&quot; / &quot;Emitent&quot;) informuje, iż w dniu 11 marca 2014 r. w odpowiedzi na złożony przez akcjonariusza wniosek, o kt&oacute;rym Sp&oacute;łka informowała raportem bieżącym nr 17/2014 z dnia 6 marca 2014 r. Emitent zawarł z Panią Dorotą Denis - Brewczyńską umowę lock up na posiadane przez nią akcje.&nbsp;</p>\r\n\r\n<p>Traktując nabycie akcji Sp&oacute;łki jako długofalową inwestycję akcjonariusz dobrowolnie zobowiązała się do ograniczenia zbywania posiadanych przez nią 4.200.000 akcji Sp&oacute;łki w okresie od 11.03.2014 r. do 28.02.2016 r.&nbsp;</p>\r\n\r\n<p>Zgodnie z postanowieniami wskazanej umowy lock up akcjonariusz jest uprawniony do sprzedaży akcji, przeniesienia posiadania i praw do rozporządzania akcjami w inny spos&oacute;b lub obciążania akcji tylko w wypadku otrzymania pisemnej zgody Walnego Zgromadzenia Akcjonariuszy Emitenta. W przypadku złamania postanowień umowy lock up, akcjonariusz jest zobowiązany do zapłaty Sp&oacute;łce kary umownej w wysokości 4.200.000,00 zł. Umowa nie dotyczy sprzedaży akcji przez akcjonariusza w przypadku ogłoszenia publicznego wezwania na sprzedaż lub zamianę akcji Emitenta lub ogłoszenia i wzięcia udziału w skupie akcji własnych Sp&oacute;łki. Umowa obowiązuje do 28 lutego 2016 r.</p>\r\n\r\n<p>Podstawa prawna: &sect; 3 ust. 1 Załącznika Nr 3 do Regulaminu Alternatywnego Systemu Obrotu &quot;Informacje bieżące i okresowe przekazywane w alternatywnym systemie obrotu na rynku NewConnect&quot;.</p>\r\n', '<p>Traktując nabycie akcji Sp&oacute;łki jako długofalową inwestycję akcjonariusz dobrowolnie zobowiązała się do ograniczenia zbywania posiadanych przez nią 4.200.000 akcji Sp&oacute;łki w okresie od 11.03.2014 r. do 28.02.2016 r.</p>\r\n', 0, 0, NULL, NULL, 0, '', ''),
(22, 0, '2014-03-13 14:30:00', 'przyjecie-prognoz-finansowych-na-rok-2014-przez-zarzad-spolki', 'Przyjęcie prognoz finansowych na rok 2014 przez Zarząd Spółki', '<p>Zarząd STI GROUP S.A. (zwanej dalej &quot;Sp&oacute;łką&quot; lub &quot;Emitentem&quot;) z siedzibą w Częstochowie w dniu 13 marca 2014 roku mając na uwadze zmiany zachodzące w Sp&oacute;łce zar&oacute;wno w obszarze zarządczym jak r&oacute;wnież operacyjnym przyjął prognozę finansową oraz kierunki rozwoju na rok 2014.</p>\r\n\r\n<p>Prognoza finansowa Emitenta na rok 2014 została przygotowana w oparciu o przyjęty przez Sp&oacute;łkę plan sprzedaży i budżet koszt&oacute;w.&nbsp;</p>\r\n\r\n<p>Prognozowane przychody finansowe Sp&oacute;łki w roku 2014 powinny kształtować się na poziomie 1 490 000,00 zł, przychody ze sprzedaży 3 300 000,00 zł, zysk netto powinien osiągnąć wartość 2.000.000,00 zł, a EBITDA 2 460 000,00 zł, przychody łącznie 4 790 000,00 zł, koszty działalności operacyjnej 320 000,00 zł.</p>\r\n\r\n<p>Prognozowany w 2014 roku łączny wzrost przychod&oacute;w ze sprzedaży STI GROUP S.A. o ponad 4100 % w por&oacute;wnaniu z rokiem 2013 jest wynikiem realizacji um&oacute;w już zawartych przez Emitenta, jak r&oacute;wnież tych, kt&oacute;re w bieżącym roku planuje podpisać. Wpływ na wynik finansowy STI GROUP S.A. w roku 2014 pr&oacute;cz działalności operacyjnej będzie miała r&oacute;wnież działalność inwestycyjna polegająca na inwestowania środk&oacute;w własnych Sp&oacute;łki, o czym Zarząd Emitenta informował w raporcie za IV kwartał 2013 roku (EBI 13/2014).&nbsp;</p>\r\n\r\n<p>Zgodnie z komunikatem z dnia 11 marca 2014 roku (EBI 19/2014) STI GROUP S.A. rozpoczęła wdrażanie strategii aktywnego lokowania środk&oacute;w własnych. Zarząd Emitenta będzie przedstawiał stosownym raportem bieżącym wskaźnikową rentowność przeprowadzanych inwestycji. Zarząd Sp&oacute;łki spodziewa się wysokich marż na dokonanych i organizowanych transakcjach, co może przyśpieszyć realizację prezentowanych prognoz.</p>\r\n\r\n<p>Zarząd STI GROUP S.A. pragnie poinformować, iż decyzja dotycząca publikacji prognoz finansowych Sp&oacute;łki na rok 2014 podyktowana jest r&oacute;wnież wysokim poziom ich realizacji w bieżącym okresie. Ma to związek z dynamicznym rozwojem bazy klient&oacute;w w zakresie oferty usług doradczych.&nbsp;</p>\r\n\r\n<p>Opublikowane prognozy nie ujmują przychod&oacute;w z dw&oacute;ch lub więcej nowych portali jakie zamierza Sp&oacute;łka oddać do użytkowania w roku 2014. Jakiekolwiek przesunięcia w tym zakresie nie będą miały wpływu na zmniejszenie przychod&oacute;w i zysku operacyjnego Sp&oacute;łki. Do wykonania nowych portali Zarząd zamierza zaprosić inwestora, kt&oacute;ry będzie wsp&oacute;łfinansował prace nad portalami oraz wykorzystać środki unijne. Zgodnie z informacjami jakie zostały przekazane w raporcie okresowym za IV kwartał 2013 roku (EBI 13/2014) Zarząd w wypadku pozyskania wystarczającego finansowania w II kwartale 2014 r. i po spłacie zadłużenia uruchomi nowe portale, odpowiednio:<br />\r\n- w III kwartale 2014 r. uruchomi portal dla branży nieruchomości komercyjnych&nbsp;<br />\r\n- w IV kwartale 2014 r. uruchomi portal w branży finans&oacute;w&nbsp;</p>\r\n\r\n<p>W przypadku zawarcia kolejnych um&oacute;w jakie Sp&oacute;łka negocjuje i zamierza podpisać w okresie pierwszego kwartału 2014 roku, Zarząd Sp&oacute;łki opublikuje skorygowane prognozy wynik&oacute;w finansowych.</p>\r\n\r\n<p>Podstawa prawna:<br />\r\n&sect; 3 ust. 1 Załącznika nr 3 do regulaminu ASO - &quot;Informacje bieżące i okresowe przekazywane w Alternatywnym Systemie Obrotu na rynku NewConnect&quot;.</p>\r\n', '<p>Zarząd STI GROUP S.A. mając na uwadze zmiany zachodzące w Sp&oacute;łce zar&oacute;wno w obszarze zarządczym jak r&oacute;wnież operacyjnym przyjął prognozę finansową oraz kierunki rozwoju na rok 2014.<br />\r\n&nbsp;</p>\r\n', 0, 0, NULL, NULL, 0, '', ''),
(23, 0, '2014-03-13 14:30:00', 'zawarcie-umowy-posrednictwa-z-solar-energy-sa', 'Zawarcie umowy pośrednictwa z Solar-Energy S.A.', '<p>Zarząd STI GROUP S.A. z siedzibą w Częstochowie (zwanej dalej &quot;Sp&oacute;łką&quot; lub &quot;Emitentem&quot;) informuje, iż w dniu 12 marca 2014 roku Emitent podpisał ze Sp&oacute;łką Solar-Energy S.A. z siedzibą w Warszawie jako Zlecającym umowę, przedmiotem kt&oacute;rej jest pośredniczenie w zakresie sprzedaży produkt&oacute;w Zlecającego. Solar-Energy S.A. specjalizuje się w fotowoltanice, dostarczając klientom zaawansowane rozwiązania obejmujące projekty, produkcję, dystrybucję oraz utrzymanie urządzeń. Nowoczesne instalacje, tzw. farmy solarne (dachowe i wolnostojące), pozwalają nie tylko na obniżenie koszt&oacute;w produkcji energii elektrycznej, ale także na ochronę środowiska. Firma jako najbardziej innowacyjna i jedna z pierwszych w Polsce zajęła się produkcją innowacyjnych moduł&oacute;w fotowoltaicznych z cienkich monokrystalicznych oraz polikrystalicznych ogniw krzemowych o wysokiej sprawności. Oferta firmy obejmuje r&oacute;wnież hybrydę modułu PV z kolektorem słonecznym oraz nowoczesną dach&oacute;wkę fotowoltaiczną. Cały proces produkcyjny odbywa się na nowoczesnej, automatycznej linii technologicznej, co eliminuje ryzyko błędu technicznego. Dzięki zastosowaniu najnowocześniejszych technik produkcji oraz wykorzystaniu komponent&oacute;w najwyższej klasy, produkty PV Solar-Energy S.A. przewyższają jakościowo większość produkt&oacute;w dostępnych na europejskim rynku, przy czym - co istotnie - są r&oacute;wnież konkurencyjne cenowo. Szczeg&oacute;ły umowy nie stanowią treści niniejszej informacji z uwagi na klauzulę poufności. Emitent podaje niniejszą informacje, gdyż podpisanie przedmiotowej umowy ma zapewnić Emitentowi kolejne źr&oacute;dło przychodu. Emitent uznał tę informację za istotną z uwagi na jej wpływ na sytuację gospodarczą, majątkową i finansową emitenta oraz jego perspektywy rozwoju. Informacja jest istotna r&oacute;wnież z uwagi na fakt, że przychody wynikają z działalności, kt&oacute;rej dotychczas Sp&oacute;łka nie wykonywała. O planach w zakresie rozszerzenia działalności Zarząd Emitenta informował w raporcie za IV kwartał 2013 w punkcie 11.10. Emitent z uwagi na dużą ilość kontakt&oacute;w handlowych zamierza pośredniczyć w sprzedaży produkt&oacute;w i usług Soalr-Energy S.A. na terenie Polski i poza nią. Zgodnie z umową Emitent ma opcję objęcia akcji Solar-Energy S.A. przed jej debiutem giełdowym.</p>\r\n\r\n<p>Podstawa prawna: &sect; 3 ust. 1 Załącznika Nr 3 do Regulaminu Alternatywnego Systemu Obrotu &quot;Informacje bieżące i okresowe przekazywane w alternatywnym systemie obrotu na rynku NewConnect&quot;.</p>\r\n', '<p>Zarząd STI GROUP S.A. informuje, iż w dniu 12 marca 2014 roku Emitent podpisał ze Sp&oacute;łką Solar-Energy S.A. umowę, przedmiotem kt&oacute;rej jest pośredniczenie w zakresie sprzedaży produkt&oacute;w Solar-Energy S.A.</p>\r\n', 0, 0, NULL, NULL, 0, '', ''),
(24, 0, '2014-03-25 14:30:00', 'zawarcie-z-akcjonariuszem-januarym-ciszewskim-umowy-lock-up', 'Zawarcie z akcjonariuszem Januarym Ciszewskim umowy lock up', '<p>Zarząd STI GROUP S.A. z siedzibą w Częstochowie (&quot;Sp&oacute;łka&quot; / &quot;Emitent&quot;) informuje, iż w dniu 24 marca 2014 r. w odpowiedzi na złożony przez akcjonariusza wniosek, o kt&oacute;rym Sp&oacute;łka informowała raportem bieżącym nr 15/2014 z dnia 5 marca 2014 r. Emitent zawarł z Panem Januarym Ciszewskim umowę lock up na posiadane przez niego akcje.&nbsp;</p>\r\n\r\n<p>Traktując nabycie akcji Sp&oacute;łki jako długofalową inwestycję akcjonariusz dobrowolnie zobowiązał się do ograniczenia zbywania posiadanych przez niego 4.200.000 akcji Sp&oacute;łki w okresie od 24.03.2014 r. do 28.02.2016 r.&nbsp;</p>\r\n\r\n<p>Zgodnie z postanowieniami wskazanej umowy lock up akcjonariusz jest uprawniony do sprzedaży akcji, przeniesienia posiadania i praw do rozporządzania akcjami w inny spos&oacute;b lub obciążania akcji tylko w wypadku otrzymania pisemnej zgody Walnego Zgromadzenia Akcjonariuszy Emitenta. W przypadku złamania postanowień umowy lock up akcjonariusz jest zobowiązany do zapłaty Sp&oacute;łce kary umownej w wysokości 4.200.000,00 zł. Umowa nie dotyczy sprzedaży akcji przez akcjonariusza w przypadku ogłoszenia publicznego wezwania na sprzedaż lub zamianę akcji Emitenta lub ogłoszenia i wzięcia udziału w skupie akcji własnych Sp&oacute;łki. Umowa obowiązuje do 28 lutego 2016 r.&nbsp;</p>\r\n\r\n<p>Podstawa prawna: &sect; 3 ust. 1 Załącznika Nr 3 do Regulaminu Alternatywnego Systemu Obrotu &quot;Informacje bieżące i okresowe przekazywane w alternatywnym systemie obrotu na rynku NewConnect&quot;.</p>\r\n', '<p>Zarząd STI GROUP S.A. informuje, iż w dniu 24 marca 2014 r. Emitent zawarł z Panem Januarym Ciszewskim umowę lock up na posiadane przez niego akcje.</p>\r\n', 0, 0, NULL, NULL, 0, '', ''),
(25, 0, '2014-04-17 14:30:00', 'zmiana-firmy-spolki-i-adresu-siedziby-oraz-rejestracja-zmian-statutu', 'Zmiana firmy Spółki i adresu siedziby oraz rejestracja zmian statutu', '<p>Zarząd COLUMBUS CAPITAL SA (poprzednia nazwa STI Group S.A.) z siedzibą w Katowicach informuje, iż w dniu 16 kwietnia 2014 r. otrzymał postanowienie Sądu Rejonowego w Częstochowie XVII Wydział Gospodarczy Krajowego Rejestru Sądowego z dnia 28 marca 2014 r. o rejestracji w dniu 28 marca 2014 r. przez Sąd Rejestrowy zmian do statutu Sp&oacute;łki wynikających z uchwał Nadzwyczajnego Walnego Zgromadzenia Akcjonariuszy z dnia 29 stycznia 2014 roku, o kt&oacute;rych Sp&oacute;łka informowała w raporcie bieżącym nr 7/2014 w dniu 30.01.2014 r. W wyniku podjętych uchwał zmianie uległa firma Sp&oacute;łki i adres siedziby. Firma Sp&oacute;łki brzmi: COLUMBUS CAPITAL Sp&oacute;łka Akcyjna. Sp&oacute;łka może używać skr&oacute;tu COLUMBUS CAPITAL SA Nowy adres siedziby Sp&oacute;łki i jednocześnie adres do korespondencji jest następujący:</p>\r\n\r\n<p>COLUMBUS CAPITAL SA<br />\r\nul. Moniuszki 7<br />\r\n40-005 Katowice</p>\r\n\r\n<p>W załączeniu jednolity tekst statutu uwzględniający ww. zmiany.</p>\r\n\r\n<p>Podstawa prawna:<br />\r\n&sect; 4 ust. 2 pkt 2) oraz &sect; 3 ust. 1 Załącznika nr 3 do Regulaminu Alternatywnego Systemu Obrotu &quot;Informacje bieżące i okresowe przekazywane w alternatywnym systemie obrotu na rynku NewConnect&quot;.</p>\r\n', '<p>W wyniku podjętych uchwał zmianie uległa firma Sp&oacute;łki i adres siedziby. Firma Sp&oacute;łki brzmi: COLUMBUS CAPITAL Sp&oacute;łka Akcyjna. Sp&oacute;łka może używać skr&oacute;tu COLUMBUS CAPITAL SA</p>\r\n', 0, 0, NULL, NULL, 0, '', ''),
(26, 0, '2014-04-23 14:30:00', 'strategia-rozwoju-spolki-columbus-capital-sa-na-lata-2014-2016', 'Strategia rozwoju Spółki COLUMBUS CAPITAL SA na lata 2014-2016', '<p>Zarząd COLUMBUS CAPITAL SA&nbsp;z siedzibą w Katowicach (dalej Sp&oacute;łka) przekazuje do wiadomości strategię rozwoju Sp&oacute;łki COLUMBUS CAPITAL SA na lata 2014 - 2016, uchwaloną&nbsp;przez Zarząd w dniu 22 kwietnia 2014 r., zatwierdzoną przez Radę Nadzorczą uchwałą nr 1 z dnia 22 kwietnia 2014 r. Podstawa prawna: &sect;3 ust. 1 Załącznika Nr 3 do Regulaminu&nbsp;Alternatywnego Systemu Obrotu &quot;Informacje bieżące i okresowe przekazywane w alternatywnym systemie obrotu na rynku NewConnect&quot;.&nbsp;</p>\r\n\r\n<p><strong><a href="http://www.columbuscapital.pl/files/reports/Columbus_Capital_SA_STRATEGIA_2014-2016.pdf">Pobierz&nbsp;Strategię&nbsp;rozwoju Sp&oacute;łki COLUMBUS CAPITAL SA na lata 2014-2016</a>&nbsp;(PDF)</strong></p>\r\n', '<p>Zarząd COLUMBUS CAPITAL SA z siedzibą w Katowicach przekazuje do wiadomości strategię rozwoju na lata 2014 - 2016.</p>\r\n', 0, 0, NULL, NULL, 0, '', ''),
(27, 0, '2014-04-23 14:30:00', 'zmiana-logo-oraz-zasad-identyfikacji-wizualnej-columbus-capital-sa', 'Zmiana logo oraz zasad identyfikacji wizualnej COLUMBUS CAPITAL SA', '<p>Zarząd sp&oacute;łki COLUMBUS CAPITAL SA z siedzibą w Katowicach (&quot;Sp&oacute;łka&quot;, &quot;Emitent&quot;) informuje, że Rada Nadzorcza Emitenta w dniu 22 kwietnia 2014 r., uchwałą numer 2 zatwierdziła decyzję Zarządu dotyczącą zmiany identyfikacji wizualnej Emitenta, strony internetowej oraz logo Sp&oacute;łki. Decyzja to związana jest z rozwojem organizacyjnym, geograficznym oraz biznesowym COLUMBUS CAPITAL SA, a także wprowadzeniem do jej oferty nowych usług. Wprowadzenie nowej polityki dotyczącej identyfikacji wizualnej ma na celu zachowanie sp&oacute;jności wizerunkowej działań rozwojowych oraz podporządkowanie aktywności w r&oacute;żnych wymiarach nadrzędnemu celowi Emitenta, jakim jest budowanie wartości COLUMBUS CAPITAL SA dla udziałowc&oacute;w i interesariuszy. Kluczowe założenia, kt&oacute;re przyjęto w konstruowaniu koncepcji identyfikacji wizualnej: - znak graficzny, wspierający siłę marki COLUMBUS CAPITAL SA - logo dające możliwość sp&oacute;jnej komunikacji z klientem, rynkiem oraz interesariuszami, - logo wspierające synergię działań COLUMBUS CAPITAL SA W załączeniu Zarząd Sp&oacute;łki przekazuje nowe logo Emitenta. Adres strony internetowej Sp&oacute;łki to www.columbuscapital.pl Podstawa prawna: &sect; 3 ust. 1 Załącznika Nr 3 do Regulaminu Alternatywnego Systemu Obrotu &quot;Informacje bieżące i okresowe przekazywane w alternatywnym systemie obrotu na rynku NewConnect&quot;.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p style="text-align:center"><img alt="" src="http://columbuscapital.pl/img/logo.png" /></p>\r\n', '<p>Rada Nadzorcza Emitenta w dniu 22 kwietnia 2014 r., uchwałą numer 2 zatwierdziła decyzję Zarządu dotyczącą zmiany identyfikacji wizualnej Emitenta, strony internetowej oraz logo Sp&oacute;łki.</p>\r\n', 0, 0, NULL, NULL, 0, '', ''),
(28, 0, '2014-05-12 14:30:00', 'zarzad-columbus-capital-sa-podpisal-deklaracje-czlonkowska', 'Zarząd COLUMBUS CAPITAL SA podpisał deklarację członkowską', '<p>Zarząd Columbus Capital S.A. (dalej: Emitent, Sp&oacute;łka) z siedzibą w Katowicach informuje, iż w dniu 7 maja 2014 r. Emitent podpisał deklarację członkowską przystępując do Porozumienia kooperacyjnego Klaster Dolina Czystej Energii z siedzibą w Gdańsku, kt&oacute;rego celem są innowacje w odnawialne źr&oacute;dła energii oraz wprowadzanie nowych technologii przyjaznych środowisku, takich jak m.in. fotowoltaika.</p>\r\n\r\n<p>Emitent podaje powyższą informację, gdyż w jego ocenie przystąpienie do Klastra Dolina Czystej Energii z siedzibą w Gdańsku może mieć w przyszłości wpływ na rozw&oacute;j i przychody osiągane przez Emitenta.&nbsp;</p>\r\n', '<p>Zarząd COLUMBUS CAPITAL SA informuje, iż w dniu 7 maja 2014 r. Emitent podpisał deklarację członkowską przystępując do Porozumienia kooperacyjnego Klaster Dolina Czystej Energii.</p>\r\n', 0, 0, NULL, NULL, 0, '', ''),
(29, 0, '2014-05-16 14:30:00', 'raport-kwartalny-columbus-capital-sa-za-i-kwartal-2014-roku', 'Raport kwartalny COLUMBUS CAPITAL SA za I kwartał 2014 roku', '<p>Zarząd Sp&oacute;łki&nbsp;Columbus Capital S.A. /dawniej STI Group S.A./&nbsp;z siedzibą w Katowicach przekazuje w załączeniu raport okresowy za I kwartał 2014 roku.</p>\r\n\r\n<p>Podstawa prawna:<br />\r\nPar. 5 ust. 1 pkt. 1 Załącznika Nr 3 do Regulaminu Alternatywnego Systemu Obrotu &quot;Informacje bieżące i okresowe przekazywane w alternatywnym systemie obrotu na rynku NewConnect&quot;.</p>\r\n', '<p>Zarząd Sp&oacute;łki COLUMBUS CAPITAL SA z siedzibą w Katowicach przekazuje w załączeniu raport okresowy za I kwartał 2014 roku.</p>\r\n', 0, 0, NULL, NULL, 0, '', ''),
(30, 0, '2014-05-18 14:30:00', 'zmiana-klasyfikacji-sektorowej-columbus-capital-sa', 'Zmiana klasyfikacji sektorowej COLUMBUS CAPITAL SA', '<article class="left-span1 span11 column">\r\n<p>Zarząd Sp&oacute;łki Columbus Capital S.A. (dawniej STI Group S.A.) z siedzibą w Katowicach, niniejszym informuje, iż w dniu 16 maja 2014 roku powziął informacje, że na podstawie Uchwały Nr 594/2008 Zarządu Giełdy z dnia 31 lipca 2008 r. w sprawie klasyfikacji sektorowej sp&oacute;łek giełdowych, kt&oacute;rych akcje są wprowadzone do alternatywnego systemu obrotu na rynku NewConnect (z p&oacute;źn. zm.), Giełda Papier&oacute;w Wartościowych w Warszawie S.A. poinformowała, że po sesji w dniu 30 maja 2014 r. ulegnie zmianie kwalifikacja sektorowa sp&oacute;łki COLUMBUS (ISIN PLSTIGR00012) z sektora &quot;informatyka&quot; na sektor &quot;inwestycje&quot;.</p>\r\n\r\n<p>Podstawa prawna :</p>\r\n\r\n<p>&sect;3 ust.1 załącznika nr 3 do Regulaminu Alternatywnego Systemu Obrotu &quot;Informacje bieżące i okresowe przekazywane w alternatywnym systemie obrotu na rynku New Connect&quot;</p>\r\n</article>\r\n', '<p>Giełda Papier&oacute;w Wartościowych w Warszawie S.A. poinformowała, że po sesji w dniu 30 maja 2014 r. ulegnie zmianie kwalifikacja sektorowa sp&oacute;łki z sektora &quot;informatyka&quot; na sektor &quot;inwestycje&quot;.</p>\r\n', 0, 0, NULL, NULL, 0, '', ''),
(31, 0, '2014-05-20 14:30:00', 'spotkanie-z-inwestorami-i-akcjonariuszami-columbus-capital-sa', 'Spotkanie z Inwestorami i Akcjonariuszami COLUMBUS CAPITAL SA', '<article class="left-span1 span11 column">\r\n<p>Zarząd Columbus Capital S.A. z siedzibą w Katowicach informuje, iż w dniu 16 czerwca 2014 r. o godzinie 15:00 w Centrum Szkoleniowo Biurowym w Katowicach przy ul. Moniuszki 7 w sali &quot;NIEBIESKA&quot; - I piętro, odbędzie się spotkanie dla Inwestor&oacute;w i Akcjonariuszy Sp&oacute;łki, mające na celu om&oacute;wienie plan&oacute;w rozwoju Emitenta, związanej z realizacją przyjętej przez Zarząd strategii rozwoju Sp&oacute;łki Columbus Capital S.A. na lata 2014 -2016, o kt&oacute;rej Emitent informował rynek raportem bieżącym nr 33/2014 w dniu 23.04.2014 r.&nbsp;</p>\r\n\r\n<p>Zarząd Columbus Capital S.A. w załączeniu przekazuje zaproszenie dla wszystkich zainteresowanych.&nbsp;</p>\r\n\r\n<p><a href="http://www.columbuscapital.pl/files/reports/EBI_38_2014_zal.pdf">POBIERZ ZAPROSZENIE (PDF)</a></p>\r\n\r\n<p>Podstawa Prawna:&nbsp;<br />\r\n&sect; 3 ust. 1 Załącznika Nr 3 do Regulaminu Alternatywnego Systemu Obrotu \\&quot;Informacje Bieżące i Okresowe przekazywane w alternatywnym systemie obrotu na rynku NewConnect\\&quot;.</p>\r\n</article>\r\n', '<p>Zapraszamy na spotkanie dla Inwestor&oacute;w i Akcjonariuszy Sp&oacute;łki, kt&oacute;re odbędzie się dnia 16 czerwca 2014 r. o godzinie 15:00 w Centrum Szkoleniowo Biurowym w Katowicach przy ul. Moniuszki 7.<br />\r\n&nbsp;</p>\r\n', 0, 0, NULL, NULL, 0, '', ''),
(32, 0, '2014-05-24 14:30:00', 'podpisanie-listu-intencyjnego-1', 'Podpisanie listu intencyjnego', '<article class="left-span1 span11 column">\r\n<p>Zarząd Columbus Capital S.A. z siedzibą w Katowicach (zwanej dalej &quot;Sp&oacute;łką&quot; lub &quot;Emitentem)&quot; informuje, iż w dniu 21 maja 2014 roku podpisał z Partnerami porozumienia tj. z grupą pięciu os&oacute;b fizycznych list intencyjny, potwierdzający rozpoczęcie wsp&oacute;łpracy, kt&oacute;ra ma prowadzić do zawarcia pomiędzy nimi umowy inwestycyjnej, a wcześniej powołania nowej Sp&oacute;łki Akcyjnej do realizowania planu wsp&oacute;lnej inwestycji w rozw&oacute;j technologii 3D, produkcji sprzętu drukującego w technologii 3D, przejęcia portali tematycznych obejmujących szeroki zakres zagadnień druku i technologii 3D i dalszego rozwoju technologii 3D, jej promocji na świecie oraz wszelkich czynności mających na celu zwiększania wartości nowo powołanej technologicznej Sp&oacute;łki Akcyjnej na rzecz inwestor&oacute;w. Nowo założona Sp&oacute;łka Akcyjna w założeniu ma być podmiotem, kt&oacute;rej akcje mają być notowane na rynku ASO Giełdy Papier&oacute;w Wartościowych w Warszawie we wsp&oacute;lnie uzgodnionym przez Emitenta i Partner&oacute;w terminie. Zgodnie z uzgodnioną przez strony strategią nowo założona Sp&oacute;łka Akcyjna powoła swoje oddziały w Niemczech, Czechach, Słowacji i innych krajach w kt&oacute;rych będzie możliwa ekspansja z gotowym produktem.&nbsp;<br />\r\nSzczeg&oacute;ły transakcji nie stanowią treści niniejszej informacji z uwagi na klauzulę poufności. Emitent o realizacji transakcji poinformuje oddzielnym raportem bieżącym. Emitent przekazuje do wiadomości publicznej podstawowe warunki przyszłej transakcji:<br />\r\n- Nowo powołana Sp&oacute;łka Akcyjna będzie posiadała kapitał założycielski 1 milion złotych,<br />\r\n- Siedzibą nowo powołanej Sp&oacute;łka Akcyjnej będzie miasto na terenie wojew&oacute;dztwa śląskiego,<br />\r\n- Emitent będzie posiadał bezpośrednią kontrolę nad Sp&oacute;łką przez kontrolę wynikającą z uprawnień osobistych do powoływania Rady Nadzorczej nowo powołanej Sp&oacute;łki Akcyjnej, a przez to Zarządu Sp&oacute;łki celowej sp&oacute;łki technologicznej.&nbsp;<br />\r\n<br />\r\nEmitent podaje niniejszą informację, gdyż stanowi ona realizację strategii rozwoju przyjętej i realizowanej przez Sp&oacute;łkę poprzez zaangażowanie Sp&oacute;łki w projekt inwestycyjny. W opinii Zarządu, zrealizowanie transakcji, będącej przedmiotem wyżej wskazanego listu intencyjnego, wpłynie pozytywnie na przychody i wycenę Emitenta oraz spowoduje powstanie grupy kapitałowej Columbus Capital S.A.&nbsp;<br />\r\n<br />\r\nPodstawa prawna: &sect; 3 ust. 1 Załącznika Nr 3 do Regulaminu Alternatywnego Systemu Obrotu &quot;Informacje bieżące i okresowe przekazywane w alternatywnym systemie obrotu na rynku NewConnect&quot;.</p>\r\n</article>\r\n', '<p>Zarząd COLUMBUS CAPITAL SA podpisał z Partnerami porozumienia tj. z grupą pięciu os&oacute;b fizycznych list intencyjny, potwierdzający rozpoczęcie wsp&oacute;łpracy, kt&oacute;ra ma prowadzić do zawarcia pomiędzy nimi umowy inwestycyjnej, a wcześniej powołania nowej Sp&oacute;łki Akcyjnej</p>\r\n', 0, 0, NULL, NULL, 0, '', '');
INSERT INTO `ofi_news` (`id`, `category_id`, `datetime`, `url`, `title`, `content`, `short_content`, `view_count`, `hidden`, `header_image`, `thumbnail`, `only_logged`, `description`, `keywords`) VALUES
(33, 0, '2014-05-27 14:30:00', 'podpisanie-listu-intencyjnego-w-sprawie-przejecia-podmiotu', 'Podpisanie listu intencyjnego w sprawie przejęcia podmiotu', '<p>Zarząd Columbus Capital S.A. z siedzibą w Katowicach (zwanej dalej &quot;Sp&oacute;łką&quot; lub &quot;Emitentem)&quot; informuje, iż w dniu 26 maja 2014 roku podpisał z osobą fizyczną list intencyjny, zmierzający do nabycia przez Emitenta, dającego 40 % głos&oacute;w w walnym zgromadzeniu, pakietu akcji&nbsp;w Sp&oacute;łce, prowadzącej działalność w zakresie doradztwa energetycznego oraz sprzedaży odnawialnych źr&oacute;deł energii, w szczeg&oacute;lności instalacji fotowoltaicznych - w podmiocie specjalizującym się we wdrażaniu rozwiązań energooszczędnych poprzez wielu partner&oacute;w, kt&oacute;rzy są liderem w swojej dziedzinie. Strony listu intencyjnego przyjęły strategię budowy og&oacute;lnopolskiej sieci sprzedaży produkt&oacute;w, ustalając iż wsp&oacute;lnie będą rozbudowywać gamę produkt&oacute;w dla klient&oacute;w biznesowych oraz indywidualnych, skupiając się na wysokiej jakości instalacjach fotowoltaicznych, pompach ciepła oraz innych innowacyjnych rozwiązaniach w tym sprzedaży nośnik&oacute;w energii. Przejmowana Sp&oacute;łka posiadając bogate doświadczenie w branży energetycznej pomoże w wytworzeniu tematycznych portali internetowych, kt&oacute;re będą targetowane na poszczeg&oacute;lne grupy docelowe. W związku z faktem, iż Columbus Capital S.A. jest wsp&oacute;łzałożycielem klastra Dolina Czystej Energii, przejmowana Sp&oacute;łka z racji posiadanego branżowego know-how będzie realizować szkolenia i konferencje z ramienia klastra. Strony ustaliły, że wszystkie wymienione działania służyć będą zwiększeniu wartości Sp&oacute;łki na rzecz Inwestor&oacute;w. Przejęcie Sp&oacute;łki ma na celu szybki i dynamiczny rozw&oacute;j grupy Columbus Capital S.A. zwiększenie wyceny Inwestora i jego przychod&oacute;w oraz wprowadzenie do obrotu publicznego akcji sp&oacute;łki przejmowanej na rynek ASO lub jeśli to będzie możliwe na podstawie wyceny na rynek regulowany Giełdy Papier&oacute;w Wartościowych w Warszawie w 2015 lub w 2016 roku.</p>\r\n\r\n<p>Emitent nie podaje danych osobowych osoby fizycznej oraz sp&oacute;łki przejmowanej ze względu na zawartą w liście intencyjnym klauzulę poufności. Emitent o zawarciu umowy inwestycyjnej oraz dalszej realizacji transakcji będzie informować rynek stosownymi raportami bieżącymi.</p>\r\n\r\n<p>Emitent podaje niniejszą informację, gdyż stanowi ona realizację przyjętej i realizowanej strategii rozwoju poprzez zaangażowanie Sp&oacute;łki w projekty inwestycyjne. W opinii Zarządu, zrealizowanie transakcji, będącej przedmiotem wyżej wskazanego listu intencyjnego, wpłynie pozytywnie na przychody i wycenę Emitenta oraz spowoduje rozw&oacute;j grupy kapitałowej Columbus Capital S.A.</p>\r\n\r\n<p>Podstawa prawna: &sect; 3 ust. 1 Załącznika Nr 3 do Regulaminu Alternatywnego Systemu Obrotu &quot;Informacje bieżące i okresowe przekazywane w alternatywnym systemie obrotu na rynku NewConnect&quot;.</p>\r\n', '<p>Zarząd COLUMBUS CAPITAL SA w dniu 26 maja 2014 roku podpisał z osobą fizyczną list intencyjny, zmierzający do nabycia przez Emitenta, dającego 40 % głos&oacute;w w walnym zgromadzeniu, pakietu akcji w Sp&oacute;łce.</p>\r\n', 0, 0, NULL, NULL, 0, '', ''),
(34, 0, '2014-06-05 14:30:00', 'umowa-nabycia-400000-akcji', 'Umowa nabycia 400.000 akcji', '<article class="left-span1 span11 column">\r\n<p>Zarząd Columbus Capital S.A. z siedzibą w Katowicach (zwanej dalej &quot;Sp&oacute;łką&quot; lub &quot;Emitentem)&quot; informuje w nawiązaniu do komunikatu EBI nr 42/2014 z dnia 27 maja 2014 r., iż w dniu 4 czerwca 2014 roku podpisał umowę z osobą fizyczną nabycia 400.000 (słownie: czterysta tysięcy) akcji serii A na okaziciela, uprzywilejowanych co do głosu w ten spos&oacute;b, iż posiadaczom jednej akcji uprzywilejowanej przysługuje prawo do oddania dw&oacute;ch głos&oacute;w na walnym zgromadzeniu akcjonariuszy na każdą akcję uprzywilejowaną, Sp&oacute;łki Akcyjnej PIE Polskie Innowacje Energetyczne S.A. z siedziba w Krakowie, wpisanej do rejestru przedsiębiorc&oacute;w KRS prowadzonego przez Sąd Rejonowy dla Krakowa &ndash; Śr&oacute;dmieścia w Krakowie XI Wydział Gospodarczy Krajowego Rejestru Sądowego pod nr KRS 0000425536, o wartości nominalnej 0,10 zł (dziesięć groszy) każda akcja, co stanowi 40 % w kapitale zakładowym Sp&oacute;łki i uprawnia do 800.000 (słownie: ośmiuset tysięcy) głos&oacute;w na Walnym Zgromadzeniu, co stanowi 40 % og&oacute;lnej liczby głos&oacute;w na Walnym Zgromadzeniu Sp&oacute;łki.</p>\r\n\r\n<p>Sp&oacute;łka w kt&oacute;rej udziały zostały nabyte przez Columbus Capital S.A. prowadzi działalność w zakresie doradztwa energetycznego, wykonywania audyt&oacute;w energetycznych oraz sprzedaży odnawialnych źr&oacute;deł energii, w szczeg&oacute;lności instalacji fotowoltaicznych, pomp ciepła. Sp&oacute;łka w kt&oacute;rej udziały zostały nabyte przez Columbus Capital S.A. jest podmiotem specjalizującym się we wdrażaniu rozwiązań energooszczędnych dla domu oraz dla przemysłu poprzez wielu partner&oacute;w i wsp&oacute;łpracownik&oacute;w, kt&oacute;rzy są liderem w swojej dziedzinie.</p>\r\n\r\n<p>Sp&oacute;łka doradza klientom w obniżaniu koszt&oacute;w za wszelkie media i przy wdrażaniu nowych energooszczędnych technologii.</p>\r\n\r\n<p>Zgodnie z przyjętą przez Strony strategią i biznesplanem, zakłada się, że przejęta Sp&oacute;łka Akcyjna po dofinansowaniu ze środk&oacute;w własnych Inwestora oraz po przeprowadzeniu emisji przed IPO powinna w okresie od 2014 roku do 2018 osiągnąć 420 000 000 zł przychodu ze sprzedaży oraz 70 000 000 zł zysku netto, a EBITDA powinna wynieść 85 000 000 zł.</p>\r\n\r\n<p>Columbus Capital S.A. jako Inwestor uzyska po nabyciu akcji w PIE Polskie Innowacje Energetyczne S.A. uprawnienia osobiste do powoływania trzech członk&oacute;w Rady Nadzorczej Sp&oacute;łki przejmowanej, co tym samym da mu możliwość wyboru zarządu w sp&oacute;łce przejmowanej oraz pozwoli realizować zaplanowaną strategię i przyjęty biznes plan.</p>\r\n\r\n<p>W związku z faktem, iż Columbus Capital S.A. jest wsp&oacute;łzałożycielem klastra Dolina Czystej Energii, przejmowana Sp&oacute;łka z racji posiadanego branżowego know-how będzie realizować szkolenia i konferencje z ramienia klastra. Strony ustaliły, że wszystkie wymienione działania służyć będą zwiększeniu wartości Sp&oacute;łki na rzecz Inwestor&oacute;w. Przejęcie Sp&oacute;łki ma na celu szybki i dynamiczny rozw&oacute;j grupy Columbus Capital S.A. zwiększenie wyceny Inwestora i jego przychod&oacute;w oraz wprowadzenie do obrotu publicznego akcji sp&oacute;łki przejmowanej na rynek ASO lub jeśli to będzie możliwe na podstawie wyceny na rynek regulowany Giełdy Papier&oacute;w Wartościowych w Warszawie w 2015 lub w 2016 roku.</p>\r\n</article>\r\n', '<p>Zarząd COLUMBUS CAPITAL SA iż w dniu 4 czerwca 2014 roku podpisał umowę z osobą fizyczną nabycia 400.000 akcji serii A na okaziciela, uprzywilejowanych co do głosu Sp&oacute;łki Akcyjnej PIE Polskie Innowacje Energetyczne S.A. z siedziba w Krakowie.</p>\r\n', 0, 0, NULL, NULL, 0, '', ''),
(35, 0, '2014-06-12 14:30:00', 'podpisanie-listu-intencyjnego-2', 'Podpisanie listu intencyjnego', '<article class="left-span1 span11 column">\r\n<p>Zarząd Columbus Capital S.A. z siedzibą w Katowicach (zwanej dalej &quot;Sp&oacute;łką&quot; lub &quot;Emitentem)&quot; informuje, iż w dniu 12 czerwca 2014 roku podpisał ze Sp&oacute;łką JR Invest Sp&oacute;łka Akcyjna z siedzibą w Krakowie, kt&oacute;rej akcje są notowane w alternatywnym systemie obrotu NewConnect, list intencyjny, potwierdzający rozpoczęcie wsp&oacute;łpracy, kt&oacute;ra ma prowadzić do realizacji zamierzonego celu, jakim jest prowadzenie i rozw&oacute;j przedsiębiorstwa o charakterze niezależnego funduszu nieruchomości komercyjnych, pozyskiwaniu dla takiego funduszu dalszego finansowania, ewentualne wprowadzenie takiego podmiotu na Giełdę Papier&oacute;w Wartościowych.&nbsp;<br />\r\n<br />\r\nObaj Emitenci uzyskali zgody władz obu Sp&oacute;łek, aby Columbus Capital SA objęła w terminie do 31.07.2014 r. w sp&oacute;łce zależnej od JR Invest SA Ganador sp. z o.o. za wkład got&oacute;wkowy do 50% udział&oacute;w.&nbsp;</p>\r\n\r\n<p><br />\r\nGanador sp. z o.o. działa w formie funduszu nieruchomości, kt&oacute;ry kupuje nieruchomości komercyjne do 1 mln zł, a następnie w kr&oacute;tkim okresie czasu odsprzedaje je z dużą marżą. Gł&oacute;wnym założeniem funduszu nieruchomości Ganador sp. z o.o. jest kupowanie nieruchomości komercyjnych za got&oacute;wkę, bez korzystania z kredyt&oacute;w bankowych. W ten spos&oacute;b działalności sp&oacute;łka jest stabilna, a nabywanie nieruchomości nieobciążone.&nbsp;<br />\r\n<br />\r\nSzczeg&oacute;łowe rozwiązania Strony listu intencyjnego uzgodnią w umowie inwestycyjnej lub w umowie objęcia udział&oacute;w sp&oacute;łki Ganador sp. z o.o.&nbsp;<br />\r\n<br />\r\nCelem jak najpełniejszego poinformowania rynku Strony transakcji zdecydowały się ujawnić treść podpisanego listu intencyjnego, kt&oacute;ry stanowi załącznik do niniejszego komunikatu.&nbsp;<br />\r\n<br />\r\nEmitent podaje niniejszą informację, gdyż stanowi ona realizację strategii rozwoju przyjętej i realizowanej przez Sp&oacute;łkę poprzez zaangażowanie Sp&oacute;łki w projekt inwestycyjny. W opinii Zarządu, zrealizowanie transakcji, będącej przedmiotem wyżej wskazanego listu intencyjnego, wpłynie pozytywnie na przychody i wycenę Emitenta oraz spowoduje rozw&oacute;j grupy kapitałowej Columbus Capital S.A.&nbsp;</p>\r\n\r\n<p><a href="http://www.columbuscapital.pl/files/reports/EBI_51_2014_z.pdf">List intencyjny (PDF)</a></p>\r\n</article>\r\n', '<p>Zarząd COLUMBUS CAPITAL SA iż w dniu 4 czerwca 2014 roku podpisał umowę z osobą fizyczną nabycia 400.000 akcji serii A na okaziciela, uprzywilejowanych co do głosu Sp&oacute;łki Akcyjnej PIE Polskie Innowacje Energetyczne S.A. z siedziba w Krakowie.</p>\r\n', 0, 0, NULL, NULL, 0, '', ''),
(36, 0, '2014-06-14 14:30:00', 'podpisanie-porozumienia-o-zachowaniu-poufnosci', 'Podpisanie porozumienia o zachowaniu poufności', '<article class="left-span1 span11 column">\r\n<p>Zarząd Columbus Capital S.A. z siedzibą w Katowicach (zwanej dalej &quot;Sp&oacute;łką&quot; lub &quot;Emitentem)&quot; informuje, iż w dniu 12 czerwca 2014 roku podpisał z Partnerem Webvizarts sp. z o.o z siedzibą w Warszawie porozumienie o zachowaniu poufności, przedmiotem, kt&oacute;rego jest rozpoczęcie wsp&oacute;łpracy w zakresie finansowania rozwoju oprogramowania (Projekt) w zamian za udziały w sp&oacute;łce Webvizarts sp. z o.o. z siedzibą w Warszawie. Partner zobowiązał się w okresie 30 dni od podpisania wskazanego porozumienia dać pierwszeństwo Emitentowi w objęciu udział&oacute;w w sp&oacute;łce Webvizarts sp. z o.o. z siedzibą w Warszawie. Ponadto celem Partner&oacute;w porozumienia będzie powołanie Sp&oacute;łki celowej z Webvizarts sp. z o.o. zajmującej się innowacyjnymi systemami lojalnościowymi i partnerskimi z modułem promocji wirusowej w mediach społecznościowych i targetowania behawioralnego komunikacji firm z targetu Horeca oraz Fitness/Spa/Beaty. Wartość transakcji objęta jest klauzulą poufności.</p>\r\n\r\n<p>Porozumienie będzie obowiązywało przez okres trzech lat. Po okresie trzech lat niniejsze porozumienie ulega automatycznemu przedłużeniu o 6 miesięcy o ile w czasie ostatnich trzech miesięcy jego obowiązywania kt&oacute;rakolwiek ze stron przekazała drugiej jakiekolwiek informacje.</p>\r\n\r\n<p>Emitent podaje niniejszą informację, gdyż stanowi ona realizację strategii rozwoju przyjętej i realizowanej przez Sp&oacute;łkę poprzez zaangażowanie Sp&oacute;łki w projekt inwestycyjny. W opinii Zarządu, zrealizowanie transakcji, będącej przedmiotem wyżej wskazanego listu intencyjnego, wpłynie pozytywnie na przychody i wycenę Emitenta oraz spowoduje rozw&oacute;j grupy kapitałowej Columbus Capital S.A.</p>\r\n</article>\r\n', '<p>Zarząd COLUMBUS CAPITAL SA w dniu 12 czerwca 2014 roku podpisał z Partnerem Webvizarts sp. z o.o porozumienie o zachowaniu poufności,</p>\r\n', 0, 0, NULL, NULL, 0, '', ''),
(37, 0, '2014-06-09 14:45:00', 'podpisanie-umowy-przez-spolke-zalezna', 'Podpisanie umowy przez spółkę zależną', '<article class="left-span1 span11 column">\r\n<p>7 lipca 2014 roku nasza sp&oacute;łka zależna PIE &ndash; Polskie Innowacje Energetyczne S.A., podpisała umowę na zaprojektowanie, zdobycie wymaganych przepisami pozwoleń budowlanych, dostawę, montaż i uruchomienie instalacji zespołu kolektor&oacute;w słonecznych na obiekcie sportowym Klubu Sportowego Bronowianka - ul. Zarzecze w Krakowie.&nbsp;<br />\r\n<br />\r\nWartość podpisanej umowy wynosi 99.500,00 złotych brutto, płatnej etapami.&nbsp;<br />\r\n<br />\r\nW naszej ocenie podpisanie niniejszej umowy między innymi dzięki zdobytym referencjom będzie mieć wpływ na sytuację majątkową, finansową i gospodarczą naszej Grupy Kapitałowej. Podpisanie tej umowy to jednocześnie dow&oacute;d, iż nasz biznes rozwija się zgodnie z zmierzonym planem w ramach realizowanej strategii, a nasza sp&oacute;łka zależna ma zdolność pozyskiwania coraz większych zleceń.&nbsp;</p>\r\n\r\n<p><br />\r\nWierzymy, że Dawid Zieliński będzie realizował budowę wartości Grupy Kapitałowej w interesie wszystkich akcjonariuszy.</p>\r\n</article>\r\n', '<p>Sp&oacute;łka zależna PIE &ndash; Polskie Innowacje Energetyczne S.A., podpisała umowę na zaprojektowanie, zdobycie wymaganych przepisami pozwoleń budowlanych, dostawę, montaż i uruchomienie instalacji zespołu kolektor&oacute;w słonecznych.</p>\r\n', 0, 0, NULL, NULL, 0, '', ''),
(38, 0, '2014-07-10 14:45:00', 'istotne-zmiany-w-spolce-zaleznej', 'Istotne zmiany w spółce zależnej', '<p>Zarząd Columbus Capital S.A. z siedzibą w Katowicach informuje, iż w dniu 9 lipca 2014 roku Zwyczajne Walne Zgromadzenie Akcjonariuszy sp&oacute;łki zależnej PIE &ndash; Polskie Innowacje Energetyczne S.A. z siedzibą w Krakowie dokonało szeregu istotnych zmian w Sp&oacute;łce:&nbsp;<br />\r\n- powołano trzy osobową Radę Nadzorczą drugiej kadencji, w skład kt&oacute;rej powołano: Dorotę Denis &ndash; Brewczyńską, Elżbietę Zielińską oraz Renatę Ciszewską,&nbsp;<br />\r\n- dokonano zmiany firmy Sp&oacute;łki na Columbus Energy S.A,&nbsp;<br />\r\n- Emitent zapewnił sobie prawo powołania i odwołania trzech członk&oacute;w Rady Nadzorczej,&nbsp;<br />\r\n- ustalono treść jednolitą statutu,&nbsp;<br />\r\n- dokonano podwyższenia kapitału zakładowego sp&oacute;łki zależnej z kwoty 100.000 zł do kwoty nie większej niż 250.000 zł tj. o kwotę nie większą niż 150.000 zł w drodze emisji 1.500.000 (jeden milion pięćset tysięcy) akcji serii B na okaziciela uprzywilejowanych co do głos&oacute;w &ndash; każdej akcji przyznaje się dwa głosy na walnym zgromadzeniu o wartości nominalnej 0,10 zł (dziesięć groszy) każda akcja. Akcje serii B zostaną zaoferowane dotychczasowym akcjonariuszom. Umowy objęcia akcji serii B mają zostać zawarte do dnia 31 sierpnia 2014 r.&nbsp;</p>\r\n\r\n<p>Zarząd Columbus Capital S.A. przekazuje niniejszą informację, gdyż dotyczy ona rozwoju Grupy Kapitałowej i w ocenie Emitenta będzie mieć wpływ na sytuację majątkową, finansową i gospodarczą zar&oacute;wno jego jak i grupy kapitałowej.&nbsp;</p>\r\n', '<p>W dniu 9 lipca 2014 roku Zwyczajne Walne Zgromadzenie Akcjonariuszy sp&oacute;łki zależnej PIE &ndash; Polskie Innowacje Energetyczne S.A. dokonało szeregu istotnych zmian w Sp&oacute;łce.</p>\r\n', 0, 0, NULL, NULL, 0, '', ''),
(39, 0, '2014-07-11 14:45:00', 'zaproszenie', 'Zaproszenie', '<p>NA II SPOTKANIE ZARZADU COLUMBUS CAPITAL S.A.&nbsp;<br />\r\nZ INWESTORAMI I AKCJONARIUSZAMI&nbsp;<br />\r\n&nbsp;<br />\r\nZarząd COLUMBUS CAPITAL S.A. (&bdquo;Sp&oacute;łka&rdquo;)&nbsp;<br />\r\nma przyjemność zaprosić&nbsp;<br />\r\n&nbsp;<br />\r\nINWESTOR&Oacute;W I AKCJONARIUSZY&nbsp;<br />\r\n&nbsp;<br />\r\nna II spotkanie mające na celu om&oacute;wienie pierwszych projekt&oacute;w&nbsp;<br />\r\ninwestycyjnych oraz szerszych plan&oacute;w rozwoju Sp&oacute;łki,&nbsp;<br />\r\nzwiązanych z realizacją przyjętej przez Zarząd&nbsp;<br />\r\nStrategii rozwoju Sp&oacute;łki Columbus Capital S.A. na lata 2014 -2016&nbsp;<br />\r\n&nbsp;<br />\r\nSzczeg&oacute;łowych informacji o spotkaniu udzielają:&nbsp;<br />\r\nJanuary Ciszewski &ndash; GSM 602 26 26 26&nbsp;<br />\r\ne-mail : january.ciszewski@columbuscapital.pl&nbsp;<br />\r\noraz&nbsp;<br />\r\nPiotr Brewczyński &ndash; GSM 601 721 721&nbsp;<br />\r\ne-mail : piotr.brewczynski@columbuscapital.pl&nbsp;<br />\r\n&nbsp;<br />\r\nTermin spotkania:&nbsp;<br />\r\n21 lipca 2014 r.&nbsp;<br />\r\nPlanowany czas spotkania: godz. 15:00 &ndash; 17:30&nbsp;<br />\r\n&nbsp;<br />\r\nMiejsce Spotkanie :&nbsp;<br />\r\nCzęstochowski Park Przemysłowo-Technologiczny&nbsp;<br />\r\nul. Wały Dwernickiego 117/121&nbsp;<br />\r\n42-202 Częstochowa&nbsp;<br />\r\nSala konferencyjna &quot;SALA KONFERENCYJNA nr 3&quot; &ndash; parter&nbsp;<br />\r\n&nbsp;<br />\r\nProsimy o potwierdzenie udziału do 18 lipca 2014&nbsp;<br />\r\ndo godziny 12.00 pod adres e-mail : biuro@columbuscapital.pl&nbsp;</p>\r\n', '<p>Zarząd COLUMBUS CAPITAL SA ma przyjemność zaprosić na II spotkanie zarządu z Inwestorami i Akcjonariuszami</p>\r\n', 0, 0, NULL, NULL, 0, '', ''),
(40, 0, '2014-08-02 14:45:00', 'wycena-wartosci-rynkowej', 'Wycena wartości rynkowej', '<p>Zarząd Columbus Capital S.A. z siedzibą w Katowicach (Emitent, Sp&oacute;łka) informuje, iż w dniu 31 lipca 2014 r. otrzymał sporządzoną przez analityka finansowego Wycenę wartości rynkowej Columbus Capital S.A., kt&oacute;rą Sp&oacute;łka przedstawia w załączeniu. Emitent zlecił sporządzenie wskazanej wyceny w związku ze zmianą na stanowisku Prezesa Zarządu oraz kontrolą realizacji przyjętej przez Sp&oacute;łkę strategii rozwoju, o kt&oacute;rej Emitent informował raportem EBI 33/2014 z dnia 23 kwietnia 2014 r. Emitent dokonał już wcześniej wycenę realizowanych przez Sp&oacute;łkę projekt&oacute;w inwestycyjnych, o kt&oacute;rych informował rynek raportem bieżącym EBI nr 59/2014 z dnia 16 czerwca 2014 r.</p>\r\n\r\n<p>Zgodnie z otrzymaną wyceną, wartości udziału Columbus Capital S.A. we wskazanych projektach oszacowano na 48.146.850 zł, w tym wartość udział&oacute;w w: :</p>\r\n\r\n<p>- projekt w branży doradztwa energetycznego oraz sprzedaży odnawialnych źr&oacute;deł energii, o kt&oacute;rym Emitent informował raportem EBI nr 42/2014 z dnia 27 maja 2014 r, oszacowano na kwotę 29.932.421 zł,</p>\r\n\r\n<p>- projekt w branży druku przestrzennego (drukarek 3D), o kt&oacute;rym Emitent informował raportem EBI nr 41/2014 z dnia 22 maja 2014 r. oszacowano na kwotę 11.966.784 zł,</p>\r\n\r\n<p>- projekt udziału w Ganador sp. z o.o. funduszu nieruchomości, o kt&oacute;rym Emitent informował raportem EBI nr 51/2014 z dnia 12 czerwca 2014 r., oszacowano na kwotę 6.035.478 zł.</p>\r\n\r\n<p>Wskazana przez analityka finansowego wartość realizowanych projekt&oacute;w potwierdza trafność podjętych przez Zarząd Sp&oacute;łki decyzji inwestycyjnych a realizowanie wskazanych projekt&oacute;w wpłynie na rynkową wartość Sp&oacute;łki. Z tego r&oacute;wnież powodu Emitent podaje niniejszy komunikat.</p>\r\n', '<p>Zarząd informuje, iż w dniu 31 lipca 2014 r. otrzymał sporządzoną przez analityka finansowego wycenę wartości rynkowej COLUMBUS CAPITAL SA.</p>\r\n', 0, 0, NULL, NULL, 0, '', ''),
(41, 0, '2014-08-05 14:45:00', 'zalozenie-spolki-zaleznej-3fx-sp-z-oo', 'Założenie spółki zależnej 3FX sp. z o.o.', '<article class="left-span1 span11 column">\r\n<p>Realizując nasze zobowiązania inwestycyjne wobec partner&oacute;w biznesowych, inwestor&oacute;w oraz akcjonariuszy Columbus Capital Sp&oacute;łka Akcyjna, zawiązaliśmy w dniu 4 sierpnia 2014 roku wraz z pięcioma osobami fizycznymi tj. Jarosławem Krykwińskim, Marcinem Krykwińskim, Michałem Liberdą, Jarosławem Operaczem, Pawłem Szywaczem Sp&oacute;łkę technologiczną pod firmą 3FX sp&oacute;łka z ograniczoną odpowiedzialnością z siedzibą w Częstochowie, kt&oacute;rej celem jest realizowanie wsp&oacute;lnej inwestycji w rozw&oacute;j&nbsp;technologii 3D, produkcji sprzętu drukującego w technologii 3D, przejęcia portali tematycznych obejmujących szeroki zakres zagadnień druku i technologii 3D i dalszego rozwoju technologii 3D, jej promocji na świecie oraz wszelkich czynności mających na celu zwiększania wartości nowo powołanego podmiotu.</p>\r\n\r\n<p>Czas trwania Sp&oacute;łki jest nieoznaczony, a w związku z przedłużaniem się pozyskania kapitału akcyjnego w planowanej wysokości 1.000.000 zł (jednego miliona złotych), zdecydowaliśmy wraz z innymi Wsp&oacute;lnikami o jak najszybszym powołaniu sp&oacute;łki z o.o., aby podmiot m&oacute;gł rozpocząć działalność operacyjną i realizować zaplanowane kontrakty i brać udział w wydarzeniach targowych i szkoleniowych jakie są już zaplanowane na najbliższy okres, a o kt&oacute;rych realizacji Columbus Capital S.A. będzie informował rynek w komunikatach bieżących.</p>\r\n\r\n<p>Obecny, początkowy kapitał zakładowy wynosi 6.000 zł (sześć tysięcy złotych), przy czym Wsp&oacute;lnicy przewidują kolejne podwyższenia kapitału zakładowego, a zgodnie z postanowieniami umowy Sp&oacute;łki podwyższenie kapitału zakładowego do wysokości 10.000.000 zł (dziesięciu milion&oacute;w złotych) w terminie do dnia 31.12.2030 r. nie stanowi zmiany umowy Sp&oacute;łki. Columbus Capital S.A. objął 40 (czterdzieści) udział&oacute;w o wartości nominalnej 50 zł (pięćdziesiąt złotych) każdy, o łącznej wartości nominalnej 2.000 zł (dwa tysiące złotych), co stanowi 33,33 % w kapitale zakładowym Sp&oacute;łki.</p>\r\n\r\n<p>Columbus Capital S.A. posiada bezpośrednią kontrolę nad Sp&oacute;łką wynikającą z uprawnień osobistych do powoływania Rady Nadzorczej, a przez to i Zarządu celowej sp&oacute;łki technologicznej.</p>\r\n\r\n<p>Zamierzeniem założycieli 3XF sp. z o.o. jest po osiągnięciu przez Sp&oacute;łkę odpowiedniego poziomu rozwoju i zdolności produkcyjnej oraz po zwiększeniu jej wartości przekształcenie jednostki w Sp&oacute;łkę Akcyjną i wprowadzenie jej akcji do Alternatywnego Systemu Obrotu NewConnect zgodnie z umową inwestycyjną oraz w zgodzie ze strategią Columbus Capital S.A.</p>\r\n</article>\r\n', '<p>Realizując nasze zobowiązania inwestycyjne wobec partner&oacute;w biznesowych, inwestor&oacute;w oraz akcjonariuszy Columbus Capital Sp&oacute;łka Akcyjna, zawiązaliśmy w dniu 4 sierpnia 2014 roku wraz z pięcioma osobami fizycznymi Sp&oacute;łkę technologiczną pod firmą 3FX.</p>\r\n', 0, 0, NULL, NULL, 0, '', ''),
(42, 0, '2014-08-07 14:45:00', 'zalozenie-spolki-zaleznej-columbus-tax-law-sp-z-oo', 'Założenie spółki zależnej Columbus Tax & Law sp. z o.o.', '<article class="left-span1 span11 column">\r\n<p>Columbus Capital Sp&oacute;łka Akcyjna z siedzibą w Katowicach (zwana dalej Emitent, Sp&oacute;łka) informuje, iż w dniu 7 sierpnia 2014 r. założył wraz z partnerami, dwiema osobami fizycznymi, adwokatami z adwokatury katowickiej Sp&oacute;łkę zależną pod firmą Columbus Tax &amp; Law sp&oacute;łka z ograniczoną odpowiedzialnością z siedzibą w Katowicach, kt&oacute;ra ma prowadzić na skalę og&oacute;lnopolską działalność w zakresie doradztwa prawnego typu:<br />\r\n- fuzje, przejęcia;<br />\r\n- przekształcenia sp&oacute;łek;<br />\r\n- tworzenie i restrukturyzacja sp&oacute;łek;<br />\r\n- audyt prawny;<br />\r\n- sprzedaż sp&oacute;łek;<br />\r\n- obsługa prawna podmiot&oacute;w gospodarczych;<br />\r\n- szkolenia podmiot&oacute;w gospodarczych i zarząd&oacute;w sp&oacute;łek prawa handlowego.&nbsp;<br />\r\n<br />\r\nKapitał zakładowy Sp&oacute;łki wynosi 8.000 zł (osiem tysięcy złotych) i dzieli się 160 (sto sześćdziesiąt) r&oacute;wnych i niepodzielnych udział&oacute;w o wartości nominalnej 50,00 zł (pięćdziesiąt) złotych każdy udział.&nbsp;<br />\r\nEmitent w nowo założonym podmiocie objął 144 (sto czterdzieści cztery) udziały o wartości nominalnej 50,00 zł (pięćdziesiąt) złotych o łącznej wartości nominalnej 7.200 zł (siedem tysięcy dwieście złotych), co stanowi 90 % w kapitale zakładowym, przy czym udział Emitenta w zysku nowo założonej Sp&oacute;łki zależnej wynosić będzie 40 %. Celem zarejestrowania Sp&oacute;łki zależnej zostaną niezwłocznie złożone stosowne dokumenty do Sądu Rejestrowego.&nbsp;<br />\r\n<br />\r\nPoprzez założenie sp&oacute;łki Columbus Tax &amp; Law sp. z o.o. Emitent realizuje zamierzenie inwestycyjne, o kt&oacute;rych informował raportem EBI nr 56/2014 z dnia 13 czerwca 2014 r.&nbsp;</p>\r\n</article>\r\n', '<p>COLUMBUS CAPITAL SA informuje, iż w dniu 7 sierpnia 2014 r. założył wraz z partnerami, dwiema osobami fizycznymi, adwokatami z adwokatury katowickiej Sp&oacute;łkę zależną pod firmą Columbus Tax &amp; Law sp. z o.o.</p>\r\n', 0, 0, NULL, NULL, 0, '', ''),
(43, 0, '2014-09-04 14:45:00', 'zakup-inwestycyjny', 'Zakup inwestycyjny', '<p>Zarząd Sp&oacute;łki Columbus Capital S.A. z siedzibą w Katowicach (&quot;Sp&oacute;łka&quot; / &quot;Emitent&quot;) informuje, iż w dniu 2 września 2014 r. nabył udziały w Sp&oacute;łce z ograniczoną odpowiedzialnością, działającą w sektorze inwestycji kapitałowych oraz zajmującej się handlem produktami elektronicznymi, za łączną kwotę 490.000 zł (słownie: czterysta dziewięćdziesiąt tysięcy) złotych za nabyte udziały.&nbsp;</p>\r\n\r\n<p>Sp&oacute;łka w kt&oacute;rej Emitent nabył udziały jest firmą inwestycyjną, kt&oacute;ra realizuje strategię rozwoju opartą na inwestowaniu środk&oacute;w w szeroko rozumianą branżę dystrybucji elektroniki użytkowej oraz przez inwestycje średnio i długoterminowe nabywając udziały i akcje dywidendowych sp&oacute;łek niepublicznych oraz akcje sp&oacute;łek notowanych na Giełdzie Papier&oacute;w Wartościowych w Warszawie, co jest zbieżne ze Strategią Emitenta na lata 2014 &ndash; 2016.&nbsp;</p>\r\n\r\n<p>Emitent przekazuje powyższą informacje, gdyż nabycie udział&oacute;w stanowi realizację przyjętej przez Columbus Capital S.A. Strategii rozwoju oraz może mieć wpływ na sytuację finansową i gospodarczą Emitenta. Zakup inwestycyjny jest inwestycją o charakterze długoterminowym w sp&oacute;łkę dywidendową zgodnie z przyjętą Strategią na lata 2014-2016.</p>\r\n', '<p>COLUMBUS CAPITAL SA informuje, iż w dniu 2 września 2014 r. nabył udziały w Sp&oacute;łce z ograniczoną odpowiedzialnością, działającą w sektorze inwestycji kapitałowych oraz zajmującej się handlem produktami elektronicznymi, za łączną kwotę 490.000 zł.</p>\r\n', 0, 0, NULL, NULL, 0, '', ''),
(44, 0, '2014-09-10 14:45:00', 'kontrakt-spolki-zaleznej-z-firma-maskpol-sa', 'Kontrakt spółki zależnej z firmą MASKPOL S.A.', '<p>Informujemy, że w dniu 29 sierpnia 2014 r. sp&oacute;łka zależna 3FX sp. z o.o. w organizacji z siedzibą w Częstochowie rozpoczęła działalność operacyjną poprzez zrealizowanie zam&oacute;wienia dla firmy MASKPOL S.A. - sp&oacute;łki należącej do grupy kapitałowej Polskiego Holdingu Obronnego Sp. z o.o. (dawny Bumar). Przedmiot zam&oacute;wienia jest objęty tajemnicą. W związku z rozbudową parku maszynowego sp&oacute;łki zależnej w&nbsp;najbliższym czasie 3FX sp. z o.o. planuje pozyskanie zleceń, a przez to wzrost przychod&oacute;w z tytułu prototypowania oraz druku 3D.&nbsp;</p>\r\n\r\n<p>Zarząd Columbus Capital S.A. przekazał niniejszą informację, gdyż dotyczy ona rozwoju Grupy Kapitałowej tworzonej przez Sp&oacute;łkę i w naszej ocenie, między innymi dzięki zdobytym referencjom, zdobyty i zrealizowany kontrakt będzie mieć wpływ na sytuację majątkową, finansową i gospodarczą naszej sp&oacute;łki oraz Grupy Kapitałowej.&nbsp;</p>\r\n\r\n<p>Zdobyte referencje z obszaru przemysłu zbrojeniowego pozwolą naszej sp&oacute;łce zależnej 3FX sp. z o.o. w organizacji, zajmującej się produkcją urządzeń do druku przestrzennego na realizację nowych zleceń dla innych kontrahent&oacute;w z kt&oacute;rymi są prowadzone rozmowy.</p>\r\n', '<p>Sp&oacute;łka zależna 3FX sp. z o.o. w organizacji z siedzibą w Częstochowie rozpoczęła działalność operacyjną poprzez zrealizowanie zam&oacute;wienia dla firmy MASKPOL S.A.</p>\r\n', 0, 0, NULL, NULL, 0, '', ''),
(45, 0, '2014-09-10 14:45:00', 'kontrakt-z-firma-yawal-sa', 'Kontrakt z firmą YAWAL S.A.', '<p>Informujemy, że w dniu 8 września 2014 r. sp&oacute;łka zależna 3FX sp. z o.o. w organizacji z siedzibą w Częstochowie zrealizowała zam&oacute;wienia dla firmy Yawal S.A. z siedzibą w Herbach, będącej jednym z największych dostawc&oacute;w architektonicznych system&oacute;w profili aluminiowych w Polsce.</p>\r\n', '<p>Sp&oacute;łka zależna 3FX sp. z o.o. zrealizowała zam&oacute;wienia dla firmy Yawal S.A., będącej jednym z największych dostawc&oacute;w architektonicznych system&oacute;w profili aluminiowych w Polsce.</p>\r\n', 0, 0, NULL, NULL, 0, '', ''),
(46, 0, '2014-09-10 14:45:00', 'zakoczenie-subskrypcji-prywatnej', 'Zakończenie subskrypcji prywatnej', '<p>Przekazaliśmy dziś do publicznej wiadomości informacje dotyczące zakończenia subskrypcji prywatnej akcji zwykłych na okaziciela serii D Emitenta Columbus Capital SA, emitowanych na podstawie Uchwały nr 1 Zarządu Columbus Capital S.A. z dnia 27 sierpnia 2014 roku w przedmiocie podwyższenia kapitału zakładowego w ramach kapitału docelowego o kwotę 700.000,00 zł (siedemset tysięcy złotych) w drodze emisji subskrypcji prywatnej.&nbsp;</p>\r\n\r\n<p>Z oczywistych względ&oacute;w to dla naszej sp&oacute;łki w tym roku jedno z najważniejszych wydarzeń i jednocześnie ogromny mandat zaufania inwestor&oacute;w finansowych dla naszego całego zespołu. Jest to r&oacute;wnież potwierdzenie słuszności naszej strategii na lata 2014 - 2016 i wyraz akceptacji dokonanych już inwestycji. Liczymy że jeszcze w tym roku pozyskamy nowe środki na nasze projekty, co wydarzy się być może już niebawem. Dziękujemy przy tej okazji wnikliwym inwestorom za słowa uznania i za wsparcie.&nbsp;</p>\r\n\r\n<p>Poniżej opis subskrypcji:</p>\r\n\r\n<p>1. Daty rozpoczęcia i zakończenia subskrypcji:</p>\r\n\r\n<p>Rozpoczęcie subskrypcji prywatnej akcji serii D nastąpiło w dniu 2 września 2014 r., natomiast jej zakończenie miało miejsce w dniu 8 września 2014 roku.</p>\r\n\r\n<p>2. Daty przydziału instrument&oacute;w finansowych Emitenta:</p>\r\n\r\n<p>Przydział akcji serii D nastąpił w momencie podpisania wszystkich um&oacute;w objęcia akcji serii D, co nastąpiło z dniem 8 września 2014 roku.</p>\r\n\r\n<p>3. Liczba instrument&oacute;w finansowych Emitenta objętych subskrypcją:</p>\r\n\r\n<p>Subskrypcją objętych było łącznie 700 000 (siedemset tysięcy) sztuk akcji zwykłych na okaziciela serii D o wartości nominalnej 1,00 (jeden złoty) każda.</p>\r\n\r\n<p>4. Stopa redukcji w poszczeg&oacute;lnych transzach w przypadku, gdy choć w jednej transzy liczba przydzielonych akcji była mniejsza od liczby instrument&oacute;w finansowych Emitenta, na kt&oacute;re złożono zapisy:</p>\r\n\r\n<p>Subskrypcja akcji zwykłych na okaziciela serii D przeprowadzona została bez redukcji.</p>\r\n\r\n<p>5. Liczba instrument&oacute;w finansowych Emitenta, kt&oacute;re zostały przydzielone w ramach przeprowadzonej subskrypcji:</p>\r\n\r\n<p>W ramach przeprowadzonej subskrypcji prywatnej zostało przydzielonych i objętych 700 000 (siedemset tysięcy) sztuk akcji zwykłych na okaziciela akcji serii D o wartości nominalnej 1,00 (jeden złoty) każda.</p>\r\n\r\n<p>6. Cena, po jakiej instrumenty finansowe Emitenta były nabywane:</p>\r\n\r\n<p>Akcji serii D w liczbie 700 000 (siedemset tysięcy) były obejmowane po cenie emisyjnej r&oacute;wnej 1,00 (jeden złoty) za jedną akcję.</p>\r\n\r\n<p>7. Liczba os&oacute;b, kt&oacute;re złożyły zapisy na instrumenty finansowe Emitenta objęte subskrypcją w poszczeg&oacute;lnych transzach:</p>\r\n\r\n<p>Oferty objęcia akcji serii D zostały skierowane do dw&oacute;ch os&oacute;b prawnych. Akcje serii D zostały wyemitowane z zamiarem wprowadzenia tych instrument&oacute;w finansowych do obrotu w alternatywnym systemie na rynku NewConnect. Wszyscy inwestorzy, kt&oacute;rym zaoferowano akcje serii D dokonali zapis&oacute;w na oferowane akcje.</p>\r\n\r\n<p>8. Liczba os&oacute;b, kt&oacute;rym przydzielono instrumenty finansowe Emitenta w ramach przeprowadzonej subskrypcji:</p>\r\n\r\n<p>Akcje serii D zostały przydzielone dw&oacute;m inwestorom, z kt&oacute;rymi następnie zostały zawarte umowy objęcia akcji serii D.</p>\r\n\r\n<p>9. Nazwy (firmy) subemitent&oacute;w, kt&oacute;rzy objęli instrumenty finansowe w ramach wykonywania um&oacute;w o subemisję, z określeniem liczby instrument&oacute;w finansowych, kt&oacute;re objęli, wraz z faktyczną ceną jednostki instrumentu finansowego (cena emisyjna lub sprzedaży, po odliczeniu wynagrodzenia za objęcie jednostki instrumentu finansowego, w wykonaniu umowy subemisji, nabytej przez subemitenta):</p>\r\n\r\n<p>Akcje serii D nie były obejmowane przez subemitent&oacute;w. Nie została zawarta żadna umowa o subemisję.</p>\r\n\r\n<p>10. Łączne określenie wysokości koszt&oacute;w, kt&oacute;re zostały zaliczone do koszt&oacute;w emisji, ze wskazaniem wysokości koszt&oacute;w według ich tytuł&oacute;w, w podziale przynajmniej na koszty:</p>\r\n\r\n<p>a) przygotowania i przeprowadzenia oferty,<br />\r\nb) wynagrodzenia subemitent&oacute;w, oddzielenie dla każdego z nich,<br />\r\nc) sporządzenia publicznego dokumentu informacyjnego lub dokumentu informacyjnego, z uwzględnieniem koszt&oacute;w doradztwa,<br />\r\nd) promocji oferty</p>\r\n\r\n<p>wraz z metodami rozliczenia tych koszt&oacute;w w księgach rachunkowych i sposobem ich ujęcia w sprawozdaniu finansowym Emitenta.</p>\r\n\r\n<p>Sp&oacute;łka Columbus Capital S.A. nie poniosła żadnych koszt&oacute;w związanych z ofertą emisji akcji serii D, za wyjątkiem koszt&oacute;w związanych z podwyższeniem kapitału zakładowego w łącznej kwocie 5.943,08 zł brutto.</p>\r\n', '<p>Przekazaliśmy dziś do publicznej wiadomości informacje dotyczące zakończenia subskrypcji prywatnej akcji zwykłych na okaziciela serii D.</p>\r\n', 0, 0, NULL, NULL, 0, '', ''),
(47, 0, '2014-09-19 14:45:00', 'rejestracja-spolki-zaleznej', 'Rejestracja spółki zależnej', '<article class="left-span1 span11 column">\r\n<p>W nawiązaniu do raportu bieżącego EBI nr 73/2014 z dnia 5 sierpnia 2014 r., Zarząd sp&oacute;łki Columbus Capital S.A. (dalej zwanej &quot;Emitentem&quot;, &rdquo;Sp&oacute;łka&rdquo;) z siedzibą w Katowicach informuje, że w dniu 16 września 2014 r. powziął informację o zarejestrowaniu w rejestrze przedsiębiorc&oacute;w KRS sp&oacute;łki zależnej pod firmą 3FX z o.o. Sp&oacute;łka 3FX sp. z o.o. została zarejestrowana pod numerem KRS 0000523728. Siedzibą Sp&oacute;łki jest miasto Częstochowa. Kapitał zakładowy wynosi 6.000 zł (sześć tysięcy złotych) i dzieli się na 120 (sto dwadzieścia) r&oacute;wnych udział&oacute;w. Pozostałymi wsp&oacute;lnikami jest 5 os&oacute;b fizycznych niepowiązanych osobowo z Emitentem.</p>\r\n\r\n<p>Columbus Capital S.A. objął 40 (czterdzieści) udział&oacute;w o wartości nominalnej 50 zł (pięćdziesiąt złotych) każdy, o łącznej wartości nominalnej 2.000 zł (dwa tysiące złotych), co stanowi 33,33 % w kapitale zakładowym Sp&oacute;łki. Emitent posiada bezpośrednią kontrolę nad Sp&oacute;łką wynikającą z uprawnień osobistych do powoływania Rady Nadzorczej, a przez to i Zarządu celowej sp&oacute;łki technologicznej.</p>\r\n\r\n<p>Przedmiotem działalności sp&oacute;łki 3FX sp. z o.o. jest realizowanie wsp&oacute;lnej inwestycji w rozw&oacute;j technologii 3D, produkcji sprzętu drukującego w technologii 3D, przejęcia portali tematycznych, obejmujących szeroki zakres zagadnień druku i technologii 3D i dalszego rozwoju technologii 3D, jej promocji na świecie oraz wszelkich czynności mających na celu zwiększanie wartości nowo powołanego podmiotu. Zamierzeniem Założycieli jest - po osiągnięciu przez Sp&oacute;łkę zależną odpowiedniego poziomu rozwoju i zdolności produkcyjnej oraz zwiększeniu jej wartości - przekształcenie jednostki w Sp&oacute;łkę Akcyjną i wprowadzenie jej akcji do Alternatywnego Systemu Obrotu NewConnect.</p>\r\n</article>\r\n', '<p>COLUMBUS CAPITAL SA informuje, że w dniu 16 września 2014 r. powziął informację o zarejestrowaniu w rejestrze przedsiębiorc&oacute;w KRS sp&oacute;łki zależnej pod firmą 3FX z o.o. Sp&oacute;łka 3FX sp. z o.o. została zarejestrowana pod numerem KRS 0000523728.</p>\r\n', 0, 0, NULL, NULL, 0, '', ''),
(48, 0, '2014-10-27 14:45:00', 'prezentacja-columbus-energy-sa', 'Prezentacja Columbus Energy SA', '<p>Zapraszamy do zapoznania się z aktualną prezentacją naszej Sp&oacute;łki zależnej, Columbus Energy SA.<br />\r\n<br />\r\n<a href="http://columbusenergy.pl/ColumbusEnergySA_102014.pdf">http://columbusenergy.pl/ColumbusEnergySA_102014.pdf</a></p>\r\n', '<p>Zapraszamy do zapoznania się z aktualną prezentacją naszej Sp&oacute;łki zależnej, Columbus Energy SA.</p>\r\n', 0, 0, NULL, NULL, 0, '', ''),
(49, 0, '2014-11-21 14:45:00', 'podpisanie-porozumienia-inwestycyjnego-z-jr-invest-sa', 'Podpisanie porozumienia inwestycyjnego z JR INVEST S.A.', '<article class="left-span1 span11 column">\r\n<p>Informujemy, iż w dniu 20 listopada 2014 roku podpisaliśmy z&nbsp;JR INVEST&nbsp;S.A. z siedzibą w Krakowie Porozumienie Inwestycyjne, w kt&oacute;rym to&nbsp;Strony zgodnie ustaliły, iż&nbsp;COLUMBUS CAPITAL S.A.&nbsp;jest zainteresowana&nbsp;&nbsp;nabyciem wszystkich posiadanych przez&nbsp;JR INVEST&nbsp;&nbsp;S.A.udział&oacute;w w Sp&oacute;łce zależnej GANADOR Sp. z o.o. z siedzibą w Krakowie zarejestrowanej pod numerem KRS 0000497726.</p>\r\n\r\n<p>Na mocy zawartego Porozumienia Inwestycyjnego Zarząd&nbsp;COLUMBUS CAPITAL&nbsp;S.A. zobowiązał się do uzyskania zgody Rady Nadzorczej na przeprowadzenie transakcji nabycia przez Emitenta od&nbsp;JR INVEST&nbsp;S.A. wszystkich udział&oacute;w w Sp&oacute;łce GANADOR Sp. z o.o. oraz zatwierdzenia formy płatności za wszystkie udziały w spos&oacute;b, kt&oacute;ry zostanie określony w Umowie.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>GANADOR Sp. z o.o. z siedzibą w Krakowie&nbsp;działa w formie funduszu nieruchomości, kt&oacute;ry kupuje nieruchomości komercyjne o wartości do 1 mln zł, a następnie w kr&oacute;tkim okresie czasu odsprzedaje je z dużą marżą. Sp&oacute;łka obecnie wchodzi w skład Grupy Kapitałowej JR INVEST S.A. i korzysta z doświadczenia w nabywaniu oraz w zbywaniu nieruchomości komercyjnych nabytego na przestrzeni kilkunastu lat. Dzięki temu realizowane transakcje charakteryzują się wysokim stopniem bezpieczeństwa i niskim stopniem ryzyka przy jednoczesnym wysokim poziomie marż. Taki model biznesowy daje Sp&oacute;łce możliwość nabywania tylko sprawdzonych i pewnych nieruchomości komercyjnych r&oacute;wnocześnie generując duże zyski. Wskutek tego nabywca, kt&oacute;ry kupuje od Sp&oacute;łki daną nieruchomość ma pewność, że stanie się właścicielem nieruchomości zweryfikowanej i o uregulowanym stanie prawnym, ponieważ była ona uprzednio dogłębnie zbadana przez ekspert&oacute;w w swojej dziedzinie. Gł&oacute;wnym założeniem funduszu nieruchomości GANADOR Sp. z o.o. jest kupowanie nieruchomości komercyjnych tylko za got&oacute;wkę, nie korzystając z kredyt&oacute;w bankowych. W ten spos&oacute;b działalność Sp&oacute;łki jest stabilna, a nabywane nieruchomości są nieobciążone. Sp&oacute;łka nie będąc pod presją ze strony banku sama decyduje, kiedy i co kupuje oraz sprzedaje. Rozw&oacute;j funduszu nieruchomości GANADOR Sp. z o.o. zaplanowany jest długofalowo, a dynamika tego rozwoju konsekwentnie będzie zwiększana dzięki zaufaniu inwestor&oacute;w i powiększaniu generowanych zysk&oacute;w. Fundusz planuje być sp&oacute;łką dywidendową.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Szczeg&oacute;ły&nbsp;transakcji zostaną precyzyjnie określone w&nbsp;Umowie nabycia przez&nbsp;COLUMBUS CAPITAL&nbsp;S.A.&nbsp;od&nbsp;JR INVEST S.A. udział&oacute;w w Sp&oacute;łce&nbsp;GANADOR sp. z o.o., kt&oacute;rej termin&nbsp;realizacji&nbsp;określono&nbsp;do dnia 31 marca 2015 r.&nbsp;W przypadku nie zawarcia Umowy w terminie do dnia 31.03.2015 r. niniejsze Porozumienie Inwestycyjne ulegnie automatycznemu rozwiązaniu.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Niniejsza informacja stanowi realizację strategii i rozwoju przyjętej i realizowanej przez Sp&oacute;łkę&nbsp;COLUMBUS CAPITAL&nbsp;S.A. poprzez zaangażowanie Sp&oacute;łki w kolejny projekt inwestycyjny. W opinii Zarządu Sp&oacute;łki, zrealizowanie transakcji, będącej przedmiotem wyżej wskazanego Porozumienia Intencyjnego, wpłynie pozytywnie na przychody i wycenę Emitenta oraz spowoduje rozw&oacute;j grupy kapitałowejCOLUMBUS CAPITAL&nbsp;S.A.</p>\r\n</article>\r\n', '<p>Strony zgodnie ustaliły, iż COLUMBUS CAPITAL S.A. jest zainteresowana nabyciem wszystkich posiadanych przez JR INVEST S.A. udział&oacute;w w Sp&oacute;łce zależnej GANADOR Sp. z o.o.</p>\r\n', 0, 0, NULL, NULL, 0, '', ''),
(50, 0, '2015-01-07 14:45:00', 'zgoda-na-wprowadzenie-akcji-columbus-energy-sa-na-newconnect', 'Zgoda na wprowadzenie akcji Columbus Energy S.A. na NewConnect', '<article class="left-span1 span11 column">\r\n<p>Informujemy, iż w dniu 30 grudnia 2014r. Walne Zgromadzenie Akcjonariuszy naszej sp&oacute;łki zależnej Columbus Energy S.A., wyraziło zgodę na ubieganie się o wprowadzenie akcji Columbus Energy S.A. serii C i E oraz akcji serii D i PDA dla akcji serii D do obrotu na rynku NewConnect. Ponadto akcjonariusze wyrazili zgodę na złożenie akcji Sp&oacute;łki serii C i E oraz akcji serii D i PDA dla akcji serii D do depozytu prowadzonego przez firmę inwestycyjną oraz dokonanie dematerializacji akcji Sp&oacute;łki serii C i E oraz akcji serii D i PDA dla akcji serii D w rozumieniu przepis&oacute;w ustawy z dnia 29 lipca 2005 r. o obrocie instrumentami finansowymi. Nadzwyczajne Walne Zgromadzenie upoważniło r&oacute;wnież Zarząd Sp&oacute;łki zależnej Columbus Energy S.A. do podjęcia wszelkich czynności faktycznych i prawnych, kt&oacute;re będą zmierzały do wprowadzenia akcji Sp&oacute;łki serii C i E oraz akcji serii D i PDA dla akcji serii D do obrotu zorganizowanego w Alternatywnym Systemie Obrotu rynku NewConnect, oraz do zawarcia z KDPW wszystkich niezbędnych um&oacute;w, dotyczących rejestracji w depozycie, prowadzonym przez KDPW, akcji serii C i E oraz akcji serii D i PDA dla akcji serii D. Debiut akcji serii C i E planowany jest po 01 stycznia 2017 r. Akcjonariusze sp&oacute;łki zależnej Columbus Energy S.A. wyrazili zgodę na ubieganie się o dopuszczenie i wprowadzenie akcji Sp&oacute;łki zależnej Columbus Energy S.A. na okaziciela, istniejących i wyemitowanych przez Sp&oacute;łkę zależną w przyszłości, do obrotu na rynku regulowanym organizowanym przez Giełdę Papier&oacute;w Wartościowych w Warszawie S.A. Ponadto COLUMBUS CAPITAL S.A. korzystając ze swojego statutowego uprawnienia powołał do Rady Nadzorczej naszej sp&oacute;łki zależnej Columbus Energy dw&oacute;ch nowych członk&oacute;w w osobach: Izabela Dorosińska oraz Dominik Walig&oacute;rski i obecnie liczba członk&oacute;w Rady Nadzorczej wynosi 5 os&oacute;b i jej członkami są:</p>\r\n\r\n<p>- Pani Dorota Denis &ndash; Brewczyńska, Przewodnicząca Rady Nadzorczej oraz pozostali Członkowie Rady Nadzorczej</p>\r\n\r\n<p>- Pani Elżbieta Zielińska</p>\r\n\r\n<p>- Pani Izabela Dorosińska</p>\r\n\r\n<p>- Pan Łukasz Kaleta</p>\r\n\r\n<p>- Pan Dominik Walig&oacute;rski</p>\r\n\r\n<p>​Obecnie nasza sp&oacute;łka zależna, Columbus Energy S.A. podejmuje zgodnie z uchwałami wszelkie czynności aby ​niezwłocznie po zakończeniu rozm&oacute;w z inwestorami, kt&oacute;rzy planują objąć akcje serii D doprowadzić do debiutu na rynku NewConnect akcji naszej sp&oacute;łki zależnej oraz p&oacute;źniej na rynku regulowanym organizowanym przez Giełdę Papier&oacute;w Wartościowych w Warszawie S.A.</p>\r\n</article>\r\n', '<p>Walne Zgromadzenie Akcjonariuszy naszej sp&oacute;łki zależnej Columbus Energy S.A., wyraziło zgodę na ubieganie się o wprowadzenie akcji Columbus Energy S.A. do obrotu na rynku NewConnect.</p>\r\n', 0, 0, NULL, NULL, 0, '', ''),
(51, 0, '2015-01-31 14:45:00', 'zwolanie-nadzwyczajnego-walnego-zgromadzenia-emitenta', 'Zwołanie Nadzwyczajnego Walnego Zgromadzenia Emitenta', '<article class="left-span1 span11 column">\r\n<p>Zarząd sp&oacute;łki Columbus Capital Sp&oacute;łka Akcyjna z siedzibą w Katowicach (&quot;Sp&oacute;łka&quot;, &bdquo;Emitent&rdquo;) w związku z żądaniem akcjonariusza, posiadającego ponad 5 % głos&oacute;w na walnym zgromadzeniu, zwołania Nadzwyczajnego Walnego Zgromadzenia Sp&oacute;łki, o kt&oacute;rym Emitent informował raportem bieżącym EBI nr 6/2015 z dnia 16 stycznia 2015 r. informuje o zwołaniu na dzień 29 maja 2015 roku Nadzwyczajnego Walnego Zgromadzenia, kt&oacute;re odbędzie się o godzinie 10:00 w Krakowie przy ul. Kuźnicy Kołłątajowskiej 13 lok. 210.&nbsp;</p>\r\n\r\n<p><a href="http://newconnect.pl/index.php?page=1045&amp;ph_main_content_start=show&amp;id=75350&amp;ncc_index=CLC&amp;id_tr=1">http://newconnect.pl/index.php?page=1045&amp;ph_main_content_start=show&amp;id=75350&amp;ncc_index=CLC&amp;id_tr=1</a></p>\r\n</article>\r\n', '<p>Columbus Capital S.A. informuje o zwołaniu na dzień 29 maja 2015 roku Nadzwyczajnego Walnego Zgromadzenia, kt&oacute;re odbędzie się o godzinie 10:00 w Krakowie przy ul. Kuźnicy Kołłątajowskiej 13 lok. 210.</p>\r\n', 0, 0, NULL, NULL, 0, '', '');
INSERT INTO `ofi_news` (`id`, `category_id`, `datetime`, `url`, `title`, `content`, `short_content`, `view_count`, `hidden`, `header_image`, `thumbnail`, `only_logged`, `description`, `keywords`) VALUES
(52, 0, '2015-07-17 14:30:00', 'columbus-capital-sa-podpisuje-porozumienie-w-sprawie-polaczenia', 'Columbus Capital S.A. podpisuje porozumienie w sprawie połączenia', '<p>Columbus Capital S.A., Sp&oacute;łka notowana na rynku NewConnect, zawarła porozumienie o podstawowych warunkach transakcji połączenia z Columbus Energy S.A. Jest to zarazem kolejny krok w kierunku przeprowadzenia fuzji tych dw&oacute;ch podmiot&oacute;w.</p>\r\n\r\n<p>Podpisanie porozumienia przez obie strony transakcji stanowi kontynuację zawartego przez nie listu intencyjnego. Celem planowanego połączenia jest powstanie mocnego podmiotu działającego w branży Odnawialnych Źr&oacute;deł Energii oraz przeniesienie notowań akcji na rynek regulowany GPW w Warszawie. Po przeprowadzonej transakcji Sp&oacute;łka zmieni nazwę na Columbus Holding S.A.</p>\r\n\r\n<p>&bdquo;Podpisanie porozumienia o podstawowych warunkach transakcji połączenia Columbus Capital S.A. z Columbus Energy S.A. jest następstwem zawarcia przez obie sp&oacute;łki listu intencyjnego. Term Sheet reguluje gł&oacute;wne założenia odnośnie ustalenia parytetu połączenia oraz wartości dw&oacute;ch sp&oacute;łek. Pozytywna finalizacja transakcji pozwoli nam zbudować jeden z największych podmiot&oacute;w w branży fotowoltaiki w Polsce. Będziemy także rozważali zmianę rynku notowań naszych akcji na rynek regulowany.&rdquo; - komentuje January Ciszewski, Prezes Zarządu Sp&oacute;łki Columbus Capital S.A.</p>\r\n\r\n<p>Strony ustaliły r&oacute;wnież spos&oacute;b określenia parytetu wartości sp&oacute;łek. W przypadku Columbus Capital S.A. będzie to średnia cena akcji na zamknięciu notowań z ostatnich 30-stu dni sesyjnych od dnia zawarcie porozumienia do ostatniego dnia miesiąca poprzedzającego miesiąc, w kt&oacute;rym podpisany zostanie plan połączenia. Natomiast wartość Columbus Energy S.A. zostanie ustalona na podstawie wyceny metodą zdyskontowanych przepływ&oacute;w pieniężnych (DCF) na ostatni dzień miesiąca, kt&oacute;ry poprzedza podpisanie przez strony porozumienia. Uzyskane wyceny zostaną skorygowane uwzględniając pozycję rynkową obu podmiot&oacute;w, a także płynność obrotu akcjami Columbus Capital S.A. Dla Akcjonariuszy Emitenta przewidziana została r&oacute;wnież premia w wysokości 30% wartości akcji podczas ustalenia partytetu. W ramach połączenia akcjonariusze Columbus Energy S.A. otrzymają akcje Columbus Capital S.A. nowej emisji serii E.</p>\r\n\r\n<p>&bdquo;Będę dążył do tego, aby wypracowany parytet wyceny obu sp&oacute;łek odzwierciedlał ich rzeczywistą wartość. Akcjonariusze Columbus Capital S.A. będą mogli liczyć na otrzymanie premii w wycenie, kt&oacute;ra będzie wynikała z faktu, że sp&oacute;łka jest notowana na rynku NewConnect, a także należy do indeksu NCIndex30, co świadczy o wysokiej płynności obrotu jej akcjami. Jako Columbus Holding S.A. zamierzamy nadal prowadzić działalność inwestycyjną, więc nie wykluczam, że będziemy chcieli funkcjonować jako Grupa Kapitałowa, tak by poszczeg&oacute;lne podmioty realizowały osobne projekty inwestycyjne.&rdquo; - podsumowuje Ciszewski.</p>\r\n\r\n<p>Columbus Capital S.A. zakończyła 2014 r. skonsolidowanym zyskiem netto wynoszącym 936 tys. zł. Plany Emitenta zakładają poszukiwanie nowych projekt&oacute;w inwestycyjnych oraz zbudowanie nowej strategii rozwoju, kt&oacute;ra pozwoli zmaksymalizować wykorzystanie potencjału zasob&oacute;w Sp&oacute;łki.</p>\r\n', '<p>Columbus Capital S.A., Sp&oacute;łka notowana na rynku NewConnect, zawarła porozumienie o podstawowych warunkach transakcji połączenia z Columbus Energy S.A. Jest to zarazem kolejny krok w kierunku przeprowadzenia fuzji tych dw&oacute;ch podmiot&oacute;w.<br />\r\n&nbsp;</p>\r\n', 0, 0, NULL, NULL, 0, '', ''),
(53, 0, '2015-07-14 14:30:00', 'columbus-capital-sa-blisko-fuzji-z-oze', 'Columbus Capital są blisko fuzji z OZE', '<p>Columbus Capital S.A., Sp&oacute;łka notowana na rynku NewConnect, podpisała w dniu 13.07.2015 r. list intencyjny dotyczący połączenia ze Sp&oacute;łką Columbus Energy S.A. Celem potencjalnej fuzji jest powstanie mocnego podmiotu w branży OZE oraz zmiana rynku notowań akcji na rynek regulowany GPW w Warszawie.</p>\r\n\r\n<p>Podpisanie listu intencyjnego otwiera proces due diligence obu Sp&oacute;łek, a także umożliwia rozpoczęcie prac nad opracowaniem porozumienia w sprawie podstawowych warunk&oacute;w transakcji. Planowane połączenie ma nastąpić poprzez przeniesienie całego majątku Columbus Energy S.A. jako sp&oacute;łki przejmowanej na Columbus Capital S.A. jako sp&oacute;łki przejmującej w zamian za wydanie dotychczasowym akcjonariuszom Columbus Energy S.A. akcji Columbus Capital S.A. Połączenie sp&oacute;łek zostanie przeprowadzone po ustaleniu parytetu wymiany akcji i pod warunkiem wyrażenia zgody przez Walne Zgromadzenie Akcjonariuszy każdej ze Sp&oacute;łek.</p>\r\n\r\n<p>&bdquo;Podpisanie listu intencyjnego umożliwia nam rozpoczęcie badania obu sp&oacute;łek w celu ustalenia ich wyceny oraz parytetu wymiany akcji. Do dnia 15.07.2015 r. podpiszemy porozumienie w sprawie ustalenia podstawowych warunk&oacute;w planowanej transakcji. Jestem przekonany, że połączenie przyniosłoby korzyści obu podmiotom dzięki wykorzystaniu synergii płynącej z ich potencjału. Moim gł&oacute;wnym celem będzie stworzenie jak najlepszych warunk&oacute;w transakcji dla naszych Akcjonariuszy i temu będę podporządkowywał wszelkie działania oraz decyzje.&rdquo; - m&oacute;wi January Ciszewski, Prezes Zarządu Sp&oacute;łki Columbus Capital S.A.</p>\r\n\r\n<p>Sp&oacute;łka Columbus Energy S.A. pozyskała w ostatnim czasie ponad 5 mln zł kapitału z emisji akcji serii D oraz F, kt&oacute;ry przeznaczy na realizowanie założeń strategii rozwoju. Powstały po połączeniu podmiot ma zagospodarować efekty synergii wynikające m.in. z wykorzystania oraz wzajemnego uzupełnienia potencjału obu firm, optymalizacji i zwiększenia skali działania, a także istotnej redukcji koszt&oacute;w działalności. Warto r&oacute;wnież dodać, że zamiarem Zarząd&oacute;w obu sp&oacute;łek jest ubieganie się przez nowy podmiot o dopuszczenie jego akcji do obrotu na rynku regulowanym GPW w Warszawie.</p>\r\n\r\n<p>&bdquo;Sp&oacute;łka Columbus Energy S.A., jej model biznesowy oraz perspektywy rozwoju są nam dobrze znane, bowiem byliśmy jednymi z pierwszych inwestor&oacute;w, kt&oacute;rzy zdecydowali się zainwestować w to przedsięwzięcie. Udany start Programu PROSUMENT oraz duża liczba podpisanych w nim um&oacute;w pozwalają z optymizmem spoglądać na przyszłość tego podmiotu. Sp&oacute;łka z sukcesem pozyskuje r&oacute;wnież kolejne umowy w ramach taryf gwarantowanych, zapisanych w nowej Ustawie o OZE. Uważam, że potencjalne połączenie z Columbus Capital S.A. może przyczynić się do jeszcze szybszego rozwoju Columbus Energy S.A., co przyniesie oczywiste korzyści Akcjonariuszom obydwu podmiot&oacute;w. Liczę r&oacute;wnież, że uda nam się zrealizować nasz cel, jakim jest debiut nowego podmiotu na rynku regulowanym.&rdquo; - dodaje Ciszewski.</p>\r\n\r\n<p>Columbus Capital S.A. zakończyła 2014 r. skonsolidowanym zyskiem netto wynoszącym 936 tys. zł. Plany Emitenta zakładają poszukiwanie nowych projekt&oacute;w inwestycyjnych oraz zbudowanie nowej strategii rozwoju, kt&oacute;ra pozwoli zmaksymalizować wykorzystanie potencjału zasob&oacute;w Sp&oacute;łki.</p>\r\n', '<p>Columbus Capital S.A., Sp&oacute;łka notowana na rynku NewConnect, podpisała w dniu 13.07.2015 r. list intencyjny dotyczący połączenia ze Sp&oacute;łką Columbus Energy S.A. Celem potencjalnej fuzji jest powstanie mocnego podmiotu w branży OZE oraz zmiana rynku notowań akcji na rynek regulowany GPW w Warszawie.</p>\r\n', 0, 0, NULL, NULL, 0, '', ''),
(54, 0, '2015-07-03 14:30:00', 'columbus-capital-przygotuje-nowa-strategie-rozwoju', 'COLUMBUS CAPITAL przygotuje nową strategię rozwoju', '<p>COLUMBUS CAPITAL S.A., Sp&oacute;łka notowana na rynku NewConnect, poinformowała o powołaniu Januarego Ciszewskiego na stanowisko Prezesa Zarządu. Zadaniem nowego Prezesa będzie opracowanie nowej strategii rozwoju oraz uporządkowanie Grupy Kapitałowej.</p>\r\n\r\n<p>W dniu 30.06.2015 r. Rada Nadzorcza COLUMBUS CAPITAL S.A. zdecydowała o powołaniu na stanowisko Prezesa Zarządu Januarego Ciszewskiego. Jest on obecnie największym akcjonariuszem Sp&oacute;łki, bowiem posiada akcje stanowiące 36,4% udział&oacute;w w jej kapitale zakładowym. Nowy Prezes będzie odpowiedzialny za przygotowanie nowej strategii rozwoju, kt&oacute;ra wkr&oacute;tce powinna zostać przedstawiona inwestorom.</p>\r\n\r\n<p>&bdquo;W najbliższym czasie chciałbym prowadzić działania, kt&oacute;re pozwolą zbudować nową strategię rozwoju Sp&oacute;łki i dokończyć jej restrukturyzację. Będę z pewnością dążył do rozwijania naszych przedsięwzięć oraz poszukiwania nowych interesujących projekt&oacute;w inwestycyjnych. Moje doświadczenie w zakresie zarządzania projektami oraz finansowania inwestycji na pewno pomoże mi zwiększać wartość Sp&oacute;łki.&rdquo; - stwierdza January Ciszewski, Prezes Zarządu Sp&oacute;łki COLUMBUS CAPITAL S.A.</p>\r\n\r\n<p>W tym roku Emitent dokonał sprzedaży posiadanych udział&oacute;w w sp&oacute;łce 3FX Sp. z o.o., a także w podmiocie zajmującym się handlem produktami elektronicznymi. Sp&oacute;łka posiada udziały w Columbus Tax &amp; Law Sp. z o.o., a jej najważniejszym aktywem pozostają akcje Columbus Energy S.A., działającej w branży Odnawialnych Źr&oacute;deł Energii.</p>\r\n\r\n<p>&bdquo;Inwestorzy dostrzegają dobre perspektywy dla COLUMBUS CAPITAL S.A., o czym świadczy bardzo wysoka płynność obrotu jej akcji na rynku NewConnect. Dzięki temu Sp&oacute;łka trafiła do indeksu NCIndex30, kt&oacute;ry grupuje najpłynniejsze sp&oacute;łki z alternatywnego rynku. Jestem przekonany, że wyb&oacute;r odpowiedniego modelu biznesowego przez nowy Zarząd będzie miał pozytywny wpływ na nasze wyniki finansowe w kolejnych latach.&rdquo; - dodaje Ciszewski.</p>\r\n\r\n<p>COLUMBUS CAPITAL S.A. zakończyła 2014 r. skonsolidowanym zyskiem netto wynoszącym 936 tys. z</p>\r\n', '<p>COLUMBUS CAPITAL S.A., Sp&oacute;łka notowana na rynku NewConnect, poinformowała o powołaniu Januarego Ciszewskiego na stanowisko Prezesa Zarządu. Zadaniem nowego Prezesa będzie opracowanie nowej strategii rozwoju oraz uporządkowanie Grupy Kapitałowej.</p>\r\n', 0, 0, NULL, NULL, 0, '', ''),
(55, 0, '2015-03-13 14:30:00', 'glowni-akcjonariusze-zawieraja-ugode', 'Główni akcjonariusze zawierają ugodę', '<p>COLUMBUS CAPITAL S.A., Sp&oacute;łka notowana na rynku NewConnect, poinformowała w dniu wczorajszym o zawarciu ugody przez jej gł&oacute;wnych akcjonariuszy. Na jej podstawie January Ciszewski dokona zakupu 4.200.000 szt. akcji Sp&oacute;łki należących do Doroty Denis-Brewczyńskiej i stanie się tym samym jej większościowym akcjonariuszem.</p>\r\n\r\n<p>Zawarta ugoda pozwoli Sp&oacute;łce na kontynuowanie realizacji strategii rozwoju oraz na rozwiązanie obecnej sytuacji w jej akcjonariacie. Podczas zwołanego na dzień 08.04.2015 r. NWZA COLUMBUS CAPITAL S.A., akcjonariusze podejmą uchwały w sprawie wyrażenia zgody na zbycie akcji objętych umową lock-up, kt&oacute;re należą do Doroty Denis-Brewczyńskiej oraz o zmianach w składzie Rady Nadzorczej Sp&oacute;łki. Akcjonariusze zadecydują r&oacute;wnież o przeniesieniu siedziby Emitenta do Krakowa.</p>\r\n\r\n<p>&bdquo;Po wielu rozmowach doszliśmy do kompromisu, czego efektem jest podpisana ugoda, kt&oacute;ra pozwoli na przyspieszenie tempa rozwoju Sp&oacute;łki. Columbus Capital skupi się na odnawialnych źr&oacute;dłach energii, wykorzystując do tego potencjał Columbus Energy S.A. Cieszę się, że sytuacja się wyjaśniła, ponieważ jest to korzystne dla dobra Sp&oacute;łki i jej akcjonariuszy, a ja dołożę wszelkich starań, żeby dalej budować jej wartość, co na pewno przełoży się w przyszłości na kurs akcji.&rdquo; - komentuje January Ciszewski.</p>\r\n\r\n<p>January Ciszewski po nabyciu 4.200.000 szt. akcji COLUMBUS CAPITAL S.A. zostanie większościowym akcjonariuszem Sp&oacute;łki, co będzie gwarantowało jej dalszy stabilny rozw&oacute;j zgodnie z założeniami przyjętej strategii. Emitent dokona także sprzedaży wszystkich posiadanych udział&oacute;w w sp&oacute;łce 3FX Sp. z o.o. i skoncentruje się na rozwoju Columbus Energy S.A., podmiotu zajmującego się sprzedażą instalacji fotowoltaicznych i Raportami Efektywności Energetycznej, kt&oacute;rego posiada 1.000.000 szt. akcji.</p>\r\n\r\n<p>W związku z podpisaniem przez Prezydenta RP nowej Ustawy o Odnawialnych Źr&oacute;dłach Energii, perspektywy dla branży OZE w Polsce wydają się być obecnie niezwykle dobre. Istotnym aspektem dla Columbus Energy S.A. jest r&oacute;wnież przyjęcie poprawki dotyczącej prosument&oacute;w, kt&oacute;ra zakłada wprowadzenie taryf gwarantowanych za sprzedaż do sieci produkowanej przez nich energii. W najbliższym czasie rozpoczną się też nabory na dofinansowania w ramach Programu Prosument, finansowanego ze środk&oacute;w Narodowego Funduszu Ochrony Środowiska i Gospodarki Wodnej. Jego gł&oacute;wnym celem jest promowanie nowych technologii OZE oraz postaw prosumenckich, a także rozw&oacute;j rynku dostawc&oacute;w urządzeń i instalator&oacute;w.</p>\r\n\r\n<p>&bdquo;Wejście w życie nowej Ustawy o Odnawialnych Źr&oacute;dłach Energii, a szczeg&oacute;lnie zapis&oacute;w dotyczących prosument&oacute;w, to dla nas bardzo dobra informacja. Mocno liczymy także na Program Prosument, kt&oacute;ry będzie finansowany ze środk&oacute;w NFOŚiGW, a już wkr&oacute;tce ruszą nabory na dofinansowania projekt&oacute;w. Nadchodzące lata powinny być więc dla branży OZE bardzo udane i zamierzamy to wykorzystać.&rdquo; - dodaje Dawid Zieliński, Prezes Zarządu Sp&oacute;łki COLUMBUS CAPITAL S.A.</p>\r\n', '<p>COLUMBUS CAPITAL S.A., Sp&oacute;łka notowana na rynku NewConnect, poinformowała w dniu wczorajszym o zawarciu ugody przez jej gł&oacute;wnych akcjonariuszy. Na jej podstawie January Ciszewski dokona zakupu 4.200.000 szt. akcji Sp&oacute;łki należących do Doroty Denis-Brewczyńskiej i stanie się tym samym jej większościowym akcjonariuszem.</p>\r\n', 0, 0, NULL, NULL, 0, '', ''),
(56, 0, '2015-01-20 14:45:00', 'columbus-capital-z-dobrymi-perspektywami-dzieki-ustawie-o-oze', ' 		 	 COLUMBUS CAPITAL z dobrymi perspektywami dzięki Ustawie o OZE', '<p>Columbus Capital S.A., Sp&oacute;łka notowana na rynku NewConnect, kt&oacute;ra posiada 1.000.000 szt. akcji Columbus Energy S.A., podmiotu zajmującego się sprzedażą instalacji fotowoltaicznych i Raportami Efektywności Energetycznej, spodziewa się, że dzięki przegłosowaniu w miniony piątek przez Sejm nowej Ustawy o Odnawialnych Źr&oacute;dłach Energii odniesie ona znaczące korzyści. Najważniejsze dla rozwoju Sp&oacute;łki są także przyjęte poprawki dotyczące prosument&oacute;w.</p>\r\n\r\n<p>W poprzedni piątek, tj. 16. stycznia br., Sejm większością głos&oacute;w przegłosował projekt nowej Ustawy o Odnawialnych Źr&oacute;dłach Energii, na kt&oacute;rą cała branża czekała już kilka lat. Jedną z kluczowych zmian w stosunku do obecnie obowiązujących przepis&oacute;w prawa dotyczących wspierania OZE jest zmiana systemu świadectw pochodzenia energii na system aukcyjny. Dla Columbus Energy S.A. bardzo ważna jest przede wszystkim przyjęta poprawka dotycząca prosument&oacute;w, kt&oacute;ra wprowadza taryfy gwarantowane za sprzedaż do sieci produkowanej przez nich energii.</p>\r\n\r\n<p>&bdquo;Dzięki taryfom gwarantowanym uchwalonym w Ustawie o OZE zmieni się bardzo wiele. Znaczenie konsumpcji własnej maleje, bo będziemy mieć dzięki nowym przepisom pozorny &bdquo;net-metering&rdquo;, kt&oacute;ry pozwoli na sprzedaż energii z nadprodukcji po godziwej cenie. Oczywiście liczymy na to, że zapisy te przejdą w Senacie i Prezydent RP podpisze Ustawę. Niewątpliwie jest to bardzo duży krok dla prosument&oacute;w, gdzie jesteśmy w stanie jako Columbus Energy S.A. proponować bardzo zyskowne rozwiązania w zakresie fotowoltaiki.&rdquo; - m&oacute;wi Dawid Zieliński, Prezes Zarządu Sp&oacute;łki Columbus Capital S.A.</p>\r\n\r\n<p>W okresie od lipca do grudnia 2014 r. Columbus Energy S.A. podpisała oraz częściowo zrealizowała 338 umowy sprzedażowe na instalacje fotowoltaiczne oraz Raporty Efektywności Energetycznej i Audyty fotowoltaiczne. Łączna wartość zawartych um&oacute;w przekroczyła 12,64 mln zł netto. Tak wysoka sprzedaż była możliwa dzięki efektywnemu wykorzystaniu autorskiego systemu sprzedaży opartego o edukację klient&oacute;w w zakresie energetyki.</p>\r\n\r\n<p>Ten rok powinien być dla Sp&oacute;łki jeszcze lepszy gł&oacute;wnie z uwagi na wejście w życie nowej Ustawy o OZE oraz rozpoczęcie nabor&oacute;w projekt&oacute;w do dofinansowania w ramach Programu Prosument, kt&oacute;ry jest finansowany ze środk&oacute;w Narodowego Funduszu Ochrony Środowiska i Gospodarki Wodnej. Jego gł&oacute;wnym celem jest promowanie nowych technologii OZE oraz postaw prosumenckich, a także rozw&oacute;j rynku dostawc&oacute;w urządzeń i instalator&oacute;w. Beneficjentami Programu będą osoby fizyczne, sp&oacute;łdzielnie mieszkaniowe, wsp&oacute;lnoty mieszkaniowe oraz jednostki samorządu terytorialnego i ich związki. Instrumentami finansowania w ramach Prosumenta są dotacje w wysokości 40% wraz z preferencyjnym kredytem oprocentowanym na poziomie 1% w skali roku na 15 lat.</p>\r\n\r\n<p>&bdquo;Strategia rozwoju Sp&oacute;łki Columbus Energy S.A. oparta jest gł&oacute;wnie o Program Prosument, gdzie przez najbliższe lata będziemy wykonywać tysiące instalacji w domach jednorodzinnych, zdobywając dużą część rynku. Liczymy na to, że nasza ciężka roczna praca nad budową działu technicznego oraz doświadczenia w zakresie energetyki odnawialnej przyniesie w 2015 roku wielki efekt w postaci zaufania i zadowolenia klient&oacute;w, a co za tym idzie kontrakt&oacute;w na lata.&rdquo; - dodaje Zieliński.</p>\r\n\r\n<p>COLUMBUS CAPITAL S.A. jest Sp&oacute;łką notowaną na rynku NewConnect od maja 2011 r. i w najbliższym czasie chce być nastawiona na wspieranie projekt&oacute;w innowacyjnych oraz na realizację strategii inwestycyjnych, opartych przede wszystkim na fundamentalnej wycenie niedowartościowanych projekt&oacute;w. W dłuższym okresie ma to zagwarantować akcjonariuszom ciągły wzrost wartości przedsiębiorstwa, a Sp&oacute;łce umożliwić stały wzrost zainteresowania wśr&oacute;d inwestor&oacute;w na rynku publicznym.</p>\r\n\r\n<p>Więcej informacji o Columbus Energy S.A. można znaleźć na stronie internetowej Sp&oacute;łki znajdującej się pod adresem: <a href="http://www.columbusenergy.pl" title="http://www.columbusenergy.pl">http://www.columbusenergy.pl</a></p>\r\n', '<p>Columbus Capital S.A., Sp&oacute;łka notowana na rynku NewConnect, kt&oacute;ra posiada 1.000.000 szt. akcji Columbus Energy S.A., podmiotu zajmującego się sprzedażą instalacji fotowoltaicznych i Raportami Efektywności Energetycznej, spodziewa się, że dzięki przegłosowaniu w miniony piątek przez Sejm nowej Ustawy o Odnawialnych Źr&oacute;dłach Energii odniesie ona znaczące korzyści. Najważniejsze dla rozwoju Sp&oacute;łki są także przyjęte poprawki dotyczące prosument&oacute;w.</p>\r\n', 0, 0, NULL, NULL, 0, '', ''),
(57, 0, '2015-10-30 14:30:00', 'Columbus-Energy-S.A.-osiąga-wysoki-zysk-w-3-kw.-2015-r.', 'Columbus Energy S.A. osiąga wysoki zysk w 3 kw. 2015 r.', '<p>Columbus Energy S.A. rozpoczęła swoją właściwą działalność operacyjną, a więc realizację instalacji fotowoltaicznych, w lipcu tego roku, przekraczając już w pierwszym miesiącu pr&oacute;g rentowności. Sp&oacute;łka we wrześniu 2015 r. zanotowała przychody brutto na poziomie blisko 3,15 mln zł, a w całym 3 kwartale br. wartość sprzedaży sięgnęła 7,9 mln zł. Zysk brutto wypracowany przez Columbus Energy S.A. we wrześniu br. wyni&oacute;sł prawie 0,75 mln zł.</p>\r\n\r\n<p>&bdquo;Jesteśmy bardzo zadowoleni z osiągniętych wynik&oacute;w finansowych w pierwszym kwartale właściwej działalności operacyjnej Sp&oacute;łki. Potencjał rozwoju naszej branży jest bardzo duży i zamierzamy to wykorzystać. Co prawda zrealizowany zysk brutto w 3 kwartale 2015 r. był nieco niższy od prognozowanego, ale wynikało to z wydłużonej procedury płatności z Programu PROSUMENT. Jestem przekonany, że kolejne miesiące będą dla Sp&oacute;łki bardzo udanym okresem.&rdquo; - ocenia Łukasz G&oacute;rski, Prezes Zarządu Sp&oacute;łki Columbus Energy S.A.</p>\r\n\r\n<p>W sierpniu br. Sp&oacute;łka opublikowała prognozy finansowe zakładające osiągnięcie w tym roku prawie 4,7 mln zł zysku brutto oraz 20,6 mln zł przychod&oacute;w brutto ze sprzedaży. Prognozowane przychody ze sprzedaży we wrześniu 2015 r. miały wynieść 4 mln zł brutto, a zysk brutto sięgnąć 1,1 mln zł. Z kolei w 2016 r. Sp&oacute;łka ma zamiar wypracować przychody brutto ze sprzedaży w kwocie 145 mln zł.</p>\r\n\r\n<p>W dniu 26.10.2015 r. Sp&oacute;łka podpisała uzgodniony Plan Połączenia z COLUMBUS CAPITAL S.A. Połączenie nastąpi w drodze przejęcia przez Sp&oacute;łkę COLUMBUS CAPITAL S.A. Sp&oacute;łki Columbus Energy S.A. poprzez przeniesienie całego majątku Columbus Energy S.A. na COLUMBUS CAPITAL S.A. w zamian za nowo emitowane Akcje z Połączenia, kt&oacute;re COLUMBUS CAPITAL S.A. wyda akcjonariuszom Columbus Energy S.A. W ramach transakcji Połączenia, COLUMBUS CAPITAL S.A. jako Sp&oacute;łka przejmująca wyemituje maksymalnie 253.163.457 akcji serii E dla Akcjonariuszy Columbus Energy S.A. jako Sp&oacute;łki przejmowanej, uwzględniając przyjęty parytet wymiany akcji ustalony na podstawie wycen obu sp&oacute;łek, kt&oacute;ry wynosi 11,6 akcji COLUMBUS CAPITAL S.A. za 1 akcję Columbus Energy S.A.</p>\r\n\r\n<p>Celem planowanego Połączenia będzie dążenie do zdobycia mocnej pozycji konkurencyjnej na rynku Odnawialnych Źr&oacute;deł Energii, gł&oacute;wnie w branży fotowoltaiki, wykorzystując efekty synergii wynikające z potencjału obu podmiot&oacute;w, redukcji koszt&oacute;w funkcjonowania oraz optymalizacji struktury organizacyjnej.</p>\r\n\r\n<p>&bdquo;Podpisany przez Sp&oacute;łkę Plan Połączenia z COLUMBUS CAPITAL S.A. to kolejny krok w kierunku finalizacji transakcji połączenia obu podmiot&oacute;w. Dzięki niej uda nam się zbudować przedsiębiorstwo, kt&oacute;re będzie posiadało mocną pozycję rynkową w branży OZE, kt&oacute;ra ma przed sobą w Polsce bardzo dobrą przyszłość. W pierwszym p&oacute;łroczu 2016 r. planujemy utworzyć nowe oddziały, kt&oacute;re pozwolą nam zwiększyć nasz zasięg terytorialny do ponad 80% powierzchni kraju. Chcemy też rozszerzyć nasze portfolio o sprzedaż nowego produktu związanego z termomodernizacją dom&oacute;w jednorodzinnych.&rdquo; - podsumowuje G&oacute;rski.</p>\r\n\r\n<p>Gł&oacute;wnym Akcjonariuszem Columbus Energy S.A. jest obecnie JR HOLDING S.A., kt&oacute;ra wraz z podmiotami powiązanymi posiada łącznie 10.307.740 sztuk akcji Sp&oacute;łki, co stanowi 49,50% udziału w jej kapitale zakładowym oraz 46,91% w og&oacute;lnej liczbie głos&oacute;w na Walnym Zgromadzeniu Akcjonariuszy.</p>\r\n\r\n<p>Więcej informacji o Columbus Energy S.A. - http://www.columbusenergy.pl</p>\r\n', '<p>Columbus Energy S.A., Sp&oacute;łka działająca w branży Odnawialnych Źr&oacute;deł Energii, osiągnęła ponad 2,7 mln zł zysku brutto w 3 kwartale 2015 r. Sp&oacute;łka podpisała także Plan Połączenia z notowaną na rynku NewConnect - COLUMBUS CAPITAL S.A.</p>\r\n', 0, 0, NULL, NULL, 0, '', '');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ofi_offer`
--

CREATE TABLE IF NOT EXISTS `ofi_offer` (
`id` int(11) unsigned NOT NULL,
  `category_id` int(11) unsigned NOT NULL,
  `datetime` datetime NOT NULL,
  `url` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `title` varchar(250) COLLATE utf8_polish_ci NOT NULL,
  `short_content` text COLLATE utf8_polish_ci NOT NULL,
  `view_count` int(11) unsigned NOT NULL,
  `hidden` tinyint(1) unsigned NOT NULL,
  `header_image` int(11) unsigned DEFAULT NULL,
  `only_logged` tinyint(1) unsigned NOT NULL,
  `description` text COLLATE utf8_polish_ci,
  `keywords` text COLLATE utf8_polish_ci
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `ofi_offer`
--

INSERT INTO `ofi_offer` (`id`, `category_id`, `datetime`, `url`, `title`, `short_content`, `view_count`, `hidden`, `header_image`, `only_logged`, `description`, `keywords`) VALUES
(3, 1, '2015-11-20 09:30:00', 'level-I', 'Piętro I', '', 0, 0, 1287, 0, '', ''),
(4, 1, '2015-11-20 09:30:00', 'level-II', 'Piętro II', '', 0, 0, 1287, 0, '', ''),
(5, 1, '2015-11-20 09:30:00', 'level-III', 'Piętro III', '', 0, 0, 1287, 0, '', ''),
(6, 2, '2015-11-20 13:15:00', 'afasfasfas24214', 'afasfasfas', 'asfasfasfa', 0, 0, 1288, 0, '', '');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ofi_page`
--

CREATE TABLE IF NOT EXISTS `ofi_page` (
`id` int(11) unsigned NOT NULL,
  `name` varchar(128) COLLATE utf8_polish_ci NOT NULL DEFAULT '' COMMENT 'tytuł strony SEO',
  `url` varchar(134) COLLATE utf8_polish_ci NOT NULL DEFAULT '' COMMENT 'url strony',
  `description` text COLLATE utf8_polish_ci NOT NULL COMMENT 'opis SEO',
  `keywords` text COLLATE utf8_polish_ci NOT NULL COMMENT 'słowa kluczowe SEO',
  `content` mediumtext COLLATE utf8_polish_ci NOT NULL COMMENT 'treść HTML',
  `stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'timestamp',
  `internal` tinyint(1) unsigned NOT NULL COMMENT '0 - normal, 1 - internal',
  `show_slider` tinyint(1) unsigned NOT NULL,
  `gallery_id` int(11) unsigned DEFAULT NULL,
  `test` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ofi_slider_main`
--

CREATE TABLE IF NOT EXISTS `ofi_slider_main` (
`id` int(11) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `image_id` int(11) unsigned DEFAULT NULL,
  `link` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `hidden` tinyint(1) unsigned NOT NULL,
  `order` int(11) unsigned NOT NULL,
  `content` text COLLATE utf8_polish_ci
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `ofi_slider_main`
--

INSERT INTO `ofi_slider_main` (`id`, `title`, `image_id`, `link`, `hidden`, `order`, `content`) VALUES
(1, 'test', 1289, '', 0, 0, NULL),
(2, 'test', 1289, 'http://wp.pl', 0, 0, 'afasfasfas'),
(3, 'asfasfas', 1290, '#', 0, 0, 'asfasfas');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ofi_systemmailtemplate`
--

CREATE TABLE IF NOT EXISTS `ofi_systemmailtemplate` (
`templateId` int(10) unsigned NOT NULL COMMENT 'id szablonu',
  `name` varchar(64) COLLATE utf8_polish_ci NOT NULL COMMENT 'nazwa systemowa',
  `subject` varchar(250) COLLATE utf8_polish_ci NOT NULL COMMENT 'temat wiadomosci',
  `body` text COLLATE utf8_polish_ci NOT NULL COMMENT 'tresc wiadomosci'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci COMMENT='przechowuje systemowe szablony wiadomosci np pod rejestracje itd';

--
-- Zrzut danych tabeli `ofi_systemmailtemplate`
--

INSERT INTO `ofi_systemmailtemplate` (`templateId`, `name`, `subject`, `body`) VALUES
(1, 'register.activation', 'Aktywacja konta w serwisie %site_url%', '<p>Witaj,</p>\r\n\r\n<p>dokonałeś rejestracji na stronie %site_url%. Aby dokonać aktywacji konta, niezbędnej do zalogowania, prosimy o odwiedzenie poniższego linku:</p>\r\n\r\n<p><a href="%register_activation_link%">%register_activation_link%</a></p>\r\n'),
(2, 'user.forgottenPassword', 'Przypomnienie hasła w serwisie %site_url%', '<p>Witaj,</p>\r\n\r\n<p>Poprosiłeś o przypomnienie hasła na stronie %site_url%. Aby zmienić obecne hasło prosimy&nbsp;o odwiedzenie poniższego linku:</p>\r\n\r\n<p>%forgotten_password_link%</p>\r\n'),
(5, 'user.contactForm', 'Odpowiedź na formularz kontaktowy w serwisie %site_url%', '<p>Witaj,</p>\r\n\r\n<p>%name% (%email%) odpowiedział na formularz kontaktowy w serwisie %site_url%.</p>\r\n<p>&nbsp;</p>\r\n<p>Dane użytkownika:</p>\r\n<p>\r\n<b>Imię i nazwisko:</b>%name% %surname% <br />\r\n</p>\r\n<p>&nbsp;</p>\r\n<p>Treść wiadomości:</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>%content%</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Pozdrawiamy</p>');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ofi_uploader`
--

CREATE TABLE IF NOT EXISTS `ofi_uploader` (
`id` int(11) unsigned NOT NULL,
  `file` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'nazwa pliku na serwerze',
  `name` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'oryginalna nazwa pliku',
  `size` smallint(5) unsigned NOT NULL COMMENT 'rozmiar w bajtach',
  `uniqueDir` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'podkatalog miniatur',
  `mime` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'typ mime grafiki',
  `addedTime` datetime NOT NULL COMMENT 'czas utworzenia',
  `delete` tinyint(1) unsigned NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ofi_user`
--

CREATE TABLE IF NOT EXISTS `ofi_user` (
`id` int(10) unsigned NOT NULL,
  `email` varchar(128) COLLATE utf8_polish_ci NOT NULL COMMENT 'jako login',
  `password` varchar(64) COLLATE utf8_polish_ci NOT NULL COMMENT 'md5',
  `firstname` varchar(32) COLLATE utf8_polish_ci NOT NULL DEFAULT '' COMMENT 'imię',
  `lastname` varchar(32) COLLATE utf8_polish_ci NOT NULL DEFAULT '' COMMENT 'nazwisko',
  `active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `hash` varchar(128) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `ofi_user`
--

INSERT INTO `ofi_user` (`id`, `email`, `password`, `firstname`, `lastname`, `active`, `hash`) VALUES
(2, 'admin@admin.pl', '$1$abfa64c6$Z0Q32ZbulB4qJ.r.cepTM0', 'Admin', 'Admin', 1, '$1$0520d3bcaec4$');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ofi_user_login_history`
--

CREATE TABLE IF NOT EXISTS `ofi_user_login_history` (
`id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `ip` varchar(15) CHARACTER SET utf8 NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `success` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=494 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `ofi_user_login_history`
--

INSERT INTO `ofi_user_login_history` (`id`, `user_id`, `ip`, `date`, `success`) VALUES
(1, 1, '192.168.131.2', '2014-02-08 13:23:58', 1),
(2, 1, '192.168.131.2', '2014-02-08 14:57:31', 1),
(3, 1, '192.168.131.2', '2014-02-08 14:58:56', 1),
(4, 1, '192.168.131.2', '2014-02-10 18:40:44', 1),
(5, 1, '192.168.131.2', '2014-02-10 21:39:40', 1),
(6, 1, '192.168.131.2', '2014-02-16 14:49:09', 0),
(7, 1, '192.168.131.2', '2014-02-16 14:49:12', 1),
(8, 1, '192.168.131.2', '2014-02-16 19:24:55', 1),
(9, 1, '192.168.131.2', '2014-02-17 23:30:07', 0),
(10, 1, '192.168.131.2', '2014-02-17 23:30:09', 1),
(11, 1, '192.168.131.2', '2014-02-18 20:14:15', 0),
(12, 1, '192.168.131.2', '2014-02-18 20:14:45', 0),
(13, 1, '192.168.131.2', '2014-02-18 20:15:59', 0),
(14, 1, '192.168.131.2', '2014-02-18 20:16:04', 0),
(15, 1, '192.168.131.2', '2014-02-18 20:16:17', 0),
(16, 1, '192.168.131.2', '2014-02-18 20:16:28', 0),
(17, 1, '192.168.131.2', '2014-02-18 20:16:38', 0),
(18, 1, '192.168.131.2', '2014-02-18 20:16:53', 0),
(19, 1, '192.168.131.2', '2014-02-18 20:17:01', 0),
(20, 1, '192.168.131.2', '2014-02-18 20:17:12', 0),
(21, 1, '192.168.131.2', '2014-02-18 20:22:30', 0),
(22, 1, '192.168.131.2', '2014-02-18 20:22:33', 1),
(23, 1, '192.168.131.2', '2014-02-18 20:24:13', 1),
(24, 1, '192.168.131.2', '2014-02-18 21:54:57', 1),
(25, 1, '192.168.131.2', '2014-02-18 23:32:40', 1),
(26, 1, '192.168.131.2', '2014-02-20 18:41:30', 1),
(27, 1, '192.168.131.2', '2014-02-20 23:30:20', 1),
(28, 1, '192.168.131.2', '2014-02-21 20:24:31', 0),
(29, 1, '192.168.131.2', '2014-02-21 20:24:34', 1),
(30, 1, '192.168.131.2', '2014-02-22 00:30:46', 1),
(31, 1, '192.168.131.2', '2014-02-22 10:58:30', 1),
(32, 1, '192.168.131.2', '2014-02-22 12:45:08', 1),
(33, 1, '192.168.131.2', '2014-02-24 22:21:17', 1),
(34, 1, '192.168.131.2', '2014-02-27 21:43:27', 1),
(35, 1, '192.168.131.2', '2014-02-28 22:02:23', 1),
(36, 1, '192.168.131.2', '2014-03-02 11:50:26', 1),
(37, 1, '192.168.131.2', '2014-05-22 21:32:14', 0),
(38, 1, '192.168.131.2', '2014-05-22 21:32:18', 1),
(39, 1, '192.168.131.2', '2014-05-22 22:55:45', 1),
(40, 1, '192.168.131.2', '2014-05-22 23:02:51', 1),
(41, 1, '192.168.131.2', '2014-05-22 23:40:21', 1),
(43, 1, '192.168.131.2', '2014-05-23 07:33:11', 1),
(44, 1, '192.168.131.2', '2014-05-24 23:41:07', 1),
(45, 1, '192.168.131.2', '2014-05-26 19:15:15', 1),
(46, 1, '192.168.131.2', '2014-05-26 22:52:52', 1),
(47, 1, '192.168.131.2', '2014-05-26 23:38:25', 1),
(48, 1, '192.168.131.2', '2014-05-27 22:31:23', 1),
(49, 1, '192.168.131.2', '2014-05-31 21:48:11', 1),
(52, 1, '192.168.131.2', '2014-05-31 21:56:40', 1),
(53, 1, '192.168.131.2', '2014-05-31 22:15:33', 1),
(54, 1, '192.168.131.2', '2014-05-31 22:17:08', 1),
(55, 1, '192.168.131.2', '2014-06-12 08:16:30', 1),
(56, 1, '192.168.131.2', '2014-06-12 20:50:17', 1),
(57, 1, '192.168.131.2', '2014-06-17 10:33:25', 1),
(58, 1, '192.168.131.2', '2014-06-17 12:17:19', 1),
(59, 1, '192.168.228.2', '2014-07-11 18:43:53', 1),
(60, 1, '192.168.228.2', '2014-07-16 17:57:26', 1),
(61, 1, '192.168.228.2', '2014-09-09 20:58:23', 0),
(62, 1, '192.168.228.2', '2014-09-09 20:58:25', 1),
(63, 1, '192.168.228.2', '2014-09-09 21:20:21', 0),
(64, 1, '192.168.228.2', '2014-09-09 21:20:24', 1),
(65, 1, '192.168.228.2', '2014-09-09 22:04:35', 1),
(66, 1, '192.168.228.2', '2014-09-09 22:48:29', 1),
(67, 1, '192.168.228.2', '2014-09-10 07:37:04', 1),
(68, 1, '192.168.228.2', '2014-09-11 17:46:38', 1),
(69, 1, '192.168.228.2', '2014-09-11 17:47:03', 1),
(70, 1, '192.168.228.2', '2014-09-14 09:29:26', 1),
(71, 1, '192.168.228.2', '2014-09-14 10:02:55', 0),
(72, 1, '192.168.228.2', '2014-09-14 10:02:57', 1),
(73, 1, '192.168.228.2', '2014-09-14 10:11:03', 1),
(74, 1, '192.168.228.2', '2014-09-14 10:42:16', 1),
(75, 1, '192.168.228.2', '2014-09-14 11:12:33', 1),
(76, 1, '192.168.228.2', '2014-09-14 11:42:32', 1),
(77, 1, '192.168.228.2', '2014-09-14 12:49:06', 1),
(78, 1, '192.168.228.2', '2014-09-15 19:30:50', 1),
(79, 1, '192.168.228.2', '2014-09-16 22:10:59', 1),
(80, 1, '192.168.228.2', '2014-09-17 00:02:10', 1),
(81, 1, '192.168.228.2', '2014-09-17 18:48:10', 1),
(82, 1, '192.168.228.2', '2014-09-17 19:52:47', 1),
(83, 1, '192.168.228.2', '2014-09-19 07:12:40', 1),
(84, 1, '85.176.236.187', '2014-09-20 09:46:07', 1),
(85, 2, '89.72.37.230', '2014-09-20 09:51:58', 1),
(86, 2, '89.72.37.230', '2014-09-25 11:51:47', 1),
(87, 2, '159.205.195.24', '2014-09-29 15:42:39', 1),
(88, 1, '192.168.228.2', '2014-09-30 09:37:56', 1),
(89, 2, '159.205.207.208', '2014-09-30 14:45:59', 1),
(90, 2, '159.205.207.208', '2014-09-30 17:49:12', 1),
(91, 2, '159.205.207.208', '2014-09-30 18:31:24', 1),
(92, 2, '159.205.207.208', '2014-09-30 21:00:44', 1),
(93, 2, '159.205.207.208', '2014-09-30 22:13:06', 1),
(94, 2, '159.205.207.208', '2014-10-01 08:31:08', 1),
(95, 2, '159.205.207.208', '2014-10-01 10:20:42', 1),
(96, 2, '159.205.207.208', '2014-10-01 10:35:49', 1),
(97, 2, '85.232.234.199', '2014-10-01 13:53:19', 1),
(98, 2, '85.232.234.199', '2014-10-01 18:31:23', 1),
(99, 1, '31.61.136.155', '2014-10-01 21:38:01', 1),
(100, 2, '85.232.234.199', '2014-10-01 22:45:43', 1),
(101, 1, '31.61.136.43', '2014-10-02 07:25:31', 1),
(102, 2, '77.255.104.129', '2014-10-02 08:53:42', 1),
(103, 2, '85.232.234.199', '2014-10-02 17:15:10', 1),
(104, 2, '85.232.234.199', '2014-10-02 17:34:18', 1),
(105, 2, '85.232.234.199', '2014-10-03 13:12:49', 1),
(106, 2, '85.232.234.199', '2014-10-03 15:33:02', 1),
(107, 2, '89.72.37.230', '2014-10-03 20:24:01', 1),
(112, 1, '188.165.17.252', '2014-10-03 22:34:04', 1),
(117, 2, '188.165.17.252', '2014-10-05 20:27:22', 1),
(122, 1, '188.165.17.252', '2014-10-05 20:29:12', 1),
(127, 2, '188.165.17.252', '2014-10-06 08:53:05', 1),
(132, 2, '188.165.17.252', '2014-10-06 08:58:37', 1),
(137, 2, '188.165.17.252', '2014-10-06 09:13:54', 1),
(142, 1, '188.165.17.252', '2014-10-06 09:14:20', 0),
(147, 1, '188.165.17.252', '2014-10-06 09:14:23', 1),
(152, 2, '188.165.17.252', '2014-10-06 16:11:36', 1),
(157, 2, '188.165.17.252', '2014-10-06 20:38:33', 1),
(162, 2, '188.165.17.252', '2014-10-07 08:45:34', 1),
(167, 2, '188.165.17.252', '2014-10-07 15:41:35', 1),
(172, 2, '188.165.17.252', '2014-10-08 08:12:51', 1),
(177, 2, '188.165.17.252', '2014-10-09 18:44:03', 1),
(182, 2, '188.165.17.252', '2014-10-10 08:26:45', 1),
(187, 2, '178.32.203.33', '2014-10-13 12:37:07', 1),
(192, 1, '178.32.203.33', '2014-10-13 12:42:12', 0),
(197, 1, '178.32.203.33', '2014-10-13 12:42:14', 1),
(202, 2, '178.32.203.33', '2014-10-13 14:12:13', 1),
(207, 1, '178.32.203.33', '2014-10-13 22:59:43', 1),
(212, 2, '178.32.203.33', '2014-10-16 09:08:50', 1),
(217, 2, '178.32.203.33', '2014-10-16 15:07:31', 1),
(222, 1, '178.32.203.33', '2014-10-16 15:19:19', 1),
(227, 2, '178.32.203.33', '2014-10-17 08:33:19', 1),
(232, 2, '178.32.203.33', '2014-10-17 12:25:57', 1),
(237, 2, '178.32.203.33', '2014-10-21 11:02:54', 1),
(242, 2, '178.32.203.33', '2014-10-24 10:56:28', 1),
(247, 2, '178.32.203.33', '2014-11-06 19:41:10', 1),
(252, 2, '178.32.203.33', '2014-11-07 14:48:02', 1),
(253, 2, '85.232.234.199', '2014-11-10 11:34:53', 1),
(254, 1, '85.232.234.199', '2014-11-10 14:48:26', 1),
(255, 2, '85.232.234.199', '2014-11-10 16:38:29', 1),
(256, 1, '85.232.234.199', '2014-11-10 16:42:09', 1),
(257, 2, '85.232.234.199', '2014-11-10 17:00:33', 1),
(258, 2, '77.255.123.189', '2014-11-10 17:23:59', 1),
(259, 1, '85.232.234.199', '2014-11-10 17:26:28', 1),
(260, 1, '10.0.2.2', '2014-11-13 14:53:31', 1),
(261, 1, '10.0.2.2', '2014-11-17 17:42:54', 1),
(262, 1, '10.0.2.2', '2014-11-21 10:11:26', 1),
(263, 1, '10.0.2.2', '2014-11-21 10:58:01', 1),
(264, 1, '10.0.2.2', '2014-11-21 12:00:03', 1),
(265, 2, '10.0.2.2', '2014-12-02 14:33:14', 1),
(266, 2, '10.0.2.2', '2014-12-03 10:33:11', 0),
(267, 2, '10.0.2.2', '2014-12-03 10:33:15', 1),
(268, 2, '10.0.2.2', '2014-12-12 11:58:12', 1),
(269, 2, '10.0.2.2', '2014-12-12 14:31:17', 1),
(270, 2, '10.0.2.2', '2014-12-16 18:17:32', 1),
(271, 2, '10.0.2.2', '2015-01-29 09:30:43', 1),
(272, 2, '10.0.2.2', '2015-01-29 11:00:19', 1),
(273, 2, '10.0.2.2', '2015-04-03 13:52:55', 1),
(274, 2, '10.0.2.2', '2015-04-03 15:14:21', 1),
(275, 2, '10.0.2.2', '2015-05-07 11:00:48', 1),
(276, 2, '10.0.2.2', '2015-06-02 16:44:47', 1),
(277, 2, '10.0.2.2', '2015-06-09 08:34:04', 1),
(278, 2, '10.0.2.2', '2015-06-09 11:45:18', 1),
(279, 2, '10.0.2.2', '2015-06-10 07:32:08', 1),
(280, 2, '10.0.2.2', '2015-06-10 12:45:35', 1),
(281, 2, '10.0.2.2', '2015-06-11 07:34:18', 1),
(282, 2, '10.0.2.2', '2015-06-11 08:39:10', 1),
(283, 2, '10.0.2.2', '2015-06-11 09:14:07', 1),
(284, 2, '10.0.2.2', '2015-06-11 14:31:46', 1),
(285, 2, '10.0.2.2', '2015-06-12 12:51:21', 1),
(286, 2, '31.175.114.1', '2015-06-15 08:15:22', 1),
(287, 2, '31.175.114.1', '2015-06-15 09:07:18', 1),
(288, 2, '31.175.114.1', '2015-06-15 09:50:37', 1),
(289, 2, '31.175.114.1', '2015-06-15 13:47:38', 1),
(290, 2, '31.175.114.1', '2015-06-15 14:03:17', 1),
(291, 2, '31.175.114.1', '2015-06-16 08:37:03', 0),
(292, 2, '31.175.114.1', '2015-06-16 08:37:06', 1),
(293, 2, '31.175.114.1', '2015-06-16 10:22:36', 1),
(294, 2, '31.175.114.1', '2015-06-16 10:36:30', 1),
(295, 2, '31.175.114.1', '2015-06-16 11:50:29', 1),
(296, 2, '83.26.252.251', '2015-06-16 13:01:34', 1),
(297, 2, '31.175.114.1', '2015-06-16 14:01:39', 1),
(298, 2, '31.175.114.1', '2015-06-17 08:06:16', 1),
(299, 2, '31.175.114.1', '2015-06-17 09:33:55', 1),
(300, 2, '31.175.114.1', '2015-06-17 12:43:12', 1),
(301, 2, '31.175.114.1', '2015-06-18 14:38:06', 1),
(302, 2, '31.175.114.1', '2015-06-19 08:28:19', 1),
(303, 2, '31.175.114.1', '2015-06-19 10:37:53', 1),
(304, 2, '31.175.114.1', '2015-06-19 11:53:16', 1),
(305, 2, '31.175.114.1', '2015-06-19 12:11:54', 1),
(306, 2, '31.175.114.1', '2015-06-19 15:08:45', 1),
(307, 2, '31.175.114.1', '2015-06-19 15:30:20', 1),
(308, 2, '89.79.37.143', '2015-06-21 22:54:03', 1),
(309, 2, '31.175.114.1', '2015-06-22 07:41:21', 0),
(310, 2, '31.175.114.1', '2015-06-22 07:41:25', 1),
(311, 2, '31.175.114.1', '2015-06-22 08:58:42', 1),
(312, 2, '31.175.114.1', '2015-06-22 09:38:53', 1),
(313, 2, '31.175.114.1', '2015-06-22 10:13:36', 1),
(314, 2, '31.175.114.1', '2015-06-23 07:18:45', 1),
(315, 2, '31.175.114.1', '2015-06-23 09:43:01', 1),
(316, 2, '31.175.114.1', '2015-06-23 12:11:36', 1),
(317, 2, '31.175.114.1', '2015-06-24 15:12:27', 1),
(318, 2, '31.175.114.1', '2015-06-24 15:15:43', 1),
(319, 2, '31.175.114.1', '2015-06-24 15:16:07', 1),
(320, 2, '31.175.114.1', '2015-06-25 07:32:34', 1),
(321, 2, '31.175.114.1', '2015-06-25 08:12:45', 1),
(322, 2, '31.175.114.1', '2015-06-25 10:18:52', 1),
(323, 2, '31.175.114.1', '2015-06-25 10:34:24', 1),
(324, 2, '31.175.114.1', '2015-06-25 11:36:36', 1),
(325, 2, '31.175.114.1', '2015-06-25 12:17:14', 1),
(326, 2, '31.175.114.1', '2015-06-25 13:37:22', 1),
(327, 2, '31.175.114.1', '2015-06-25 15:02:20', 1),
(328, 2, '31.175.114.1', '2015-06-26 11:51:42', 1),
(329, 2, '31.175.114.1', '2015-06-26 13:10:46', 1),
(330, 2, '31.175.114.1', '2015-06-26 14:14:53', 1),
(331, 2, '31.175.174.167', '2015-06-29 08:16:01', 1),
(332, 2, '31.175.174.167', '2015-06-29 08:59:38', 1),
(333, 2, '31.175.174.167', '2015-06-29 13:06:14', 1),
(334, 2, '31.175.174.167', '2015-06-29 13:26:09', 1),
(335, 2, '31.175.174.167', '2015-06-29 13:42:11', 1),
(336, 2, '31.175.174.167', '2015-06-29 14:07:10', 1),
(337, 2, '31.175.174.167', '2015-06-29 14:07:26', 1),
(338, 2, '31.175.174.167', '2015-06-30 08:22:49', 1),
(339, 2, '31.175.174.167', '2015-06-30 08:50:02', 1),
(340, 2, '31.175.174.167', '2015-06-30 09:30:14', 1),
(341, 2, '31.175.174.167', '2015-06-30 10:09:48', 1),
(342, 2, '31.175.174.167', '2015-06-30 10:25:15', 1),
(343, 2, '31.175.174.167', '2015-06-30 12:04:38', 1),
(344, 2, '31.175.174.167', '2015-06-30 12:05:46', 1),
(345, 2, '31.175.174.167', '2015-06-30 12:35:09', 1),
(346, 2, '31.175.174.167', '2015-06-30 12:37:12', 1),
(347, 2, '31.175.174.167', '2015-06-30 13:40:27', 1),
(348, 2, '31.175.174.167', '2015-06-30 18:04:33', 1),
(349, 2, '31.175.174.167', '2015-07-01 07:24:56', 1),
(350, 2, '31.175.174.167', '2015-07-01 11:48:20', 1),
(351, 2, '31.175.174.167', '2015-07-02 09:51:41', 1),
(352, 2, '31.175.174.167', '2015-07-03 08:50:22', 1),
(353, 2, '31.175.174.167', '2015-07-03 09:40:33', 1),
(354, 2, '89.79.37.143', '2015-07-03 19:14:37', 1),
(355, 2, '31.175.174.167', '2015-07-06 10:31:54', 1),
(356, 2, '31.175.174.167', '2015-07-06 13:22:14', 1),
(357, 2, '31.175.174.167', '2015-07-06 14:50:23', 1),
(358, 2, '31.175.174.167', '2015-07-06 16:09:59', 1),
(359, 2, '31.175.174.167', '2015-07-06 16:09:59', 1),
(360, 2, '31.175.174.167', '2015-07-06 16:42:01', 1),
(361, 2, '31.175.174.167', '2015-07-06 17:11:20', 1),
(362, 2, '31.175.174.167', '2015-07-06 17:22:33', 1),
(363, 2, '89.79.37.143', '2015-07-06 19:56:27', 1),
(364, 2, '31.175.174.167', '2015-07-07 07:12:26', 1),
(365, 2, '31.175.174.167', '2015-07-07 07:49:11', 1),
(366, 2, '31.175.174.167', '2015-07-07 09:35:15', 1),
(367, 2, '31.175.174.167', '2015-07-07 11:36:04', 1),
(368, 2, '31.175.174.167', '2015-07-07 13:04:52', 1),
(369, 2, '31.175.174.167', '2015-07-07 13:14:01', 1),
(370, 2, '31.175.174.167', '2015-07-07 14:30:50', 1),
(371, 2, '31.175.174.167', '2015-07-07 14:31:29', 1),
(372, 2, '31.175.174.167', '2015-07-08 08:29:46', 1),
(373, 2, '31.175.174.167', '2015-07-08 08:30:14', 1),
(374, 2, '31.175.174.167', '2015-07-08 11:19:09', 1),
(375, 2, '31.175.174.167', '2015-07-09 09:51:13', 1),
(376, 2, '31.175.174.167', '2015-07-09 11:55:52', 1),
(377, 2, '31.175.174.167', '2015-07-09 13:00:58', 1),
(378, 2, '31.175.174.167', '2015-07-09 13:46:27', 1),
(379, 2, '31.175.174.167', '2015-07-09 13:53:38', 1),
(380, 2, '31.175.174.167', '2015-07-10 09:43:24', 1),
(381, 2, '31.175.174.167', '2015-07-10 09:50:07', 1),
(382, 2, '31.175.174.167', '2015-07-10 11:34:07', 1),
(383, 2, '10.0.2.2', '2015-07-10 14:16:19', 0),
(384, 2, '10.0.2.2', '2015-07-10 14:16:25', 1),
(385, 2, '10.0.2.2', '2015-07-13 07:36:55', 1),
(386, 2, '10.0.2.2', '2015-07-13 08:54:54', 1),
(387, 2, '10.0.2.2', '2015-07-13 09:41:34', 1),
(388, 2, '10.0.2.2', '2015-07-13 12:16:59', 1),
(389, 2, '10.0.2.2', '2015-07-14 08:10:05', 1),
(390, 2, '10.0.2.2', '2015-07-14 08:21:22', 1),
(391, 2, '10.0.2.2', '2015-07-16 07:49:26', 1),
(392, 2, '10.0.2.2', '2015-07-16 12:17:57', 1),
(393, 2, '10.0.2.2', '2015-07-17 09:02:12', 1),
(394, 2, '10.0.2.2', '2015-07-17 09:43:51', 1),
(395, 2, '10.0.2.2', '2015-07-17 11:48:33', 1),
(396, 2, '10.0.2.2', '2015-07-20 07:43:50', 1),
(397, 2, '10.0.2.2', '2015-07-20 11:21:22', 1),
(398, 2, '10.0.2.2', '2015-07-20 12:10:30', 1),
(399, 2, '10.0.2.2', '2015-07-28 12:54:30', 1),
(400, 2, '10.0.2.2', '2015-07-30 08:28:50', 1),
(401, 2, '10.0.2.2', '2015-07-30 08:47:41', 1),
(402, 2, '10.0.2.2', '2015-07-31 07:51:43', 1),
(403, 2, '10.0.2.2', '2015-08-03 07:32:33', 1),
(404, 2, '10.0.2.2', '2015-08-03 08:33:19', 1),
(405, 2, '10.0.2.2', '2015-08-03 09:18:57', 1),
(406, 2, '10.0.2.2', '2015-08-03 12:07:30', 1),
(407, 2, '10.0.2.2', '2015-08-03 12:44:33', 1),
(408, 2, '10.0.2.2', '2015-08-03 13:47:18', 1),
(409, 2, '31.175.253.83', '2015-08-04 09:03:04', 1),
(410, 2, '31.175.253.83', '2015-08-04 12:07:39', 1),
(411, 2, '31.175.253.83', '2015-08-04 13:59:31', 1),
(412, 2, '31.175.253.83', '2015-08-05 07:34:15', 1),
(413, 2, '31.175.116.73', '2015-08-11 09:55:49', 1),
(414, 2, '31.175.116.73', '2015-08-11 11:03:41', 1),
(415, 2, '31.174.248.190', '2015-08-24 10:31:31', 1),
(416, 2, '31.174.248.190', '2015-08-25 09:16:30', 1),
(417, 2, '89.74.240.144', '2015-08-25 09:41:15', 1),
(418, 2, '94.254.145.113', '2015-10-14 08:37:08', 1),
(419, 2, '31.175.83.98', '2015-10-19 09:47:01', 1),
(420, 2, '31.175.83.98', '2015-10-19 09:49:33', 1),
(421, 2, '31.175.83.98', '2015-10-19 11:26:32', 1),
(422, 2, '31.175.248.82', '2015-10-20 07:38:58', 1),
(423, 2, '89.74.227.73', '2015-10-21 06:41:37', 0),
(424, 2, '89.74.227.73', '2015-10-21 06:41:42', 1),
(425, 2, '31.175.146.137', '2015-10-22 12:59:02', 1),
(426, 2, '31.175.146.137', '2015-10-22 13:21:04', 1),
(427, 2, '89.74.227.73', '2015-10-22 13:34:40', 1),
(428, 2, '84.10.37.190', '2015-10-23 08:33:32', 1),
(429, 2, '84.10.37.190', '2015-10-23 09:14:50', 1),
(430, 2, '84.10.37.190', '2015-10-23 11:06:25', 1),
(431, 2, '31.175.146.137', '2015-10-23 11:15:43', 0),
(432, 2, '31.175.146.137', '2015-10-23 11:15:47', 0),
(433, 2, '84.10.37.190', '2015-10-23 11:16:07', 1),
(434, 2, '31.175.146.137', '2015-10-23 11:16:42', 0),
(435, 2, '31.175.146.137', '2015-10-23 11:16:49', 0),
(436, 2, '31.175.146.137', '2015-10-23 11:17:02', 0),
(437, 2, '31.175.146.137', '2015-10-23 11:17:26', 0),
(438, 2, '31.175.146.137', '2015-10-23 11:17:52', 0),
(439, 2, '31.175.146.137', '2015-10-23 11:17:56', 0),
(440, 2, '31.175.196.54', '2015-10-26 08:39:48', 0),
(441, 2, '31.175.196.54', '2015-10-26 08:40:52', 1),
(442, 2, '31.175.196.54', '2015-10-26 08:44:57', 0),
(443, 2, '84.10.37.190', '2015-10-26 13:53:25', 1),
(444, 2, '84.10.37.190', '2015-10-26 15:24:42', 1),
(445, 2, '31.175.91.114', '2015-10-26 15:29:01', 1),
(446, 2, '84.10.37.190', '2015-10-27 10:12:08', 1),
(447, 2, '84.10.37.190', '2015-10-28 08:36:46', 1),
(448, 2, '84.10.37.190', '2015-10-28 10:05:45', 1),
(449, 2, '31.175.223.55', '2015-10-28 12:36:12', 0),
(450, 2, '31.175.223.55', '2015-10-28 12:37:01', 1),
(451, 2, '31.175.193.139', '2015-10-28 15:18:54', 1),
(452, 2, '31.175.193.139', '2015-10-29 08:30:52', 1),
(453, 2, '31.175.193.139', '2015-10-29 08:32:07', 0),
(454, 2, '31.175.193.139', '2015-10-29 08:33:28', 1),
(455, 2, '84.10.37.190', '2015-10-29 08:57:23', 1),
(456, 2, '84.10.37.190', '2015-10-29 13:13:39', 1),
(457, 2, '31.175.193.139', '2015-10-29 13:20:08', 1),
(458, 2, '84.10.37.190', '2015-10-30 09:10:50', 1),
(459, 2, '31.175.193.139', '2015-10-30 09:32:52', 1),
(460, 2, '84.10.37.190', '2015-10-30 13:30:00', 1),
(461, 2, '89.68.24.13', '2015-11-03 08:41:49', 1),
(462, 2, '31.174.226.246', '2015-11-03 08:47:10', 0),
(463, 2, '31.174.226.246', '2015-11-03 08:48:35', 1),
(464, 2, '31.174.160.98', '2015-11-03 12:44:39', 1),
(465, 2, '31.174.47.6', '2015-11-03 15:27:38', 1),
(466, 2, '31.174.47.6', '2015-11-04 08:27:46', 1),
(467, 2, '31.174.47.6', '2015-11-04 10:20:15', 1),
(468, 2, '31.175.73.227', '2015-11-04 16:04:35', 1),
(469, 2, '31.175.176.72', '2015-11-05 09:52:14', 1),
(470, 2, '89.68.24.13', '2015-11-06 10:33:15', 1),
(471, 2, '89.68.24.13', '2015-11-09 00:30:16', 1),
(472, 2, '89.68.24.13', '2015-11-09 00:35:33', 1),
(473, 2, '10.0.2.2', '2015-11-16 10:57:05', 0),
(474, 2, '10.0.2.2', '2015-11-16 10:57:44', 1),
(475, 2, '10.0.2.2', '2015-11-17 09:19:10', 1),
(476, 2, '10.0.2.2', '2015-11-17 12:31:36', 1),
(477, 2, '10.0.2.2', '2015-11-18 14:59:51', 1),
(478, 2, '10.0.2.2', '2015-11-19 08:52:55', 1),
(479, 2, '10.0.2.2', '2015-11-19 10:31:22', 0),
(480, 2, '10.0.2.2', '2015-11-19 10:31:44', 0),
(481, 2, '10.0.2.2', '2015-11-19 10:32:41', 0),
(482, 2, '10.0.2.2', '2015-11-19 10:33:16', 0),
(483, 2, '10.0.2.2', '2015-11-19 10:33:45', 1),
(484, 2, '10.0.2.2', '2015-11-19 13:51:04', 0),
(485, 2, '10.0.2.2', '2015-11-19 13:51:07', 1),
(486, 2, '10.0.2.2', '2015-11-19 15:39:53', 0),
(487, 2, '10.0.2.2', '2015-11-19 15:39:57', 1),
(488, 2, '10.0.2.2', '2015-11-20 08:38:19', 1),
(489, 2, '10.0.2.2', '2015-11-20 12:17:40', 1),
(490, 2, '10.0.2.2', '2015-11-20 14:44:39', 1),
(491, 2, '10.0.2.2', '2015-12-15 09:55:15', 1),
(492, 2, '10.0.2.2', '2015-12-15 11:22:48', 1),
(493, 2, '10.0.2.2', '2016-01-12 14:50:43', 1);

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `ofi_about`
--
ALTER TABLE `ofi_about`
 ADD PRIMARY KEY (`id`), ADD KEY `type_id` (`datetime`,`url`), ADD KEY `date` (`datetime`), ADD KEY `url` (`url`), ADD KEY `hidden` (`hidden`), ADD KEY `category_id` (`category_id`), ADD KEY `header_image` (`header_image`), ADD KEY `only_logged` (`only_logged`);

--
-- Indexes for table `ofi_auth_assignment`
--
ALTER TABLE `ofi_auth_assignment`
 ADD PRIMARY KEY (`itemname`,`userid`), ADD KEY `userid` (`userid`);

--
-- Indexes for table `ofi_auth_item`
--
ALTER TABLE `ofi_auth_item`
 ADD PRIMARY KEY (`name`);

--
-- Indexes for table `ofi_auth_item_child`
--
ALTER TABLE `ofi_auth_item_child`
 ADD PRIMARY KEY (`parent`,`child`), ADD KEY `child` (`child`);

--
-- Indexes for table `ofi_category`
--
ALTER TABLE `ofi_category`
 ADD PRIMARY KEY (`id`), ADD KEY `hidden` (`hidden`), ADD KEY `url` (`url`);

--
-- Indexes for table `ofi_configuration`
--
ALTER TABLE `ofi_configuration`
 ADD PRIMARY KEY (`key`);

--
-- Indexes for table `ofi_file`
--
ALTER TABLE `ofi_file`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ofi_gallery`
--
ALTER TABLE `ofi_gallery`
 ADD PRIMARY KEY (`id`), ADD KEY `thumb_id` (`thumb_id`), ADD KEY `header_image` (`header_image`), ADD KEY `date` (`date`), ADD KEY `hidden` (`hidden`), ADD KEY `url` (`url`), ADD KEY `type` (`type`);

--
-- Indexes for table `ofi_gallery_image`
--
ALTER TABLE `ofi_gallery_image`
 ADD PRIMARY KEY (`id`), ADD KEY `uniqueDir` (`uniqueDir`), ADD KEY `gallery_id` (`gallery_id`), ADD KEY `order` (`order`), ADD KEY `hidden` (`hidden`);

--
-- Indexes for table `ofi_image`
--
ALTER TABLE `ofi_image`
 ADD PRIMARY KEY (`id`), ADD KEY `uniqueDir` (`uniqueDir`);

--
-- Indexes for table `ofi_locals_category`
--
ALTER TABLE `ofi_locals_category`
 ADD PRIMARY KEY (`id`), ADD KEY `hidden` (`hidden`), ADD KEY `url` (`url`);

--
-- Indexes for table `ofi_news`
--
ALTER TABLE `ofi_news`
 ADD PRIMARY KEY (`id`), ADD KEY `type_id` (`datetime`,`url`), ADD KEY `date` (`datetime`), ADD KEY `url` (`url`), ADD KEY `hidden` (`hidden`), ADD KEY `category_id` (`category_id`), ADD KEY `header_image` (`header_image`), ADD KEY `only_logged` (`only_logged`), ADD KEY `thumbnail` (`thumbnail`);

--
-- Indexes for table `ofi_offer`
--
ALTER TABLE `ofi_offer`
 ADD PRIMARY KEY (`id`), ADD KEY `type_id` (`datetime`,`url`), ADD KEY `date` (`datetime`), ADD KEY `url` (`url`), ADD KEY `hidden` (`hidden`), ADD KEY `category_id` (`category_id`), ADD KEY `header_image` (`header_image`), ADD KEY `only_logged` (`only_logged`);

--
-- Indexes for table `ofi_page`
--
ALTER TABLE `ofi_page`
 ADD PRIMARY KEY (`id`), ADD KEY `url` (`url`), ADD KEY `gallery_id` (`gallery_id`);

--
-- Indexes for table `ofi_slider_main`
--
ALTER TABLE `ofi_slider_main`
 ADD PRIMARY KEY (`id`), ADD KEY `hidden` (`hidden`), ADD KEY `image_id` (`image_id`), ADD KEY `order` (`order`);

--
-- Indexes for table `ofi_systemmailtemplate`
--
ALTER TABLE `ofi_systemmailtemplate`
 ADD PRIMARY KEY (`templateId`), ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `ofi_uploader`
--
ALTER TABLE `ofi_uploader`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ofi_user`
--
ALTER TABLE `ofi_user`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `email` (`email`), ADD UNIQUE KEY `hash` (`hash`), ADD KEY `active` (`active`);

--
-- Indexes for table `ofi_user_login_history`
--
ALTER TABLE `ofi_user_login_history`
 ADD PRIMARY KEY (`id`), ADD KEY `userId` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `ofi_about`
--
ALTER TABLE `ofi_about`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT dla tabeli `ofi_category`
--
ALTER TABLE `ofi_category`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `ofi_file`
--
ALTER TABLE `ofi_file`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=640;
--
-- AUTO_INCREMENT dla tabeli `ofi_gallery`
--
ALTER TABLE `ofi_gallery`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT dla tabeli `ofi_gallery_image`
--
ALTER TABLE `ofi_gallery_image`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=127;
--
-- AUTO_INCREMENT dla tabeli `ofi_image`
--
ALTER TABLE `ofi_image`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1291;
--
-- AUTO_INCREMENT dla tabeli `ofi_locals_category`
--
ALTER TABLE `ofi_locals_category`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT dla tabeli `ofi_news`
--
ALTER TABLE `ofi_news`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=58;
--
-- AUTO_INCREMENT dla tabeli `ofi_offer`
--
ALTER TABLE `ofi_offer`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT dla tabeli `ofi_page`
--
ALTER TABLE `ofi_page`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `ofi_slider_main`
--
ALTER TABLE `ofi_slider_main`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT dla tabeli `ofi_systemmailtemplate`
--
ALTER TABLE `ofi_systemmailtemplate`
MODIFY `templateId` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id szablonu',AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT dla tabeli `ofi_uploader`
--
ALTER TABLE `ofi_uploader`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `ofi_user`
--
ALTER TABLE `ofi_user`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT dla tabeli `ofi_user_login_history`
--
ALTER TABLE `ofi_user_login_history`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=494;
--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `ofi_about`
--
ALTER TABLE `ofi_about`
ADD CONSTRAINT `ofi_about_ibfk_1` FOREIGN KEY (`header_image`) REFERENCES `ofi_image` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `ofi_auth_item_child`
--
ALTER TABLE `ofi_auth_item_child`
ADD CONSTRAINT `ofi_auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `ofi_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `ofi_auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `ofi_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `ofi_gallery`
--
ALTER TABLE `ofi_gallery`
ADD CONSTRAINT `ofi_gallery_ibfk_1` FOREIGN KEY (`thumb_id`) REFERENCES `ofi_gallery_image` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `ofi_news`
--
ALTER TABLE `ofi_news`
ADD CONSTRAINT `ofi_news_ibfk_1` FOREIGN KEY (`header_image`) REFERENCES `ofi_image` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
ADD CONSTRAINT `ofi_news_ibfk_2` FOREIGN KEY (`thumbnail`) REFERENCES `ofi_image` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `ofi_offer`
--
ALTER TABLE `ofi_offer`
ADD CONSTRAINT `ofi_offer_ibfk_1` FOREIGN KEY (`header_image`) REFERENCES `ofi_image` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `ofi_page`
--
ALTER TABLE `ofi_page`
ADD CONSTRAINT `ofi_page_ibfk_1` FOREIGN KEY (`gallery_id`) REFERENCES `ofi_gallery` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
