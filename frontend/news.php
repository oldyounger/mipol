<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="Resource-type" content="Document" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0,user-scalable=0;">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Columbus Capital</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- fullpage.js  style -->
    <link rel="stylesheet" type="text/css" href="css/jquery.fullPage.css" />
    <link rel="stylesheet" type="text/css" href="css/examples.css" />

    <!-- Author syles   -->
    <link rel="stylesheet" type="text/css" href="css/style.css" />

    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic&subset=latin,latin-ext' rel='stylesheet' type='text/css'>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!--[if IE]>
    <script type="text/javascript">
        var console = { log: function() {} };
    </script>
    <![endif]-->

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script>

    <!-- FULL PAGE SCRIPTS-->
    <script type="text/javascript" src="/vendors/jquery.slimscroll.min.js"></script>
    <script type="text/javascript" src="js/jquery.fullPage.js"></script>
    <script type="text/javascript" src="js/examples.js"></script>
    <script>
        //        MENU SCRIPT
        $(document).ready(function() {
            var FeelItem = 'NULL';
            $('button.show_menu').on('click', function () {
                if(FeelItem == 'NULL')
                {
                    $('div.overlay-menu').addClass('show_overlay');
                    $('.menu').addClass('show_block');
                    FeelItem = '1';
                }
            });
            $('button.exit').on('click', function () {
                if(FeelItem == '1')
                {
                    $('div.overlay-menu').removeClass('show_overlay');
                    $('.menu').removeClass('show_block');
                    FeelItem = 'NULL'
                }
            });
        });
        //        END MENU SCRIPT
    </script>
    <script type="text/javascript">
//        FULLPAGE SCRIPT
        $(document).ready(function() {
                $('#fullpage').fullpage({
                    sectionsColor: ['#ffffff', '#ffffff', '#434343'],
                    menu: '#menu',
                    scrollBar:true,
                    slidesNavigation: false,
                    autoScrolling: false,
                    fitToSection: false
                });
        });
//        END FULLPAGE SCRIPT
    </script>
    <script>
        //NEWS BUTTON SCTIPT
        $(document).ready(function() {
            $('button.another_news_button').on('click', function () {
                $("div.news").removeClass("news_visible");
                var id;
                id = $(this).attr("id");
                $("div."+id).addClass("news_visible");
            });
        });
        //END NEWS BUTTON SCTIPT
    </script>
    <script>
        //        BACKGROUND LOGO AND MENU SCTIPT
        $(window).scroll(function() {
            if($( window ).width()<=1630 && $( window ).width()>991) {
                var scroll = jQuery(window).scrollTop();
                if (scroll >= 200) {
                    jQuery('header.page').addClass('page_active');
                }
                else {
                    jQuery('header.page').removeClass('page_active');
                }
            }
        });

        //        END BACKGROUND LOGO AND MENU SCTIPT
    </script>

</head>
<body>
<!--MENU-->
<header id="main" class="page">
    <div class="col-md-12">
        <div class="col-md-3">
            <div class="logo">
                <a href="/index.php"><img src="imgs/columbus.png" alt=""/></a>
            </div>
        </div>
        <div class="col-md-offset-8 col-md-1">
            <button class="show_menu">MENU<img src="imgs/hamburger.png" alt=""/></button>
        </div>
        <div class="menu">
            <div class="content_menu">
                <button class="exit"><img src="imgs/exit.png" alt=""/></button>
                <ul class="menu_items">
                    <li><a href="/index.php">Strona Głowna</a></li>
                    <li><a href="/about_us.php">O Nas</a></li>
                    <li><a href="/offer.php">Oferta</a></li>
                    <li><a href="/news.php">Aktualności</a></li>
                    <li><a href="/investors_relation.php">Relacje Inwestorskie</a></li>
                    <li><a href="/contact.php">Kontakt</a></li>
                </ul>
                <ul class="social_items">
                    <li><a href="#"><img src="imgs/facebook.png" alt=""/></a></li>
                    <li><a href="#"><img src="imgs/linkin.png" alt=""/></a></li>
                    <li><a href="#"><img src="imgs/twitter.png" alt=""/></a></li>
                </ul>
            </div>
        </div>
        <div class="overlay-menu"></div>
    </div>
</header>
<!--END MENU-->
<div id="fullpage">

    <!--    CONTENT NEWS-->
    <section class="section" id="news_content">
        <div class="container">
            <div class="news1 news news_visible">
                <h3>31 styczeń 2015</h3>
                <h2 class="title text_gold">Podpisanie porozumienia inwestycyjnego z JR INVEST S.A.</h2>
                <div class="news_text">Informujemy, iż w dniu 20 listopada 2014 roku podpisaliśmy z JR INVEST S.A.
                    z siedzibą w Krakowie Porozumienie Inwestycyjne, w którym to Strony zgodnie ustaliły, iż
                    COLUMBUS CAPITAL S.A. jest zainteresowana  nabyciem wszystkich posiadanych przez JR INVEST
                    S.A.udziałów w Spółce zależnej GANADOR Sp. z o.o. z siedzibą w Krakowie zarejestrowanej pod
                    numerem KRS 0000497726.</div>
                <div class="news_text">Na mocy zawartego Porozumienia Inwestycyjnego Zarząd COLUMBUS CAPITAL S.A.
                    zobowiązał się do uzyskania zgody Rady Nadzorczej na przeprowadzenie transakcji nabycia przez
                    Emitenta od JR INVEST S.A. wszystkich udziałów w Spółce GANADOR Sp. z o.o. oraz zatwierdzenia
                    formy płatności za wszystkie udziały w sposób, który zostanie określony w Umowie.</div>
                <div class="news_text">GANADOR Sp. z o.o. z siedzibą w Krakowie działa w formie funduszu nieruchomości,
                    który kupuje nieruchomości komercyjne o wartości do 1 mln zł, a następnie w krótkim okresie czasu
                    odsprzedaje je z dużą marżą. Spółka obecnie wchodzi w skład Grupy Kapitałowej JR INVEST S.A. i
                    korzysta z doświadczenia w nabywaniu oraz w zbywaniu nieruchomości komercyjnych nabytego na
                    przestrzeni kilkunastu lat. Dzięki temu realizowane transakcje charakteryzują się wysokim stopniem
                    bezpieczeństwa i niskim stopniem ryzyka przy jednoczesnym wysokim poziomie marż. Taki model
                    biznesowy daje Spółce możliwość nabywania tylko sprawdzonych i pewnych nieruchomości komercyjnych
                    równocześnie generując duże zyski. Wskutek tego nabywca, który kupuje od Spółki daną nieruchomość
                    ma pewność, że stanie się właścicielem nieruchomości zweryfikowanej i o uregulowanym stanie
                    prawnym, ponieważ była ona uprzednio dogłębnie zbadana przez ekspertów w swojej dziedzinie.
                    Głównym założeniem funduszu nieruchomości GANADOR Sp. z o.o. jest kupowanie nieruchomości
                    komercyjnych tylko za gotówkę, nie korzystając z kredytów bankowych. W ten sposób działalność
                    Spółki jest stabilna, a nabywane nieruchomości są nieobciążone. Spółka nie będąc pod presją ze
                    strony banku sama decyduje, kiedy i co kupuje oraz sprzedaje. Rozwój funduszu nieruchomości
                    GANADOR Sp. z o.o. zaplanowany jest długofalowo, a dynamika tego rozwoju konsekwentnie będzie
                    zwiększana dzięki zaufaniu inwestorów i powiększaniu generowanych zysków. Fundusz planuje być
                    spółką dywidendową.</div>
                <div class="news_text">Szczegóły transakcji zostaną precyzyjnie określone w Umowie nabycia przez
                    COLUMBUS CAPITAL S.A. od JR INVEST S.A. udziałów w Spółce GANADOR sp. z o.o., której termin
                    realizacji określono do dnia 31 marca 2015 r. W przypadku nie zawarcia Umowy w terminie do dnia
                    31.03.2015 r. niniejsze Porozumienie Inwestycyjne ulegnie automatycznemu rozwiązaniu.</div>
                <div class="news_text">Niniejsza informacja stanowi realizację strategii i rozwoju przyjętej i
                    realizowanej przez Spółkę COLUMBUS CAPITAL S.A. poprzez zaangażowanie Spółki w kolejny
                    projekt inwestycyjny. W opinii Zarządu Spółki, zrealizowanie transakcji, będącej przedmiotem
                    wyżej wskazanego Porozumienia Intencyjnego, wpłynie pozytywnie na przychody i wycenę Emitenta
                    oraz spowoduje rozwój grupy kapitałowejCOLUMBUS CAPITAL S.A.</div>
            </div>
            <div class="news2 news">
                <h3>31 styczeń 2015</h3>
                <h2 class="title text_gold">Podpisanie porozumienia inwestycyjnego z JR INVEST S.A.</h2>
                <div class="news_text">Columbus Capital S.A. informuje o zwołaniu na dzień 29 maja 2015 roku
                    Nadzwyczajnego Walnego Zgromadzenia, które odbędzie się o godzinie 10:00
                    w Krakowie przy ul. Kuźnicy Kołłątajowskiej 13 lok. 210... </div>
                <div class="news_text">Columbus Capital S.A. informuje o zwołaniu na dzień 29 maja 2015 roku
                    Nadzwyczajnego Walnego Zgromadzenia, które odbędzie się o godzinie 10:00
                    w Krakowie przy ul. Kuźnicy Kołłątajowskiej 13 lok. 210... </div>
                <div class="news_text">Columbus Capital S.A. informuje o zwołaniu na dzień 29 maja 2015 roku
                    Nadzwyczajnego Walnego Zgromadzenia, które odbędzie się o godzinie 10:00
                    w Krakowie przy ul. Kuźnicy Kołłątajowskiej 13 lok. 210... </div>
                <div class="news_text">Columbus Capital S.A. informuje o zwołaniu na dzień 29 maja 2015 roku
                    Nadzwyczajnego Walnego Zgromadzenia, które odbędzie się o godzinie 10:00
                    w Krakowie przy ul. Kuźnicy Kołłątajowskiej 13 lok. 210... </div>
                <div class="news_text">Columbus Capital S.A. informuje o zwołaniu na dzień 29 maja 2015 roku
                    Nadzwyczajnego Walnego Zgromadzenia, które odbędzie się o godzinie 10:00
                    w Krakowie przy ul. Kuźnicy Kołłątajowskiej 13 lok. 210... </div>
                <div class="news_text">Columbus Capital S.A. informuje o zwołaniu na dzień 29 maja 2015 roku
                    Nadzwyczajnego Walnego Zgromadzenia, które odbędzie się o godzinie 10:00
                    w Krakowie przy ul. Kuźnicy Kołłątajowskiej 13 lok. 210... </div>
            </div>
            <div class="news3 news">
                <h3>31 styczeń 2015</h3>
                <h2 class="title text_gold">Podpiscvxnfhdienia inwestycyjnego z JR INVEST S.A.</h2>
                <div class="news_text">Columbus Capital S.A. informuje o zwołaniu na dzień 29 maja 2015 roku
                    Nadzwyczajnego Walnego Zgromadzenia, które odbędzie się o godzinie 10:00
                    w Krakowie przy ul. Kuźnicy Kołłątajowskiej 13 lok. 210... </div>
                <div class="news_text">Columbus Capital S.A. informuje o zwołaniu na dzień 29 maja 2015 roku
                    Nadzwyczajnego Walnego Zgromadzenia, które odbędzie się o godzinie 10:00
                    w Krakowie przy ul. Kuźnicy Kołłątajowskiej 13 lok. 210... </div>
                <div class="news_text">Columbus Capital S.A. informuje o zwołaniu na dzień 29 maja 2015 roku
                    Nadzwyczajnego Walnego Zgromadzenia, które odbędzie się o godzinie 10:00
                    w Krakowie przy ul. Kuźnicy Kołłątajowskiej 13 lok. 210... </div>
                <div class="news_text">Columbus Capital S.A. informuje o zwołaniu na dzień 29 maja 2015 roku
                    Nadzwyczajnego Walnego Zgromadzenia, które odbędzie się o godzinie 10:00
                    w Krakowie przy ul. Kuźnicy Kołłątajowskiej 13 lok. 210... </div>
                <div class="news_text">Columbus Capital S.A. informuje o zwołaniu na dzień 29 maja 2015 roku
                    Nadzwyczajnego Walnego Zgromadzenia, które odbędzie się o godzinie 10:00
                    w Krakowie przy ul. Kuźnicy Kołłątajowskiej 13 lok. 210... </div>
            </div>
        </div>
    </section>
    <!--    CONTENT NEWS-->
    <!--    NEWS PAGE-->
    <section class="section" id="another_news">
        <div class="another_news">
            <h3>Inne wiadomości</h3>
        <!--        SLIDE 1-->
        <div class="slide" id="slide1" data-anchor="slide1">
            <div class="container">
                <ul>
                    <li class="col-md-4">
                        <button class="another_news_button" id="news1">
                            Zwołanie Nadzwyczajnego Walnego Zgromadzenia Emitenta
                        </button>
                    </li>
                    <li class="col-md-4">
                        <button class="another_news_button" id="news2">
                            Zgoda na wprowadzenie akcji Columbus Energy S.A na NewConnect
                        </button>
                    </li>
                    <li class="col-md-4">
                        <button class="another_news_button" id="news3">
                            Podpisanie porozumienia inwestycyjnego z JR INVEST S.A.
                        </button>
                    </li>
                </ul>
            </div>
        </div>
        <!--        END SLIDE 1-->

        <!--        SLIDE 2-->
        <div class="slide" id="slide2" data-anchor="slide2">
            <div class="container">
                <ul>
                    <li class="col-md-4">
                        <button class="another_news_button" id="news1">
                            Zwołanie Nadzwyczajnego Walnego Zgromadzenia Emitenta
                        </button>
                    </li>
                    <li class="col-md-4">
                        <button class="another_news_button" id="news2">
                            Zgoda na wprowadzenie akcji Columbus Energy S.A na NewConnect
                        </button>
                    </li>
                    <li class="col-md-4">
                        <button class="another_news_button" id="news3">
                            Podpisanie porozumienia inwestycyjnego z JR INVEST S.A.
                        </button>
                    </li>
                </ul>
            </div>
        </div>
        <!--        END SLIDE 2-->

        <!--        SLIDE 3-->
        <div class="slide" id="slide3" data-anchor="slide3">
            <div class="container">
                <ul>
                    <li class="col-md-4">
                        <button class="another_news_button" id="news1">
                            Zwołanie Nadzwyczajnego Walnego Zgromadzenia Emitenta
                        </button>
                    </li>
                    <li class="col-md-4">
                        <button class="another_news_button" id="news2">
                            Zgoda na wprowadzenie akcji Columbus Energy S.A na NewConnect
                        </button>
                    </li>
                    <li class="col-md-4">
                        <button class="another_news_button" id="news3">
                            Podpisanie porozumienia inwestycyjnego z JR INVEST S.A.
                        </button>
                    </li>
                </ul>
            </div>
        </div>
        <!--        END SLIDE 3-->
            </div>
    </section>
    <!--    END NEWS PAGE-->


    <!--    CONTACT-->
    <section class="section" id="contact-page">
        <h3>Kontakt</h3>

        <form action="">
            <div class="container">
                <div class="col-md-6">
                    <input type="text" name="name" id="name" required="required" placeholder="IMIĘ..."/>
                    <input type="text" name="surname" id="surname" required="required" placeholder="NAZWISKO..."/>
                    <input type="email" name="email" id="email" required="required" placeholder="TWÓJ ADRES E-MAIL.."/>
                </div>
                <div class="col-md-6">
                    <textarea name="content" id="content" placeholder="WPISZ TREŚĆ..."></textarea>
                </div>
                <div class="col-md-12">
                    <input type="submit" value="Wyślij">
                </div>
            </div>
        </form>
        <ul class="contact_info">
            <li><img src="imgs/tel.png" alt=""/>tel. +48 32 203 31 30</li>
            <li><img src="imgs/mail.png" alt=""/>biuro@columbuscapital.pl</li>
        </ul>
        <div class="contact_text">
            Sąd Rejonowy Katowice - Wschód w Katowicach, VIII Wydział Gospodarczy, Spółka wpisana do Krajowego
            Rejestru Przedsiębiorców pod numerem KRS 0000373608, Spółka posiada REGON 241811803, NIP 9492163154
        </div>
        <footer>
            <p>Wszystkie prawa zastrzeżone. COLUMBUS CAPITAL SA © 2015 | Wykonanie: <a href="http://besite.pl/">BeSite.pl</a></p>
        </footer>
    </section>
    <!--    END CONTACT-->

</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>-->
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
<script>

</script>
</body>
</html>