<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="Resource-type" content="Document" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0,user-scalable=0;">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Columbus Capital</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- fullpage.js  style -->
    <link rel="stylesheet" type="text/css" href="css/jquery.fullPage.css" />
    <link rel="stylesheet" type="text/css" href="css/examples.css" />

    <!-- Author syles   -->
    <link rel="stylesheet" type="text/css" href="css/style.css" />

    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic&subset=latin,latin-ext' rel='stylesheet' type='text/css'>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!--[if IE]>
    <script type="text/javascript">
        var console = { log: function() {} };
    </script>
    <![endif]-->

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script>

    <!-- FULL PAGE SCRIPTS-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script type="text/javascript" src="/vendors/jquery.slimscroll.min.js"></script>
    <script type="text/javascript" src="js/jquery.fullPage.js"></script>
    <script type="text/javascript" src="js/examples.js"></script>
    <script>
        //        MENU SCRIPT
        $(document).ready(function() {
            var FeelItem = 'NULL';
            $('button.show_menu').on('click', function () {
                if(FeelItem == 'NULL')
                {
                    $('div.overlay-menu').addClass('show_overlay');
                    $('.menu').addClass('show_block');
                    FeelItem = '1';
                }
            });
            $('button.exit').on('click', function () {
                if(FeelItem == '1')
                {
                    $('div.overlay-menu').removeClass('show_overlay');
                    $('.menu').removeClass('show_block');
                    FeelItem = 'NULL'
                }
            });
        });
        //        END MENU SCRIPT
    </script>
    <script type="text/javascript">
        //        FULLPAGE SCRIPT
        $(document).ready(function() {
            $('#fullpage').fullpage({
                sectionsColor: ['#ffffff', '#ffffff','#434343'],
                menu: '#menu',
                scrollBar:true,
                slidesNavigation: false,
                autoScrolling: false,
                fitToSection: false
            });
        });
        //       END  FULLPAGE SCRIPT
        //        BACKGROUND LOGO AND MENU SCTIPT
        $(window).scroll(function() {
            if($( window ).width()<=1630 && $( window ).width()>991) {
                var scroll = jQuery(window).scrollTop();
                if (scroll >= 200) {
                    jQuery('header.page').addClass('page_active');
                }
                else {
                    jQuery('header.page').removeClass('page_active');
                }
            }
        });
        //        END BACKGROUND LOGO AND MENU SCTIPT
    </script>

</head>
<body>

<!--MENU-->
<header id="main" class="page">
    <div class="col-md-12">
        <div class="col-md-3">
            <div class="logo">
                <a href="/index.php"><img src="imgs/columbus.png" alt=""/></a>
            </div>
        </div>
        <div class="col-md-offset-8 col-md-1">
            <button class="show_menu">MENU<img src="imgs/hamburger.png" alt=""/></button>
        </div>
        <div class="menu">
            <div class="content_menu">
                <button class="exit"><img src="imgs/exit.png" alt=""/></button>
                <ul class="menu_items">
                    <li><a href="/index.php">Strona Głowna</a></li>
                    <li><a href="/about_us.php">O Nas</a></li>
                    <li><a href="/offer.php">Oferta</a></li>
                    <li><a href="/news.php">Aktualności</a></li>
                    <li><a href="/investors_relation.php">Relacje Inwestorskie</a></li>
                    <li><a href="/contact.php">Kontakt</a></li>
                </ul>
                <ul class="social_items">
                    <li><a href="#"><img src="imgs/facebook.png" alt=""/></a></li>
                    <li><a href="#"><img src="imgs/linkin.png" alt=""/></a></li>
                    <li><a href="#"><img src="imgs/twitter.png" alt=""/></a></li>
                </ul>
            </div>
        </div>
        <div class="overlay-menu"></div>
    </div>
</header>
<!--END MENU-->
<div id="fullpage">



    <!--    ABOUT US PAGE-->
    <section class="section" id="section_page_investors_relation">
        <div class="container">
            <div class="col-md-12">
                <h3>Relacje inwestorskie</h3>
                <h2 class="text_intro text_white">Jesteśmy dla Ciebie odkrywcami możliwości</h2>
                <div class="triangle_white"></div>
            </div>
        </div>
    </section>
    <!--    END ABOUT US PAGE-->

    <!--    ABOUT US CONTENT-->
    <section class="section" id="section_investors_relation_content">
        <div class="col-md-12">
            <div class="container">
                <div class="submenu">
                    <ul id="relations_menu" class="nav nav-tabs">
                        <li class="col-md-3"><a href="#">Notowania</a></li>
                        <li class="col-md-3"><a href="#">Akcjonariat</a></li>
                        <li class="col-md-3"><a href="#">Rada nadzorcza</a></li>
                        <li class="col-md-3 active"><a href="#">Walne Zgromadzenia</a></li>
                        <li class="col-md-3"><a href="#">Dokumentacja</a></li>
                        <li class="col-md-3"><a href="#">Raporty bieżące</a></li>
                        <li class="col-md-3"><a href="#">Kalendarium</a></li>
                        <li class="col-md-3"><a href="#">Instytucje rk</a></li>
                        <li class="col-md-3"><a href="#">Dok. informacyjny</a></li>
                        <li class="col-md-3"><a href="#">Centrum prasowe</a></li>
                        <li class="col-md-3"><a href="#">O nas w mediach</a></li>
                        <li class="col-md-3"><a href="#">Raporty okresowe</a></li>
                    </ul>
            </div>
            <div class="col-md-12">
                <div class="content_text">
                    <h3>Walne Zgromadzenia</h3>
                    <h2 class="title">Zwyczajne Walne zgromadzenie Akcjonariuszy 30-06-2015</h2>
                    <table>
                        <thead>
                        <tr>
                            <td>Nazwa</td>
                            <td>Pobierz</td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Ogłoszenie o zwołaniu zwyczajnego Walnego Zgormadzenia Akcjonariuszy</td>
                            <td><a href="#">Pobierz</a></td>
                        </tr>
                        <tr>
                            <td>Projekty uchwał na zwyczajne Walne Zgormadzenie</td>
                            <td><a href="#">Pobierz</a></td>
                        </tr>
                        <tr>
                            <td>Informacja o ogólnej liczbie akcji w Spółce i liczbie głosów</td>
                            <td><a href="#">Pobierz</a></td>
                        </tr>
                        <tr>
                            <td>Pełnomocnictwo udzielane przez osoby prawne i spółki osobowe</td>
                            <td><a href="#">Pobierz</a></td>
                        </tr>
                        <tr>
                            <td>Pełnomocnictwo udzielane przez osoby fizyczne</td>
                            <td><a href="#">Pobierz</a></td>
                        </tr>
                        <tr>
                            <td>Formularz pozwalający na wykonywanie prawa głosu przez pełnomocnika</td>
                            <td><a href="#">Pobierz</a></td>
                        </tr>
                        </tbody>
                    </table>

                    <h2 class="title">Nadwyczajne Walne zgromadzenie Akcjonariuszy 08-04-2015</h2>
                    <table>
                        <thead>
                        <tr>
                            <td>Nazwa</td>
                            <td>Pobierz</td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Ogłoszenie o zwołaniu nadzwyczajnego Walnego Zgormadzenia Akcjonariuszy</td>
                            <td><a href="#">Pobierz</a></td>
                        </tr>
                        <tr>
                            <td>Projekty uchwał na nadzwyczajne Walne Zgormadzenie</td>
                            <td><a href="#">Pobierz</a></td>
                        </tr>
                        <tr>
                            <td>Informacja o ogólnej liczbie akcji w Spółce i liczbie głosów</td>
                            <td><a href="#">Pobierz</a></td>
                        </tr>
                        <tr>
                            <td>Pełnomocnictwo udzielane przez osoby prawne i spółki osobowe</td>
                            <td><a href="#">Pobierz</a></td>
                        </tr>
                        <tr>
                            <td>Pełnomocnictwo udzielane przez osoby fizyczne</td>
                            <td><a href="#">Pobierz</a></td>
                        </tr>
                        <tr>
                            <td>Formularz pozwalający na wykonywanie prawa głosu przez pełnomocnika</td>
                            <td><a href="#">Pobierz</a></td>
                        </tr>
                        </tbody>
                    </table>


                    <h2 class="title">Nadwyczajne Walne zgromadzenie Akcjonariuszy 30-06-2014</h2>
                    <table>
                        <thead>
                        <tr>
                            <td>Nazwa</td>
                            <td>Pobierz</td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Ogłoszenie o zwołaniu zwyczajnego Walnego Zgormadzenia Akcjonariuszy</td>
                            <td><a href="#">Pobierz</a></td>
                        </tr>
                        <tr>
                            <td>Projekty uchwał na nadzwyczajne Walne Zgormadzenie</td>
                            <td><a href="#">Pobierz</a></td>
                        </tr>
                        <tr>
                            <td>Formularz pozwalający na wykonywanie prawa głosu przez pełnomocnika</td>
                            <td><a href="#">Pobierz</a></td>
                        </tr>
                        <tr>
                            <td>Pełnomocnictwo udzielane przez osoby fizyczne</td>
                            <td><a href="#">Pobierz</a></td>
                        </tr>
                        <tr>
                            <td>Pełnomocnictwo udzielane przez osoby prawne i spółki osobowe</td>
                            <td><a href="#">Pobierz</a></td>
                        </tr>
                        </tbody>
                    </table>
                    <button class="more text_gold">Rozwiń więcej</button>
                </div>
            </div>
    </section>
    <!--    END ABOUT US CONTENT-->
    
    <!--    CONTACT-->
    <section class="section" id="contact-page">
        <h3>Kontakt</h3>

        <form action="">
            <div class="container">
                <div class="col-md-6">
                    <input type="text" name="name" id="name" required="required" placeholder="IMIĘ..."/>
                    <input type="text" name="surname" id="surname" required="required" placeholder="NAZWISKO..."/>
                    <input type="email" name="email" id="email" required="required" placeholder="TWÓJ ADRES E-MAIL.."/>
                </div>
                <div class="col-md-6">
                    <textarea name="content" id="content" placeholder="WPISZ TREŚĆ..."></textarea>
                </div>
                <div class="col-md-12">
                    <input type="submit" value="Wyślij">
                </div>
            </div>
        </form>
        <ul class="contact_info">
            <li><img src="imgs/tel.png" alt=""/>tel. +48 32 203 31 30</li>
            <li><img src="imgs/mail.png" alt=""/>biuro@columbuscapital.pl</li>
        </ul>
        <div class="contact_text">
            Sąd Rejonowy Katowice - Wschód w Katowicach, VIII Wydział Gospodarczy, Spółka wpisana do Krajowego
            Rejestru Przedsiębiorców pod numerem KRS 0000373608, Spółka posiada REGON 241811803, NIP 9492163154
        </div>
        <footer>
            <p>Wszystkie prawa zastrzeżone. COLUMBUS CAPITAL SA © 2015 | Wykonanie: <a href="http://besite.pl/">BeSite.pl</a></p>
        </footer>
    </section>
    <!--    END CONTACT-->

</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&language=pl"></script>
<script>

</script>
</body>
</html>