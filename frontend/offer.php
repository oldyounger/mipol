<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="Resource-type" content="Document" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0,user-scalable=0;">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Columbus Capital</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- fullpage.js  style -->
    <link rel="stylesheet" type="text/css" href="css/jquery.fullPage.css" />
    <link rel="stylesheet" type="text/css" href="css/examples.css" />

    <!-- Author syles   -->
    <link rel="stylesheet" type="text/css" href="css/style.css" />

    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic&subset=latin,latin-ext' rel='stylesheet' type='text/css'>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!--[if IE]>
    <script type="text/javascript">
        var console = { log: function() {} };
    </script>
    <![endif]-->

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script>

    <!-- FULL PAGE SCRIPTS-->
    <script type="text/javascript" src="/vendors/jquery.slimscroll.min.js"></script>
    <script type="text/javascript" src="js/jquery.fullPage.js"></script>
    <script type="text/javascript" src="js/examples.js"></script>
    <script>
        //        MENU SCRIPT
        $(document).ready(function() {
            var FeelItem = 'NULL';
            $('button.show_menu').on('click', function () {
                if(FeelItem == 'NULL')
                {
                    $('div.overlay-menu').addClass('show_overlay');
                    $('.menu').addClass('show_block');
                    FeelItem = '1';
                }
            });
            $('button.exit').on('click', function () {
                if(FeelItem == '1')
                {
                    $('div.overlay-menu').removeClass('show_overlay');
                    $('.menu').removeClass('show_block');
                    FeelItem = 'NULL'
                }
            });
        });
        //        END MENU SCRIPT
    </script>
    <script type="text/javascript">
        //        FULLPAGE SCRIPT
        $(document).ready(function() {
                $('#fullpage').fullpage({
                    sectionsColor: ['#ffffff', '#ffffff','#434343'],
                    scrollBar:true,
                    slidesNavigation: false,
                    autoScrolling: false,
                    fitToSection: false
                });
        });
        //        END FULLPAGE SCRIPT
        //        BACKGROUND LOGO AND MENU SCTIPT
        $(window).scroll(function() {
            if($( window ).width()<=1630 && $( window ).width()>991) {
                var scroll = jQuery(window).scrollTop();
                if (scroll >= 200) {
                    jQuery('header.page').addClass('page_active');
                }
                else {
                    jQuery('header.page').removeClass('page_active');
                }
            }
        });
        //        END BACKGROUND LOGO AND MENU SCTIPT
    </script>
</head>
<body>
<!--MENU-->
<header id="main" class="page">
    <div class="col-md-12">
        <div class="col-md-3">
            <div class="logo">
                <a href="/index.php"><img src="imgs/columbus.png" alt=""/></a>
            </div>
        </div>
        <div class="col-md-offset-8 col-md-1">
            <button class="show_menu">MENU<img src="imgs/hamburger.png" alt=""/></button>
        </div>
        <div class="menu">
            <div class="content_menu">
                <button class="exit"><img src="imgs/exit.png" alt=""/></button>
                <ul class="menu_items">
                    <li><a href="/index.php">Strona Głowna</a></li>
                    <li><a href="/about_us.php">O Nas</a></li>
                    <li><a href="/offer.php">Oferta</a></li>
                    <li><a href="/news.php">Aktualności</a></li>
                    <li><a href="/investors_relation.php">Relacje Inwestycyjne</a></li>
                    <li><a href="/contact.php">Kontakt</a></li>
                </ul>
                <ul class="social_items">
                    <li><a href="#"><img src="imgs/facebook.png" alt=""/></a></li>
                    <li><a href="#"><img src="imgs/linkin.png" alt=""/></a></li>
                    <li><a href="#"><img src="imgs/twitter.png" alt=""/></a></li>
                </ul>
            </div>
        </div>
        <div class="overlay-menu"></div>
    </div>
</header>
<!--END MENU-->
<div id="fullpage">
    <!--    PAGE OFFER-->
    <section class="section" id="section_page_offer">
        <div class="container">
            <div class="col-md-12">
                <h3>Oferta</h3>
                <h2 class="text_intro text_white">Działalność inwestycyjna jest filarem naszej działalności.</h2>
                <div class="triangle_white"></div>
            </div>
        </div>
    </section>
    <!--    END PAGE OFFER-->


    <!--    OFFER CONTENT-->
    <section class="section" id="offer">
        <div class="container">
            <div class="col-md-12">
                <div class="content_offer">
                    <h3>Oferta</h3>
                    <h2 class="text_intro text_gold more_padding">Nasza oferta skierowana jest do podmiotów poszukujących rozwiązań i doradztwa w zakresie
                        finansowania i rozwijania swojego biznesu oraz do Inwestorów zainteresowanych
                        inwestycjami na rynkach publicznych i niepublicznych.</h2>
                    <div class="text_content ">Działalność inwestycyjna jest filarem naszej działalności. Zespół
                        COLUMBUS CAPITAL SA posiada wiedzę i doświadczenie w pozyskiwaniu kapitału dla spółek
                        publicznych i niepublicznych, zarówno od inwestorów instytucjonalnych jak i inwestorów
                        prywatnych. Sami występujemy w roli inwestora i angażujemy się w atrakcyjne przedsięwzięcia,
                        które są zgodne z naszą etyką biznesową i strategią. Współpracujemy z grupą inwestorów z
                        Polski i z zagranicy w oparciu o wypracowane przez lata relacje. Pomagamy w doborze
                        odpowiedniej formy inwestycji (kapitał udziałowy, dłużny).</div>
                    <div class="text_content ">Rynek Polski jest niezwykle atrakcyjnym inwestycyjnie obszarem,
                        jednak wiele spółek napotyka na bariery rozwoju podyktowane trudnym dostępem do kapitału
                        oraz brakiem niezbędnego know-how, aby się dynamicznie rozwijać. Pomagamy przezwyciężyć
                        klientom wszystkie te bariery. Naszym priorytetem jest najwyższa jakość świadczonych usług
                        oraz  osiąganie wyznaczonego celu.</div>

                    <h2 class="text_intro text_gold">Doradztwo Inwestycyjne</h2>
                    <div class="text_content ">Dla potrzeb firm oraz projektów inwestycyjnych, w ramach bankowości
                        inwestycyjnej, dostosowujemy różne formy pozyskania kapitału. Konstruujemy w optymalny sposób
                        strukturę finansowania w oparciu o różne formy kapitału własnego oraz obcego. Dobrze dobrane
                        finansowanie zwiększa dyscyplinę finansową w firmie i elastyczność jej działania, czego efektem
                        jest szybsze osiągnięcie celów inwestycyjnych. Pozyskanie finansowania. Usługa polega na
                        pozyskaniu kapitału odpowiadającego potrzebom i przede wszystkim możliwościom klienta.</div>
                    <div class="text_content ">Doradztwo strategiczne. Na bazie przyjętej strategii rozwoju doradzamy
                        właścicielom firm w zakresie ukształtowania właściwej struktury kapitału na rozwój.
                        Proponujemy kilka wariantów finansowania rozwoju. Określamy możliwe źródła pozyskania
                        kapitałów własnych lub zewnętrznych.</div>
                    <div class="text_content ">Organizacja fuzji i przejęć. Zajmujemy się poszukiwaniem na zlecenie
                        klienta i w jego imieniu spółek  spełniających określone wymogi i parametry operacyjne,
                        mogących być przedmiotem przejęcia lub połączenia. Określamy warunki transakcji,
                        opracowujemy strategię, wyceniamy przedmiot transakcji, uczestniczymy w pełnym audycie
                        realizując określony cel klienta. Każdy etap współpracy z nami poparty jest zachowaniem
                        poufności potwierdzonym stosownymi dokumentami.</div>

                    <h2 class="text_intro text_gold">Inwestycje Własne</h2>
                    <div class="text_content ">Zespół stanowią osoby inwestujące od lat na różnych rynkach osiągając
                        stopy zwrotu na poziomie znacząco przekraczającym średnią rynkową przy jednoczesnym
                        bezpieczeństwie zachowania kapitału bazując na inwestycjach opartych przede wszystkim na
                        fundamentalnej wycenie niedowartościowanych projektów i podmiotow. Realizujemy inwestycje
                        na rynkach publicznych, angażujemy się również w podmioty niepubliczne w tym w spółki rodzinne.
                        Część naszego kapitału inwestujemy na rynku Forex.</div>

                    <h2 class="text_intro text_gold">Portale Internetowe</h2>
                    <div class="text_content ">COLUMBUS CAPITAL SA na początku swojej działalności specjalizował
                        się w kompleksowym świadczeniu usług z zakresu projektowania i tworzenia portali oraz serwisów
                        internetowych w najnowszej technologii, wykonywaniem i projektowaniem graficznych elementów
                        wizerunkowych oraz konsultingu i marketingu sportowego na rzecz klientów – podmiotów
                        gospodarczych, a także osób fizycznych.</div>
                </div>
            </div>
        </div>
    </section>
    <!--    END OFFER CONTENT-->



    <!--    CONTACT-->
    <section class="section" id="contact-page">
        <h3>Kontakt</h3>

        <form action="">
            <div class="container">
                <div class="col-md-6">
                    <input type="text" name="name" id="name" required="required" placeholder="IMIĘ..."/>
                    <input type="text" name="surname" id="surname" required="required" placeholder="NAZWISKO..."/>
                    <input type="email" name="email" id="email" required="required" placeholder="TWÓJ ADRES E-MAIL.."/>
                </div>
                <div class="col-md-6">
                    <textarea name="content" id="content" placeholder="WPISZ TREŚĆ..."></textarea>
                </div>
                <div class="col-md-12">
                    <input type="submit" value="Wyślij">
                </div>
            </div>
        </form>
        <ul class="contact_info">
            <li><img src="imgs/tel.png" alt=""/>tel. +48 32 203 31 30</li>
            <li><img src="imgs/mail.png" alt=""/>biuro@columbuscapital.pl</li>
        </ul>
        <div class="contact_text">
            Sąd Rejonowy Katowice - Wschód w Katowicach, VIII Wydział Gospodarczy, Spółka wpisana do Krajowego
            Rejestru Przedsiębiorców pod numerem KRS 0000373608, Spółka posiada REGON 241811803, NIP 9492163154
        </div>
        <footer>
            <p>Wszystkie prawa zastrzeżone. COLUMBUS CAPITAL SA © 2015 | Wykonanie: <a href="http://besite.pl/">BeSite.pl</a></p>
        </footer>
    </section>
    <!--    END CONTACT-->
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&language=pl"></script>
<script>

</script>
</body>
</html>