<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="Resource-type" content="Document" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0,user-scalable=0;">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Columbus Capital</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- fullpage.js  style -->
    <link rel="stylesheet" type="text/css" href="css/jquery.fullPage.css" />
    <link rel="stylesheet" type="text/css" href="css/examples.css" />

    <!-- Author syles   -->
    <link rel="stylesheet" type="text/css" href="css/style.css" />

    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic&subset=latin,latin-ext' rel='stylesheet' type='text/css'>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!--[if IE]>
    <script type="text/javascript">
        var console = { log: function() {} };
    </script>
    <![endif]-->

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script>

    <!-- FULL PAGE SCRIPTS-->
    <script type="text/javascript" src="/vendors/jquery.slimscroll.min.js"></script>
    <script type="text/javascript" src="js/jquery.fullPage.js"></script>
    <script type="text/javascript" src="js/examples.js"></script>
    <script type="text/javascript" src="js/spin.js"></script>
    <script>
        $(document).ready(function() {
            var opts = {
                lines: 13 // The number of lines to draw
                , length: 10 // The length of each line
                , width: 5 // The line thickness
                , radius: 20 // The radius of the inner circle
                , scale: 1 // Scales overall size of the spinner
                , corners: 1 // Corner roundness (0..1)
                , color: '#fff' // #rgb or #rrggbb or array of colors
                , opacity: 0.25 // Opacity of the lines
                , rotate: 0 // The rotation offset
                , direction: 1 // 1: clockwise, -1: counterclockwise
                , speed: 1 // Rounds per second
                , trail: 60 // Afterglow percentage
                , fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
                , zIndex: 2e9 // The z-index (defaults to 2000000000)
                , className: 'spinner' // The CSS class to assign to the spinner
                , top: '50%' // Top position relative to parent
                , left: '50%' // Left position relative to parent
                , shadow: false // Whether to render a shadow
                , hwaccel: false // Whether to use hardware acceleration
                , position: 'absolute' // Element positioning
            }
            var target = document.getElementById('preloader')
            var spinner = new Spinner(opts).spin(target);
        });
    </script>
    <script>
//        MENU SCRIPT
        $(document).ready(function() {
            var FeelItem = 'NULL';
            $('button.show_menu').on('click', function () {
                if(FeelItem == 'NULL')
                {
                    $('div.overlay-menu').addClass('show_overlay');
                    $('.menu').addClass('show_block');
                    FeelItem = '1';
                }
            });
            $('button.exit').on('click', function () {
                if(FeelItem == '1')
                {
                    $('div.overlay-menu').removeClass('show_overlay');
                    $('.menu').removeClass('show_block');
                    FeelItem = 'NULL'
                }
            });
        });
//        END MNU SCRIPT
    </script>
    <script type="text/javascript">
        //        FULLPAGE SCRIPT
        $(document).ready(function() {
            if($( window ).width()<=1025) {
                $('#fullpage').fullpage({
                    sectionsColor: ['#ffffff', '#ccab81', '#ffffff', '#ccab81', '#ffffff', '#434343'],
                    menu: '#menu',
                    scrollBar:true,
                    slidesNavigation: false,
                    autoScrolling: false,
                    fitToSection: false
                });
            }
            else{
                $('#fullpage').fullpage({
                    sectionsColor: ['#ffffff', '#ccab81', '#ffffff', '#ccab81', '#ffffff', '#434343'],
                    anchors: ['Columbus Capital', 'O nas', 'Relacje Inwestorskie', 'Aktualności', 'Mapa', 'Kontakt'],
                    menu: '#menu',
                    navigation: true,
                    navigationPosition: 'left',
                    navigationTooltips: ['Columbus Capital', 'O nas', 'Relacje Inwestorskie', 'Aktualności', 'Mapa', 'Kontakt'],
                    scrollOverflow: true,
                    scrollBar:true,
                    slidesNavigation: true
                });
            }
//        END FULLPAGE SCRIPT
//            PRELOADER
            jQuery(document).ready(function($) {
// site preloader -- also uncomment the div in the header and the css style for #preloader
                $(window).load(function(){
                    $('#preloader').fadeOut('slow',function(){$(this).remove();});
                });
            });
//            END PRELOADER
//            GOOGLE MAP SCRIPT
            function initialize() {
                // Basic options for a simple Google Map
                // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
                var my = new google.maps.LatLng(50.260594,19.023892);
                var image = ('imgs/marker.png');
                if($( window ).width()<=1025) {
                    var mapOptions = {
                        scrollwheel: false,
                        zoom: 16,
                        center: my,
                        icon: image,
                        draggable: false,
                        mapTypeControl:true,
                        panControl: true,
                        zoomControl: true,
                        zoomControlOptions: {
                            style: google.maps.ZoomControlStyle.LARGE,
                            position: google.maps.ControlPosition.LEFT_BOTTOM
                        },
                        panControlOptions: {
                            position: google.maps.ControlPosition.LEFT_BOTTOM
                        },
                        mapTypeControlOptions: {
                            style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                            position: google.maps.ControlPosition.RIGHT_BOTTOM
                        },
                        styles: [
                            {
                                featureType:'water',stylers:[{color:'#a8a8a8'},{visibility:'on'}]
                            },
                            {
                                featureType:'landscape',stylers:[{color:'#f2f2f2'}]
                            },
                            {
                                featureType:'road',stylers:[{saturation:-100},{lightness:45}]
                            },
                            {
                                featureType:'road.highway',stylers:[{visibility:'simplified'}]
                            },
                            {
                                featureType:'road.arterial',elementType:'labels.icon', stylers:[{visibility:'off'}]
                            },
                            {
                                featureType:'administrative',elementType:'labels.text.fill',stylers:[{color:'#444444'}]
                            },
                            {
                                featureType:'transit',    stylers:[{visibility:'off'}]
                            },
                            {
                                featureType:'poi',    stylers:[{visibility:'off'}]
                            }
                        ]
                    };
                }
                else{
                    var mapOptions = {
                        scrollwheel: false,
                        zoom: 16,
                        center: my,
                        icon: image,
                        mapTypeControl:true,
                        panControl: true,
                        zoomControl: true,
                        zoomControlOptions: {
                            style: google.maps.ZoomControlStyle.LARGE,
                            position: google.maps.ControlPosition.LEFT_BOTTOM
                        },
                        panControlOptions: {
                            position: google.maps.ControlPosition.LEFT_BOTTOM
                        },
                        mapTypeControlOptions: {
                            style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                            position: google.maps.ControlPosition.RIGHT_BOTTOM
                        },
                        styles: [
                            {
                                featureType:'water',stylers:[{color:'#a8a8a8'},{visibility:'on'}]
                            },
                            {
                                featureType:'landscape',stylers:[{color:'#f2f2f2'}]
                            },
                            {
                                featureType:'road',stylers:[{saturation:-100},{lightness:45}]
                            },
                            {
                                featureType:'road.highway',stylers:[{visibility:'simplified'}]
                            },
                            {
                                featureType:'road.arterial',elementType:'labels.icon',stylers:[{visibility:'off'}]
                            },
                            {
                                featureType:'administrative',elementType:'labels.text.fill',stylers:[{color:'#444444'}]
                            },
                            {
                                featureType:'transit',stylers:[{visibility:'off'}]
                            },
                            {
                                featureType:'poi',stylers:[{visibility:'off'}]
                            }
                        ]
                    };
                }
                var contentString = 'Columbus Capital';
                var infowindow = new google.maps.InfoWindow({
                    content: contentString
                });
                var map = new google.maps.Map(document.getElementById('map'),
                    mapOptions);
                google.maps.event.addListener(map, "click", function(event) {
                    infowindow.close();
                });
                var marker = new google.maps.Marker({
                    position: my,
                    map: map,
                    icon: image,
                    title: 'Columbus Capital'
                });
                google.maps.event.addListener(marker, 'click', function() {
                    infowindow.open(map,marker);
                });
            }
            google.maps.event.addDomListener(window, 'load',initialize());
        });
//        END GOOGLE MAP SCRIPT

    </script>
    <script>
//        CHANGE LOGO AND MENU COLOR SCRIPT
        $(window).scroll(function() {
            if($( window ).width()>=1024) {
                var scroll = $(window).scrollTop();
                var about = $("section#section_about_us").position();
                var investors = $("section#investors_relation").position();
                var news = $("section#news").position();
                var map = $("section#google_map").position();
                var contact = $("section#contact").position();
                var logo_height = $(".logo img").height();
                if ( (scroll >= about.top-logo_height*4 && scroll < investors.top) || (scroll >= news.top-logo_height*4 && scroll < map.top) ) {
                    $(".logo img").attr("src",'imgs/columbus_white.png');
                    $(".show_menu img").attr("src",'imgs/hamburger_white.png');
                    $(".show_menu").addClass("white");
                }
                else {
                    $(".logo img").attr("src",'imgs/columbus.png');
                    $(".show_menu img").attr("src",'imgs/hamburger.png');
                    $(".show_menu").removeClass("white");
                }
                if ( scroll >= map.top-logo_height*4 && scroll < contact.top ) {
                    $(".logo img").attr("src",'imgs/columbus.png');
                    $(".show_menu img").attr("src",'imgs/hamburger.png');
                    $(".show_menu").removeClass("white");
                }
            if ( scroll >= map.top-logo_height*4 && scroll < contact.top ) {
                $("div.fp-tooltip.left").css("color","#ccab81");
                $("#fp-nav ul li a span").css("border","2px solid #ccab81");
                $(".fp-slidesNav ul li a span").css("border","2px solid #ccab81");
                $("#fp-nav ul li a.active span").css("background-color","#ccab81");
                $(".fp-slidesNav ul li a.active span").css("background-color","#ccab81");
            }
            else {
                $("div.fp-tooltip.left").css("color","#ffffff");
                $("#fp-nav ul li a span").css("border","2px solid #fff");
                $(".fp-slidesNav ul li a span").css("border","2px solid #fff");
                $("#fp-nav ul li a.active span").css("background-color","#fff");
                $(".fp-slidesNav ul li a.active span").css("background-color","#fff");
            }
         }
        });
//      CHANGE LOGO AND MENU COLOR SCRIPT
    </script>
</head>
<body>
<div id="preloader">
    <div class="spinner"></div>
</div>
<!--MENU-->
<header id="main">
    <div class="col-md-12">
        <div class="col-md-3">
            <div class="logo">
                <a href="/index.php"><img src="imgs/columbus.png" alt=""/></a>
            </div>
        </div>
        <div class="col-md-offset-8 col-md-1">
            <button class="show_menu">MENU<img src="imgs/hamburger.png" alt=""/></button>
        </div>
        <div class="menu">
            <div class="content_menu">
                <button class="exit"><img src="imgs/exit.png" alt=""/></button>
                <ul class="menu_items">
                    <li><a href="/index.php">Strona Głowna</a></li>
                    <li><a href="/about_us.php">O Nas</a></li>
                    <li><a href="/offer.php">Oferta</a></li>
                    <li><a href="/news.php">Aktualności</a></li>
                    <li><a href="/investors_relation.php">Relacje Inwestorskie</a></li>
                    <li><a href="/contact.php">Kontakt</a></li>
                </ul>
                <ul class="social_items">
                    <li><a href="#"><img src="imgs/facebook.png" alt=""/></a></li>
                    <li><a href="#"><img src="imgs/linkin.png" alt=""/></a></li>
                    <li><a href="#"><img src="imgs/twitter.png" alt=""/></a></li>
                </ul>
            </div>
        </div>
        <div class="overlay-menu"></div>
    </div>
</header>
<!--END MENU-->
<div id="fullpage">
<!--    COLUMBUS-->
    <section class="section" id="section_columbus">
        <div class="container">
            <div class="col-md-12">
                <h3>Columbus Capital</h3>
                <h2 class="text_intro text_white">Columbus Capital SA to wysoka jakość oferowanych usług i projektów inwestycyjnych, natychmiastowa reakcja na
                    potrzeby rynku, nieustanny rozwój oparty o dotychczasową wiedzę i realizowane cele.</h2>
                <a class="more text_gold" href="/about_us.php">Dowiedz się więcej</a>
                <div class="triangle_white"></div>
            </div>
        </div>
    </section>
<!--    END COLUMBUS-->

<!--    ABOUT US-->
    <section class="section" id="section_about_us">
        <div class="col-md-6">
            <div class="content_text">
                <h3>O nas</h3>
                <h2 class="text_intro text_white">Columbus Capital SA to wysoka jakość oferowanych usług i projektów inwestycyjnych, natychmiastowa reakcja na
                    potrzeby rynku, nieustanny rozwój oparty o dotychczasową wiedzę i realizowane cele.</h2>
                <a class="more text_white" href="/about_us.php">Poznaj nas</a>
                <div class="triangle_white"></div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="about_us_img"></div>
        </div>
    </section>
<!--    END ABOUT US-->

<!--    INVESTORS RELATION-->
    <section class="section" id="investors_relation">
        <div class="col-md-6">
            <div class="investors_relation_img"></div>
        </div>
        <div class="col-md-6">
            <div class="content_text">
                <h3>Relacje inwestorskie</h3>
                <h2 class="text_intro text_gold">Spółka ewoluując w swoim rozwoju, pozyskuje nowych inwestorów oraz zarządzających, którzy doskonale
                    rozumieją nasz biznes oraz rynek kapitałowy.</h2>
                <a class="more text_gold" href="/investors_relation.php">Dowiedz się więcej</a>
                <div class="triangle_white"></div>
            </div>
        </div>
    </section>
<!--    END INVESTORS RELATION-->

<!--    NEWS-->
    <section class="section" id="news">
        <h3>Aktualności</h3>
<!--        SLIDE 1-->
        <div class="slide" id="slide1" data-anchor="slide1">
            <div class="container">
                <h2 class="title text_white">Zwołanie Nadzwyczajnego Walnego Zgromadzenia Emitenta</h2>
                <i class="article_date text_white">31 styczeń 2015</i>
                <span class="article_text text_white">Columbus Capital S.A. informuje o zwołaniu na dzień 29 maja 2015 roku
                    Nadzwyczajnego Walnego Zgromadzenia, które odbędzie się o godzinie 10:00
                    w Krakowie przy ul. Kuźnicy Kołłątajowskiej 13 lok. 210... </span>
                <a class="more text_white" href="/news.php">Czytaj więcej</a>
                <div class="triangle_white"></div>
            </div>
        </div>
<!--        END SLIDE 1-->

<!--        SLIDE 2-->
        <div class="slide" id="slide2" data-anchor="slide2">
            <div class="container">
                <h2 class="title text_white">Zwołanie Nadzwyczajnego Walnego Zgromadzenia Emitenta</h2>
                <i class="article_date text_white">31 styczeń 2015</i>
                <span class="article_text text_white">Columbus Capital S.A. informuje o zwołaniu na dzień 29 maja 2015 roku
                    Nadzwyczajnego Walnego Zgromadzenia, które odbędzie się o godzinie 10:00
                    w Krakowie przy ul. Kuźnicy Kołłątajowskiej 13 lok. 210... </span>
                <a class="more text_white" href="/news.php">Czytaj więcej</a>
                <div class="triangle_white"></div>
            </div>
        </div>
<!--        END SLIDE 2-->

<!--        SLIDE 3-->
        <div class="slide" id="slide3" data-anchor="slide3">
            <div class="container">
                <h2 class="title text_white">Zwołanie Nadzwyczajnego Walnego Zgromadzenia Emitenta</h2>
                <i class="article_date text_white">31 styczeń 2015</i>
                <span class="article_text text_white">Columbus Capital S.A. informuje o zwołaniu na dzień 29 maja 2015 roku
                    Nadzwyczajnego Walnego Zgromadzenia, które odbędzie się o godzinie 10:00
                    w Krakowie przy ul. Kuźnicy Kołłątajowskiej 13 lok. 210... </span>
                <a class="more text_white" href="/news.php">Czytaj więcej</a>
                <div class="triangle_white"></div>
            </div>
        </div>
<!--        END SLIDE 3-->
    </section>
<!--    END NEWS-->

<!--    GOOGLE MAP-->
    <section class="section" id="google_map">
            <div id="map"></div>
            <div class="info_map">
                <h3>Adres</h3>
                <p class="map_adress">ul. Moniuszki 7, Katowice</p>
            </div>
    </section>
<!--    END GOOGLE MAP-->

<!--    CONTACT-->
    <section class="section" id="contact">
        <h3>Kontakt</h3>
        <form action="">
            <div class="container">
                <div class="col-md-6">
                    <input type="text" name="name" id="name" required="required" placeholder="IMIĘ..."/>
                    <input type="text" name="surname" id="surname" required="required" placeholder="NAZWISKO..."/>
                    <input type="email" name="email" id="email" required="required" placeholder="TWÓJ ADRES E-MAIL.."/>
                </div>
                <div class="col-md-6">
                    <textarea name="content" id="content" placeholder="WPISZ TREŚĆ..."></textarea>
                </div>
                <div class="col-md-12">
                    <input type="submit" value="Wyślij">
                </div>
            </div>
        </form>
        <ul class="contact_info">
            <li><img src="imgs/tel.png" alt=""/>tel. +48 32 203 31 30</li>
            <li><img src="imgs/mail.png" alt=""/>biuro@columbuscapital.pl</li>
        </ul>
        <div class="contact_text">
            Sąd Rejonowy Katowice - Wschód w Katowicach, VIII Wydział Gospodarczy, Spółka wpisana do Krajowego
            Rejestru Przedsiębiorców pod numerem KRS 0000373608, Spółka posiada REGON 241811803, NIP 9492163154
        </div>
        <footer>
            <p>Wszystkie prawa zastrzeżone. COLUMBUS CAPITAL SA © 2015 | Wykonanie: <a href="http://besite.pl/">BeSite.pl</a></p>
        </footer>
    </section>
<!--    END CONTACT-->

</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>-->
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&language=pl"></script>
<script>

</script>
</body>
</html>