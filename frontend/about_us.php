<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="Resource-type" content="Document" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0,user-scalable=0;">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Columbus Capital</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- fullpage.js  style -->
    <link rel="stylesheet" type="text/css" href="css/jquery.fullPage.css" />
    <link rel="stylesheet" type="text/css" href="css/examples.css" />

    <!-- Author syles   -->
    <link rel="stylesheet" type="text/css" href="css/style.css" />

    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic&subset=latin,latin-ext' rel='stylesheet' type='text/css'>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!--[if IE]>
    <script type="text/javascript">
        var console = { log: function() {} };
    </script>
    <![endif]-->

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script>

    <!-- FULL PAGE SCRIPTS-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script type="text/javascript" src="/vendors/jquery.slimscroll.min.js"></script>
    <script type="text/javascript" src="js/jquery.fullPage.js"></script>
    <script type="text/javascript" src="js/examples.js"></script>
    <script>
        //        MENU SCRIPT
        $(document).ready(function() {
            var FeelItem = 'NULL';
            $('button.show_menu').on('click', function () {
                if(FeelItem == 'NULL')
                {
                    $('div.overlay-menu').addClass('show_overlay');
                    $('.menu').addClass('show_block');
                    FeelItem = '1';
                }
            });
            $('button.exit').on('click', function () {
                if(FeelItem == '1')
                {
                    $('div.overlay-menu').removeClass('show_overlay');
                    $('.menu').removeClass('show_block');
                    FeelItem = 'NULL'
                }
            });
        });
        //        END MENU SCRIPT
    </script>
    <script type="text/javascript">
        //        FULLPAGE SCRIPT
        $(document).ready(function() {
                $('#fullpage').fullpage({
                    sectionsColor: ['#ffffff', '#ffffff','#434343'],
                    menu: '#menu',
                    scrollBar:true,
                    slidesNavigation: false,
                    autoScrolling: false,
                    fitToSection: false
                });
        });
        //       END FULLPAGE SCRIPT

//        TABS SCRIPT
        $('#myTabs a').click(function (e) {
            e.preventDefault()
            $(this).tab('show')
        })
//        END TABS SCTIPT

//        BACKGROUND LOGO AND MENU SCTIPT
        $(window).scroll(function() {
            if($( window ).width()<=1630 && $( window ).width()>991) {
                var scroll = jQuery(window).scrollTop();
                if (scroll >= 200) {
                    jQuery('header.page').addClass('page_active');
                }
                else {
                    jQuery('header.page').removeClass('page_active');
                }
            }
        });
//        END BACKGROUND LOGO AND MENU SCTIPT
    </script>

</head>
<body>

<!--MENU-->
<header id="main" class="page">
    <div class="col-md-12">
        <div class="col-md-3">
            <div class="logo">
                <a href="/index.php"><img src="imgs/columbus.png" alt=""/></a>
            </div>
        </div>
        <div class="col-md-offset-8 col-md-1">
            <button class="show_menu">MENU<img src="imgs/hamburger.png" alt=""/></button>
        </div>
        <div class="menu">
            <div class="content_menu">
                <button class="exit"><img src="imgs/exit.png" alt=""/></button>
                <ul class="menu_items">
                    <li><a href="/index.php">Strona Głowna</a></li>
                    <li><a href="/about_us.php">O Nas</a></li>
                    <li><a href="/offer.php">Oferta</a></li>
                    <li><a href="/news.php">Aktualności</a></li>
                    <li><a href="/investors_relation.php">Relacje Inwestorskie</a></li>
                    <li><a href="/contact.php">Kontakt</a></li>
                </ul>
                <ul class="social_items">
                    <li><a href="#"><img src="imgs/facebook.png" alt=""/></a></li>
                    <li><a href="#"><img src="imgs/linkin.png" alt=""/></a></li>
                    <li><a href="#"><img src="imgs/twitter.png" alt=""/></a></li>
                </ul>
            </div>
        </div>
        <div class="overlay-menu"></div>
    </div>
</header>
<!--END MENU-->
<div id="fullpage">

    <!--    ABOUT US PAGE-->
    <section class="section" id="section_page_about_us">
        <div class="container">
            <div class="col-md-12">
                <h3>O nas</h3>
                <h2 class="text_intro text_white">Jesteśmy dla Ciebie odkrywcami możliwości</h2>
                <div class="triangle_white"></div>
            </div>
        </div>
    </section>
    <!--    END ABOUT US PAGE-->

    <!--    ABOUT US CONTENT-->
    <section class="section" id="section_about_us_content">
        <div class="col-md-12">
            <div class="container">
                <div class="submenu">
                    <div class="bs-docs-section">
                        <div class="bs-example bs-example-tabs" data-example-id="togglable-tabs">
<!--                            TABS MENU-->
                            <ul id="myTabs" class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="col-md-2 active"><a href="#about_us" id="about_us-tab" role="tab" data-toggle="tab" aria-controls="about_us" aria-expanded="true">O nas</a></li>
                                <li role="presentation" class="col-md-2"><a href="#activity" role="tab" id="activity-tab" data-toggle="tab" aria-controls="activity">Działalność</a></li>
                                <li role="presentation" class="col-md-2"><a href="#company_mission" id="company_mission-tab" role="tab" data-toggle="tab" aria-controls="company_mission" aria-expanded="true">Misja firmy</a></li>
                                <li role="presentation" class="col-md-2"><a href="#management" role="tab" id="management-tab" data-toggle="tab" aria-controls="management">Zarząd</a></li>
                                <li role="presentation" class="col-md-2"><a href="#board_of_directors" id="board_of_directors-tab" role="tab" data-toggle="tab" aria-controls="board_of_directors" aria-expanded="true">Rada nadzorcza</a></li>
                                <li role="presentation" class="col-md-2"><a href="#shareholders" role="tab" id="shareholders-tab" data-toggle="tab" aria-controls="shareholders">Akcjonariat</a></li>
                            </ul>
<!--                            END TABS MENU-->
                            <div id="myTabContent" class="tab-content">
<!--                                TAB CONTENT-->
                                <div role="tabpanel" class="tab-pane fade in active" id="about_us" aria-labelledBy="about_us-tab">
                                    <h3>O nas</h3>
                                    <div class="content_text"><span>COLUMBUS CAPITAL SA</span> jest Spółką publiczną,
                                        która w najbliższym okresie swojego rozwoju chce być nastawiona na wspieranie
                                        projektów innowacyjnych oraz na realizację strategii inwestycyjnych opartych
                                        przede wszystkim na fundamentalnej wycenie niedowartościowanych projektów, co
                                        w długim terminie ma zagwarantować akcjonariuszom ciągły wzrost wartości
                                        przedsiębiorstwa, a Spółce stały wzrost zainteresowania przedsiębiorstwem
                                        Emitenta na rynku publicznym. Spółka zadebiutowała na rynku NewConnect w dniu
                                        4 maja 2011 roku jako STI Group S.A. Obecnie celem Zarządu oraz kluczowych
                                        akcjonariuszy jest realizacja własnych inwestycji na rynkach kapitałowych,
                                        budowa fundamentów funduszu Venture Capital, a następnie rozpoczęcie procesu
                                        transferu nadwyżek finansowych pochodzących z realizowanych inwestycji własnych,
                                        świadczonych usług doradczych oraz rozpoczętych przedsięwzięć Venture Capital
                                        poprzez powołanie TFI COLUMBUS CAPITAL SA.</div>
                                    <div class="content_text">Właściwa realizacja strategii na lata 2014 – 2016 pozwoli na
                                        osiągnięcie kapitalizacji COLUMBUS CAPITAL SA na poziomie 12 mln Euro wraz z
                                        końcem 2016 roku, przy jak najmniejszej ilości nowych emisji akcji i przy jasnej
                                        polityce inwestycyjnej bez zgody na nierentowne projekty i te inwestycje, gdzie
                                        nie będzie możliwe do przewidzenia efektów ekonomicznych.</div>
                                    <div class="content_text">Początki działalności Spółki to tworzenie i wdrażanie
                                        nowoczesnych technologii przekazu informacji. Działalność ta była wspierana
                                        świadczeniem usług z zakresu konsultingu i marketingu sportowego, wykonywaniem
                                        projektów graficznych oraz elementów wizerunkowych dla klientów zewnętrznych
                                        oraz budując własne projekty internetowe. Spółka zdobyła w tym początkowym
                                        okresie swojej działalności szereg ważnych kompetencji w zakresie pozyskiwania
                                        środków unijnych, budowania marki i zasad kreacji biznesu w mediach. Obecnie
                                        Spółka będzie wykorzystywać te doświadczenia i stworzone relacje z inwestorami
                                        nie odżegnując się od realizacji projektów innowacyjnych w branży projektów
                                        opartych  o potencjał Internetu, rozwiązań chmurowych w szeroko pojętej branży
                                        IT.</div>
                                    <div class="content_text">Spółka ewoluując w swoim rozwoju pozyskuje nowych
                                        inwestorów oraz zarządzających, którzy doskonale rozumieją nasz biznes
                                        oraz rynek kapitałowy. Dzięki temu efektywnie wdrażamy rozmaite rozwiązania
                                        operacyjne i zarządcze, które wzmacniają naszą Spółkę. Po szeregu zmian Spółka
                                        w kwietniu 2014 roku ma możliwość pokazania swojej nowej nazwy z logo COLUMBUS
                                        CAPITAL SA. Już jako COLUMBUS CAPITAL SA podmiot chce być utożsamiany,
                                        jako publiczna Spółka inwestycyjna aktywnie inwestująca własne środki na
                                        światowych rynkach finansowych i w projektach pozagiełdowych w oparciu o własne
                                        analizy zespołu ekspertów i unikatowe strategie inwestycyjne. Poprzez
                                        kluczowych akcjonariuszy, własny zespół i partnerów współpracujących z COLUMBUS
                                        CAPITAL SA. Spółka ma dostęp do wielu atrakcyjnych projektów inwestycyjnych na
                                        całym świecie.</div>
                                </div>
<!--                                TAB CONTENT-->
                                <div role="tabpanel" class="tab-pane fade" id="activity" aria-labelledBy="activity-tab">
                                    <h3>Działalność</h3>
                                    <div class="content_text"><span>COLUMBUS CAPITAL SA</span> jest Spółką publiczną,
                                        która w najbliższym okresie swojego rozwoju chce być nastawiona na wspieranie
                                        projektów innowacyjnych oraz na realizację strategii inwestycyjnych opartych
                                        przede wszystkim na fundamentalnej wycenie niedowartościowanych projektów, co
                                        w długim terminie ma zagwarantować akcjonariuszom ciągły wzrost wartości
                                        przedsiębiorstwa, a Spółce stały wzrost zainteresowania przedsiębiorstwem
                                        Emitenta na rynku publicznym. Spółka zadebiutowała na rynku NewConnect w dniu
                                        4 maja 2011 roku jako STI Group S.A. Obecnie celem Zarządu oraz kluczowych
                                        akcjonariuszy jest realizacja własnych inwestycji na rynkach kapitałowych,
                                        budowa fundamentów funduszu Venture Capital, a następnie rozpoczęcie procesu
                                        transferu nadwyżek finansowych pochodzących z realizowanych inwestycji własnych,
                                        świadczonych usług doradczych oraz rozpoczętych przedsięwzięć Venture Capital
                                        poprzez powołanie TFI COLUMBUS CAPITAL SA.</div>
                                    <div class="content_text">Właściwa realizacja strategii na lata 2014 – 2016 pozwoli na
                                        osiągnięcie kapitalizacji COLUMBUS CAPITAL SA na poziomie 12 mln Euro wraz z
                                        końcem 2016 roku, przy jak najmniejszej ilości nowych emisji akcji i przy jasnej
                                        polityce inwestycyjnej bez zgody na nierentowne projekty i te inwestycje, gdzie
                                        nie będzie możliwe do przewidzenia efektów ekonomicznych.</div>
                                    <div class="content_text">Początki działalności Spółki to tworzenie i wdrażanie
                                        nowoczesnych technologii przekazu informacji. Działalność ta była wspierana
                                        świadczeniem usług z zakresu konsultingu i marketingu sportowego, wykonywaniem
                                        projektów graficznych oraz elementów wizerunkowych dla klientów zewnętrznych
                                        oraz budując własne projekty internetowe. Spółka zdobyła w tym początkowym
                                        okresie swojej działalności szereg ważnych kompetencji w zakresie pozyskiwania
                                        środków unijnych, budowania marki i zasad kreacji biznesu w mediach. Obecnie
                                        Spółka będzie wykorzystywać te doświadczenia i stworzone relacje z inwestorami
                                        nie odżegnując się od realizacji projektów innowacyjnych w branży projektów
                                        opartych  o potencjał Internetu, rozwiązań chmurowych w szeroko pojętej branży
                                        IT.</div>
                                    <div class="content_text">Spółka ewoluując w swoim rozwoju pozyskuje nowych
                                        inwestorów oraz zarządzających, którzy doskonale rozumieją nasz biznes
                                        oraz rynek kapitałowy. Dzięki temu efektywnie wdrażamy rozmaite rozwiązania
                                        operacyjne i zarządcze, które wzmacniają naszą Spółkę. Po szeregu zmian Spółka
                                        w kwietniu 2014 roku ma możliwość pokazania swojej nowej nazwy z logo COLUMBUS
                                        CAPITAL SA. Już jako COLUMBUS CAPITAL SA podmiot chce być utożsamiany,
                                        jako publiczna Spółka inwestycyjna aktywnie inwestująca własne środki na
                                        światowych rynkach finansowych i w projektach pozagiełdowych w oparciu o własne
                                        analizy zespołu ekspertów i unikatowe strategie inwestycyjne. Poprzez
                                        kluczowych akcjonariuszy, własny zespół i partnerów współpracujących z COLUMBUS
                                        CAPITAL SA. Spółka ma dostęp do wielu atrakcyjnych projektów inwestycyjnych na
                                        całym świecie.</div>
                                </div>
<!--                                TAB CONTENT-->
                                <div role="tabpanel" class="tab-pane fade" id="company_mission" aria-labelledBy="company_mission-tab">
                                    <h3>Misja firmy</h3>
                                    <div class="content_text"><span>COLUMBUS CAPITAL SA</span> jest Spółką publiczną,
                                        która w najbliższym okresie swojego rozwoju chce być nastawiona na wspieranie
                                        projektów innowacyjnych oraz na realizację strategii inwestycyjnych opartych
                                        przede wszystkim na fundamentalnej wycenie niedowartościowanych projektów, co
                                        w długim terminie ma zagwarantować akcjonariuszom ciągły wzrost wartości
                                        przedsiębiorstwa, a Spółce stały wzrost zainteresowania przedsiębiorstwem
                                        Emitenta na rynku publicznym. Spółka zadebiutowała na rynku NewConnect w dniu
                                        4 maja 2011 roku jako STI Group S.A. Obecnie celem Zarządu oraz kluczowych
                                        akcjonariuszy jest realizacja własnych inwestycji na rynkach kapitałowych,
                                        budowa fundamentów funduszu Venture Capital, a następnie rozpoczęcie procesu
                                        transferu nadwyżek finansowych pochodzących z realizowanych inwestycji własnych,
                                        świadczonych usług doradczych oraz rozpoczętych przedsięwzięć Venture Capital
                                        poprzez powołanie TFI COLUMBUS CAPITAL SA.</div>
                                    <div class="content_text">Właściwa realizacja strategii na lata 2014 – 2016 pozwoli na
                                        osiągnięcie kapitalizacji COLUMBUS CAPITAL SA na poziomie 12 mln Euro wraz z
                                        końcem 2016 roku, przy jak najmniejszej ilości nowych emisji akcji i przy jasnej
                                        polityce inwestycyjnej bez zgody na nierentowne projekty i te inwestycje, gdzie
                                        nie będzie możliwe do przewidzenia efektów ekonomicznych.</div>
                                    <div class="content_text">Początki działalności Spółki to tworzenie i wdrażanie
                                        nowoczesnych technologii przekazu informacji. Działalność ta była wspierana
                                        świadczeniem usług z zakresu konsultingu i marketingu sportowego, wykonywaniem
                                        projektów graficznych oraz elementów wizerunkowych dla klientów zewnętrznych
                                        oraz budując własne projekty internetowe. Spółka zdobyła w tym początkowym
                                        okresie swojej działalności szereg ważnych kompetencji w zakresie pozyskiwania
                                        środków unijnych, budowania marki i zasad kreacji biznesu w mediach. Obecnie
                                        Spółka będzie wykorzystywać te doświadczenia i stworzone relacje z inwestorami
                                        nie odżegnując się od realizacji projektów innowacyjnych w branży projektów
                                        opartych  o potencjał Internetu, rozwiązań chmurowych w szeroko pojętej branży
                                        IT.</div>
                                    <div class="content_text">Spółka ewoluując w swoim rozwoju pozyskuje nowych
                                        inwestorów oraz zarządzających, którzy doskonale rozumieją nasz biznes
                                        oraz rynek kapitałowy. Dzięki temu efektywnie wdrażamy rozmaite rozwiązania
                                        operacyjne i zarządcze, które wzmacniają naszą Spółkę. Po szeregu zmian Spółka
                                        w kwietniu 2014 roku ma możliwość pokazania swojej nowej nazwy z logo COLUMBUS
                                        CAPITAL SA. Już jako COLUMBUS CAPITAL SA podmiot chce być utożsamiany,
                                        jako publiczna Spółka inwestycyjna aktywnie inwestująca własne środki na
                                        światowych rynkach finansowych i w projektach pozagiełdowych w oparciu o własne
                                        analizy zespołu ekspertów i unikatowe strategie inwestycyjne. Poprzez
                                        kluczowych akcjonariuszy, własny zespół i partnerów współpracujących z COLUMBUS
                                        CAPITAL SA. Spółka ma dostęp do wielu atrakcyjnych projektów inwestycyjnych na
                                        całym świecie.</div>
                                </div>
<!--                                TAB CONTENT-->
                                <div role="tabpanel" class="tab-pane fade" id="management" aria-labelledBy="management-tab">
                                    <h3>Zarząd</h3>
                                    <div class="content_text"><span>COLUMBUS CAPITAL SA</span> jest Spółką publiczną,
                                        która w najbliższym okresie swojego rozwoju chce być nastawiona na wspieranie
                                        projektów innowacyjnych oraz na realizację strategii inwestycyjnych opartych
                                        przede wszystkim na fundamentalnej wycenie niedowartościowanych projektów, co
                                        w długim terminie ma zagwarantować akcjonariuszom ciągły wzrost wartości
                                        przedsiębiorstwa, a Spółce stały wzrost zainteresowania przedsiębiorstwem
                                        Emitenta na rynku publicznym. Spółka zadebiutowała na rynku NewConnect w dniu
                                        4 maja 2011 roku jako STI Group S.A. Obecnie celem Zarządu oraz kluczowych
                                        akcjonariuszy jest realizacja własnych inwestycji na rynkach kapitałowych,
                                        budowa fundamentów funduszu Venture Capital, a następnie rozpoczęcie procesu
                                        transferu nadwyżek finansowych pochodzących z realizowanych inwestycji własnych,
                                        świadczonych usług doradczych oraz rozpoczętych przedsięwzięć Venture Capital
                                        poprzez powołanie TFI COLUMBUS CAPITAL SA.</div>
                                    <div class="content_text">Właściwa realizacja strategii na lata 2014 – 2016 pozwoli na
                                        osiągnięcie kapitalizacji COLUMBUS CAPITAL SA na poziomie 12 mln Euro wraz z
                                        końcem 2016 roku, przy jak najmniejszej ilości nowych emisji akcji i przy jasnej
                                        polityce inwestycyjnej bez zgody na nierentowne projekty i te inwestycje, gdzie
                                        nie będzie możliwe do przewidzenia efektów ekonomicznych.</div>
                                    <div class="content_text">Początki działalności Spółki to tworzenie i wdrażanie
                                        nowoczesnych technologii przekazu informacji. Działalność ta była wspierana
                                        świadczeniem usług z zakresu konsultingu i marketingu sportowego, wykonywaniem
                                        projektów graficznych oraz elementów wizerunkowych dla klientów zewnętrznych
                                        oraz budując własne projekty internetowe. Spółka zdobyła w tym początkowym
                                        okresie swojej działalności szereg ważnych kompetencji w zakresie pozyskiwania
                                        środków unijnych, budowania marki i zasad kreacji biznesu w mediach. Obecnie
                                        Spółka będzie wykorzystywać te doświadczenia i stworzone relacje z inwestorami
                                        nie odżegnując się od realizacji projektów innowacyjnych w branży projektów
                                        opartych  o potencjał Internetu, rozwiązań chmurowych w szeroko pojętej branży
                                        IT.</div>
                                    <div class="content_text">Spółka ewoluując w swoim rozwoju pozyskuje nowych
                                        inwestorów oraz zarządzających, którzy doskonale rozumieją nasz biznes
                                        oraz rynek kapitałowy. Dzięki temu efektywnie wdrażamy rozmaite rozwiązania
                                        operacyjne i zarządcze, które wzmacniają naszą Spółkę. Po szeregu zmian Spółka
                                        w kwietniu 2014 roku ma możliwość pokazania swojej nowej nazwy z logo COLUMBUS
                                        CAPITAL SA. Już jako COLUMBUS CAPITAL SA podmiot chce być utożsamiany,
                                        jako publiczna Spółka inwestycyjna aktywnie inwestująca własne środki na
                                        światowych rynkach finansowych i w projektach pozagiełdowych w oparciu o własne
                                        analizy zespołu ekspertów i unikatowe strategie inwestycyjne. Poprzez
                                        kluczowych akcjonariuszy, własny zespół i partnerów współpracujących z COLUMBUS
                                        CAPITAL SA. Spółka ma dostęp do wielu atrakcyjnych projektów inwestycyjnych na
                                        całym świecie.</div>
                                </div>
<!--                                TAB CONTENT-->
                                <div role="tabpanel" class="tab-pane fade" id="board_of_directors" aria-labelledBy="board_of_directors-tab">
                                    <h3>Rada Nadzorcza</h3>
                                    <div class="content_text"><span>COLUMBUS CAPITAL SA</span> jest Spółką publiczną,
                                        która w najbliższym okresie swojego rozwoju chce być nastawiona na wspieranie
                                        projektów innowacyjnych oraz na realizację strategii inwestycyjnych opartych
                                        przede wszystkim na fundamentalnej wycenie niedowartościowanych projektów, co
                                        w długim terminie ma zagwarantować akcjonariuszom ciągły wzrost wartości
                                        przedsiębiorstwa, a Spółce stały wzrost zainteresowania przedsiębiorstwem
                                        Emitenta na rynku publicznym. Spółka zadebiutowała na rynku NewConnect w dniu
                                        4 maja 2011 roku jako STI Group S.A. Obecnie celem Zarządu oraz kluczowych
                                        akcjonariuszy jest realizacja własnych inwestycji na rynkach kapitałowych,
                                        budowa fundamentów funduszu Venture Capital, a następnie rozpoczęcie procesu
                                        transferu nadwyżek finansowych pochodzących z realizowanych inwestycji własnych,
                                        świadczonych usług doradczych oraz rozpoczętych przedsięwzięć Venture Capital
                                        poprzez powołanie TFI COLUMBUS CAPITAL SA.</div>
                                    <div class="content_text">Właściwa realizacja strategii na lata 2014 – 2016 pozwoli na
                                        osiągnięcie kapitalizacji COLUMBUS CAPITAL SA na poziomie 12 mln Euro wraz z
                                        końcem 2016 roku, przy jak najmniejszej ilości nowych emisji akcji i przy jasnej
                                        polityce inwestycyjnej bez zgody na nierentowne projekty i te inwestycje, gdzie
                                        nie będzie możliwe do przewidzenia efektów ekonomicznych.</div>
                                    <div class="content_text">Początki działalności Spółki to tworzenie i wdrażanie
                                        nowoczesnych technologii przekazu informacji. Działalność ta była wspierana
                                        świadczeniem usług z zakresu konsultingu i marketingu sportowego, wykonywaniem
                                        projektów graficznych oraz elementów wizerunkowych dla klientów zewnętrznych
                                        oraz budując własne projekty internetowe. Spółka zdobyła w tym początkowym
                                        okresie swojej działalności szereg ważnych kompetencji w zakresie pozyskiwania
                                        środków unijnych, budowania marki i zasad kreacji biznesu w mediach. Obecnie
                                        Spółka będzie wykorzystywać te doświadczenia i stworzone relacje z inwestorami
                                        nie odżegnując się od realizacji projektów innowacyjnych w branży projektów
                                        opartych  o potencjał Internetu, rozwiązań chmurowych w szeroko pojętej branży
                                        IT.</div>
                                    <div class="content_text">Spółka ewoluując w swoim rozwoju pozyskuje nowych
                                        inwestorów oraz zarządzających, którzy doskonale rozumieją nasz biznes
                                        oraz rynek kapitałowy. Dzięki temu efektywnie wdrażamy rozmaite rozwiązania
                                        operacyjne i zarządcze, które wzmacniają naszą Spółkę. Po szeregu zmian Spółka
                                        w kwietniu 2014 roku ma możliwość pokazania swojej nowej nazwy z logo COLUMBUS
                                        CAPITAL SA. Już jako COLUMBUS CAPITAL SA podmiot chce być utożsamiany,
                                        jako publiczna Spółka inwestycyjna aktywnie inwestująca własne środki na
                                        światowych rynkach finansowych i w projektach pozagiełdowych w oparciu o własne
                                        analizy zespołu ekspertów i unikatowe strategie inwestycyjne. Poprzez
                                        kluczowych akcjonariuszy, własny zespół i partnerów współpracujących z COLUMBUS
                                        CAPITAL SA. Spółka ma dostęp do wielu atrakcyjnych projektów inwestycyjnych na
                                        całym świecie.</div>
                                </div>
<!--                                TAB CONTENT-->
                                <div role="tabpanel" class="tab-pane fade" id="shareholders" aria-labelledBy="shareholders-tab">
                                    <h3>Akcjonariat</h3>
                                    <div class="content_text"><span>COLUMBUS CAPITAL SA</span> jest Spółką publiczną,
                                        która w najbliższym okresie swojego rozwoju chce być nastawiona na wspieranie
                                        projektów innowacyjnych oraz na realizację strategii inwestycyjnych opartych
                                        przede wszystkim na fundamentalnej wycenie niedowartościowanych projektów, co
                                        w długim terminie ma zagwarantować akcjonariuszom ciągły wzrost wartości
                                        przedsiębiorstwa, a Spółce stały wzrost zainteresowania przedsiębiorstwem
                                        Emitenta na rynku publicznym. Spółka zadebiutowała na rynku NewConnect w dniu
                                        4 maja 2011 roku jako STI Group S.A. Obecnie celem Zarządu oraz kluczowych
                                        akcjonariuszy jest realizacja własnych inwestycji na rynkach kapitałowych,
                                        budowa fundamentów funduszu Venture Capital, a następnie rozpoczęcie procesu
                                        transferu nadwyżek finansowych pochodzących z realizowanych inwestycji własnych,
                                        świadczonych usług doradczych oraz rozpoczętych przedsięwzięć Venture Capital
                                        poprzez powołanie TFI COLUMBUS CAPITAL SA.</div>
                                    <div class="content_text">Właściwa realizacja strategii na lata 2014 – 2016 pozwoli na
                                        osiągnięcie kapitalizacji COLUMBUS CAPITAL SA na poziomie 12 mln Euro wraz z
                                        końcem 2016 roku, przy jak najmniejszej ilości nowych emisji akcji i przy jasnej
                                        polityce inwestycyjnej bez zgody na nierentowne projekty i te inwestycje, gdzie
                                        nie będzie możliwe do przewidzenia efektów ekonomicznych.</div>
                                    <div class="content_text">Początki działalności Spółki to tworzenie i wdrażanie
                                        nowoczesnych technologii przekazu informacji. Działalność ta była wspierana
                                        świadczeniem usług z zakresu konsultingu i marketingu sportowego, wykonywaniem
                                        projektów graficznych oraz elementów wizerunkowych dla klientów zewnętrznych
                                        oraz budując własne projekty internetowe. Spółka zdobyła w tym początkowym
                                        okresie swojej działalności szereg ważnych kompetencji w zakresie pozyskiwania
                                        środków unijnych, budowania marki i zasad kreacji biznesu w mediach. Obecnie
                                        Spółka będzie wykorzystywać te doświadczenia i stworzone relacje z inwestorami
                                        nie odżegnując się od realizacji projektów innowacyjnych w branży projektów
                                        opartych  o potencjał Internetu, rozwiązań chmurowych w szeroko pojętej branży
                                        IT.</div>
                                    <div class="content_text">Spółka ewoluując w swoim rozwoju pozyskuje nowych
                                        inwestorów oraz zarządzających, którzy doskonale rozumieją nasz biznes
                                        oraz rynek kapitałowy. Dzięki temu efektywnie wdrażamy rozmaite rozwiązania
                                        operacyjne i zarządcze, które wzmacniają naszą Spółkę. Po szeregu zmian Spółka
                                        w kwietniu 2014 roku ma możliwość pokazania swojej nowej nazwy z logo COLUMBUS
                                        CAPITAL SA. Już jako COLUMBUS CAPITAL SA podmiot chce być utożsamiany,
                                        jako publiczna Spółka inwestycyjna aktywnie inwestująca własne środki na
                                        światowych rynkach finansowych i w projektach pozagiełdowych w oparciu o własne
                                        analizy zespołu ekspertów i unikatowe strategie inwestycyjne. Poprzez
                                        kluczowych akcjonariuszy, własny zespół i partnerów współpracujących z COLUMBUS
                                        CAPITAL SA. Spółka ma dostęp do wielu atrakcyjnych projektów inwestycyjnych na
                                        całym świecie.</div>
                                </div>
                            </div>
                        </div><!-- /example -->
                </div>
            </div>
        </div>
    </section>
    <!--    END ABOUT US CONTENT-->

    <!--    CONTACT-->
    <section class="section" id="contact-page">
        <h3>Kontakt</h3>

        <form action="">
            <div class="container">
                <div class="col-md-6">
                    <input type="text" name="name" id="name" required="required" placeholder="IMIĘ..."/>
                    <input type="text" name="surname" id="surname" required="required" placeholder="NAZWISKO..."/>
                    <input type="email" name="email" id="email" required="required" placeholder="TWÓJ ADRES E-MAIL.."/>
                </div>
                <div class="col-md-6">
                    <textarea name="content" id="content" placeholder="WPISZ TREŚĆ..."></textarea>
                </div>
                <div class="col-md-12">
                    <input type="submit" value="Wyślij">
                </div>
            </div>
        </form>
        <ul class="contact_info">
            <li><img src="imgs/tel.png" alt=""/>tel. +48 32 203 31 30</li>
            <li><img src="imgs/mail.png" alt=""/>biuro@columbuscapital.pl</li>
        </ul>
        <div class="contact_text">
            Sąd Rejonowy Katowice - Wschód w Katowicach, VIII Wydział Gospodarczy, Spółka wpisana do Krajowego
            Rejestru Przedsiębiorców pod numerem KRS 0000373608, Spółka posiada REGON 241811803, NIP 9492163154
        </div>
        <footer>
            <p>Wszystkie prawa zastrzeżone. COLUMBUS CAPITAL SA © 2015 | Wykonanie: <a href="http://besite.pl/">BeSite.pl</a></p>
        </footer>
    </section>
    <!--    END CONTACT-->

</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&language=pl"></script>
<script>

</script>
</body>
</html>