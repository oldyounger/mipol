<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="Resource-type" content="Document" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0,user-scalable=0;">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Columbus Capital</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- fullpage.js  style -->
    <link rel="stylesheet" type="text/css" href="css/jquery.fullPage.css" />
    <link rel="stylesheet" type="text/css" href="css/examples.css" />

    <!-- Author syles   -->
    <link rel="stylesheet" type="text/css" href="css/style.css" />

    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic&subset=latin,latin-ext' rel='stylesheet' type='text/css'>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!--[if IE]>
    <script type="text/javascript">
        var console = { log: function() {} };
    </script>
    <![endif]-->

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script>

    <!-- FULL PAGE SCRIPTS-->
    <script type="text/javascript" src="/vendors/jquery.slimscroll.min.js"></script>
    <script type="text/javascript" src="js/jquery.fullPage.js"></script>
    <script type="text/javascript" src="js/examples.js"></script>
    <script>

        //        MENU SCRIPT
        $(document).ready(function() {
            var FeelItem = 'NULL';
            $('button.show_menu').on('click', function () {
                if(FeelItem == 'NULL')
                {
                    $('div.overlay-menu').addClass('show_overlay');
                    $('.menu').addClass('show_block');
                    FeelItem = '1';
                }
            });
            $('button.exit').on('click', function () {
                if(FeelItem == '1')
                {
                    $('div.overlay-menu').removeClass('show_overlay');
                    $('.menu').removeClass('show_block');
                    FeelItem = 'NULL'
                }
            });
        });
        //        END MENU SCRIPT

        //        FULLPAGE SCRIPT
        $(document).ready(function() {
            $('#fullpage').fullpage({
                sectionsColor: ['#434343'],
                menu: '#menu',
                scrollBar:true,
                slidesNavigation: false,
                autoScrolling: false,
                fitToSection: false
            });
        });
        //        END FULLPAGE SCRIPT

    </script>
</head>
<body>
<!--MENU-->
<header id="main">
    <div class="col-md-12">
        <div class="col-md-3">
            <div class="logo">
                <a href="/index.php"><img src="imgs/columbus.png" alt=""/></a>
            </div>
        </div>
        <div class="col-md-offset-8 col-md-1">
            <button class="show_menu">MENU<img src="imgs/hamburger.png" alt=""/></button>
        </div>
        <div class="menu">
            <div class="content_menu">
                <button class="exit"><img src="imgs/exit.png" alt=""/></button>
                <ul class="menu_items">
                    <li><a href="/index.php">Strona Głowna</a></li>
                    <li><a href="/about_us.php">O Nas</a></li>
                    <li><a href="/offer.php">Oferta</a></li>
                    <li><a href="/news.php">Aktualności</a></li>
                    <li><a href="/investors_relation.php">Relacje Inwestorskie</a></li>
                    <li><a href="/contact.php">Kontakt</a></li>
                </ul>
                <ul class="social_items">
                    <li><a href="#"><img src="imgs/facebook.png" alt=""/></a></li>
                    <li><a href="#"><img src="imgs/linkin.png" alt=""/></a></li>
                    <li><a href="#"><img src="imgs/twitter.png" alt=""/></a></li>
                </ul>
            </div>
        </div>
        <div class="overlay-menu"></div>
    </div>
</header>
<!--END MENU-->
<div id="fullpage">

    <!--    CONTACT-->
    <section class="section" id="contact-page">
        <h3>Kontakt</h3>

        <form action="">
            <div class="container">
                <div class="col-md-6">
                    <input type="text" name="name" id="name" required="required" placeholder="IMIĘ..."/>
                    <input type="text" name="surname" id="surname" required="required" placeholder="NAZWISKO..."/>
                    <input type="email" name="email" id="email" required="required" placeholder="TWÓJ ADRES E-MAIL.."/>
                </div>
                <div class="col-md-6">
                    <textarea name="content" id="content" placeholder="WPISZ TREŚĆ..."></textarea>
                </div>
                <div class="col-md-12">
                    <input type="submit" value="Wyślij">
                </div>
            </div>
        </form>
        <ul class="contact_info">
            <li><img src="imgs/tel.png" alt=""/>tel. +48 32 203 31 30</li>
            <li><img src="imgs/mail.png" alt=""/>biuro@columbuscapital.pl</li>
        </ul>
        <div class="contact_text">
            Sąd Rejonowy Katowice - Wschód w Katowicach, VIII Wydział Gospodarczy, Spółka wpisana do Krajowego
            Rejestru Przedsiębiorców pod numerem KRS 0000373608, Spółka posiada REGON 241811803, NIP 9492163154
        </div>
        <footer>
            <p>Wszystkie prawa zastrzeżone. COLUMBUS CAPITAL SA © 2015 | Wykonanie: <a href="http://besite.pl/">BeSite.pl</a></p>
        </footer>
    </section>
    <!--    END CONTACT-->
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
<script>

</script>
</body>
</html>