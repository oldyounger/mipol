//TODO: finish this!! style, etc. give download links for chrome & firefox
document.attachEvent("onreadystatechange", function(){
	if(document.readyState === "complete"){
		var div = document.createElement('div');
		div.className = 'ie8-box';
		div.innerHTML = '<div class="alert"></div>' +
            '<div class="text">' +
                'Używasz przestarzałej, podatnej na ataki hakerów przeglądarki na której ta strona, jak i wiele ' +
                'innych może nie działać poprawnie. Zapewnij sobie komfort i bezpieczeństwo przeglądania - ' +
                'zainstaluj nowszą przeglądarkę, np. <a href="https://www.mozilla.org/pl/firefox/new/" target="_blank">Mozilla Firefox</a> ' +
                'lub <a href="http://www.google.com/intl/pl/chrome/" target="_blank">Google Chrome</a>.' +
            '</div>';

		document.body.appendChild(div);
	}
});